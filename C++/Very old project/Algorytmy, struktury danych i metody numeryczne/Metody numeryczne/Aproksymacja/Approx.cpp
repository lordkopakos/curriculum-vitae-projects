#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>

using namespace std;

struct Point {
	double x, y;
};

struct Polynomial {
	double *coeffs;
};

template <typename T>
int countChars(T const& value) {
	stringstream sstr;
	sstr << value;
	return sstr.str().length();
}

template <typename T>
T pow(T x, int power) {
	T ret = 1;
	bool flip = false;
	if (power < 0) {
		power *= -1;
		flip = true;
	}
	for (int p = 0; p < power; ++p) {
		ret *= x;
	}
	return flip ? 1 / ret : ret;
}

// FIX: DEGREE -1 DIFF
double polynomial(double x, double *a, int degree) {
	double ret = 0;
	for (int j = 0; j <= degree; ++j) {
		ret += a[j] * pow(x, j);
	}
	return ret;
}


void printMatrix(char *heading, double**M, int m, int n) {
	char topLeft = (char)218;
	char botLeft = (char)192;
	char bar = (char)179;
	char topRight = (char)191;
	char botRight = (char)217;

	int *chars = new int[n];
	for (int i = 0; i < n; ++i) {
		chars[i] = countChars(M[0][i]);
		for (int j = 1; j < m; ++j) {
			int temp = countChars(M[j][i]);
			if (temp > chars[i]) chars[i] = temp;
		}
	}

	cout << heading << endl;
	for (int i = 0; i < m; ++i) {
		if (i == 0) cout << topLeft;
		else if (i == m - 1) cout << botLeft;
		else cout << bar;
		cout << "  ";

		for (int j = 0; j < n; ++j) {
			cout << setw(chars[j]) << M[i][j] << "  ";
		}

		if (i == 0) cout << topRight;
		else if (i == m - 1) cout << botRight;
		else cout << bar;
		cout << endl;
	}
	cout << endl;
	
	delete[] chars;
}

void printVector(char *heading, double *V, int dim, bool transpose) {
	cout << heading << endl;

	if (transpose) {
		int chars = countChars(V[0]);
		for (int i = 1; i < dim; ++i) {
			int temp = countChars(V[i]);
			if (temp > chars) chars = temp;
		}


		for (int i = 0; i < dim; ++i) {
				cout << setw(chars) << V[i] << endl;
		}
		cout << endl;
	}
	else {
		for (int i = 0; i < dim; ++i) {
			cout << V[i] << "  ";
		}
		cout << endl << endl;
	}
}

double **getMatrixG(Point *points, int nodes, int degree) {
	int deg = degree + 1;
	double **g = new double *[deg];

	for (int i = 0; i < deg; ++i)
		g[i] = new double[deg];

	for (int i = 0; i < deg; ++i) {
		for (int j = 0; j < deg; ++j) {
			g[i][j] = 0;
			for (int k = 0; k < nodes; ++k)
				g[i][j] += pow(points[k].x, i + j);
		}
	}
	return g;
}

double *getVectorRho(Point *points, int nodes, int degree) {
	int deg = degree + 1;
	double *r = new double[deg];

	for (int i = 0; i < deg; ++i) {
		r[i] = 0;
		for (int k = 0; k < nodes; ++k)
			r[i] += points[k].y * pow(points[k].x, i);
	}
	return r;
}

double **getMatrixAB(double **A, double *B, int degree) {
	int deg = degree + 1;
	int deg1 = deg + 1;

	double **AB = new double *[deg];
	for (int i = 0; i < deg; ++i)
		AB[i] = new double[deg1];

	for (int i = 0; i < deg; ++i) {
		for (int j = 0; j < deg; ++j)
			AB[i][j] = A[i][j];
		AB[i][deg] = B[i];
	}
	return AB;
}

void performGaussElimination(double **AB, int degree) {
	int deg = degree + 1;
	int deg1 = deg + 1;

	for (int i = 1; i < deg; i++) {
		for (int j = i; j < deg; j++) {
			double Aji = AB[j][i - 1];
			for (int k = 0; k < deg1; k++)
				AB[j][k] -= AB[i - 1][k] * Aji / AB[i - 1][i - 1];
		}
	}
}

double *getCoeffs(double **AB, int degree) {
	int deg = degree + 1;

	double *coeffs = new double[deg];
	for (int i = deg - 1; i >= 0; --i) {
		coeffs[i] = AB[i][deg];
		for (int j = i + 1; j < deg; j++) {
			coeffs[i] -= AB[i][j] * coeffs[j];
		}
		coeffs[i] /= AB[i][i];
	}
	return coeffs;
}

double det(int n, int w, int * WK, double ** A) {
	int    i, j, k, m, *KK;
	double s;

	if (n == 1)                                    // sprawdzamy warunek zako�czenia rekurencji
		return A[w][WK[0]];                         // macierz 1 x 1, wyznacznik r�wny elementowi
	else {
		KK = new int[n - 1];                        // tworzymy dynamiczny wektor kolumn
		s = 0;                                      // zerujemy warto�� rozwini�cia
		m = 1;                                      // pocz�tkowy mno�nik

		for (i = 0; i < n; i++) {                     // p�tla obliczaj�ca rozwini�cie
			k = 0;                                    // tworzymy wektor kolumn dla rekurencji

			for (j = 0; j < n - 1; j++) {                // ma on o 1 kolumn� mniej ni� WK
				if (k == i) k++;                         // pomijamy bie��c� kolumn�
				KK[j] = WK[k++];                        // pozosta�e kolumny przenosimy do KK
			}

			s += m * A[w][WK[i]] * det(n - 1, w + 1, KK, A);
			m *= -1;                                   // kolejny mno�nik
		}
		delete[] KK;                               // usuwamy zb�dn� ju� tablic� dynamiczn�
		return s;                                   // ustalamy warto�� funkcji
	}
}

double **replaceColumn(double **a, double *r, int degree, int column)
{
	int deg = degree + 1;
	double **g = new double *[deg];

	for (int i = 0; i < deg; ++i)
		g[i] = new double[deg];

	for (int i = 0; i < deg; ++i) {
		for (int j = 0; j < deg; ++j) {
			if (j == column)
				g[i][j] = r[i];
			else
				g[i][j] = a[i][j];
		}
	}
	return g;
}

double *getCoeffsDeterminant(double **g, double *r, int degree)
{
	int deg = degree + 1;

	double w;
	double *wn = new double[deg];
	double *coeffs = new double[deg];
	
	int *wk = new int[deg];
	for (int i = 0; i < deg; ++i)
		wk[i] = i;

	w = det(deg, 0, wk, g);

	for (int i = 0; i < deg; ++i)
	{
		if (w == 0)
			coeffs[i] = 0;
		else
		{
			wn[i] = det(deg, 0, wk, replaceColumn(g, r, degree, i));

			coeffs[i] = wn[i] / w;
		}
	}

	return coeffs;
}


void print_stat(Point *points, int nodes, double *coeffs, int degree)  {
	for (int i = 0; i < nodes; ++i)
	{
		cout << "y = " << points[i].y << ", f(" << points[i].x << ") = " << polynomial(points[i].x, coeffs, degree) << "\n";
	}
	cout << "\n";



	double sse = 0;
	for (int i = 0; i < nodes; ++i) {
		double addend = points[i].y - polynomial(points[i].x, coeffs, degree);
		sse += pow(addend, 2);
	}

	double avg_y = 0;
	for (int i = 0; i < nodes; ++i)
		avg_y += points[i].y;
	avg_y /= nodes;


	double tss = 0;
	for (int i = 0; i < nodes; ++i)
	{
		double temp = points[i].y;
		temp -= avg_y;
		temp *= temp;
		tss += temp;
	}

	double r_squared = 1 - sse / tss;

	//	cout << "avg(y) = " << avg_y << "\n";
	cout << "SSE = " << sse << "\n";
	cout << "R^2 = " << r_squared << "\n\n";
}


int main() {
	/////////////////////////////// CONFIG
	int nodes = 8; // ilo�� punktow
	Point points[] = {
		{ 1,1 },
		{ 3,2 },
		{ 4,4 },
		{ 6,4 },
		{ 8,5 },
		{ 9,7 },
		{ 11,8 },
		{ 14,9 }
	};
	int degree = 1; // stopien aproksymowanego wielomianu
	/////////////////////////////// END CONFIG


	cout << "Stopien aproksymowanego wielomianu: " << degree << "\n\n";

	double x;
	int rows = degree + 1;
	int cols = rows + 1;

	double **g = getMatrixG(points, nodes, degree);
	printMatrix("Macierz g", g, rows, rows);

	double *r = getVectorRho(points, nodes, degree);
	printVector("Wektor ro", r, rows, true);


	// METODA WYZNACZNIKOW
	cout << "\n\n" << "METODA WYZNACZNIKOW" << "\n\n";

	double *coeffs2 = getCoeffsDeterminant(g, r, degree);
	printVector("Wektor wspolczynnikow (a_0, a_1, ..., a_n)", coeffs2, rows, false);
	print_stat(points, nodes, coeffs2, degree);

	x = 1;
	cout << "F(" << x << ") = " << polynomial(x, coeffs2, degree) << endl;


	// METODA ELIMINACJI GAUSSA
	cout << "\n\n" << "METODA ELIMINACJI GAUSSA" << "\n\n";

	double **AB = getMatrixAB(g, r, degree);
	performGaussElimination(AB, degree);
	printMatrix("Macierz AB po eliminacji Gaussa", AB, rows, cols);

	double *coeffs = getCoeffs(AB, degree);
	printVector("Wektor wspolczynnikow (a_0, a_1, ..., a_n)", coeffs, rows, false);
	print_stat(points, nodes, coeffs, degree);

	x = 1;
	cout << "F(" << x << ") = " << polynomial(x, coeffs, degree) << endl;


	system("pause");
	return 0;
}