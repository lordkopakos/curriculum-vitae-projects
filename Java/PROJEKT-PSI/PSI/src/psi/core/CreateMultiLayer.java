/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.core;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Transparency;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicComboBoxUI;
import psi.Jattributes.ImagePanel;
import psi.Jattributes.Input;
import psi.Jattributes.text.InputComponent;
import psi.network.input.VectorX;
import psi.neural.AdelineCell;

/**
 *
 * @author lordkopakos
 */
public class CreateMultiLayer extends CreatePerceptron{
    
    public static void main(String[] args) {
        new CreateMultiLayer().menurun();
    }

    private ImagePanel plMLP;
    private JButton btnCloseMLP;
    private JLabel lbTitleMLP;
    private ImageIcon imgTitleMLP;

    //Options
    private ImagePanel plOptionsMLP;
    private int firstMLP;
    private JComboBox cbTrainingSetMLP;
    private JComboBox cbValidSetMLP;
    private JComboBox cbTestSetMLP;
    private JButton btnLearnMLP;
    private JButton btnValidMLP;
    private JButton btnTestMLP;

    //Construction of Neural
    private ImagePanel plConstructionMLP;
    private JButton btnMLP;
    private Input xAndWeightMLP;
    private int elementsXAndWeightMLP;
    private int sizeXAndWeightMLP;
    private JLabel lbYMLP;
    private JLabel lbZMLP;
    private InputComponent txtYMLP;
    private InputComponent txtZMLP;
    
    //Better Construction
    private ImagePanel plVisualisationMLP;
    private JButton btnCloseVisualisationMLP;
    private JLabel lbNumberOfNeuralInLayerMLP;
    private InputComponent txtNumberOfNeuralInLayerMLP;
    private JButton btnAddNewLayerMLP;
    private ImageIcon imgNeuralsMLP;
    private ArrayList<JLabel> lbNeurals;

    //Neural functions panel.
    private ImagePanel plFunctionsMLP;
    private JButton btnMSEMLP;
    private JButton btnMAPEMLP;

    //Neural parametrs
    private ImagePanel plParametrsMLP;
    private JButton btnCloseParametrsMLP;
    private JButton btnSetParametrsMLP;
    private ImageIcon imgBiasMLP;
    private ImageIcon imgBetaMLP;
    private JLabel lbBiasMLP;
    private JLabel lbBetaMLP;
    private InputComponent txtBiasMLP;
    private InputComponent txtBetaMLP;
    private InputComponent txtRandomWeightFromMLP;
    private InputComponent txtRandomWeightToMLP;
    private int widthLabelMLP, widthTextMLP, heightMLP;

    //Activation Function
    private JButton btnBipolarSigmoidalMLP;
    private JButton btnUnipolarSigmoidalMLP;
    private JButton btnBipolarThresholdMLP;
    private JButton btnUnipolarThresholdMLP;

    //Perceptron learning
    private AdelineCell perceptronMLP;
    private boolean perceptronVectorInit;

    //Training and Valid set
    private ArrayList<VectorX> vectorsMLP;
    private String filePathMLP;
    private String fileNameMLP;

    //Graph
    private JButton btnCloseGraphMSEMLP;
    private JButton btnCloseGraphMAPEMLP;
    private ImagePanel plGraphMSEMLP;
    private ImagePanel plGraphMAPEMLP;
    private static XYGraph graphMSEMLP;
    private static XYGraph graphMAPEMLP;
    private ArrayList<Integer> errorMAPEMLP;
    private ArrayList<Integer> errorMSEMLP;
    
    //messages
    private JButton btnCloseFinishedLearningMLP;
    private ImagePanel plFinishedLearningMLP;
    private JButton btnCloseFinishedValidMLP;
    private ImagePanel plFinishedValidMLP;
    private JLabel lbGoodAnswersMLP;

    @Override
    public void menurun() {
        super.menurun();
    }

    @Override
    public void menuinit() {
        super.menuinit();

        ///////////////////       PERCEPTRON          //////////////////////////
        ////////////////////////////////////////////////////////////////////////
        perceptronMLP = new AdelineCell();
        perceptronVectorInit=false;
        perceptronMLP.setBias(0);
        perceptronMLP.setBeta(0);
        errorMAPEMLP=new ArrayList<>(1000);
        errorMSEMLP=new ArrayList<>(1000);

        ///////////////////           GUI           ////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        firstMLP = 180;

        Border border
                = BorderFactory.createLineBorder(new Color((float) 0.9, (float) 0.4, (float) 0.02), 5);

        //Panele używane w wizualizacji perceptronu
        plMLP = new ImagePanel("images/attributes/backgroundNetwork.jpg");
        setPanel(plMLP, border, false);

        plOptionsMLP = new ImagePanel("images/attributes/optionsPanel.png");
        setPanel(plOptionsMLP, border, true);

        plConstructionMLP = new ImagePanel("images/attributes/neuralPanel.png");
        setPanel(plConstructionMLP, border, true);

        plFunctionsMLP = new ImagePanel("images/attributes/functionsPanel.png");
        setPanel(plFunctionsMLP, border, true);

        plParametrsMLP = new ImagePanel("images/attributes/parametrsPanel.png");
        setPanel(plParametrsMLP, border, false);
        
        plFinishedLearningMLP = new ImagePanel("images/attributes/goodlearningPanel.png");
        setPanel(plFinishedLearningMLP, border, false);
        
        plFinishedValidMLP = new ImagePanel("images/attributes/validPanel.png");
        setPanel(plFinishedValidMLP, border, false);
        
        plFinishedValidMLP = new ImagePanel("images/attributes/validPanel.png");
        setPanel(plFinishedValidMLP, border, false);
        
        plGraphMSEMLP = new ImagePanel("images/attributes/msePanel.png");
        setPanel(plGraphMSEMLP, border, false);
        
        plGraphMAPEMLP = new ImagePanel("images/attributes/mapePanel.png");
        setPanel(plGraphMAPEMLP, border, false);
        
        plVisualisationMLP= new ImagePanel("images/attributes/MLPConstructionPanel.png");
        setPanel(plVisualisationMLP, border, false);

        //Tytuł panelu
        imgTitleMLP = new ImageIcon("images/menu/main/titleMLP.png");
        lbTitleMLP = new JLabel(imgTitleMLP);
        lbTitleMLP.setSize(900, 150);
        lbTitleMLP.setLocation(plOptionsMLP.getWidth() + 100, plMLP.getHeight() / 2 - plOptionsMLP.getHeight() / 2);
        
        lbGoodAnswersMLP = new JLabel("www");
        lbGoodAnswersMLP.setSize(140, 100);
        lbGoodAnswersMLP.setLocation(plFinishedValidMLP.getWidth()-lbGoodAnswersMLP.getWidth()-10, plFinishedValidMLP.getHeight()-lbGoodAnswersMLP.getHeight()-5);
        lbGoodAnswersMLP.setBackground(new Color((float)0.5, (float)0.8, (float)0.1));
        lbGoodAnswersMLP.setForeground(new Color((float)0.5, (float)0.8, (float)0.1));
        lbGoodAnswersMLP.setFont(new Font("Gill Sans Ultra Bold", Font.PLAIN, 40));
        
        //Obrazki w zmianie parametrów BIAS
        imgBiasMLP = new ImageIcon("images/menu/main/parametrs/bias.png");
        lbBiasMLP= new JLabel(imgBiasMLP);
        txtBiasMLP= new InputComponent();
        widthLabelMLP=30; widthTextMLP=30; heightMLP=30;
        txtBiasMLP.setText("0");
        setFieldWithLabel(lbBiasMLP, txtBiasMLP, Color.yellow, border, "Bias", plParametrsMLP.getWidth()/3-(widthLabelMLP+widthTextMLP)/2, 120, widthLabelMLP, widthTextMLP, heightMLP, true);

        //Obrazki w zmianie parametrów BETA
        imgBetaMLP = new ImageIcon("images/menu/main/parametrs/beta.png");
        lbBetaMLP= new JLabel(imgBetaMLP);
        txtBetaMLP= new InputComponent();
        widthLabelMLP=30; widthTextMLP=30; heightMLP=30;
        setFieldWithLabel(lbBetaMLP, txtBetaMLP, Color.yellow, border, "Beta", 2*plParametrsMLP.getWidth()/3-(widthLabelMLP+widthTextMLP)/2, 120, widthLabelMLP, widthTextMLP, heightMLP, true);
        txtBetaMLP.setText("0");
        
        //Random Weight
        txtRandomWeightFromMLP=new InputComponent();
        setField(txtRandomWeightFromMLP, Color.yellow, border, "Beginning of the interval", plParametrsMLP.getWidth()/4-(widthTextMLP*3)/2, 250, widthTextMLP*3, heightMLP, true);
        
        txtRandomWeightToMLP=new InputComponent();
        setField(txtRandomWeightToMLP, Color.yellow, border, "Ending of the interval", 3*plParametrsMLP.getWidth()/4-(widthTextMLP*3)/2, 250, widthTextMLP*3, heightMLP, true);
        
        //Wartości oczekiwane i wyjściowe
        widthLabelMLP=30; widthTextMLP=50; heightMLP=50;
        lbYMLP=new JLabel("Y:");
        txtYMLP=new InputComponent();
        setFieldWithLabel(lbYMLP, txtYMLP, Color.yellow, border, "Y:", plConstructionMLP.getWidth()-(widthLabelMLP+widthTextMLP)-20, plConstructionMLP.getHeight()/2-heightMLP*2, widthLabelMLP, widthTextMLP, heightMLP, false);
        
        lbZMLP=new JLabel("Z:");
        txtZMLP=new InputComponent();
        setFieldWithLabel(lbZMLP, txtZMLP, Color.yellow, border, "Z:", plConstructionMLP.getWidth()-(widthLabelMLP+widthTextMLP)-20, plConstructionMLP.getHeight()/2+heightMLP*2, widthLabelMLP, widthTextMLP, heightMLP, false);
        
        widthLabelMLP=80; widthTextMLP=50; heightMLP=50;
        lbNumberOfNeuralInLayerMLP=new JLabel("Neurals:");
        txtNumberOfNeuralInLayerMLP=new InputComponent();
        setFieldWithLabel(lbNumberOfNeuralInLayerMLP, txtNumberOfNeuralInLayerMLP, Color.GREEN, border, "Number of neurals in layer:", plVisualisationMLP.getWidth()/2-(widthLabelMLP+widthTextMLP)-5, 200, widthLabelMLP, widthTextMLP, heightMLP, true);
        
        
        //Rysunek Neuronu
        btnMLP = createButton("/MLP/MLPConstruction", "Add a Layer");
        btnMLP.setSize(700, 600);
        btnMLP.setLocation(plConstructionMLP.getWidth() / 2 - btnMLP.getWidth() / 2, plConstructionMLP.getHeight() / 2 - btnMLP.getHeight() / 2);
        
        btnAddNewLayerMLP= createButton("/MLP/addLayers", "Select a MLP parametrs");
        btnAddNewLayerMLP.setSize(102, 48);
        btnAddNewLayerMLP.setLocation(plVisualisationMLP.getWidth() / 2 +10, 201);

        //Tworzenie przycisków
        btnCloseMLP = createButton("close", "Zamknij");
        btnCloseMLP.setSize(27, 24);
        btnCloseMLP.setLocation(plMLP.getWidth() - btnCloseMLP.getWidth() - 10, 10);
        
        btnCloseVisualisationMLP = createButton("close", "Zamknij");
        btnCloseVisualisationMLP.setSize(27, 24);
        btnCloseVisualisationMLP.setLocation(plVisualisationMLP.getWidth() - btnCloseVisualisationMLP.getWidth() - 10, 10);
        
        btnCloseGraphMAPEMLP = createButton("close", "Zamknij");
        btnCloseGraphMAPEMLP.setSize(27, 24);
        btnCloseGraphMAPEMLP.setLocation(plGraphMAPEMLP.getWidth() - btnCloseGraphMAPEMLP.getWidth() - 10, 10);
        
        btnCloseGraphMSEMLP = createButton("close", "Zamknij");
        btnCloseGraphMSEMLP.setSize(27, 24);
        btnCloseGraphMSEMLP.setLocation(plGraphMSEMLP.getWidth() - btnCloseGraphMSEMLP.getWidth() - 10, 10);

        btnCloseParametrsMLP = createButton("close", "Zamknij");
        btnCloseParametrsMLP.setSize(27, 24);
        btnCloseParametrsMLP.setLocation(plParametrsMLP.getWidth() - btnCloseParametrsMLP.getWidth() - 10, 10);
        
        btnCloseFinishedLearningMLP = createButton("close", "Zamknij");
        btnCloseFinishedLearningMLP.setSize(27, 24);
        btnCloseFinishedLearningMLP.setLocation(plFinishedLearningMLP.getWidth() - btnCloseFinishedLearningMLP.getWidth() - 10, 10);
        
        btnCloseFinishedValidMLP = createButton("close", "Zamknij");
        btnCloseFinishedValidMLP.setSize(27, 24);
        btnCloseFinishedValidMLP.setLocation(plFinishedValidMLP.getWidth() - btnCloseFinishedValidMLP.getWidth() - 10, 10);
        
        btnSetParametrsMLP = createButton("parametrs/set", "Zamknij");
        btnSetParametrsMLP.setSize(121, 50);
        btnSetParametrsMLP.setLocation(plParametrsMLP.getWidth()/2 - btnSetParametrsMLP.getWidth()/2, plParametrsMLP.getHeight()-btnSetParametrsMLP.getHeight()-20);

        btnLearnMLP = createButton("learn", "Zamknij");
        btnLearnMLP.setSize(188, 70);
        btnLearnMLP.setLocation(plOptionsMLP.getWidth() / 2 - btnLearnMLP.getWidth() / 2, 600);
        btnLearnMLP.setVisible(false);

        btnValidMLP = createButton("valid", "Zamknij");
        btnValidMLP.setSize(182, 70);
        btnValidMLP.setLocation(plOptionsMLP.getWidth() / 2 - btnValidMLP.getWidth() / 2, 600);
        btnValidMLP.setVisible(false);

        btnTestMLP = createButton("test", "Zamknij");
        btnTestMLP.setSize(170, 70);
        btnTestMLP.setLocation(plOptionsMLP.getWidth() / 2 - btnValidMLP.getWidth() / 2, 600);
        btnTestMLP.setVisible(false);
        
        btnMSEMLP = createButton("functions/mse", "Zamknij");
        btnMSEMLP.setSize(121, 50);
        btnMSEMLP.setLocation(plFunctionsMLP.getWidth() / 2 - btnMSEMLP.getWidth() / 2, 120);
        
        btnMAPEMLP = createButton("functions/mape", "Zamknij");
        btnMAPEMLP.setSize(121, 50);
        btnMAPEMLP.setLocation(plFunctionsMLP.getWidth() / 2 - btnMAPEMLP.getWidth() / 2, 180);

        //Funkcje Aktywacji
        btnBipolarSigmoidalMLP = createButton("/activFunction/bipolarSigmoidal", "Select an activation function");
        btnBipolarSigmoidalMLP.setSize(274, 243);
        btnBipolarSigmoidalMLP.setLocation(plOptionsMLP.getWidth() + plConstructionMLP.getWidth() + 150, plMLP.getHeight() / 2 - plOptionsMLP.getHeight() / 2);

        btnUnipolarSigmoidalMLP = createButton("/activFunction/unipolarSigmoidal", "Select an activation function");
        btnUnipolarSigmoidalMLP.setSize(274, 243);
        btnUnipolarSigmoidalMLP.setLocation(plOptionsMLP.getWidth() + plConstructionMLP.getWidth() + 150, plMLP.getHeight() / 2 - plOptionsMLP.getHeight() / 2);
        btnUnipolarSigmoidalMLP.setVisible(false);

        btnBipolarThresholdMLP = createButton("/activFunction/bipolarThreshold", "Select an activation function");
        btnBipolarThresholdMLP.setSize(274, 243);
        btnBipolarThresholdMLP.setLocation(plOptionsMLP.getWidth() + plConstructionMLP.getWidth() + 150, plMLP.getHeight() / 2 - plOptionsMLP.getHeight() / 2);
        btnBipolarThresholdMLP.setVisible(false);

        btnUnipolarThresholdMLP = createButton("/activFunction/unipolarThreshold", "Select an activation function");
        btnUnipolarThresholdMLP.setSize(274, 243);
        btnUnipolarThresholdMLP.setLocation(plOptionsMLP.getWidth() + plConstructionMLP.getWidth() + 150, plMLP.getHeight() / 2 - plOptionsMLP.getHeight() / 2);
        btnUnipolarThresholdMLP.setVisible(false);

        elementsXAndWeightMLP = 13;
        sizeXAndWeightMLP = 50;
        xAndWeightMLP = new Input(elementsXAndWeightMLP);
        for (int i = 0; i < elementsXAndWeightMLP; i++) {
            xAndWeightMLP.addVector(i, sizeXAndWeightMLP, sizeXAndWeightMLP, 20, sizeXAndWeightMLP * i);
        }

        //Wybór zbiorów
        cbTrainingSetMLP = new JComboBox();
        comboSet(0, 200, 50, border, "Select TrainingSet", cbTrainingSetMLP, plOptionsMLP);
        cbTrainingSetMLP.addActionListener((ActionEvent e) -> {
            JComboBox combo = (JComboBox) e.getSource();
            System.out.println(combo.getSelectedItem());
            fileNameMLP = combo.getSelectedItem().toString();
            filePathMLP = "learn/trainingSet/";

            new Thread() {
                @Override
                public void run() {
                    
                    try {
                        loadTrainingSet(fileNameMLP);
                        
                        perceptronVectorInit=true;
                        System.out.println("Ile Inputów dodaję:"+vectorsMLP.get(0).getLength()+"\n");
                        perceptronMLP.addInput(vectorsMLP.get(0).getLength());
                        
                        drawInput();
                        
                        showZAndY();
                        
                        cleanWeight();

                    } catch (IOException ex) {
                        Logger.getLogger(CreatePerceptron.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }.start();

            btnLearnMLP.setVisible(true);
            btnValidMLP.setVisible(false);
            btnTestMLP.setVisible(false);
        });

        cbValidSetMLP = new JComboBox();
        comboSet(1, 200, 50, border, "Select ValidSet", cbValidSetMLP, plOptionsMLP);
        cbValidSetMLP.addActionListener((ActionEvent e) -> {
            JComboBox combo = (JComboBox) e.getSource();
            System.out.println(combo.getSelectedItem());
            fileNameMLP = combo.getSelectedItem().toString();
            filePathMLP = "learn/validSet/";

            new Thread() {
                @Override
                public void run() {

                    try {
                        loadTrainingSet(fileNameMLP);

                        perceptronVectorInit=true; 
                        
                        drawInput();
                        
                        showZAndY();

                    } catch (IOException ex) {
                        Logger.getLogger(CreatePerceptron.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }.start();

            btnLearnMLP.setVisible(false);
            btnValidMLP.setVisible(true);
            btnTestMLP.setVisible(false);
        });

        cbTestSetMLP = new JComboBox();
        comboSet(2, 200, 50, border, "Select TestSet", cbTestSetMLP, plOptionsMLP);
        cbTestSetMLP.addActionListener((ActionEvent e) -> {
            JComboBox combo = (JComboBox) e.getSource();
            System.out.println(combo.getSelectedItem());
            fileNameMLP = combo.getSelectedItem().toString();

            new Thread() {
                @Override
                public void run() {

                    try {
                        loadTrainingSet(fileNameMLP);
                        
                        perceptronVectorInit=true; 

                        drawInput();
                        
                        showZAndY();

                    } catch (IOException ex) {
                        Logger.getLogger(CreatePerceptron.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }.start();

            filePathMLP = "learn/testSet/";
            btnLearnMLP.setVisible(false);
            btnValidMLP.setVisible(false);
            btnTestMLP.setVisible(true);
        });

        //GRAPH
        
        graphMAPEMLP = new XYGraph();
		graphMAPEMLP.setBorder(new LineBorder(new Color(0, 0, 0)));
		graphMAPEMLP.setBackground(Color.WHITE);
		graphMAPEMLP.setBounds(10, 11, 360, 360);
                
        graphMAPEMLP.setLocation(plGraphMAPEMLP.getWidth()/2-graphMAPEMLP.getWidth()/2, 40);
        
        graphMSEMLP = new XYGraph();
		graphMSEMLP.setBorder(new LineBorder(new Color(0, 0, 0)));
		graphMSEMLP.setBackground(Color.WHITE);
		graphMSEMLP.setBounds(10, 11, 360, 360);
                
        graphMSEMLP.setLocation(plGraphMSEMLP.getWidth()/2-graphMSEMLP.getWidth()/2, 40);
         
        //Ustawienie lokalizacji paneli
        plMLP.setLocation((screen.getWidth() - plMLP.getWidth()) / 2,
                (screen.getHeight() - plMLP.getHeight()) / 2 + 50);

        plOptionsMLP.setLocation(50,
                plMLP.getHeight() / 2 - plOptionsMLP.getHeight() / 2);

        plConstructionMLP.setLocation(plOptionsMLP.getWidth() + 100, plMLP.getHeight() / 2 - plOptionsMLP.getHeight() / 2 + lbTitleMLP.getHeight() + 50);

        plFunctionsMLP.setLocation(plOptionsMLP.getWidth() + plConstructionMLP.getWidth() + 150,
                plMLP.getHeight() / 2 - plOptionsMLP.getHeight() / 2 + lbTitleMLP.getHeight() + 150);

        plParametrsMLP.setLocation(plConstructionMLP.getWidth() - plParametrsMLP.getWidth() - 10,
                plConstructionMLP.getHeight() / 2 - plParametrsMLP.getHeight() / 2);
        
        plFinishedLearningMLP.setLocation(plMLP.getWidth()/2-plFinishedLearningMLP.getWidth()/2, plMLP.getHeight()/2-plFinishedLearningMLP.getHeight()/2);
        
        plFinishedValidMLP.setLocation(plMLP.getWidth()/2-plFinishedLearningMLP.getWidth()/2, plMLP.getHeight()/2-plFinishedLearningMLP.getHeight()/2);
        
        plGraphMSEMLP.setLocation(plMLP.getWidth()/2-plGraphMSEMLP.getWidth()/2, plMLP.getHeight()/2-plGraphMSEMLP.getHeight()/2);
        
        plGraphMAPEMLP.setLocation(plMLP.getWidth()/2-plGraphMAPEMLP.getWidth()/2, plMLP.getHeight()/2-plGraphMAPEMLP.getHeight()/2);
        
        plVisualisationMLP.setLocation(10, plConstructionMLP.getHeight() / 2 - plVisualisationMLP.getHeight() / 2);

        plVisualisationMLP.add(btnCloseVisualisationMLP);
        plVisualisationMLP.add(lbNumberOfNeuralInLayerMLP);
        plVisualisationMLP.add(txtNumberOfNeuralInLayerMLP);
        plVisualisationMLP.add(btnAddNewLayerMLP);
        
        
        plGraphMAPEMLP.add(btnCloseGraphMAPEMLP);
        plGraphMAPEMLP.add(graphMAPEMLP);
        
        plGraphMSEMLP.add(btnCloseGraphMSEMLP);
        plGraphMSEMLP.add(graphMSEMLP);
        
        plFunctionsMLP.add(btnMAPEMLP);
        plFunctionsMLP.add(btnMSEMLP);
        
        plFinishedLearningMLP.add(btnCloseFinishedLearningMLP);
        
        plFinishedValidMLP.add(btnCloseFinishedValidMLP);
        plFinishedValidMLP.add(lbGoodAnswersMLP);
        
        //Dodanie do paneli obiektów
        plOptionsMLP.add(cbTrainingSetMLP);
        plOptionsMLP.add(cbValidSetMLP);
        plOptionsMLP.add(cbTestSetMLP);
        plOptionsMLP.add(btnLearnMLP);
        plOptionsMLP.add(btnValidMLP);
        plOptionsMLP.add(btnTestMLP);

        plParametrsMLP.add(btnCloseParametrsMLP);
        plParametrsMLP.add(lbBiasMLP);
        plParametrsMLP.add(txtBiasMLP);
        plParametrsMLP.add(lbBetaMLP);
        plParametrsMLP.add(txtBetaMLP);
        plParametrsMLP.add(btnSetParametrsMLP);
        plParametrsMLP.add(txtRandomWeightFromMLP);
        plParametrsMLP.add(txtRandomWeightToMLP);

        plConstructionMLP.add(plParametrsMLP);
        plConstructionMLP.add(plVisualisationMLP);
        //wizualizacja wejść i wag do perceptronu
        xAndWeightMLP.lbXs.stream().forEach((label) -> {
            plConstructionMLP.add(label);
        });
        xAndWeightMLP.txtXs.stream().forEach((txt) -> {
            plConstructionMLP.add(txt);
        });
        xAndWeightMLP.lbArrows.stream().forEach((label) -> {
            plConstructionMLP.add(label);
        });
        xAndWeightMLP.lbWeights.stream().forEach((label) -> {
            plConstructionMLP.add(label);
        });
        xAndWeightMLP.txtWeights.stream().forEach((txt) -> {
            plConstructionMLP.add(txt);
        });
        plConstructionMLP.add(lbYMLP);
        plConstructionMLP.add(lbZMLP);
        plConstructionMLP.add(txtYMLP);
        plConstructionMLP.add(txtZMLP);
        plConstructionMLP.add(btnMLP);
        

        plMLP.add(btnCloseMLP);
        plMLP.add(plGraphMSEMLP);
        plMLP.add(plGraphMAPEMLP);
        plMLP.add(plFinishedValidMLP);
        plMLP.add(plFinishedLearningMLP);
        plMLP.add(plOptionsMLP);
        plMLP.add(plConstructionMLP);
        plMLP.add(plFunctionsMLP);
        plMLP.add(lbTitleMLP);
        //plPerceptron.add(graphx);

        plMLP.add(btnBipolarSigmoidalMLP);
        plMLP.add(btnUnipolarSigmoidalMLP);
        plMLP.add(btnBipolarThresholdMLP);
        plMLP.add(btnUnipolarThresholdMLP);

        screen.getFullScreenWindow().getLayeredPane().add(plMLP,
                JLayeredPane.MODAL_LAYER);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        Object src = e.getSource();
        if (src == btnMultiLayerNetwork) {
            selectNetwork.setVisible(false);
            plMLP.setVisible(true);
        }
        if (src == btnCloseMLP) {
            plMLP.setVisible(false);
        }
        
        if(src==btnCloseFinishedLearningMLP){
            plFinishedLearningMLP.setVisible(false);
        }
        
        if(src==btnCloseFinishedValidMLP){
            plFinishedValidMLP.setVisible(false);
        }

        if (src == btnBipolarSigmoidalMLP) {
            btnBipolarSigmoidalMLP.setVisible(false);
            btnUnipolarSigmoidalMLP.setVisible(true);
            btnBipolarThresholdMLP.setVisible(false);
            btnUnipolarThresholdMLP.setVisible(false);

            lbBetaMLP.setVisible(true);
            txtBetaMLP.setVisible(true);
            
            lbBiasMLP.setLocation(plParametrsMLP.getWidth()/3-(widthLabelMLP+widthTextMLP)/2, 120);
            txtBiasMLP.setLocation(plParametrsMLP.getWidth()/3-(widthLabelMLP+widthTextMLP)/2+widthLabelMLP, 120);
            
            perceptronMLP.setState(AdelineCell.Function.UNIPOLAR_SIGMOID_FUNCTION);

        }
        if (src == btnUnipolarSigmoidalMLP) {
            btnBipolarSigmoidalMLP.setVisible(false);
            btnUnipolarSigmoidalMLP.setVisible(false);
            btnBipolarThresholdMLP.setVisible(true);
            btnUnipolarThresholdMLP.setVisible(false);
            
            lbBetaMLP.setVisible(false);
            txtBetaMLP.setVisible(false);
            
            lbBiasMLP.setLocation(plParametrsMLP.getWidth()/2-(widthLabelMLP+widthTextMLP)/2, 120);
            txtBiasMLP.setLocation(plParametrsMLP.getWidth()/2-(widthLabelMLP+widthTextMLP)/2+widthLabelMLP, 120);
            
            perceptronMLP.setState(AdelineCell.Function.BIPOLAR_THRESHOLD_FUNCTION);
        }
        if (src == btnBipolarThresholdMLP) {
            btnBipolarSigmoidalMLP.setVisible(false);
            btnUnipolarSigmoidalMLP.setVisible(false);
            btnBipolarThresholdMLP.setVisible(false);
            btnUnipolarThresholdMLP.setVisible(true);
            
            lbBetaMLP.setVisible(false);
            txtBetaMLP.setVisible(false);
            
            lbBiasMLP.setLocation(plParametrsMLP.getWidth()/2-(widthLabelMLP+widthTextMLP)/2, 120);
            txtBiasMLP.setLocation(plParametrsMLP.getWidth()/2-(widthLabelMLP+widthTextMLP)/2+widthLabelMLP, 120);

            perceptronMLP.setState(AdelineCell.Function.UNIPOLAR_THRESHOLD_FUNCTION);
        }
        if (src == btnUnipolarThresholdMLP) {
            btnBipolarSigmoidalMLP.setVisible(true);
            btnUnipolarSigmoidalMLP.setVisible(false);
            btnBipolarThresholdMLP.setVisible(false);
            btnUnipolarThresholdMLP.setVisible(false);

            lbBetaMLP.setVisible(true);
            txtBetaMLP.setVisible(true);
            
            lbBiasMLP.setLocation(plParametrsMLP.getWidth()/3-(widthLabelMLP+widthTextMLP)/2, 120);
            txtBiasMLP.setLocation(plParametrsMLP.getWidth()/3-(widthLabelMLP+widthTextMLP)/2+widthLabelMLP, 120);
            
            perceptronMLP.setState(AdelineCell.Function.BIPOLAR_SIGMOID_FUNCTION);
        }

        if (src == btnMLP) {
            plParametrsMLP.setVisible(true);
            plVisualisationMLP.setVisible(true);
        }
        if (src == btnCloseParametrsMLP) {
            plParametrsMLP.setVisible(false);
        }
        
        if(src==btnSetParametrsMLP){
            if(txtBiasMLP.getText().equals("")){
                perceptronMLP.setBias(0);
            }
            else{
                perceptronMLP.setBias(Double.valueOf(txtBiasMLP.getText()));
            }
            if(txtBetaMLP.getText().equals("")){
                perceptronMLP.setBeta(0);
            }
            else{
                perceptronMLP.setBeta(Double.valueOf(txtBetaMLP.getText()));
            }
            
            setAndDrawWeight(true);
            
            plParametrsMLP.setVisible(false);
        }
        
        if(src==btnMSEMLP){
            plGraphMSEMLP.setVisible(true);
        }
        
        if(src==btnMAPEMLP){
            plGraphMAPEMLP.setVisible(true);
        }
        if(src==btnCloseGraphMSEMLP){
            plGraphMSEMLP.setVisible(false);
        }
        
        if(src==btnCloseGraphMAPEMLP){
            plGraphMAPEMLP.setVisible(false);
        }
        
        if(src==btnCloseVisualisationMLP){
            plVisualisationMLP.setVisible(false);
        }
        
        if(src==btnAddNewLayerMLP){
        }

        if (src == btnLearnMLP) {
            runSet(true);
            plFinishedLearningMLP.setVisible(true);
            int pointX=0;
                for(Integer pointY :errorMSEMLP){
                    //System.out.println("Kolejny punkt do narysowania: "+pointY);
                    graphMSEMLP.drawPoint(pointX*5, pointY, Color.blue);
                    pointX++;
                }
                pointX=0;
                for(Integer pointY :errorMAPEMLP){
                    System.out.println("Kolejny punkt do narysowania: "+pointY);
                    graphMAPEMLP.drawPoint(pointX*5, pointY, Color.blue);
                    pointX++;
                }
        }

        if (src == btnValidMLP) {
            runSet(false);
            plFinishedValidMLP.setVisible(true);
            
        }

        /*
        if (src == activationFunction) {
            System.out.println(state);
            if (state < 3) {
                state++;
            } else {
                state=0;
            }
            switch (state) {
                case 0:
                    changeButtonImage("/activFunction/bipolarSigmoidal", "Select an activation function", activationFunction);
                    break;
                case 1:
                    changeButtonImage("/activFunction/unipolarSigmoidal", "Select an activation function", activationFunction);
                    break;
                case 2:
                    changeButtonImage("/activFunction/bipolarThreshold", "Select an activation function", activationFunction);
                    break;
                case 3:
                    changeButtonImage("/activFunction/unipolarThreshold", "Select an activation function", activationFunction);
                    break;
                default:
                    System.out.println("Nieobsługiwana funkcja");
            }
        }
         */
    }
    
    private void setAndDrawWeight(boolean randomWeight){
        Border border;
        
        String txtWeightFrom=txtRandomWeightFromMLP.getText();
        String txtWeightTo=txtRandomWeightToMLP.getText();
        
        double weightFrom=Double.valueOf(txtWeightFrom);
        double weightTo=Double.valueOf(txtWeightTo);
        
        if(weightTo<weightFrom){
            double buffer=weightFrom;
            weightFrom=weightTo;
            weightTo=buffer;
            txtRandomWeightFromMLP.setText(Double.toString(weightFrom));
            txtRandomWeightToMLP.setText(Double.toString(weightTo));
        }
        
        double difference=Math.abs(weightFrom)+Math.abs(weightTo);
        Random random=new Random();
        
        for(int i=0; i<vectorsMLP.get(0).getLength(); i++){
            double weight;
            if(randomWeight){
                weight=random.nextDouble()*difference+weightFrom;
                System.out.println("Wylosowane wagi:"+weight+"\n");
                //Ustawienie wagi w perceptronie
                perceptronMLP.setInputWeight(i, weight);
            }
            else{
                weight=perceptronMLP.getInputWeight(i);
            }
            
            
            
            if(weight>0){
                xAndWeightMLP.txtWeights.get(i).setBackground(Color.GREEN);
                xAndWeightMLP.txtWeights.get(i).setForeground(Color.GREEN);
                border = BorderFactory.createLineBorder(Color.GREEN, 3);
                xAndWeightMLP.txtWeights.get(i).setBorder(border);
                String txtWeight=Double.toString(weight);
                if(txtWeight.length()>4){
                    xAndWeightMLP.txtWeights.get(i).setText(Double.toString(weight).substring(0, 4));
                }
                else{
                    xAndWeightMLP.txtWeights.get(i).setText(Double.toString(weight));
                }
                xAndWeightMLP.lbWeights.get(i).setBackground(Color.GREEN);
                xAndWeightMLP.lbWeights.get(i).setForeground(Color.GREEN);
            }
            else{
                xAndWeightMLP.txtWeights.get(i).setBackground(Color.RED);
                xAndWeightMLP.txtWeights.get(i).setForeground(Color.RED);
                border = BorderFactory.createLineBorder(Color.RED, 3);
                xAndWeightMLP.txtWeights.get(i).setBorder(border);
                String txtWeight=Double.toString(weight);
                if(txtWeight.length()>4){
                    xAndWeightMLP.txtWeights.get(i).setText(Double.toString(weight).substring(0, 4));
                }
                else{
                    xAndWeightMLP.txtWeights.get(i).setText(Double.toString(weight));
                }
                xAndWeightMLP.lbWeights.get(i).setBackground(Color.RED);
                xAndWeightMLP.lbWeights.get(i).setForeground(Color.RED);
            }
        }
        
        
    }
    public void runSet(boolean learning){
        int numberOfVector=0;
            int drawCoordinates=0;
            double y=0;
            int goodAnswers=0;
            for(VectorX vectorX : vectorsMLP){
                drawCoordinates=0;
                //Ładowanie datyX do perceptronu 
                for(InputComponent txt :xAndWeightMLP.txtXs){
                    if(drawCoordinates<vectorsMLP.get(numberOfVector).getLength()){
                    perceptronMLP.setInputData(drawCoordinates, vectorsMLP.get(numberOfVector).getCoordinates(drawCoordinates));
                    //txt.setText(Double.toString(vectors.get(numberOfVector).getCoordinates(drawCoordinates)));
                    drawCoordinates++;
                    }
                }
                
                
                y=perceptronMLP.getOutput();
                System.out.println("TAKI ZE MNIE MADRY PERCEPTRON: "+y );
                //Zmieniamy wagi, uczymy perceptron
                if(learning){
                    perceptronMLP.learnNeuralCell(y, vectorX);
                    System.out.println("Co doaję do MAPE: "+100*perceptronMLP.getErrorSignal()/(y+0.00001)+"\n");
                    errorMAPEMLP.add((int)(100*perceptronMLP.getErrorSignal()/y));
                    errorMSEMLP.add((int)(perceptronMLP.getErrorSignal()*perceptronMLP.getErrorSignal()));
                    //graphMAPE.drawPoint(numberOfVector, (int)(perceptron.getErrorSignal()/y), Color.blue);
                }
                else{
                    if(y==vectorX.getZ()){
                        goodAnswers++;
                    }
                }
                numberOfVector++;
                
            }
            
            if(!learning){
                System.out.println("Ile dobrych odpowiedzi"+goodAnswers+"\n");
                lbGoodAnswersMLP.setText(Double.toString(100*goodAnswers/numberOfVector).substring(0, 2)+"%");
            }
            
            //RYSOWANIE
            drawCoordinates=0;
            numberOfVector-=1;
            for(InputComponent txt :xAndWeightMLP.txtXs){
                if(drawCoordinates<vectorsMLP.get(numberOfVector).getLength()){
                    txt.setText(Double.toString(vectorsMLP.get(numberOfVector).getCoordinates(drawCoordinates)));
                    drawCoordinates++;
                }
            }
            
            txtYMLP.setText(Double.toString(y));
            txtZMLP.setText(Double.toString(vectorsMLP.get(numberOfVector).getZ()));
            
            //rysuje graficznie Wagi
            setAndDrawWeight(false);
    }
    
    public void cleanWeight(){
        Border border = BorderFactory.createLineBorder(Color.GREEN, 3);
        for(int i=0; i<vectorsMLP.get(0).getLength(); i++){
                xAndWeightMLP.txtWeights.get(i).setBackground(Color.GREEN);
                xAndWeightMLP.txtWeights.get(i).setForeground(Color.GREEN);
                border = BorderFactory.createLineBorder(Color.GREEN, 3);
                xAndWeightMLP.txtWeights.get(i).setBorder(border);
                xAndWeightMLP.txtWeights.get(i).setText("");
                
                xAndWeightMLP.lbWeights.get(i).setBackground(Color.GREEN);
                xAndWeightMLP.lbWeights.get(i).setForeground(Color.GREEN);
        }    
    }

    protected void changeButtonImage(String name, String toolTip, JButton button) {

        // Ładowanie rysunku:
        String imagePath = "images/menu/main/" + name + ".png";
        ImageIcon iconRollover = new ImageIcon(imagePath);

        int w = iconRollover.getIconWidth();
        int h = iconRollover.getIconHeight();

        // Pobranie kursora dla tego przycisku:
        Cursor cursor
                = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

        // Tworzenie domyślnego, przeświecającego przycisku:
        Image image = screen.createCompatibleImage(w, h,
                Transparency.TRANSLUCENT);
        Graphics2D g = (Graphics2D) image.getGraphics();
        Composite alpha = AlphaComposite.getInstance(
                AlphaComposite.SRC_OVER, .5f);
        g.setComposite(alpha);
        g.drawImage(iconRollover.getImage(), 0, 0, null);
        g.dispose();
        ImageIcon iconDefault = new ImageIcon(image);

        // Tworzenie przycisku przyciśniętego:
        image = screen.createCompatibleImage(w, h,
                Transparency.TRANSLUCENT);
        g = (Graphics2D) image.getGraphics();
        g.drawImage(iconRollover.getImage(), 2, 2, null);
        g.dispose();
        ImageIcon iconPressed = new ImageIcon(image);

        // Tworzenie przycisku:
        button.addActionListener(this);
        button.setIgnoreRepaint(true);
        button.setFocusable(false);
        button.setToolTipText(toolTip);
        button.setBorder(null);
        button.setContentAreaFilled(false);
        button.setCursor(cursor);
        button.setIcon(iconDefault);
        button.setRolloverIcon(iconRollover);
        button.setPressedIcon(iconPressed);
    }


    public void comboSet(int witch, int widthTextField, int heightTextField, Border border, String toolTipText, JComboBox combo, ImagePanel panel) {
        combo.setSize(widthTextField, heightTextField);
        combo.setLocation(panel.getWidth() / 2 - widthTextField / 2, firstMLP + witch * 140);
        combo.setBorder(border);
        combo.setToolTipText(toolTipText);
        combo.setFont(new Font("Jokerman", Font.PLAIN, 16));
        switch (witch) {

            case 0:
                Path dir = Paths.get("learn/trainingSet");
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "*.ts")) {
                    for (Path file : stream) {
                        combo.addItem(file.getFileName());
                    }
                } catch (IOException | DirectoryIteratorException x) {
                    // IOException can never be thrown by the iteration.
                    // In this snippet, it can only be thrown by newDirectoryStream.
                    System.err.println(x);
                }
                break;
            case 1:
                Path dir2 = Paths.get("learn/validSet");
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir2, "*.vs")) {
                    for (Path file : stream) {
                        combo.addItem(file.getFileName());
                    }
                } catch (IOException | DirectoryIteratorException x) {
                    // IOException can never be thrown by the iteration.
                    // In this snippet, it can only be thrown by newDirectoryStream.
                    System.err.println(x);
                }
                break;
            case 2:
                Path dir3 = Paths.get("learn/testSet");
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir3, "*.test")) {
                    for (Path file : stream) {
                        combo.addItem(file.getFileName());
                    }
                } catch (IOException | DirectoryIteratorException x) {
                    // IOException can never be thrown by the iteration.
                    // In this snippet, it can only be thrown by newDirectoryStream.
                    System.err.println(x);
                }
                break;
        }
        combo.setOpaque(false);
        combo.addActionListener(this);
        //comboBox.setBackground(new Color(0,0,0,0));
        ((JTextField) combo.getEditor().getEditorComponent()).setOpaque(false);
        combo.setUI(new BasicComboBoxUI() {
            public void paintCurrentValueBackground(Graphics g, Rectangle bounds, boolean hasFocus) {
            }
        });
    }

    public void loadTrainingSet(String fileName) throws IOException {

        vectorsMLP = new ArrayList<>();

        //lines = new ArrayList();
        int height = 0;

        // odczytanie wszystkich wierszy z pliku do listy
        BufferedReader reader = new BufferedReader(
                new FileReader(filePathMLP + fileName));
        while (true) {
            String line = reader.readLine();
            // koniec wierszy do odczytywania
            if (line == null) {
                reader.close();
                break;
            }

            // dodawanie wszystkich wierszy poza komentarzami
            if (!line.startsWith("#")) {
                Pattern comma = Pattern.compile(",");
                String[] xn = comma.split(line);
                VectorX v = new VectorX(xn.length - 1);
                for (int i = 0; i < (xn.length - 1); i++) {
                    double value = Double.valueOf(xn[i]);
                    v.setCoordinates(i, value);
                }
                double value = Double.valueOf(xn[xn.length - 1]);
                v.setZ(value);
                boolean add = vectorsMLP.add(v);
                
            }
        }
    }

    //Rysuje tyle pól x ile wynosi długość vectora
    private void drawX(ImagePanel imgPanel) {
        int vectorLength = vectorsMLP.get(0).getLength();
        int start = imgPanel.getHeight() / (vectorLength + 1);
        int count = 1;
        for (JLabel label : xAndWeightMLP.lbXs) {
            if (vectorLength > 0) {
                label.setForeground(new Color(0, (float) 0.5, (float) 1.0));
                label.setLocation(20, start * count - sizeXAndWeightMLP / 2);
                label.setVisible(true);
                count++;
                vectorLength--;
            } else {
                label.setVisible(false);
                label.setForeground(Color.BLACK);
            }
        }
        vectorLength = vectorsMLP.get(0).getLength();
        count = 1;
        for (InputComponent text : xAndWeightMLP.txtXs) {
            if (vectorLength > 0) {
                text.setForeground(Color.CYAN);
                Border border
                        = BorderFactory.createLineBorder(new Color(0, (float) 0.5, (float) 1.0), 3);
                text.setBorder(border);
                text.setLocation(20 + sizeXAndWeightMLP, start * count - sizeXAndWeightMLP / 2);
                text.setVisible(true);
                text.setText(Double.toString(vectorsMLP.get(0).getCoordinates(count-1)));
                
                //ustawienie wejscia perceptronu
                perceptronMLP.setInputData(count-1, vectorsMLP.get(0).getCoordinates(count-1));
                
                count++;
                vectorLength--;
            } else {
                text.setVisible(false);
                text.setForeground(Color.BLACK);
                Border border
                        = BorderFactory.createLineBorder(Color.BLACK, 3);
                text.setBorder(border);
            }
        }
    }
    
    //Rysuje tyle pól STRZAŁEK ile wynosi długość vectora
    private void drawArrows(ImagePanel imgPanel) {
        int vectorLength = vectorsMLP.get(0).getLength();
        int start = imgPanel.getHeight() / (vectorLength + 1);
        int count = 1;
        for (JLabel arrow : xAndWeightMLP.lbArrows) {
            if (vectorLength > 0) {
                arrow.setLocation(20 + 2 * sizeXAndWeightMLP, start * count - sizeXAndWeightMLP / 2 + ((sizeXAndWeightMLP - arrow.getHeight()) / 2));
                arrow.setVisible(true);
                count++;
                vectorLength--;
            } else {
                arrow.setVisible(false);
            }
        }
    }
    
    //Rysuje tyle pól WAG ile wynosi długość vectora
    private void drawWeight(ImagePanel imgPanel){
        int vectorLength = vectorsMLP.get(0).getLength();
        int start = imgPanel.getHeight() / (vectorLength + 1);
        int count = 1;
        for (JLabel weight : xAndWeightMLP.lbWeights) {
            if (vectorLength > 0) {
                weight.setForeground(Color.GREEN);
                weight.setLocation(20 + 2 * sizeXAndWeightMLP + xAndWeightMLP.getArrowWidth(), start * count - sizeXAndWeightMLP / 2);
                weight.setVisible(true);
                count++;
                vectorLength--;
            } else {
                weight.setVisible(false);
                weight.setForeground(Color.BLACK);
            }
        }
        vectorLength = vectorsMLP.get(0).getLength();
        count = 1;
        for (InputComponent text : xAndWeightMLP.txtWeights) {
            if (vectorLength > 0) {
                text.setForeground(Color.GREEN);
                Border border
                        = BorderFactory.createLineBorder(Color.GREEN, 3);
                text.setBorder(border);
                text.setLocation(20 + 3 * sizeXAndWeightMLP + xAndWeightMLP.getArrowWidth(), start * count - sizeXAndWeightMLP / 2);
                text.setVisible(true);
                count++;
                vectorLength--;
            } else {
                text.setVisible(false);
                text.setForeground(Color.BLACK);
                Border border
                        = BorderFactory.createLineBorder(Color.BLACK, 3);
                text.setBorder(border);
            }
        }
    }

    public void drawInput() {
        //Rysowanie Xów
        drawX(plConstructionMLP);
        //Rysowanie strzałek
        drawArrows(plConstructionMLP);
        //Rysowanie Wag
        drawWeight(plConstructionMLP);
    }

    class XYGraph extends JPanel {

        /**
         * Variables which contains info about currently added point.
         */
        Color clr;
        ArrayList<Integer> x_var;
        ArrayList<Integer> y_var;

        public XYGraph(){
            x_var=new ArrayList<>(100);
            y_var=new ArrayList<>(100);
        }
        /**
         * Override to write custom point.
         */
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.setColor(clr);
            int i=0;
            for (VectorX v:vectorsMLP) {
                g.fillRect(x_var.get(i)-2, y_var.get(i)-2, 4, 4);
                i++;
            }
            paintCross(g);
        }

        /**
         * Yet another smart function to draw a cross on the component.
         *
         * @param g
         */
        private void paintCross(Graphics g) {
            g.setColor(Color.BLACK);
            g.fillRect(180, 0, 1, 360);
            g.drawLine(176, 8, 180, 0);
            g.drawLine(184, 8, 180, 0);

            g.fillRect(0, 180, 360, 1);
            g.drawLine(352, 176, 360, 180);
            g.drawLine(352, 184, 360, 180);
            g.drawString("0", 183, 193);
        }

        /**
         * Writes a point at selected coordinates with selected colour.
         *
         * @param x coordinate X
         * @param y coordinate Y
         * @param clr selected colour
         */
        public void drawPoint(int x, int y, Color clr) {

            x += 180;
            y += 180;
            y = 360 - y;
            x_var.add(x);
            y_var.add(y);
            this.clr = clr;
            paintImmediately(x, y, 4, 4);
            
        }
    }

    public void setPanel(ImagePanel ip, Border b, boolean visible) {
        ip.setBorder(b);
        ip.setVisible(visible);
    }
    
    public void setFieldWithLabel(JLabel label, InputComponent text, Color color, Border border, String toolTipText,
            int locationX, int locationY, int widthLabel, int widthText,int height, boolean visible){
        label.setSize(widthLabel, height);
        label.setLocation(locationX, locationY);
        label.setVisible(visible);
        label.setBackground(color);
        label.setForeground(color);
        label.setFont(new Font("Gill Sans Ultra Bold", Font.PLAIN, 15));
        
        setField(text, color, border, toolTipText, locationX+widthLabel, locationY, widthText, height, visible);
    }
    
    public void setField(InputComponent text, Color color, Border border, String toolTipText,
            int locationX, int locationY, int widthText,int height, boolean visible){
        text.setSize(widthText, height);
        text.setLocation(locationX, locationY);
        text.setOpaque(false);
        text.setBorder(border);
        text.setBackground(color);
        text.setForeground(color);
        text.setVisible(visible);
        text.setToolTipText(toolTipText);
        text.setFont(new Font("Gill Sans Ultra Bold", Font.PLAIN, 15));
    }
    
    public void showZAndY(){
        lbYMLP.setVisible(true);
        lbZMLP.setVisible(true);
        txtYMLP.setVisible(true);
        
        txtZMLP.setText(Double.toString(vectorsMLP.get(0).getZ()));
        txtZMLP.setVisible(true);
    }
}
