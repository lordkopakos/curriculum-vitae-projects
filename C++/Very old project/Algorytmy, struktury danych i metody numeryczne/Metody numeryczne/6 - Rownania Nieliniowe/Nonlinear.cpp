#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

double f(double);
double f_prim(double);

double solveEulersMethod(double a, double b, unsigned int steps, double eps);
double solveNewtonsMethod(double a, double b, unsigned int steps, double eps);

int main()
{
	double a = 0.1;
	double b = 10;
	int max_steps = 20;
	double tolerance = 1e-6;


	solveEulersMethod(a, b, max_steps, tolerance);
	solveNewtonsMethod(a, b, max_steps, tolerance);

	system("pause");
	return 0;
}


inline double f(double x) {
	return ((x - 2)*x + 2)*x - 3;	//x^3 - 2x^2 + 2x - 3
}

inline double f_prim(double x) {
	return (3 * x - 4)*x + 2;		//3x^2-4x + 2
}



//Metoda siecznych
double solveEulersMethod(double a, double b, unsigned int steps, double eps)
{
	std::cout << "Metoda siecznych"<< std::endl;
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}

	double xn_2 = a,
		xn_1 = b;
	for (unsigned int i = 0; i < steps; ++i) {
		double xn = xn_1 - (f(xn_1)*(xn_1 - xn_2)) / (f(xn_1) - f(xn_2));
		if (fabs(f(xn)) < eps) {
			std::cout << "x" << i << " = " << xn << " y = " << f(xn) << std::endl;
			std::cout << "Zakonczono w " << i << " iteracji na " << steps << ".\n";
			return xn;
		}
		std::cout << "x" << i << " = " << xn << " y = " << f(xn) << std::endl;
		xn_2 = xn_1;
		xn_1 = xn;
	}
	return xn_1;
}

//Metoda stycznych
double solveNewtonsMethod(double a, double b, unsigned int steps, double eps)
{
	std::cout << "Metoda stycznych" << std::endl;
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}

	

	double xn_1 = a;

	for (unsigned int i = 0; i < steps; ++i) {
		double xn = xn_1 - f(xn_1) / f_prim(xn_1);
		if (fabs(f(xn)) < eps) {
			std::cout << "x" << i << " = " << xn << " y = " << f(xn) << std::endl;
			std::cout << "Zakonczono w " << i << " iteracji na " << steps << ".\n";
			return xn;
		}
		std::cout << "x" << i << " = " << xn << " y = " << f(xn) << std::endl;
		xn_1 = xn;
	}

	return xn_1;
}