#pragma once
#include"Postac.h"
#include"File.h"
#include<thread>

class GAME
{
	Postac *P;
	Mag M;
	Rycerz R;
	

	void choosehero();
	void newgame();
	void menu();
	Postac* load();
	void save();

	enum StatGame {
		Menu,
		Game,
		LoadGame,
		SaveGame,
		GameOver,
		GameEnd
	};

	StatGame state;
public:
	
	void runGame();
	GAME();
	~GAME();
};

