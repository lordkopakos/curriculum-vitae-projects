function [xw, yw] = wierzcholki(t, y)

f=1;
fs=1/(t(2) - t(1));

NT=floor(fs/f); %liczba punkt�w w jednym okresie

a=220:NT:length(t);
b=260:NT:length(t);

xw=[]
yw=[]

for i=1:1:length(a)-1
  p=polyfit(t(a(i):b(i)), y(a(i):b(i)), 2);
  delta=p(2)*p(2)-4*p(1)*p(3);
  xw(i)=-p(2)/(2*p(1));
  yw(i)=-delta/(4*p(1));
end

plot(xw, yw, 'g-')


