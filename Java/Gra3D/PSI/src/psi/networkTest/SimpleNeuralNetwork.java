
package psi.networkTest;

import java.awt.EventQueue;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.border.LineBorder;

import com.sun.corba.se.impl.orbutil.graph.Graph;
import com.sun.prism.shader.DrawCircle_Color_AlphaTest_Loader;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Pattern;
import javax.swing.SwingConstants;
import psi.network.VectorX;
import psi.neural.AdelineCell;
import psi.neural.NeuralCell;

public class SimpleNeuralNetwork
{

	JFrame frame;
	private static XYGraph graphx;
	/**
	 * NeuralCell Object
	 */
	private static NeuralCell cell;
	private JLabel x_weight;
	private JLabel y_weight;
        
        private String filepath;
        private ArrayList lines;
	
        //Wartosci na wejsciu
        private ArrayList<VectorX> m_VectorX;
        private ArrayList<VectorX> validVectorX;
        int numberInputs;
        int numberVectors;
        double bias;
        
	public void perceptronLearning(double result, int i){
            System.out.println("w1:"+cell.getInputWeight(0)+"\tw2:"+cell.getInputWeight(1));
            System.out.println("err=z-y:\t"+m_VectorX.get(i).getZ()+"-"+result);
            double error=m_VectorX.get(i).getZ()-result;
            cell.setInputWeight(0, bias+cell.getInputWeight(0)*0.1*error);
            cell.setInputWeight(1, bias+cell.getInputWeight(1)*0.1*error);
            x_weight.setText(Double.toString(cell.getInputWeight(0)));
            y_weight.setText(Double.toString(cell.getInputWeight(1)));
            
        }
	/*
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					SimpleNeuralNetwork window = new SimpleNeuralNetwork();
					window.frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
		
		
	}
	
	/**
	 * Create the application.
	 */
	public SimpleNeuralNetwork() throws IOException
	{
		init();
		loadTrainingSet("AND.txt");
                validSet("validAND.txt");
		/**
		 * Creating the cell.
		 * Override the finalizeData function for unique output.
		 */
		cell = new NeuralCell()
		{
			@Override
			public double finalizeData(double membranePotential)
			{
				if(membranePotential > 1)
					return 1;
				else
                                        return -1;
				
			}
		};
		
		/**
		 * Adding two dendrytes and synapses for input.
		 */
		cell.addInput(2);
		
		/**
		 * Setting random weights.
		 * Between 0 and 1.
		 */
		Random rand = new Random();
		double rx = rand.nextDouble();
		double ry = rand.nextDouble();
		cell.setInputWeight(0, bias+rx);
		cell.setInputWeight(1, bias+ry);
		
		x_weight.setText(Double.toString(rx));
		y_weight.setText(Double.toString(ry));
	}
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void init()
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 599, 420);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		graphx = new XYGraph();
		graphx.setBorder(new LineBorder(new Color(0, 0, 0)));
		graphx.setBackground(Color.WHITE);
		graphx.setBounds(10, 11, 360, 360);
		frame.getContentPane().add(graphx);
		
		JButton btnAddPoint = new JButton("Learn");
		btnAddPoint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//for(int i = 1; i<=500; i++)
					addRandomPoint();
			}
		});
		btnAddPoint.setBounds(408, 55, 144, 23);
		frame.getContentPane().add(btnAddPoint);
                
                
                JButton btnValid = new JButton("Valid");
		btnValid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//for(int i = 1; i<=500; i++)
					valid();
			}
		});
		btnValid.setBounds(408, 155, 144, 23);
		frame.getContentPane().add(btnValid);
		
		JLabel lblWagaX = new JLabel("Waga X");
		lblWagaX.setHorizontalAlignment(SwingConstants.CENTER);
		lblWagaX.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblWagaX.setBounds(408, 89, 58, 23);
		frame.getContentPane().add(lblWagaX);
		
		JLabel lblWagaY = new JLabel("Waga Y");
		lblWagaY.setHorizontalAlignment(SwingConstants.CENTER);
		lblWagaY.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblWagaY.setBounds(494, 89, 58, 23);
		frame.getContentPane().add(lblWagaY);
		
		x_weight = new JLabel("x");
		x_weight.setHorizontalAlignment(SwingConstants.CENTER);
		x_weight.setFont(new Font("Tahoma", Font.PLAIN, 16));
		x_weight.setBounds(408, 114, 58, 23);
		frame.getContentPane().add(x_weight);
		
		y_weight = new JLabel("y");
		y_weight.setHorizontalAlignment(SwingConstants.CENTER);
		y_weight.setFont(new Font("Tahoma", Font.PLAIN, 16));
		y_weight.setBounds(494, 114, 58, 23);
		frame.getContentPane().add(y_weight);
                
                filepath="learn/";
                bias=0.5;
	
	}
	
        public void loadTrainingSet(String fileName) throws IOException{

        lines = new ArrayList();
        int height = 0;

        // odczytanie wszystkich wierszy z pliku do listy
        BufferedReader reader = new BufferedReader(
                new FileReader(filepath + fileName));
        while (true) {
            String line = reader.readLine();
            // koniec wierszy do odczytywania
            if (line == null) {
                reader.close();
                break;
            }

            // dodawanie wszystkich wierszy poza komentarzami
            if (!line.startsWith("#")) {
                lines.add(line);
            }
        }

        // analizowanie wiersza
        height = lines.size();
        
        String settings = (String) lines.get(0);
        Pattern pattern = Pattern.compile(",");
        String[] infoTable = pattern.split(settings);
        numberInputs=Integer.valueOf(infoTable[0]);
        numberVectors=Integer.valueOf(infoTable[1]);
        if(numberVectors!=(height-1)){
            System.out.println("Podana liczba wektorow w pliku "
                    + "jest inna niż faktyczna liczba wektorow");
            
        }
        m_VectorX = new ArrayList<>(numberVectors);
        
        for (int y = 0; y < numberVectors; y++) {

            String line = (String) lines.get(y+1);

            Pattern comma = Pattern.compile(",");
            String[] xn = comma.split(line);
            
            VectorX v=new VectorX(numberInputs);
            for(int i=0; i<numberInputs; i++){
                double value=Double.valueOf(xn[i]);
                v.setCoordinates(i, value);
            }
            double value=Double.valueOf(xn[numberInputs]);
            v.setZ(value);
            boolean add = m_VectorX.add(v);
            if(add){
                System.out.println("Nowy VectorX został dodany");
            }
            
        }
    }
        
        public void validSet(String fileName) throws IOException{

        lines = new ArrayList();
        int height = 0;

        // odczytanie wszystkich wierszy z pliku do listy
        BufferedReader reader = new BufferedReader(
                new FileReader(filepath + fileName));
        while (true) {
            String line = reader.readLine();
            // koniec wierszy do odczytywania
            if (line == null) {
                reader.close();
                break;
            }

            // dodawanie wszystkich wierszy poza komentarzami
            if (!line.startsWith("#")) {
                lines.add(line);
            }
        }

        // analizowanie wiersza
      

        height = lines.size();
        
        String settings = (String) lines.get(0);
        Pattern pattern = Pattern.compile(",");
        String[] infoTable = pattern.split(settings);
        numberInputs=Integer.valueOf(infoTable[0]);
        numberVectors=Integer.valueOf(infoTable[1]);
        if(numberVectors!=(height-1)){
            System.out.println("Podana liczba wektorow w pliku "
                    + "jest inna niż faktyczna liczba wektorow");
            
        }
        validVectorX = new ArrayList<>(numberVectors);
        
        for (int y = 0; y < numberVectors; y++) {

            String line = (String) lines.get(y+1);

            Pattern comma = Pattern.compile(",");
            String[] xn = comma.split(line);
            
            VectorX v=new VectorX(numberInputs);
            for(int i=0; i<numberInputs; i++){
                double value=Double.valueOf(xn[i]);
                v.setCoordinates(i, value);
            }
            double value=Double.valueOf(xn[numberInputs]);
            v.setZ(value);
            boolean add = validVectorX.add(v);
            if(add){
                System.out.println("Nowy VectorX został dodany");
            }
            
        }
    }
        
     private void addRandomPoint()
     {
     	// Generate two numbers
     	Random r = new Random();
     	int x = r.nextInt()%180;
     	int y = r.nextInt()%180;
     	
        
        
     	// Pass them to the neural cell
        for(int i=0; i<numberVectors;i++){
        System.out.println("Epoka nr."+i);
        double result=0;
     	cell.setInputData(0, m_VectorX.get(i).getCoordinates(0));
     	cell.setInputData(1, m_VectorX.get(i).getCoordinates(1));
     	
        
     	// Get the output
     	result = (double)cell.getOutput();
        
        if(result == 1){
     		graphx.drawPoint(x, y, Color.BLUE);
        }
        else if(result == -1){
     		graphx.drawPoint(x, y, Color.RED);
        }
        perceptronLearning(result, i);
        System.out.println("OUTPUT:" + result);
        
        
        }
     	
     	// Draw appropriate point colour
     	
     }
     
     private void valid()
     {
     	// Generate two numbers
     	Random r = new Random();
     	int x = r.nextInt()%180;
     	int y = r.nextInt()%180;
     	
        
        
     	// Pass them to the neural cell
        for(int i=0; i<numberVectors;i++){
        double result=0;
     	cell.setInputData(0, validVectorX.get(i).getCoordinates(0));
     	cell.setInputData(1, validVectorX.get(i).getCoordinates(1));
     	
        
     	// Get the output
     	result = (double)cell.getOutput();
        
        if(result == 1){
     		graphx.drawPoint(x, y, Color.BLUE);
        }
        else if(result == -1){
     		graphx.drawPoint(x, y, Color.RED);
        }
        System.out.println("OUTPUT:" + result);
        
        
        }
     	
     	// Draw appropriate point colour
     	
     }
	
     class XYGraph extends JPanel
     {
     	/**
     	 * Variables which contains info about 
     	 * currently added point.
     	 */
     	Color clr;
     	int x_var;
     	int y_var;
     	
     	/**
     	 * Override to write custom point.
     	 */
     	@Override
     	protected void paintComponent(Graphics g)
     	{
     		super.paintComponent(g);
     		g.setColor(clr);
     		if(x_var != 0 && y_var != 0)
     			g.fillRect(x_var, y_var, 4, 4);
     		paintCross(g);
     	}
     	
     	/**
     	 * Yet another smart function to draw 
     	 * a cross on the component.
     	 * @param g
     	 */
     	private void paintCross(Graphics g)
     	{
     		g.setColor(Color.BLACK);
     		g.fillRect(180, 0, 1, 360);
     		g.drawLine(176, 8, 180, 0);
     		g.drawLine(184, 8, 180, 0);
     		
     		g.fillRect(0, 180, 360, 1);
     		g.drawLine(352, 176, 360, 180);
     		g.drawLine(352, 184, 360, 180);
     		g.drawString("0", 183, 193);
     	}
     	
     	/**
     	 * Writes a point at selected coordinates 
     	 * with selected colour.
     	 * @param x coordinate X
     	 * @param y coordinate Y
     	 * @param clr selected colour
     	 */
     	public void drawPoint(int x, int y, Color clr)
     	{
     		
     		x+=180;
     		y+=180;
     		y=360-y;
     		x_var = x;
     		y_var = y;
     		this.clr = clr;
     		paintImmediately(x,y,4,4);
     	}
     }
}