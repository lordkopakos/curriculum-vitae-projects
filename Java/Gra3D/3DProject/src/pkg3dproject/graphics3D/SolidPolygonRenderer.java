package pkg3dproject.graphics3D;

import java.awt.Color;
import java.awt.Graphics2D;
import pkg3dproject.math3D.*;

public class SolidPolygonRenderer extends PolygonRenderer{

    public SolidPolygonRenderer(Transform3D camera, ViewWindow viewWindow) {
        this(camera, viewWindow,true);
    }
    
    public SolidPolygonRenderer(Transform3D camera, ViewWindow viewWindow, boolean clearViewEveryFrame) {
        super(camera, viewWindow, clearViewEveryFrame);
    }

    /*
        Rzutuje bieżący wielokąt. W tym momencie bieżący wielokąt         
        jest transformowany, przycinany, rzutowany, konwertowany
        na skany i rysowany.
    */
    @Override
    protected void drawCurrentPolygon(Graphics2D g) {

        // ustaw kolor
        if (sourcePolygon instanceof SolidPolygon3D) {
            g.setColor(((SolidPolygon3D)sourcePolygon).getColor());
        }
        else {
            g.setColor(Color.GREEN);
        }

        // rysuj linie skanów
        int y = scanConverter.getTopBoundary();
        while (y<=scanConverter.getBottomBoundary()) {
            ScanConverter.Scan scan = scanConverter.getScan(y);
            if (scan.isValid()) {
                g.drawLine(scan.left, y, scan.right, y);
            }
            y++;
        }
    }

}
