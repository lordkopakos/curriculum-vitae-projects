/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.networkTest;

import psi.network.VectorX;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.border.LineBorder;

import com.sun.corba.se.impl.orbutil.graph.Graph;
import com.sun.prism.shader.DrawCircle_Color_AlphaTest_Loader;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;
import javax.swing.SwingConstants;
import psi.neural.AdelineCell;
/**
 *
 * @author lordkopakos
 */
public class NeuralNetwork {
        
        private JFrame frame;
	private static XYGraph graphx;
	/**
	 * NeuralCell Object
	 */
	private static AdelineCell[][] cells;
        private int layers;
        private int cellsInLayers;
	private JLabel x_weight;
	private JLabel y_weight;
        
        private String filepath;
        private ArrayList lines;
        
        private VectorX m_VectorX;
	
	
	/*
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					NeuralNetwork window = new NeuralNetwork();
					window.frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
		
		
	}
	
	/**
	 * Create the application.
	 */
	public NeuralNetwork() throws IOException
	{
		init();
		loadTrainingSet("trainingSet.txt");
                addInputDataToNeuralNetwork(2);
		/**
		 * Setting random weights.
		 * Between 0 and 1.
		 */
	
		
	}
	
        public void addInputDataToNeuralNetwork(int inputData){
            
            cells=new AdelineCell[cellsInLayers][layers];
		for(int i=0; i<cellsInLayers; i++){
                    cells[cellsInLayers][0].addInput(inputData);
                    Random rand = new Random();
		
                double rx = rand.nextDouble();
		double ry = rand.nextDouble();
		cells[cellsInLayers][0].setInputWeight(0, rx);
		cells[cellsInLayers][0].setInputWeight(1, ry);
		
                x_weight.setText(Double.toString(rx));
		y_weight.setText(Double.toString(ry));
                }
                
        }
        
        public float errorSignal(float z, float y){
            float errorSignal;
            return errorSignal=z-y;
        }

	/**
	 * Initialize the contents of the frame.
	 */
	private void init()
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 599, 420);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		graphx = new XYGraph();
		graphx.setBorder(new LineBorder(new Color(0, 0, 0)));
		graphx.setBackground(Color.WHITE);
		graphx.setBounds(10, 11, 360, 360);
		frame.getContentPane().add(graphx);
		
		JButton btnAddPoint = new JButton("Add 500 Points");
		btnAddPoint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				for(int i = 1; i<=500; i++)
					addRandomPoint();
			}
		});
		btnAddPoint.setBounds(408, 55, 144, 23);
		frame.getContentPane().add(btnAddPoint);
		
		JLabel lblWagaX = new JLabel("Waga X");
		lblWagaX.setHorizontalAlignment(SwingConstants.CENTER);
		lblWagaX.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblWagaX.setBounds(408, 89, 58, 23);
		frame.getContentPane().add(lblWagaX);
		
		JLabel lblWagaY = new JLabel("Waga Y");
		lblWagaY.setHorizontalAlignment(SwingConstants.CENTER);
		lblWagaY.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblWagaY.setBounds(494, 89, 58, 23);
		frame.getContentPane().add(lblWagaY);
		
		x_weight = new JLabel("x");
		x_weight.setHorizontalAlignment(SwingConstants.CENTER);
		x_weight.setFont(new Font("Tahoma", Font.PLAIN, 16));
		x_weight.setBounds(408, 114, 58, 23);
		frame.getContentPane().add(x_weight);
		
		y_weight = new JLabel("y");
		y_weight.setHorizontalAlignment(SwingConstants.CENTER);
		y_weight.setFont(new Font("Tahoma", Font.PLAIN, 16));
		y_weight.setBounds(494, 114, 58, 23);
		frame.getContentPane().add(y_weight);
                
                filepath="learn/";
	
	}
	
        public void loadTrainingSet(String filename) throws IOException{
            
            lines = new ArrayList();
            int width = 0;
            int height = 0;
            
        // odczytanie wszystkich wierszy z pliku do listy
        BufferedReader reader = new BufferedReader(
            new FileReader(filepath+filename));
        while (true) {
            String line = reader.readLine();
            // koniec wierszy do odczytywania
            if (line == null) {
                reader.close();
                break;
            }

            // dodawanie wszystkich wierszy poza komentarzami
            if (!line.startsWith("#")) {
                lines.add(line);
            }
        }

        // analizowanie wiersza
        height = lines.size();
        for (int y=0; y<height; y++) {
            String line = (String)lines.get(y);
            Pattern comma=Pattern.compile(",");
            String[] xn=comma.split(line);
            int count=1;
            for(String item: xn){
                System.out.println("x"+count+": "+item);
                count++;
            }
            System.out.println("z=x"+xn.length);
        }
        }
        
     private void addRandomPoint()
     {
     	// Generate two numbers
     	Random r = new Random();
     	int x = r.nextInt()%180;
     	int y = r.nextInt()%180;
     	
     	// Pass them to the neural cell
     	//cell.setInputData(0, x);
     	//cell.setInputData(1, y);
     	
     	// Get the output
     	int result=0;// = (int)cell.getOutput();
     	System.out.println("OUTPUT:" + result);
     	// Draw appropriate point colour
     	if(result == 1)
     		graphx.drawPoint(x, y, Color.BLUE);
     	else
     	if(result == -1)
     		graphx.drawPoint(x, y, Color.RED);
     }
	
     class XYGraph extends JPanel
     {
     	/**
     	 * Variables which contains info about 
     	 * currently added point.
     	 */
     	Color clr;
     	int x_var;
     	int y_var;
     	
     	/**
     	 * Override to write custom point.
     	 */
     	@Override
     	protected void paintComponent(Graphics g)
     	{
     		super.paintComponent(g);
     		g.setColor(clr);
     		if(x_var != 0 && y_var != 0)
     			g.fillRect(x_var, y_var, 4, 4);
     		paintCross(g);
     	}
     	
     	/**
     	 * Yet another smart function to draw 
     	 * a cross on the component.
     	 * @param g
     	 */
     	private void paintCross(Graphics g)
     	{
     		g.setColor(Color.BLACK);
     		g.fillRect(180, 0, 1, 360);
     		g.drawLine(176, 8, 180, 0);
     		g.drawLine(184, 8, 180, 0);
     		
     		g.fillRect(0, 180, 360, 1);
     		g.drawLine(352, 176, 360, 180);
     		g.drawLine(352, 184, 360, 180);
     		g.drawString("0", 183, 193);
     	}
     	
     	/**
     	 * Writes a point at selected coordinates 
     	 * with selected colour.
     	 * @param x coordinate X
     	 * @param y coordinate Y
     	 * @param clr selected colour
     	 */
     	public void drawPoint(int x, int y, Color clr)
     	{
     		
     		x+=180;
     		y+=180;
     		y=360-y;
     		x_var = x;
     		y_var = y;
     		this.clr = clr;
     		paintImmediately(x,y,4,4);
     	}
     }
}
