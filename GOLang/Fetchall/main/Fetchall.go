/*
   Fetchall pobiera równolegle zawartosci kilku adresów URL i raportuje czasy pobierania
   oraz rozmiary odpowiedzi.
*/
package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func main() {
	start := time.Now() //Rozpoczęcie liczenia czasu
	fmt.Println("Fatchall")
	ch := make(chan string) //tworzenie kanału łańcuchów znaków
	for _, url := range os.Args[1:] {
		go fetch(url, ch) //rozpoczęcie funkcji goroutine
	}
	for range os.Args[1:] {
		fmt.Println(<-ch) //odbieranie z kanału ch
	}
	fmt.Printf("%.2fs upłyneło\n", time.Since(start).Seconds()) //zliczanie w sekundach ile czasu upłyneło od start
}

func fetch(url string, ch chan<- string) {
	start := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		ch <- fmt.Sprint(err)
		return
	}
	nbytes, err := io.Copy(ioutil.Discard, resp.Body)
	if err != nil {
		ch <- fmt.Sprintf("podczas odczytywania %s: %v", url, err)
		return
	}
	b, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		fmt.Fprintf(os.Stderr, "fetch:odczytanie %s: %v\n", url, err)
		return
	}
	secs := time.Since(start).Seconds()
	ch <- fmt.Sprintf("%.2fs %7d %s", secs, nbytes, url)
	fmt.Printf("%s", b)
}
