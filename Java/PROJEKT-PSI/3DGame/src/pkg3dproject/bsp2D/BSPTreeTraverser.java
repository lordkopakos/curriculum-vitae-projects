package pkg3dproject.bsp2D;

import pkg3dproject.math3D.*;
import pkg3dproject.game.GameObjectManager;

/**
    Klasa BSPTreeTraverer odpowiada za przegl�danie w�z��w dwuwymiarowego
    drzewa BSP albo w porz�dku poprzeczny, albo w kolejno�ci rysowania
    (od przodu do ty�u). Odwiedzone wielok�ty otrzymuj� odpowiednie sygna�y
    za po�rednictwem obiekt�w klasy BSPTreeTraverseListener.
*/
public class BSPTreeTraverser {

    private boolean traversing;
    private float x;
    private float z;
    private GameObjectManager objectManager;
    private BSPTreeTraverseListener listener;

    /**
        Tworzy nowy obiekt BSPTreeTraverser bez obiektu klasy
        BSPTreeTraverseListener.
    */
    public BSPTreeTraverser() {
        this(null);
    }


    /**
        Tworzy nowy obiekt BSPTreeTraverser ze wskazanym obiektem
        klasy BSPTreeTraverseListener.
    */
    public BSPTreeTraverser(BSPTreeTraverseListener listener) {
        setListener(listener);
    }


    /**
        Ustawia obiekt BSPTreeTraverseListener wykorzystywany podczas
        przegl�dania drzewa.
    */
    public void setListener(BSPTreeTraverseListener listener) {
        this.listener = listener;
    }


    /**
        Ustawia obiekt GameObjectManager. Je�li podczas przegl�dania
        obiekt GameObjectManager jest r�ny od null, w�wczas wywo�ywana
        jest metoda markVisible() menad�era, kt�ra okre�la widoczne
        cz�ci drzewa.
    */
    public void setGameObjectManager(
        GameObjectManager objectManager)
    {
        this.objectManager = objectManager;
    }


    /**
        Przegl�da w�z�y drzewa w kolejno�ci rysowania reprezentowanych przez
        nie wielok�t�w (od przodu do ty�u) dla okre�lonej pozycji kamery.
    */
    public void traverse(BSPTree tree, Vector3D viewLocation) {
        x = viewLocation.x;
        z = viewLocation.z;
        traversing = true;
        traverseDrawOrder(tree.getRoot());
    }


    /**
        Przegl�da w�z�y drzewa w porz�dku poprzecznym.
    */
    public void traverse(BSPTree tree) {
        traversing = true;
        traverseInOrder(tree.getRoot());
    }


    /**
        Przegl�da w�ze� w kolejno�ci rysowania (od przodu do ty�u) dla
        okre�lonej pozycji kamery.
    */
    private void traverseDrawOrder(BSPTree.Node node) {
        if (traversing && node != null) {
            if (node instanceof BSPTree.Leaf) {
                // brak podzia��w, tylko obs�uga wielok�t�w
                visitNode(node);
            }
            else if (node.partition.getSideThin(x,z) != BSPLine.BACK) {
                traverseDrawOrder(node.front);
                visitNode(node);
                traverseDrawOrder(node.back);
            }
            else {
                traverseDrawOrder(node.back);
                visitNode(node);
                traverseDrawOrder(node.front);
            }
        }
    }



    /**
        Przegl�da w�ze� w porz�dku poprzecznym.
    */
    private void traverseInOrder(BSPTree.Node node) {
        if (traversing && node != null) {
            traverseInOrder(node.front);
            visitNode(node);
            traverseInOrder(node.back);
        }
    }


    /**
        Odwiedza w�ze� drzewa BSP. Metoda visitPolygon() interfejsu
        BSPTreeTraverseListener jest wywo�ywana dla ka�dego wielok�ta
        w danym w�le.
    */
    private void visitNode(BSPTree.Node node) {
        if (!traversing || node.polygons == null) {
            return;
        }

        boolean isBack = false;
        if (node instanceof BSPTree.Leaf) {
            BSPTree.Leaf leaf = (BSPTree.Leaf)node;
            isBack = leaf.isBack;
            // oznacza ograniczenia tego li�cia jako obszar widoczny
            // w menad�erze obiekt�w wyst�puj�cych w grze.
            if (objectManager != null && leaf.bounds != null) {
                objectManager.markVisible(leaf.bounds);
            }
        }

        // odwiedza ka�dy wielok�t
        for (int i=0; traversing && i<node.polygons.size(); i++) {
            BSPPolygon poly = (BSPPolygon)node.polygons.get(i);
            traversing = listener.visitPolygon(poly, isBack);
        }
    }


}
