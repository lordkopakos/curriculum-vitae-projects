#include "Polynomial.hpp"

unsigned int Polynomial::precision = 10000;
void Polynomial::upgrade(int rank)
{
	if (rank > 0)	for (int i = 0; i < rank; i++)	poly.insert(poly.begin(),0.0);
}

Polynomial& Polynomial::operator+=(const Polynomial &P)
{
	if (poly.size() == P.poly.size())
	{
		for (unsigned int i = 0; i < poly.size(); i++)	poly[i] += P.poly[i];
	}
	else
	{
		if (poly.size() > P.poly.size())
		{
			for (unsigned int i = 0; i < P.poly.size(); i++)	poly[i] += P.poly[i];
		}
		else
		{
			//CHANGED
			//unsigned int SIZE = poly.size();
			poly.resize(P.poly.size());
			for (unsigned int i = 0; i < poly.size(); ++i)	poly[i] += P.poly[i];
			//for (unsigned int i = 0; i < SIZE; i++)	poly[i] += P.poly[i];
			//for (unsigned int i = SIZE; i < P.poly.size(); i++)	poly.push_back(P.poly[i]);
		}
	}
	return *this;
}

Polynomial & Polynomial::operator=(const Polynomial &A)
{
	poly = A.poly;
	return *this;
}

Polynomial Polynomial::operator+(const Polynomial &P)
{
	Polynomial E(*this);
	if (E.poly.size() == P.poly.size())
	{
		for (unsigned int i = 0; i < E.poly.size(); i++)E.poly[i] += P.poly[i];
	}
	else
	{
		unsigned int SIZE;
		if (E.poly.size() > P.poly.size())
		{
			SIZE = P.poly.size();
			for (unsigned int i = 0; i < SIZE; i++)	E.poly[i] += P.poly[i];
		}
		else
		{
			//CHANGED
			E.poly.resize(P.poly.size());
			for (unsigned int i = 0; i < P.poly.size(); ++i)	E.poly[i] += P.poly[i];
			//SIZE = E.poly.size();
			//for (unsigned int i = 0; i < SIZE; i++)	E.poly[i] += P.poly[i];
			//for (unsigned int i = SIZE; i < P.poly.size(); i++)	E.poly.push_back(P.poly[i]);
		}
	}
	return E;
}

Polynomial Polynomial::operator*(const Polynomial &A)
{
	Polynomial Result;
	for (unsigned int i = 0; i < A.poly.size(); i++)
	{
			Polynomial Temp(*this);
			Temp *= A.poly[i];
			Temp.upgrade(i);
			Result += Temp;
	}
	return Result;
}



Polynomial Polynomial::operator*(const double C)
{
	Polynomial Copy(*this);
	for (unsigned int i = 0; i < poly.size(); i++)	Copy.poly[i]*=C;
	return Copy;
}

Polynomial& Polynomial::operator*=(const double C)
{
	for (unsigned int i = 0; i < poly.size(); i++)
		poly[i] *= C;
	return *this;
}


void Polynomial::raiseDeg(double C)
{
	poly.push_back(C);
}

void Polynomial::setPrecision(unsigned int prec)
{
	precision = prec;
}

unsigned int Polynomial::getPrecision()
{
	return precision;
}

double Polynomial::operator()(double C)
{
	double result = poly[poly.size()-1];
	for (unsigned int i = poly.size() - 1; i > 0; i--)
	{
		result *= C;
		result += poly[i - 1];
	}
	return result;
}

Polynomial::Polynomial(double a)
{
	poly.push_back(a);
}

Polynomial::Polynomial(double a1, double a0)
{
	//CHANGED
	poly.reserve(2);
	poly.push_back(a0);
	poly.push_back(a1);
}

Polynomial::Polynomial(double coeff, int rank)
{
	if (rank == 0)	poly.push_back(coeff);
	if (rank > 0)
	{
		//CHANGED
		poly.reserve(rank+1);
		//CHANGED
		for (int i = 0; i < rank; i++)	poly.push_back(0.0);
		poly.push_back(coeff);
	}
}

Polynomial::Polynomial(unsigned int size, double *a)
{
	poly.reserve(size);
	for (unsigned int i = 0; i < size; i++)	poly.push_back(a[i]);
}

Polynomial::Polynomial(std::vector<double> &a)
{
	poly.reserve(a.size());
	for (unsigned int i = 0; i < a.size(); i++)	poly.push_back(a[i]);
}

Polynomial::Polynomial(const Polynomial &A) { poly = A.poly; }

std::ostream& operator<<(std::ostream& strumyk, const Polynomial& p)
{
	std::cout << "f(x) = ";
	for (int i = p.poly.size()-1; i >= 0; i--)
	{
		if(i>0)
		{
			if(p.poly[i]!=0.0)
			{
				if ((p.poly[i] != 1.0) && (p.poly[i]!= -1.0))
					std::cout << p.poly[i];
				if (p.poly[i] == -1.0)
					std::cout << '-';
				std::cout << "x";
				if (i > 1)
					std::cout << "^" << i;
				if (p.poly[i - 1] >= 0.0)
					std::cout << " + ";
				else
					std::cout<<' ';
			}
		}
		else
		{
			if (p.poly[i] != 0.0)
				std::cout << p.poly[i];
			if(p.poly[i] == 0.0 && p.poly.size()==1)
				std::cout << p.poly[i];
		}
	}
	std::cout << std::endl;
	return strumyk;
}

Polynomial Polynomial::getDerivative(unsigned int d)
{
	if(poly.size() <= d)
	{
		Polynomial P;
		return P;
	}
	else
	{
		std::vector<double> vect;
		for(unsigned int i = d ; i<poly.size() ; i++)
		{
			double coeff = poly[i];
			for(unsigned int j = 0 ; j<d ; j++)	coeff *= i-j;
			
			vect.push_back(coeff);
		}
		Polynomial P(vect);
		return P;
	}
}

double Polynomial::integrateRectangle(double a, double b)
{
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}

	double result = 0.0,
			dx = (b - a) / precision,
			at = a;
	for (unsigned int i = 0; i < precision; i++) {
		result += operator()(at);// *dx;
		at += dx;
		//CHANGED
	}

	return result*dx;
}

double Polynomial::integrateTrapeze(double a, double b)
{
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}

	double result = 0.0,
		dx = (b - a) / precision,
		at = a;
	for (unsigned int i = 0; i < precision; i++) {
		result += (operator()(at) + operator()(at + dx));// *dx*0.5;
		at += dx;
	}
	//CHANGED
	return result*dx*0.5;
}

double Polynomial::integrateSimpson(double a, double b, unsigned int n)
{
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}
	if (n == 0)	n = 1;

	double sum = 0.0,
		step = (b - a) / static_cast<double>(n),
		from = a,
		to = a + step;

	for (unsigned int i = 0; i < n; i++) {
		sum += (to - from) / 6.0 * (operator()(from) + 4.0*operator()((from + to) / 2.0) + operator()(to));
		from = to;
		to += step;
	}
	return sum;
}

double Polynomial::integrateGaussQuadrature2(double a, double b)
{
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}

	double x[3] = { -0.774597, 0.0, 0.774597 },
	       A[3] = { 5.0 / 9.0, 8.0 / 9.0, 5.0 / 9.0 };
	double avrg = (a + b)*0.5,
		   length = (b - a)*0.5,
		   sum = 0.0;

	for (int k = 0; k < 3; ++k) {
		double t = avrg + length*x[k];
		sum += A[k] * operator()(t);
	}
	
	return sum*length;
}

double Polynomial::integrateGaussQuadrature4(double a, double b)
{
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}

	double x[5] = { -0.906180, -0.538469, 0.0, 0.538469 , 0.906180 },
		   A[5] = { 0.236927, 0.478629, 0.568889, 0.478629, 0.236927 };
	double avrg = (a + b)*0.5,
		   length = (b - a)*0.5,
	       sum = 0.0;

	for (int k = 0; k < 5; ++k) {
		double t = avrg + length*x[k];
		sum += A[k] * operator()(t);
	}

	return sum*length;
}

double Polynomial::solveEulersMethod(double y , double a, double b, unsigned int steps)
{
	/* Equation: a0 + a1*x + a2*x^2 + ... + an*x^n = y, basically y is right side of equation, at the beginning we simply move the y to left side by substraction */
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}
	Polynomial f(*this);
	f.poly[0] -= y;
	double xn_2 = a,
		   xn_1 = b,
		   eps = 1e-6;
	// TODO Set ending condition, otherwise may end up dividing by 0 #-nan(ind)
	for (unsigned int i = 0; i < steps; ++i) {
		double xn = xn_1 - (f(xn_1)*(xn_1 - xn_2)) / (f(xn_1) - f(xn_2));
		if (f(xn) < eps) {
			std::cout << "x" << i << " = " << xn << std::endl;
			std::cout << "Zakonczono w " << i << " iteracji na " << steps << ".\n";
			return xn;
		}
		std::cout << "x" << i  << " = " << xn << std::endl;
		xn_2 = xn_1;
		xn_1 = xn;
	}
	return xn_1;
}

double Polynomial::solveNewtonsMethod(double y, double a, double b, unsigned int steps)
{
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}
	Polynomial f(*this);
	f.poly[0] -= y;
	if (f(a)*f(b) >= 0)	throw 1.0;
	Polynomial f_prim = f.getDerivative();

	double xn_1 = a,
	   	   eps = 1e-6;
	for (unsigned int i = 0; i < steps; ++i) {
		double xn = xn_1 - f(xn_1) / f_prim(xn_1);
		if (f(xn) < eps) {
			std::cout << "x" << i << " = " << xn << std::endl;
			std::cout << "Zakonczono w " << i << " iteracji na " << steps << ".\n";
			return xn;
		}
		std::cout << "x" << i << " = " << xn << std::endl;
		xn_1 = xn;
	}

	return xn_1;
}