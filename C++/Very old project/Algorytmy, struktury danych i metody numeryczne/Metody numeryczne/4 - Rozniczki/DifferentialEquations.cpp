#include <iostream>
#include <cmath>

using namespace std;

//#define VERBOSE


//////// tu trzeba funkcje zmienic
double f(double x, double y)
{
//	return y + x*x;
	return y - x * x;
}


double** euler(double a, double b, double h, double y0)
{
	double x = a;
	double y = y0;

	int size = 0;
	for (double i = a; i < b; i += h)
		size++;

	cout << size << endl;

	double** ret = new double*[2];
	double* Xs = new double[size];
	double* Ys = new double[size];
	ret[0] = Xs;
	ret[1] = Ys;

	Xs[0] = x;
	Ys[0] = y;
	for (int i = 1; i < size; i++)
	{
		x += h;
		y += h * f(Xs[i - 1], Ys[i - 1]);
		Xs[i] = x;
		Ys[i] = y;
	}

	return ret;
}

double** eulerBetter(double a, double b, double h, double y0)
{
	double x = a;
	double y = y0;

	int size = 0;
	for (double i = a; i < b; i += h)
		size++;

	cout << size << endl;

	double** ret = new double*[2];
	double* Xs = new double[size];
	double* Ys = new double[size];
	ret[0] = Xs;
	ret[1] = Ys;

	Xs[0] = x;
	Ys[0] = y;
	for (int i = 1; i < size; i++)
	{
		x += h;
		y += h / 2 * (f(Xs[i - 1], Ys[i - 1]) + f(Xs[i - 1] + h, Ys[i - 1] + h * f(Xs[i - 1], Ys[i - 1])));
		Xs[i] = x;
		Ys[i] = y;
	}

	return ret;
}

double** eulerMod(double a, double b, double h, double y0)
{
	double x = a;
	double y = y0;

	int size = 0;
	for (double i = a; i < b; i += h)
		size++;

	cout << size << endl;

	double** ret = new double*[2];
	double* Xs = new double[size];
	double* Ys = new double[size];
	ret[0] = Xs;
	ret[1] = Ys;

	Xs[0] = x;
	Ys[0] = y;
	for (int i = 1; i < size; i++)
	{
		x += h;
		y += h *
			f(
				Xs[i - 1] + h * 0.5,
				Ys[i - 1] + 0.5 * h * f(Xs[i - 1], Ys[i - 1])
			);
		Xs[i] = x;
		Ys[i] = y;
	}

	return ret;
}

double** rk2(double a, double b, double h, double y0)
{
	double x = a;
	double y = y0;

	double k1, k2, fi;

	int size = 0;
	for (double i = a; i < b; i += h)
		size++;

	cout << size << endl;

	double** ret = new double*[2];
	double* Xs = new double[size];
	double* Ys = new double[size];
	ret[0] = Xs;
	ret[1] = Ys;

	Xs[0] = x;
	Ys[0] = y;
	for (int i = 1; i < size; i++)
	{
		x += h;

		k1 = f(Xs[i - 1], Ys[i - 1]);
		k2 = f(Xs[i - 1] + h, Ys[i - 1] + h * k1);
		fi = 0.5 * (k1 + k2);

		y += h * fi;
		Xs[i] = x;
		Ys[i] = y;

#ifdef VERBOSE
		cout << "xi=" << x << "  ";
		cout << "k1=" << k1 << "  ";
		cout << "k2=" << k2 << "  ";
		cout << "fi=" << fi << "  ";
		cout << endl;
		cout << endl;
#endif
	}

	return ret;
}

double** rk4(double a, double b, double h, double y0)
{
	double x = a;
	double y = y0;

	double k1, k2, k3, k4, fi;

	int size = 0;
	for (double i = a; i < b; i += h)
		size++;

	cout << size << endl;

	double** ret = new double*[2];
	double* Xs = new double[size];
	double* Ys = new double[size];
	ret[0] = Xs;
	ret[1] = Ys;

	Xs[0] = x;
	Ys[0] = y;
	for (int i = 1; i < size; i++)
	{
		x += h;

		k1 = f(Xs[i - 1], Ys[i - 1]);
		k2 = f(Xs[i - 1] + h / 2, Ys[i - 1] + h / 2 * k1);
		k3 = f(Xs[i - 1] + h / 2, Ys[i - 1] + h / 2 * k2);
		k4 = f(Xs[i - 1] + h, Ys[i - 1] + h * k3);
		fi = 1.0 / 6 * (k1 + k2 + k2 + k3 + k3 + k4);

		y += h * fi;
		Xs[i] = x;
		Ys[i] = y;

#ifdef VERBOSE
		cout << "xi=" << x << "  ";
		cout << "k1=" << k1 << "  ";
		cout << "k2=" << k2 << "  ";
		cout << "k3=" << k3 << "  ";
		cout << "k4=" << k4 << "  ";
		cout << "fi=" << fi << "  ";
		cout << endl;
		cout << endl;
#endif
	}

	return ret;
}

void wypisz(double** ret, double size)
{
	for (int i = 0; i < size; ++i)
	{
		cout << "x" << i << "=" << ret[0][i] << " ";
	}
	cout << endl;
	for (int i = 0; i < size; ++i)
	{
		cout << "y" << i << "=" << ret[1][i] << " ";
	}
	cout << endl << endl;
}

int main()
{
	double** wynik;
	double a, b, h, y0;
	int steps;
	
	///////////////// CONFIG

//	a = 0;
//	b = 1;
//	h = 0.1;
//	y0 = 0.1;

	a = 0;
	b = 0.1;
	h = 0.01;
	y0 = 1;

	steps = static_cast<int>((b - a) / h + 1);
	cout << steps << endl;

	///////////////// END CONFIG

	cout << "Metoda Eulera" << endl;
	wynik = euler(a, b, h, y0);
	wypisz(wynik, steps);

	cout << "Ulepszona metoda Eulera / Metoda Heuna" << endl;
	wynik = eulerBetter(a, b, h, y0);
	wypisz(wynik, steps);

	cout << "Zmodyfikowana metoda Eulera" << endl;
	wynik = eulerMod(a, b, h, y0);
	wypisz(wynik, steps);

	cout << "Metoda Rungego-Kutty 2 rzedu" << endl;
	wynik = rk2(a, b, h, y0);
	wypisz(wynik, steps);

	cout << "Metoda Rungego-Kutty 4 rzedu" << endl;
	wynik = rk4(a, b, h, y0);
	wypisz(wynik, steps);

//	cout << "W punkcie x=0,1" << "\t" << "y=" << endl;
//	double ret = rk4AtPoint(0, 1, 0.01, 0.1);
//	cout << "Wynik: " << ret << endl;

	system("pause");
	return 0;
}


//double rk4AtPoint(double x0, double y0, double h, double x)
//{
//	double** wynik;
//
//	int size = 0;
//	for (double i = x0; i < x; i += h)
//		size++;
//
//	wynik = rk4(x0, x, h, y0);
//
//	return wynik[1][size - 1];
//}