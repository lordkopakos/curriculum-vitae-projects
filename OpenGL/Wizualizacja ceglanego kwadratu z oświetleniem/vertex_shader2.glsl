#version 330
layout(location = 0) in vec3 vp;
layout(location = 0) in vec3 colour;
out vec3 vertex_colour;
void main()
{
   vertex_colour=colour;
   gl_Position = vec4(vp.x+0.25 ,vp.y-0.25 ,vp.z , 1.0);
}
