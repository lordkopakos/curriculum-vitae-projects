#version 330
layout(location=0) in vec3 position;

uniform float utrans;
 
void main() 
{
   
   gl_Position = vec4(position.x+utrans,position.y+utrans,position.z+utrans, 1.0);
}