package pkg3dproject.tests;
 
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.ArrayList;

import pkg3dproject.input.*;
import pkg3dproject.math3D.*;
import pkg3dproject.graphics3D.*;
import pkg3dproject.core.GameCore3D;

public class TextureMapTest1 extends GameCore3D {

    public static void main(String[] args) {
        new TextureMapTest1().run();
    }

    public void createPolygons() {
        Polygon3D poly;

        // Na razie tylko jedna ściana.
        poly = new Polygon3D(
            new Vector3D(-128, 256, -1000),
            new Vector3D(-128, 0, -1000),
            new Vector3D(128, 0, -1000),
            new Vector3D(128, 256, -1000));
        polygons.add(poly);
    }

    @Override
    public void createPolygonRenderer() {
        viewWindow = new ViewWindow(0, 0,
            screen.getWidth(), screen.getHeight(),
            (float)Math.toRadians(75));

        Transform3D camera = new Transform3D(0,100,0);
        polygonRenderer = new SimpleTexturedPolygonRenderer(
            camera, viewWindow, "images/test_pattern.png");
    }

}
