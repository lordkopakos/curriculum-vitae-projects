﻿// p r z y k l a d automatu dwuwymiarowego na s i e c i kwadra towe j
// Regule stanow i stan sasiedztwa 9 kwadratow ( t u t a j gw i a z d k i )
// s i e c i kwadra towe j LxL
//
// ∗ ∗ ∗
// ∗ ∗ ∗
// ∗ ∗ ∗
// S tan s i w e zl a srodkowego i w nastepnym kroku czasowym z a l e z y od s tanow
// s k d z i e w i e c i u wezlow
//w kroku poprzedn im k = 1 . . 9 :
//
// s i =suma k =1 . .9 s k
// Regula :
// if ( suma<3) s i=0
// if ( suma>=3 && suma<5) s i=1
// if ( suma>=5) s i=0
#include<iostream>
#include<fstream>
#include<cassert>
#include<vector>
#include"Enviroment.h"

using namespace std;

int main() {
	cout << "Wprowadz wymiar liniowy sieci kwadratowej L = ";
	int L;
	cin >> L;
	int L2 = L*L;
	cout << "Podaj liczbe krokow czasowych ewolucji automatu komorkowego N = ";
	int Nkrokow;
	cin >> Nkrokow;
	
	// d e k l a r a c j a dynam iczna t a b l i c y SIATKA [ ]
	
	auto grid = vector<int>(L2, 0); //w kroku t
	//w tedy n−t y elemen t t a b l i c y o zn ac z a w i e r s z i o r a z kolumne j
	// i=n/L o r a z j=n % L
	auto second_grid = vector<int>(L2, 0); // s i a t k a d u b e l w kroku t+1

	// Deklaru jemy warunk i p oc z a tk owe z a p e l n i e n i a t a b l i c y SIATKA
	//w p o s t a c i z a p el n i o n y c h 9 kwadratow
	// p o z o s t a l e elemen ty s i a t k i sa n i e z a p e l n i o n e

	Environment enviroment(L);
	enviroment.initial_conditions(grid, L);

		for (int t = 0; t<Nkrokow; t++) {
			enviroment.reset_the_grid(second_grid, L);
			//wyzerowan ie elemen tow s i a t k i d u bl a
			// s t a ny z a j e t o s c i elemen tow s i a t k i
			// d o t y c z a kroku w t+1
			for (int n = 0; n<L2; n++) {
			second_grid[n] =enviroment.specify_the_state_of_the_environment(grid, n); //argumentem
			// j e s t SIATKA d l a kroku t
		}
			enviroment.replace_the_grid(grid, second_grid, L2); //podmiana s i a t e k
	}
	enviroment.save_to_PPM_file(L, L, grid);
	system("pause");
		return 0;
}