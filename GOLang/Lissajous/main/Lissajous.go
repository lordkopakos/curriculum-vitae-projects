package main

import (
	"image"
	"image/color"
	"image/gif"
	"io"
	"math"
	"math/rand"
	"os"
)

var palette = []color.Color{color.White, color.Black} //literał złożony- wycinek

const (
	whiteIndex = 0 //piewrszy kolor w zmiennej palette
	blackIndex = 1 //drugi kolor w zmiennej palette
)

func main() {
	lissajous(os.Stdout)
}

func lissajous(out io.Writer) {
	const (
		cycles  = 1     //liczba pełnych obiegów oscylatora x
		res     = 0.001 //rozdzielczość kątowa
		size    = 100   //rozmiar płótna obrazu [-size...+size]
		nframes = 64    //liczba klatek animacji
		delay   = 8     //opóźnienie między klatkami w jednostkach 10 ms
	)
	freq := rand.Float64() * 3.0 //częstotliwość względna oscylatora
	anim := gif.GIF{LoopCount: nframes} //literał złożony- Struktura z jedna zmienną (Pole LoopCount jest ustawione na nframes
	phase := 0.0 //przesunięcie fazowe
	for i := 0; i < nframes; i++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < cycles*2*math.Pi; t += res { //wewnetrzna petla maluje niekture piksele na czarno
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			img.SetColorIndex(size+int(x*size+0.5), size+int(y*size+0.5), blackIndex)
		}
		phase += 0.1
		anim.Delay = append(anim.Delay, delay)  //dodanie do listy klatek animacji 
		anim.Image = append(anim.Image, img)
	}
	gif.EncodeAll(out, &anim) //zapisanie do formatu gif

}
