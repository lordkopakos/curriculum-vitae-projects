package pkg3dproject.bsp2D;

/**
    BSPTreeTraverseListener to interfejs umo�liwiaj�cy obiektom
    klasy BSPTreeTraverser sygnalizowanie zdarze� przegl�dania
    wielok�t�w.
*/
public interface BSPTreeTraverseListener {

    /**
        Odwiedza wielok�t BSP. Wywo�ywana przez BSPTreeTraverser.
        Je�li ta metoda zwr�ci warto�� true, klasa BSPTreeTraverser
        zako�czy bie��cy proces przegl�dania wielok�t�w. W przeciwnym
        przypadku, klasa BSPTreeTraverser b�dzie kontynuowa�a ten
        proces, je�li oczywi�cie w drzewo b�dzie jeszcze zawiera�o
        nieprzetworzone wielok�ty.
    */
    public boolean visitPolygon(BSPPolygon poly,
        boolean isBackLeaf);

}
