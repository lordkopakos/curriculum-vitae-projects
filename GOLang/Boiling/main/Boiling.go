//Boiling.go wyświetla temperaturę wrzenia wody
package main

import (
	"fmt"
)

var freezingF, boilingF = 32.0, 212.0

func main() {
	fmt.Printf("temperatura wrzenia wody = %g'F lub %g'C\n", boilingF, fToC(boilingF))
	fmt.Printf("temperatura zamarzania wody = %g'F lub %g'C\n", freezingF, fToC(freezingF))
}

func fToC(f float64) float64 {
	return (f - 32) * 5 / 9
}
