package pkg3dproject.shooter3D;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.LinkedList;

import pkg3dproject.bsp2D.*;
import pkg3dproject.input.*;
import pkg3dproject.math3D.*;
import pkg3dproject.game.*;
import pkg3dproject.graphics3D.*;
import pkg3dproject.graphics3D.texture.*;
import pkg3dproject.core.GameCore3D;

public abstract class ShooterCore extends GameCore3D {

    private static final float PLAYER_SPEED = .5f;
    private static final float PLAYER_TURN_SPEED = 0.04f;
    private static final float CAMERA_HEIGHT = 100;
    private static final float BULLET_HEIGHT = 75;

    protected GameAction fire = new GameAction("fire",
        GameAction.DETECT_INITAL_PRESS_ONLY);

    protected GameObjectManager gameObjectManager;
    protected PolygonGroup blastModel;
    protected DisplayMode[] modes;

    public ShooterCore(String[] args) {
        modes = LOW_RES_MODES;
        for (int i=0; i<args.length; i++) {
            if (args[i].equals("-lowres")) {
                modes = VERY_LOW_RES_MODES;
                fontSize = 12;
            }
        }
    }

    public void init() {

        init(modes);

        inputManager.mapToKey(fire, KeyEvent.VK_SPACE);
        inputManager.mapToMouse(fire, InputManager.MOUSE_BUTTON_1);

        // ustaw lokalne �wiat�a dla tego modelu
        float ambientLightIntensity = .8f;
        List lights = new LinkedList();
        lights.add(new PointLight3D(-100,100,100, .5f, -1));
        lights.add(new PointLight3D(100,100,0, .5f, -1));

        // wczytaj model obiektu
        ObjectLoader loader = new ObjectLoader();
        loader.setLights(lights, ambientLightIntensity);
        try {
            blastModel = loader.loadObject("../images/blast.obj");
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    public void createPolygonRenderer() {
        // w��cz tryb pe�noekranowy
        viewWindow = new ViewWindow(0, 0,
            screen.getWidth(), screen.getHeight(),
            (float)Math.toRadians(75));

        Transform3D camera = new Transform3D();
        polygonRenderer = new BSPRenderer(camera, viewWindow);
    }


    public void updateWorld(long elapsedTime) {

        float angleVelocity;

        // ustaw elapsedTime
        elapsedTime = Math.min(elapsedTime, 100);

        GameObject player = gameObjectManager.getPlayer();
        MovingTransform3D playerTransform = player.getTransform();
        Vector3D velocity = playerTransform.getVelocity();

        //playerTransform.stop();
        velocity.x = 0;
        velocity.z = 0;
        float x = -playerTransform.getSinAngleY();
        float z = -playerTransform.getCosAngleY();
        if (goForward.isPressed()) {
            velocity.add(x * PLAYER_SPEED, 0, z * PLAYER_SPEED);
        }
        if (goBackward.isPressed()) {
            velocity.add(-x * PLAYER_SPEED, 0, -z * PLAYER_SPEED);
        }
        if (goLeft.isPressed()) {
            velocity.add(z * PLAYER_SPEED, 0, -x * PLAYER_SPEED);
        }
        if (goRight.isPressed()) {
            velocity.add(-z * PLAYER_SPEED, 0, x * PLAYER_SPEED);
        }
        if (fire.isPressed()) {
            float cosX = playerTransform.getCosAngleX();
            float sinX = playerTransform.getSinAngleX();
            Blast blast = new Blast(
                (PolygonGroup)blastModel.clone(),
                new Vector3D(cosX*x, sinX, cosX*z));
            blast.getLocation().setTo(
                player.getX(),
                player.getY() + BULLET_HEIGHT,
                player.getZ());
            gameObjectManager.add(blast);
        }


        playerTransform.setVelocity(velocity);

        // patrz w g�r� i w d� (obr�t wok� osi x)
        angleVelocity = Math.min(tiltUp.getAmount(), 200);
        angleVelocity += Math.max(-tiltDown.getAmount(), -200);
        playerTransform.setAngleVelocityX(angleVelocity *
             PLAYER_TURN_SPEED / 200);

        // obr�� si� (obr�t wok� osi y)
        angleVelocity = Math.min(turnLeft.getAmount(), 200);
        angleVelocity += Math.max(-turnRight.getAmount(), -200);
        playerTransform.setAngleVelocityY(angleVelocity *
             PLAYER_TURN_SPEED / 200);

        // aktualizuj obiekty
        gameObjectManager.update(elapsedTime);

        // ogranicz mo�liwo�ci patrzenia w g�r� i w d�
        float angleX = playerTransform.getAngleX();
        float limit = (float)Math.PI / 2;
        if (angleX < -limit) {
            playerTransform.setAngleX(-limit);
        }
        else if (angleX > limit) {
            playerTransform.setAngleX(limit);
        }

        // ustaw pozycj� kamery 100 jednostek nad graczem
        Transform3D camera = polygonRenderer.getCamera();
        camera.setTo(playerTransform);
        camera.getLocation().add(0,CAMERA_HEIGHT,0);

    }



}