/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.neural;

/**
 *
 * @author PC
 */
public interface ActivationFunctions {
    public double bipolarSigmoidFunction(double membranePotential);
    public double unipolarSigmoidFunction(double membranePotential);
    public double bipolarThresholdFunction(double membranePotential);
    public double unipolarThresholdFunction(double membranePotential);
}
