#pragma once
#include <vector>
#include <iostream>


class Punkt2D
{
public:
	double x, y;
	Punkt2D(double X, double Y) : x(X), y(Y) {}
	bool operator<(const Punkt2D &B) { return (x < B.x); }
};

class Polynomial
{
	void upgrade(int);							//Podnoszenie stopnia wielomianu
	static unsigned int precision;
public:
	static void setPrecision(unsigned int);
	static unsigned int getPrecision();
	std::vector<double> poly;
	void raiseDeg(double);
	double operator()(double);					// Wartosc funkcji w punkcie f(x)

	// INTEGRATION
	double integrateRectangle(double, double);
	double integrateTrapeze(double, double);
	double integrateSimpson(double, double, unsigned int = 1);
	double integrateGaussQuadrature2(double, double);
	double integrateGaussQuadrature4(double, double);

	// SOLVING NON-LINEAR EQUATIONS
	double solveEulersMethod(double, double, double, unsigned int = 10); //Metoda siecznych
	double solveNewtonsMethod(double, double, double, unsigned int = 10);//Metoda stycznych

	// KONSTRUKTORY
	Polynomial(double = 0.0);			//Domyslny konstruktor - laduje wielomian 0-stopnia ze wspolczynnikiem 0.0
	Polynomial(double, double);			//Specjalny konstruktor do dwumianu (a1*x - a0)
	Polynomial(const Polynomial&);		//Konstruktor kopiujący
	Polynomial(double, int);
	Polynomial(unsigned int, double*);	//Konstruktor do ladowania wspolczynników z c_array
	Polynomial(std::vector<double>&);	//konstruktor do ladowania wspolczynników z std::vector

	// OPERATORY
	friend std::ostream& operator<<(std::ostream&, const Polynomial&);	//Wypisanie wielomianu w kolejności a0, a1*x, a2*x^2,...
	Polynomial& operator+=(const Polynomial&);
	Polynomial& operator=(const Polynomial&);
	Polynomial operator+(const Polynomial&);
	Polynomial operator*(const Polynomial&);
	Polynomial operator*(const double);
	Polynomial& operator*=(const double);
	Polynomial getDerivative(unsigned int = 1);	// Zwraca pochodna n-tego stopnia
};

/* fix 1 24.05.2016 */
/* fix 2 25.05.2016 
	- nonlinear equations - to adjustment
	- Gauss Quadrature integration - works as intended*/
/* fix 3 26.05.2016
	- precision for Trapeze and Rectangle integration is now static member, therefore getter/setter is so. 
	  also the default precision has been changed to 10k iterations, 1M is overkill */
/* fix 4 28.05.2016
	- added exception throwing for solving non-linear equations in case f(a)f(b)>=0*/
