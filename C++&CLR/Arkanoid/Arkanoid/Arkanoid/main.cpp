#include"MenuMain.h"
#include"Game.h"
#include"Author.h"

using namespace System;
using namespace System::Windows::Forms;
using namespace Arkanoid;

void closeWindow(Object^ sender, FormClosedEventArgs^ e)
{
	if (Application::OpenForms->Count == 0)
		Application::Exit();
	else {
		Application::OpenForms[0]->FormClosed += gcnew FormClosedEventHandler(closeWindow);
	}

}

[STAThreadAttribute]

int main(array<String ^> ^args)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	MenuMain^ menuMain = gcnew MenuMain();
	menuMain->FormClosed += gcnew FormClosedEventHandler(closeWindow);
	menuMain->Show();
	Application::Run();
	return 0;
}