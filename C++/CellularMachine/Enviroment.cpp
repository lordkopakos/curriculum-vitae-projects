﻿#include "Enviroment.h"


int Environment::specify_the_state_of_the_environment(vector<int>& neighbors, int number)
{
	int ii, jj;
	int sum = 0;
	int n, i, j;
	
	i = number / L; j = number%L; // p r z y p i s a n i e numerowi nr w s p ol r z e d ny c h ( i , j )
	// s i a t k i kwadra towe j

	// gorny w i e r s z o t o c z e n i a 9 kwadratow
	ii = periodic_conditions(i - 1);
	jj = periodic_conditions(j - 1);
	n = ii *L + jj; sum += neighbors[n];
	n = ii *L + j; sum += neighbors[n];
	jj = periodic_conditions(j + 1);
	n = ii *L + jj; sum += neighbors[n];
	// srodkowy w i e r s z o t o c z e n i a 9 kwadratow
	jj = periodic_conditions(j - 1);
	n = i *L + jj; sum += neighbors[n];
	n = i *L + j; sum += neighbors[n];
	jj = periodic_conditions(j + 1);
	n = i *L + jj; sum += neighbors[n];
	// d olny w i e r s z o t o c z e n i a 9 kwadratow
	ii = periodic_conditions(i + 1);
	jj = periodic_conditions(j - 1);
	n = ii *L + jj; sum += neighbors[n];
	n = ii *L + j; sum += neighbors[n];
	jj = periodic_conditions(j + 1);
	n = ii *L + jj; sum += neighbors[n];

	if(sum<3) state = 0; // za malo komorek z a j e t y c h
	else{
		if(sum >= 3 && sum<5) state = 1; //warunk i op tymalne d l a komorek
	else state= 0; // p r z e p e l n i e n i e
	}
		return state;
}

int Environment::periodic_conditions(int i)
{
	int k = i; // argumentu i b y l a w [ 0 ,L−1]
	if(i <0) k = L-1;
	else{
		if(i >= L) k = 0;
	}
	return k;
}


void Environment::initial_conditions(vector<int>& neighbors, int L)
{
	int n;
	for (int i = 0; i < L; i++) {
		for (int j = 0; j < L; j++) {
			n = i *L + j;
			neighbors[n] = 0;		}	}		n = (L / 2)*L + L / 2; neighbors[n] = 1; neighbors[n - 1] = 1; neighbors[n + 1] = 1;
		n = (L / 2 + 1)*L + L / 2; neighbors[n] = 1; neighbors[n - 1] = 1; neighbors[n + 1] = 1;
		n = (L / 2 - 1)*L + L / 2; neighbors[n] = 1; neighbors[n - 1] = 1; neighbors[n + 1] = 1;
}

void Environment::reset_the_grid(vector<int>& neighbors, int L)
{
	for (int i = 0; i<L; i++) {
		for (int j = 0; j<L; j++) {
			neighbors[i *L + j] = 0;
		}
	}
}

void Environment::replace_the_grid(vector<int>& grid_A, vector<int>& grid_B, int L2)
{
	for (int n = 0; n<L2; n++) {
		grid_A[n] = grid_B[n];
	}
}

void Environment::save_to_PPM_file(int N, int M, vector<int>& neighbors)
{
	fstream fout; // d e k l a r a c j a
	fout.open("serweta.ppm", ios::out); // i z a p i s do p l i k u s e rw e t a . ppm

		// Na jp ierw p iszemy symbol P3 k t o r y o zn ac z ac b e d z i e ze k o l o r y
		// beda kodowane w ASCII

	fout << "P3" << endl;
		//potem p iszemy dw ie l i c z b y c a l k o w i t e N M r e p r e z e n t u j a c e
		// o d p ow ie dn i o l i c z b e kolumn i l i c z b e w i e r s z y o b r a z k a
		//
		fout << M << " " << N << endl;
		//W k o l e j n e j l i n i i p ol e c e n p iszemy maksymalna l i c z b e k ol o r ow 255
		
		fout << 255 << endl;
		
		// N a s te pn ie p iszemy t r o j k i RGB l i c z b c a l k o w i t y c h
		// z p r z e d z i a l u [ 0 , 2 5 5 ] o l i c z b i e t r o j e k rowne j NxM

		int r, g, b;
	int n;

	for (int i = 0; i < M; i++) {
		for (int j = 0; j < N; j++) {

			n = i *M + j;

			if (neighbors[n] == 1)fout << 255 << " " << 0 << " " << 0 << endl;
			else fout << 255 << " " << 255 << " " << 255 << endl;
		}
	}
	fout.close();
}

