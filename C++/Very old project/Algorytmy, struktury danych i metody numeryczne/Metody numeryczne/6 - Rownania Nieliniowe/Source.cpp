#include <iostream>
#include <vector>
#include <math.h>
//#include "Polynomial.hpp"
using namespace std;
double f(double);
double f_prim(double);
double integrateRectangle(double, double, unsigned int);
double integrateTrapeze(double, double, unsigned int);
double integrateSimpson(double, double, unsigned int);
double integrateGaussQuadrature2(double, double);
double integrateGaussQuadrature4(double, double);
double solveEulersMethod(double a, double b, unsigned int steps, double eps);
double solveNewtonsMethod(double a, double b, unsigned int steps, double eps);

int main()
{

	system("pause");
	return 0;
}

double integrateRectangle(double a, double b, unsigned int precision){
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}

	double result = 0.0,
		dx = (b - a) / precision,
		at = a;
	for (unsigned int i = 0; i < precision; i++) {
		result += f(at);
		at += dx;
	}

	return result*dx;
}

double integrateTrapeze(double a, double b, unsigned int precision){
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}

	double result = 0.0,
		dx = (b - a) / precision,
		at = a;
	for (unsigned int i = 0; i < precision; i++) {
		result += (f(at) + f(at + dx));
		at += dx;
	}
	return result*dx*0.5;
}

double integrateSimpson(double a, double b, unsigned int n)
{
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}
	if (n == 0)	n = 1;

	double sum = 0.0,
		step = (b - a) / static_cast<double>(n),
		from = a,
		to = a + step;

	for (unsigned int i = 0; i < n; i++) {
		sum += (to - from) / 6.0 * (f(from) + 4.0*f((from + to) / 2.0) + f(to));
		from = to;
		to += step;
	}
	return sum;
}

double integrateGaussQuadrature2(double a, double b)
{
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}

	double x[3] = { -0.774597, 0.0, 0.774597 },
		A[3] = { 5.0 / 9.0, 8.0 / 9.0, 5.0 / 9.0 };
	double avrg = (a + b)*0.5,
		length = (b - a)*0.5,
		sum = 0.0;

	for (int k = 0; k < 3; ++k) {
		double t = avrg + length*x[k];
		sum += A[k] * f(t);
	}

	return sum*length;
}

double integrateGaussQuadrature4(double a, double b)
{
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}

	double x[5] = { -0.906180, -0.538469, 0.0, 0.538469 , 0.906180 },
		A[5] = { 0.236927, 0.478629, 0.568889, 0.478629, 0.236927 };
	double avrg = (a + b)*0.5,
		length = (b - a)*0.5,
		sum = 0.0;

	for (int k = 0; k < 5; ++k) {
		double t = avrg + length*x[k];
		sum += A[k] * f(t);
	}

	return sum*length;
}

double solveEulersMethod(double a, double b, unsigned int steps, double eps)
{
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}

	double xn_2 = a,
		xn_1 = b;
	for (unsigned int i = 0; i < steps; ++i) {
		double xn = xn_1 - (f(xn_1)*(xn_1 - xn_2)) / (f(xn_1) - f(xn_2));
		if (fabs(f(xn)) < eps) {
			std::cout << "x" << i << " = " << xn << " y = " << f(xn) << std::endl;
			std::cout << "Zakonczono w " << i << " iteracji na " << steps << ".\n";
			return xn;
		}
		std::cout << "x" << i << " = " << xn << " y = " << f(xn) << std::endl;
		xn_2 = xn_1;
		xn_1 = xn;
	}
	return xn_1;
}

double solveNewtonsMethod(double a, double b, unsigned int steps, double eps)
{
	if (b < a) {
		double temp = a;
		a = b;
		b = temp;
	}

	if (f(a)*f(b) >= 0)	throw 1.0;

	double xn_1 = a;

	for (unsigned int i = 0; i < steps; ++i) {
		double xn = xn_1 - f(xn_1) / f_prim(xn_1);
		if (fabs(f(xn)) < eps) {
			std::cout << "x" << i << " = " << xn << " y = " << f(xn) << std::endl;
			std::cout << "Zakonczono w " << i << " iteracji na " << steps << ".\n";
			return xn;
		}
		std::cout << "x" << i << " = " << xn << " y = " << f(xn) << std::endl;
		xn_1 = xn;
	}

	return xn_1;
}