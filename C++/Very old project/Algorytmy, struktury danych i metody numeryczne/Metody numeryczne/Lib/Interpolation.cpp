﻿#include "Interpolation.h"

#include <iostream>

using namespace std;

Interpolation::Interpolation(vector<Point>& points, vector<bool> flags)
{
	assert(points.size() == flags.size());

	all_points_ = points;
	selected_points_ = vector<Point>();

	for (unsigned int i = 0u; i < points.size(); ++i)
	{
		if (flags[i])
		{
			selected_points_.push_back(points[i]);
		}
	}

	degree = selected_points_.size() - 1;

	b = new double[selected_points_.size()];
	p = new double[selected_points_.size()];

	for (int i = 0; i <= degree; ++i)
	{
		b[i] = 0;
		p[i] = 0;
	}

	// CAŁE B
	for (int k = 0; k <= degree; ++k)
	{
		// SKŁADNIKI SUMY W B
		for (int i = 0; i <= k; ++i)
		{
			double temp = selected_points_[i].y;

			//MIANOWNIK SKŁADNIKA SUMY W B
			for (int j = 0; j <= k; ++j)
			{
				if (i == j) continue;

				temp /= (selected_points_[i].x - selected_points_[j].x);
			}

			b[k] += temp;
		}

	}
}

double Interpolation::apply(double x)
{
	double ret = 0;
	for (int i = 0; i <= degree; ++i)
	{
		double part = b[i];
		for (int j = 0; j < i; ++j)
		{
			part *= x - selected_points_[j].x;
		}
		ret += part;
	}
	return ret;
}

void Interpolation::print_x_prompted()
{
	double x;
	cout << "Podaj x: ";
	cin >> x;

	cout << "f(" << x << ") = " << apply(x) << endl;
}

void Interpolation::print_x(double x)
{
	cout << "f(" << x << ") = " << apply(x) << endl;
}

void Interpolation::print_b_values() const
{
	for (int i = 0; i <= degree; ++i)
	{
		cout << "b_" << i << " = " << b[i] << endl;
	}
	cout << "\n";
}

void Interpolation::print_stat()
{
	for (unsigned int i = 0; i < all_points_.size(); ++i)
	{
		cout << "y = " << all_points_[i].y << ", f(" << all_points_[i].x << ") = " << apply(all_points_[i].x) << "\n";
	}
	cout << "\n";

	double avg_y = 0;
	for (unsigned int i = 0; i < all_points_.size(); ++i)
		avg_y += all_points_[i].y;
	avg_y /= all_points_.size();

//	cout << "avg(y) = " << avg_y << "\n\n";

	double sse = 0;

	for (unsigned int i = 0; i < all_points_.size(); ++i)
	{
		double temp = all_points_[i].y;
		temp -= apply(all_points_[i].x);
		temp *= temp;
		sse += temp;
	}

	double tss = 0;

	for (unsigned int i = 0; i < all_points_.size(); ++i)
	{
		double temp = all_points_[i].y;
		temp -= avg_y;
		temp *= temp;
		tss += temp;
	}

	double r_squared = 1 - sse / tss;

	cout << "SSE = " << sse << "\n";
	cout << "R^2 = " << r_squared << "\n\n";
}
