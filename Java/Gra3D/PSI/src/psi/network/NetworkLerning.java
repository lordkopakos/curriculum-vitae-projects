/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.network;

/**
 *
 * @author lordkopakos
 */
public interface NetworkLerning {
    public void backPropagation(double weightFrom, double weightTo);
}
