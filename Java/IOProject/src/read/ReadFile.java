/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import convert.description.A_DescriptionReadFile;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import conteners.Week;
import conteners.Week;

/**
 * @author Dominik Kopaczka
 */
@A_DescriptionReadFile(classDescription = "Klasa reprezentujÄ…ca wczytywany plik",
        author = "Dominik Kopaczka",
        classVersion = 1)
public class ReadFile {

    private int numberOfDays;
    private Week week;

    private String fileName;
    private String filePath;
    private String fileEnlargement;


    int dayCounter;
    int lectureCounter;

    public ReadFile(String fileName) {
        dayCounter = 0;
        lectureCounter = 0;
        setFileLocation("file/", fileName, ".txt");
        loadFile();
    }

    private boolean loadFile() {
        BufferedReader reader = connectWithFile();
        String fline = readLineFromFile(reader);
        setNumberOfDays(fline);
        setWeek();

        while (true) {
            String line = readLineFromFile(reader);
            if (line == null) {
                closeConnectionWithFile(reader);
                break;
            }
            setDaysForWeek(line);
        }
        return true;
    }

    private BufferedReader connectWithFile() {
        try {
            BufferedReader reader = new BufferedReader(
                    new FileReader(filePath + fileName + fileEnlargement));
            return reader;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadFile.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private boolean closeConnectionWithFile(BufferedReader reader) {
        try {
            reader.close();
            return true;
        } catch (IOException ex) {
            Logger.getLogger(ReadFile.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private String readLineFromFile(BufferedReader reader) {
        try {
            String line = reader.readLine();
            return line;
        } catch (IOException ex) {
            Logger.getLogger(ReadFile.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private void setDaysForWeek(String line) {
        String[] pattern = line.split(";");

        if (pattern.length == 2) {
            makeDay(pattern);
            lectureCounter = 0;
            dayCounter++;
        } else if (pattern.length > 2) {
            makeLecture(pattern);
            lectureCounter++;
        }
    }

    private void makeDay(String[] pattern) {
        String dayName = pattern[0];
        int numberOfLectures = Integer.valueOf(pattern[1]);
        week.makeMyDay(dayCounter, dayName, numberOfLectures);
    }

    private void makeLecture(String[] pattern) {
        int day = dayCounter - 1;
        week.setDays(day, lectureCounter, pattern[0], pattern[1], pattern[2], pattern[3], pattern[4], pattern[5], pattern[6], pattern[7]);
    }

    private void setFileLocation(String filePath, String fileName, String fileEnlargement) {
        setFilePath(filePath);
        setFileName(fileName);
        setFileEnlargement(fileEnlargement);
    }

    private void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    private void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private void setFileEnlargement(String fileEnlargement) {
        this.fileEnlargement = fileEnlargement;
    }

    private void setNumberOfDays(String line) {
        String nop = (line.replaceAll("\\D+", ""));
        numberOfDays = Integer.parseInt(nop);
    }

    private void setWeek() {
        this.week = new Week(numberOfDays);
    }

    public Week getWeek() {
        return week;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }
}
