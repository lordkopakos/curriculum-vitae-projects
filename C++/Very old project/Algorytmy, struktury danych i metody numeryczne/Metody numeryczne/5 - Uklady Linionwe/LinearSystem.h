#pragma once

#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
using namespace std;

class LinearSystem
{
	const double eps;
	vector<vector<double>> M;
	vector<double> b,Xn;
	void checkDiagonalDominance();
public:
	LinearSystem();
	void loadGaussMatrix();
	void loadJacobiMatrix();
	void solveGauss();
	void solveJacobi(int);
};