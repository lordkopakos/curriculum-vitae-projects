package pkg3dproject.math3D;

import java.awt.Rectangle;

/**
 *
 * @author lordkopakos
 */
    /*
    Klasa ViewWindow, reprezentująca okno widoku, 
    na które rzutowane są obiekty trójwymiarowe;
    */
public class ViewWindow {
    private Rectangle bounds;
    private float angle;
    private float distanceToCamera;
    /*
    Tworzy nowe okno ViewWindow o określonych granicach na ekranie 
    i ustalonym kącie widzenia w poziomie;
    */
    public ViewWindow(int left, int top, int width, int height, float angle){
        bounds= new Rectangle();
        this.angle=angle;
        setBounds(left, top, width, height);
    }
    /*
    Definiuje granice tego okna ViewWindow na ekranie;
    */
    public void setBounds(int left, int top, int width, int height) {
        bounds.x=left;
        bounds.y=top;
        bounds.width=width;
        bounds.height=height;
        distanceToCamera=(bounds.width/2)/(float)Math.tan(angle/2);
    }
    /*
    Definiuje poziomy kąt widzenia dla tego okna ViewWindow;
    */
    public void setAngle(float angle){
        this.angle=angle;
        distanceToCamera=(bounds.width/2)/(float)Math.tan(angle/2);
    }
    /*
    Gettery
    */
    public float getAngle(){
        return angle;
    }
    public int getWidth(){
        return bounds.width;
    }
    public int getHeight(){
        return bounds.height;
    }
    /*
    Pobiera przesunięcie y tego okna widoku względem ekranu;
    */
    public int getTopOffset(){
        return bounds.y;
    }
    /*
    Pobiera przesunięcie x tego okna widoku względem ekranu;
    */
    public int getLeftOffset(){
        return bounds.x;
    }
    /*
    Pobiera odległość od kamery do okna obrazu;
    */
    public float getDistance(){
        return distanceToCamera;
    }
    /*
    Przekształca współrzędną x z tego okna obrazu 
    na odpowiadającą jej współrzędną x na ekranie;
    */
    public float convertFromViewXToScreenX(float x){
        return x+bounds.x+bounds.width/2;
    }
    /*
    Na odwrót;
    */
    public float convertFromScreenXToViewX(float x){
        return x-bounds.x-bounds.width/2;
    }
    /*
    Przekształca współrzędną y z tego okna obrazu 
    na odpowiadającą jej współrzędną y na ekranie;
    */
    public float convertFromViewYToScreenY(float y){
        return -y+bounds.y+bounds.height/2;
    }
    /*
    Na odwrót;
    */
    public float convertFromScreenYToViewY(float y){
        return -y+bounds.y+bounds.height/2;
    }
    /*
    Rzutuje podany wektor na ekran;
    */
    public void project(Vector3D v){
        //rzutuj na onno obrazu;
        v.x=distanceToCamera*v.x/-v.z;
        v.y=distanceToCamera*v.y/-v.z;
        
        //konwertuj na współrzędne ekranowe;
        v.x=convertFromViewXToScreenX(v.x);
        v.y=convertFromViewYToScreenY(v.y);
    }
    
}
