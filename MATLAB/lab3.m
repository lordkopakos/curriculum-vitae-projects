clc
clear all
close all


dane = xlsread('oscillations');

p=polyfit([1 2 3 4],[1 2 1 2],3);
t=0:0.1:5;
y=polyval(p, t);
plot([1 2 3 4],[1 2 1 2],'o',t,y,'m')

t = dane(:, 1);
y = dane(:, 2);

plot(t, y, 'ro')

%yt = A0*sin(2*pi*f*t).exp(-d*f*t);

%f=1/(x1-x2);

a=480;
b=530;

hold on
plot(t(a:b), y(a:b), 'g*')

p=polyfit(t(a:b), y(a:b), 1)

yp=p(1)*t(a:b)+p(2)

plot(t(a:b), yp, 'k-')

xz1=-p(2)/p(1);

pl = plot(xz1,0,'ys')

set(pl, 'MarkerFaceColor', 'y')

a1=1450;
b1=1600;

hold on
plot(t(a1:b1), y(a1:b1), 'g*')

p=polyfit(t(a1:b1), y(a1:b1), 1)

yp=p(1)*t(a1:b1)+p(2)

plot(t(a1:b1), yp, 'k-')

xz2=-p(2)/p(1);

pl1 = plot(xz2,0,'ys')

set(pl1, 'MarkerFaceColor', 'y')

f=1/(xz2-xz1)

a2=200;
b2=270;

p1=polyfit(t(a2:b2), y(a2:b2), 2)
plot(t(a2:b2), y(a2:b2), 'g*')

ypar=p1(1)*t(a2:b2).^2+p1(2)*t(a2:b2)+p1(3);

plot(t(a2:b2), ypar, 'k-')

%d =(1/N)* ln()

b=p1(2);
a=p1(1);
c=p1(3);
delta=b*b-4*a*c;
xw=-b/(2*a);
yw=-delta/(4*a);

pl2=plot(xw, yw ,'ys')
set(pl2, 'MarkerFaceColor', 'y')

yw1=max(ypar);
pl2=plot(xw, yw1 ,'bs')


a2=1150;
b2=1350;

p1=polyfit(t(a2:b2), y(a2:b2), 2)
plot(t(a2:b2), y(a2:b2), 'g*')

ypar=p1(1)*t(a2:b2).^2+p1(2)*t(a2:b2)+p1(3);

plot(t(a2:b2), ypar, 'k-')



b=p1(2);
a=p1(1);
c=p1(3);
delta=b*b-4*a*c;
xw=-b/(2*a);
yw2=-delta/(4*a);

pl2=plot(xw, yw2 ,'ys')
set(pl2, 'MarkerFaceColor', 'y')

yw1=max(ypar);
pl2=plot(xw, yw1 ,'bs')

d =log(yw/yw2)

yt = 4.5*sin(2*pi*f*t).*exp(-d*f*t)

plot(t, yt,'y-')

figure(2);
plot(y,'k.');





