package pkg3dproject.bsp2D;

import java.awt.geom.*;
import pkg3dproject.math3D.*;

public class BSPLine extends Line2D.Float {

    public static final int BACK = -1;
    public static final int COLLINEAR = 0;
    public static final int FRONT = 1;
    public static final int SPANNING = 2;

    /**
        Wsp�rz�dna X wektora prostopad�ego.
    */
    public float nx;

    /**
        Wsp�rz�dna Y wektora prostopad�ego.
    */
    public float ny;

    /**
        Najwy�sze po�o�enie linii reprezentuj�cej �cian�.
    */
    public float top;

    /**
        Najni�sze po�o�enie linii reprezentuj�cej �cian�.
    */
    public float bottom;

    /**
        Tworzy now� lini� z punktu (0,0) do punktu (0,0)
    */
    public BSPLine() {
        super();
    }


    /**
        Tworzy nowy obiekt BSPLine dla przekazanego obiektu BSPPolygon
        (tylko je�li obiekt BSPPolygon reprezentuje pionow� �cian�).
    */
    public BSPLine(BSPPolygon poly) {
        setTo(poly);
    }


    /**
        Tworzy nowy obiekt BSPLine dla przekazanych wsp�rz�dnych.
    */
    public BSPLine(float x1, float y1, float x2, float y2) {
        setLine(x1, y1, x2, y2);
    }


    /**
        ��czy ten obiekt BSPLine z danym obiektem BSPPolygon
        (tylko je�li obiekt BSPPolygon reprezentuje pionow� �cian�).
    */
    public void setTo(BSPPolygon poly) {
        if (!poly.isWall()) {
            throw new IllegalArgumentException(
                "BSPPolygon not a wall");
        }
        top = java.lang.Float.MIN_VALUE;
        bottom = java.lang.Float.MAX_VALUE;
        // znajduje par� najbardziej oddalonych punkt�w (ignoruj�c wsp�rz�dn� y)
        float distance = -1;
        for (int i=0; i<poly.getNumVertices(); i++) {
            Vector3D v1 = poly.getVertex(i);
            top = Math.max(top, v1.y);
            bottom = Math.min(bottom, v1.y);
            for (int j=0; j<poly.getNumVertices(); j++) {
                Vector3D v2 = poly.getVertex(j);
                float newDist = (float)Point2D.distanceSq(
                    v1.x, v1.z, v2.x, v2.z);
                if (newDist > distance) {
                    distance = newDist;
                    x1 = v1.x;
                    y1 = v1.z;
                    x2 = v2.x;
                    y2 = v2.z;
                }
            }
        }
        nx = poly.getNormal().x;
        ny = poly.getNormal().z;
    }

    /**
        Oblicza dane prostej prostopad�ej do tej linii.
    */
    public void calcNormal() {
        nx = y2 - y1;
        ny = x1 - x2;
    }

    /**
        Normalizuje d�ugo�� wektora prostopad�ego do linii podzia�u (zmienia
        jego d�ugo�� na 1 jednostk�).
    */
    public void normalize() {
        float length = (float)Math.sqrt(nx * nx + ny * ny);
        nx/=length;
        ny/=length;
    }


    public void setLine(float x1, float y1, float x2, float y2) {
        super.setLine(x1, y1, x2, y2);
        calcNormal();
    }


    public void setLine(double x1, double y1, double x2,
        double y2)
    {
        super.setLine(x1, y1, x2, y2);
        calcNormal();
    }


    /**
        Zamienia punkty ko�cowe tej linii (innymi s�owy, (x1,y1) staje si� (x2,y2)
        i odwrotnie) oraz odwraca kierunek wskazywany przez prostpad��.
    */
    public void flip() {
        float tx = x1;
        float ty = y1;
        x1 = x2;
        y1 = y2;
        x2 = tx;
        y2 = ty;
        nx = -nx;
        ny = -ny;
    }


    /**
        Ustawia wysoko�� na jakiej znajduje si� dolna i g�rna kraw�d� "�ciany".
    */
    public void setHeight(float top, float bottom) {
        this.top = top;
        this.bottom = bottom;
    }


    /**
        Zwraca true, je�li punkty ko�cowe tej linii pokrywaj� si� z punktami
        ko�cowymi przekazanej linii. Ignoruje prostopad�� i warto�ci dotycz�ce
        wysoko�ci.
    */
    public boolean equals(BSPLine line) {
        return (x1 == line.x1 && x2 == line.x2 &&
            y1 == line.y1 && y2 == line.y2);
    }


    /**
        Zwraca true, je�li punkty ko�cowe tej linii pokrywaj� si� z punktami
        ko�cowymi przekazanej linii. Ignoruje kolejno�� punkt�w ko�cowych
        (je�li pierwszy punkt ko�cowy tej linii pokrywa si� z drugim punktem
        ko�cowym przekazanej linii, i odwrotnie, metoda zwraca true). Ignoruje
        tak�e prostopad�� i warto�ci dotycz�ce wysoko�ci.
    */
    public boolean equalsIgnoreOrder(BSPLine line) {
        return equals(line) || (
            (x1 == line.x2 && x2 == line.x1 &&
            y1 == line.y2 && y2 == line.y1));
    }


    public String toString() {
        return "(" + x1 + ", " + y1 + ")->" +
            "(" + x2 + "," + y2 + ")" +
            " bottom: " + bottom + " top: " + top;
    }


    /**
        Zwraca stron�, po kt�rej znajduje si� okre�lony punkt wzgl�dem linii
        podzia�u. Metoda zak�ada, �e linia podzia�u ma grubo�� jednej jednostki,
        zatem punkty znajduj�ce si� w granicach tej pogrubionej linii s� traktowane
        jak punkty nale��ce do linii podzia�u. Aby ta metoda dzia�a�a prawid�owo, 
        nale�y znormalizowa� d�ugo�� wektora prostopad�ego do linii podzia�u (albo 
        wi���c t� lini� z wielok�tem, albo wywo�uj�c metod� normalize()).
        W zale�no�ci od po�o�enia punktu, metoda zwraca warto�� FRONT, BACK lub 
        COLLINEAR.
    */
    public int getSideThick(float x, float y) {
        int frontSide = getSideThin(x-nx/2, y-ny/2);
        if (frontSide == FRONT) {
            return FRONT;
        }
        else if (frontSide == BACK) {
            int backSide = getSideThin(x+nx/2, y+ny/2);
            if (backSide == BACK) {
                return BACK;
            }
        }
        return COLLINEAR;
    }


    /**
        Zwraca stron�, po kt�rej znajduje si� okre�lony punkt wzgl�dem linii
        podzia�u. Ze wzgl�du na niedok�adno�� oblicze� na liczbach zmiennoprze-
        cinkowych, przypadki wykrycia przynale�no�ci punktu do linii podzia�u
        s� bardzo rzadkie. Aby ta metoda dzia�a�a prawid�owo, nale�y znormalizowa�
        d�ugo�� wektora prostopad�ego do linii podzia�u (albo wi���c t� lini� z
        wielok�tem, albo wywo�uj�c metod� normalize()).
        W zale�no�ci od po�o�enia punktu, metoda zwraca warto�� FRONT, BACK lub 
        COLLINEAR.
    */
    public int getSideThin(float x, float y) {
        // il. skal. wektora prowadz�cego do punktu i w. prostopad�ego do l. podzia�u
        float side = (x - x1)*nx + (y - y1)*ny;
        return (side < 0)?BACK:(side > 0)?FRONT:COLLINEAR;
    }


    /**
        Zwraca stron� (wzgl�dem linii podzia�u), po kt�rej znajduje si�
        przekazany odcinek. Zwraca warto�� FRONT, BACK, COLINEAR lub SPANNING.
    */
    public int getSide(Line2D.Float segment) {
        if (this == segment) {
            return COLLINEAR;
        }
        int p1Side = getSideThick(segment.x1, segment.y1);
        int p2Side = getSideThick(segment.x2, segment.y2);
        if (p1Side == p2Side) {
            return p1Side;
        }
        else if (p1Side == COLLINEAR) {
            return p2Side;
        }
        else if (p2Side == COLLINEAR) {
            return p1Side;
        }
        else {
            return SPANNING;
        }
    }


    /**
        Zwraca stron� (wzgl�dem linii podzia�u), po kt�rej znajduje si�
        przekazany wielok�t. Zwraca warto�� FRONT, BACK, COLINEAR lub 
        SPANNING.
    */
    public int getSide(BSPPolygon poly) {
        boolean onFront = false;
        boolean onBack = false;

        // sprawdza ka�dy punkt
        for (int i=0; i<poly.getNumVertices(); i++) {
            Vector3D v = poly.getVertex(i);
            int side = getSideThick(v.x, v.z);
            if (side == BSPLine.FRONT) {
                onFront = true;
            }
            else if (side == BSPLine.BACK) {
                onBack = true;
            }
        }

        // klasyfikuje wielomian
        if (onFront && onBack) {
            return BSPLine.SPANNING;
        }
        else if (onFront) {
            return BSPLine.FRONT;
        }
        else if (onBack) {
            return BSPLine.BACK;
        }
        else {
            return BSPLine.COLLINEAR;
        }
    }


    /**
        Zwraca stosunek d�ugo�ci jednej z cz�ci przeci�tego odcinka
        do d�ugo�ci ca�ego odcinka. Je�li odcinki si� przecinaj�,
        zwraca warto�� od 0 do 1. Przyk�adowo, zwr�cona warto�� 0
        oznacza, �e przeci�cie nast�pi�o w punkcie (x1, y1); warto��
        1 oznacza, �e przeci�cie nast�pi�o w punkcie (x2, y2);
        natomiast warto�� 0,5 oznacza, �e przeci�cie nast�pi�o w �rodku
        odcinka. Zwraca warto�� -1, je�li odcinki s� r�wnoleg�e.
    */
    public float getIntersection(Line2D.Float line) {
        // Punkt przeci�cia I dw�ch wektor�w, A1->A2 i B1->B2:
        // I = A1 + u * (A2 - A1)
        // I = B1 + v * (B2 - B1)
        //
        // Rozwi�zanie dla u daje nam nast�puj�cy wz�r.
        // Wynikiem r�wnania jest warto�� u.
        float denominator = (line.y2 - line.y1) * (x2 - x1) - (line.x2 - line.x1) * (y2 - y1);

        // Sprawdza, czy dwie badane proste s� r�wnoleg�e
        if (denominator == 0) {
            return -1;
        }

        float numerator = (line.x2 - line.x1) * (y1 - line.y1) - (line.y2 - line.y1) * (x1 - line.x1);

        return numerator / denominator;
    }


    /**
        Zwraca wsp�rz�dne punktu przeci�cia danej prostej i prostej
        przekazanej w formie argumentu.
    */
    public Point2D.Float getIntersectionPoint(Line2D.Float line) {
        return getIntersectionPoint(line, null);
    }


    /**
        Zwraca wsp�rz�dne punktu przeci�cia danej prostej i prostej
        przekazanej w formie argumentu. Je�li argument intersection
        ma warto�� null, tworzony jest nowy punkt.
    */
    public Point2D.Float getIntersectionPoint(Line2D.Float line,
        Point2D.Float intersection)
    {
        if (intersection == null) {
            intersection = new Point2D.Float();
        }
        float fraction = getIntersection(line);
        intersection.setLocation(
            x1 + fraction * (x2 - x1),
            y1 + fraction * (y2 - y1));
        return intersection;
    }

}
