/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convert;

import conteners.Lecture;

/**
 * Klasa reprezenująca graficzny blok pojedyńczego zajęcia w danym dniu(dotyczy horyzontalnej wersji planu).
 * @see ConvertFile
 * @author Dominik Kopaczka
 * @version 1.0
 * @since   2016-12-29
 */
public final class GraphicLectureBlock {
    private String blockText;
    private String color;

    /**
     *Konstruktor klasy GraphicLectureBlock który na podstawie obiektu klasy Lessons(Lecture) 
     * pojedyńczy blok reprezentujący jedne zajęcia w danym dniu.
     * @param lecture Obiekt klasy Lessons(Lecture) zawierający dane jednego zajęcia. 
     */
    public GraphicLectureBlock(Lecture lecture) {
        String lectureName=lecture.getLectureName();
        String lecturer=lecture.getLecturer();
        String formOfLectures=lecture.getFormOfLectures();
        String buliding=lecture.getBuilding();
        String room=lecture.getRoom();
        
        blockText=lectureName+" "+lecturer+" "+formOfLectures+" "+buliding+" "+room;
        setColor(formOfLectures);
    }

    /**
     *Konstruktor przyjmuje string z danymi i koloru bloku graficznego, 
     * które zostaną zapisane w obiekcie GraphicLectureBlock.
     * @param blockText Tekst który zostanie wpisany w wyświetlany blok.
     * @param color Nazwa koloru zapisana w postaci heksadecymalnej.
     */
    public GraphicLectureBlock(String blockText, String color) {
        this.blockText = blockText;
        this.color = color;
    }
    
    /**
     *Metoda ustawia domyślny kolor dla każdego z rodzajów zajęć.
     * @param formOfLectures
     */
    public void setColor(String formOfLectures){
        
        switch(formOfLectures){
            case "CL":
                this.color="#FF7F50";
                break;
            case "W":
                this.color="#8A2BE2";
                break;
            case "CP":
                this.color="#FFF68F";
                break;
            case "S":
                this.color="#AFEEEE";
                break;
            case "L":
                this.color="#D3FFCE";
                break;
            case "WF":
                this.color="#FFC0CB";
                break;
            default:
                this.color="#FFFFFF";
                break;
        }
    }

    /**
     *Metoda pobiera tekst bloku graficznego.
     * @return Tekst bloku graficznego.
     */
    public String getBlockText() {
        return blockText;
    }

    /**
     *Metoda pobiera nazwę koloru bloku graficznego.
     * @return Koloru bloku graficznego.
     */
    public String getColor() {
        return color;
    }
    
    /**
     *Metoda pobiera pełną nazwę zajęć dla otrzymanego w argumencie. 
     * @param formOfLectures Skrót nazwy zajęć("CL", "W", "CP", "S", "L", "WF").
     * @return pełną nazwę lub domyślnie zwraca "none". 
     */
    public String getComments(String formOfLectures){
        switch(formOfLectures){
            case "CL":
                return "laboratory";
            case "W":
                return "lecture";
            case "CP":
                return "project";
            case "S":
                return "seminar";
            case "L":
                return "foreign language";
            case "WF":
                return "physical education";
            default:
                return "none";
        }
    }
}
