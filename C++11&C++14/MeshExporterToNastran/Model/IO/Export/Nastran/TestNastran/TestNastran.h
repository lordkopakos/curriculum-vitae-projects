//
//Created By Dominik Kopaczka
//
#ifndef CPP_TESTNASTRAN_H
#define CPP_TESTNASTRAN_H

#include "../../../../Common/CommonMeshRepresentation.h"
#include "../NastranExporter/NastranExporter.h"

using namespace std;
class TestNastran
{
private:	MeshIS::Model::Common::CMR cmr;


public:		void	print_CMR();
private:	void	print_vertexes();
private:	void	print_single_vertex(MeshIS::Model::Common::Vertex vertex, int id);
private:	void	print_single_element_t4(MeshIS::Model::Common::Element_T4 element,int id);
private:	void	print_single_element_p6(MeshIS::Model::Common::Element_P6 element,int id);
private:	void	print_elements_t4();
private:	void	print_elements_p6();

private:	bool load_CMR_from_file(std::string path);
private:	void load_file_lines(std::fstream &file);
private:	std::string set_first_sign_and_delete_from_line(std::string &line);
private:	std::vector<std::string> split(std::string line, char pattern);

public:		void	init_CMR();
private:
    template<typename mesh_part>
    mesh_part init_mesh_part(std::vector<string> line_vector);
	
public:		void	run_nastran(std::string path ,MeshIS::Model::Export::Nastran::NastranExporter::Type_Of_Nastran_Version type);

};

#endif //CPP_TESTNASTRAN_H