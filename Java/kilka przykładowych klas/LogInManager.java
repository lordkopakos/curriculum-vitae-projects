/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.include.menugame;

import game.include.Jattributes.ImagePanel;
import game.include.sql.MySQL;
import game.include.sql.PlayerResult;
import game.include.sql.Score;
import game.include.sql.User;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javafx.scene.chart.PieChart;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import game.include.Jattributes.text.*;
/**
 *
 * @author lordkopakos
 */
public class LogInManager extends SelectionHero {
    
    
    private ImagePanel logIn;
    private JButton logInPanelButton;
    private JButton logOutPanelButton;
    private JButton closeButton;
    
     private JButton userAlert;
    private JButton passwordAlert;
    private JButton verifyPasswordAlert;
    private JButton scoreboardButton;
    
    private JLabel userLabel;
    private JLabel passwordLabel;
    private JLabel verifyPasswordLabel;
    
    private InputComponent userText;
    private InputPasswordComponent passwordText;
    private InputPasswordComponent verifyPasswordText;
    
    
    
    private JLabel flag;
    private JLabel personalInformation;
    
    private int first;
    
    private MySQL mysql;
    private static Score score;
    private static PlayerResult playResult;
    private MySQL CointsSQL;
    private int coins;
    public static int initPlayer;
    
    private ImagePanel scorePanel;
    private JButton closeScoreBoardButton;
    private JTable scoreTable;
    private JScrollPane scrollPane;
    
    
    List<PlayerResult> result;
    
    public LogInManager(int coins)
    {
        initPlayer=0;
        this.coins=coins;
        
        
        
    }

   
    
    @Override
    public void menurun()
    {
        super.menurun();
    }
    @Override
    public void menuinit()
    {
        super.menuinit();
        
        Border scoreBorder =
            BorderFactory.createLineBorder(Color.ORANGE,5);
        scorePanel= new ImagePanel("images/attributes/JPanelScoreboard.jpg");
        scorePanel.setBorder(scoreBorder);
        scorePanel.setVisible(false);
        
        Vector<String> kolumny = new Vector<String>(); 
                kolumny.addElement("USERNAME"); 
                kolumny.addElement("SCORE"); 
                kolumny.addElement("COUNTRY"); 
                Vector<String> tmp = new Vector<String>(); 
                Vector<Vector> wiersze = new Vector<Vector>(); 
                
        if(coins<0)
        {
            user=new User();
            
        }
        
            System.out.println("Działa: "+user.getUsername());
        
                
                CointsSQL=new MySQL();
                score=new Score();
                score=CointsSQL.selectScore(coins);
                if(score.getId()==-1 )
                {
                CointsSQL.insertScore(coins);
                }
                score=CointsSQL.selectScore(coins);
                System.out.println("ID_USERA: "+user.getId()+"       ID_SCORE: "+score.getId());
                if(coins>=0)
                CointsSQL.insertPlayerResult(user.getId(), score.getId());
                
                result = CointsSQL.selectPlayResults();
                for(PlayerResult r: result)
                {
                    System.out.println(r);
                    tmp = new Vector<String>(); 
                    tmp.removeAllElements(); 
                    tmp.addElement(CointsSQL.selectUsername(r.getIdUser())); 
                    tmp.addElement(CointsSQL.selectScoreForScoreboard(r.getIdScore())); 
                    tmp.addElement(CointsSQL.selectCounntry(r.getIdUser())); 
                    
                    wiersze.add(tmp); 
                }
                CointsSQL.closeConnection();
                
             
                scoreTable=new JTable(wiersze, kolumny){

    @Override
    public boolean isCellEditable(int row, int column) {
       //all cells false
       return false;
    }
};;
                scoreTable.setPreferredScrollableViewportSize(new Dimension(500, 50));
                scoreTable.setFillsViewportHeight(true);
                scoreTable.setSize(400, 210);
                scoreTable.setLocation(150, 218);
                scoreTable.setOpaque(false);
((DefaultTableCellRenderer)scoreTable.getDefaultRenderer(Object.class)).setOpaque(false);
                scoreTable.setForeground(Color.BLACK);
                scoreTable.setFont(new Font("Jokerman", Font.PLAIN, 16));
                scoreTable.setAutoCreateRowSorter(true);
        
        
        first=80;
        Border border =
            BorderFactory.createLineBorder(Color.WHITE,5);
        logIn= new ImagePanel("images/attributes/JPanelLogin.jpg");
        logIn.setBorder(border);
        logIn.setVisible(false);
        
        
      

        
        //Stworzenie pól tekstowych
        userLabel=new JLabel("USERNAME:");
        userText=new InputComponent();
        addField(userLabel, userText,0, "Imię");
        
        passwordLabel=new JLabel("PASSWORD:");
        passwordText=new InputPasswordComponent();
        addField(passwordLabel, passwordText,1, "Hasło");
        
        verifyPasswordLabel=new JLabel("VERIFY PASSWORD:");
        verifyPasswordText=new InputPasswordComponent();
        addField(verifyPasswordLabel, verifyPasswordText,2, "Powtórz Hasło");
        
        //Dodanie klawiszy obsługi
        closeButton= createButton("close", "Zamknij");
        closeButton.setSize(27, 24);
        closeButton.setLocation(logIn.getWidth()-closeButton.getWidth()-10, 10);
        
        logInPanelButton = createButton("login_logout/login", "Zaloguj się"); 
        logInPanelButton.setSize(112, 48); 
        logInPanelButton.setLocation(logIn.getWidth()/2-logInPanelButton.getWidth()/2, 220);
        
        logOutPanelButton=createButton("login_logout/logout", "Wyloguj się");
        logOutPanelButton.setSize(119, 48);
        logOutPanelButton.setLocation(logIn.getWidth()/2-logOutPanelButton.getWidth()/2, 220);
        
        scoreboardButton=createButton("login_logout/scoreboard", "Wyloguj się");
        scoreboardButton.setSize(168,48);
        scoreboardButton.setLocation(logIn.getWidth()/2-scoreboardButton.getWidth()/2, 160);
        
        //Dodanie alertów
        
        
        userAlert=createAlertButton("Imię jest za krótkie");
        setAlertLocation(0, userAlert);
        
        passwordAlert=createAlertButton("Imię jest za krótkie");
        setAlertLocation(1, passwordAlert);
        
        verifyPasswordAlert=createAlertButton("Imię jest za krótkie");
        setAlertLocation(2, verifyPasswordAlert);
        
        //Ustawienie panelu na środek ekranu
        logIn.setLocation(
            (screen.getWidth() - logIn.getWidth()) / 2,
            (screen.getHeight() - logIn.getHeight()) / 2);
        
        scorePanel.setLocation((screen.getWidth() - scorePanel.getWidth()) / 2,
            (screen.getHeight() - scorePanel.getHeight()) / 2);
        
        
        
        
        //ScoreBoard klawisze obsługi
        closeScoreBoardButton= createButton("close", "Zamknij");
        closeScoreBoardButton.setSize(27, 24);
        closeScoreBoardButton.setLocation(scorePanel.getWidth()-closeScoreBoardButton.getWidth()-10, 10);

        
        //Dodanie komponentów
        logIn.add(logInPanelButton);
        logIn.add(closeButton);
        
        logIn.add(userAlert);
        logIn.add(passwordAlert);
        logIn.add(verifyPasswordAlert);
        
       
        
        logIn.add(verifyPasswordLabel);
        logIn.add(verifyPasswordText);
        logIn.add(userLabel);
        logIn.add(userText);
        logIn.add(passwordLabel);
        logIn.add(passwordText);
      
       
        
        
        JLabel titleScoreboard=new JLabel("Scoreboard");
        titleScoreboard.setFont(new Font("Jokerman", Font.PLAIN, 48));
        titleScoreboard.setSize(300, 50);
        titleScoreboard.setLocation(scorePanel.getWidth()/2-titleScoreboard.getWidth()/2, 10);
        
        JScrollPane js=new JScrollPane(scoreTable);
js.setVisible(true);
js.setSize(400, 210);
                js.setLocation(150, 218);
                js.setOpaque(false);
js.getViewport().setOpaque(false);
js.setFont(new Font("Jokerman", Font.PLAIN, 16));

        scorePanel.add(closeScoreBoardButton);
        scorePanel.add(js);
        scorePanel.add(titleScoreboard);
        
        // Dodanie okna dialogowego do warstwy "okien modalnych"
        // panelu ekranu.
        
        screen.getFullScreenWindow().getLayeredPane().add(scorePanel,
            JLayeredPane.MODAL_LAYER);
        
        
        screen.getFullScreenWindow().getLayeredPane().add(logIn,
            JLayeredPane.MODAL_LAYER);
        
        
    }
    @Override
    public void draw(Graphics2D g) 
    {
        super.draw(g);
    }
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        super.actionPerformed(e);
        Object src = e.getSource();
        if (src == loginButton) {
            System.out.println("CHATBOX11");
            logIn.setVisible(true);
        }
        if (src == closeButton) {
            System.out.println("CHATBOX11");
            logIn.setVisible(false);
        }
        if(src==closeScoreBoardButton)
        {
            scorePanel.setVisible(false);
        }
        if (src == logInPanelButton) {
            System.out.println("CHATBOX11");
            if(checkLoginAlert()==0){
                new Thread() {
            public void run() {
                
                mysql=new MySQL();
                user=new User();
                user=mysql.selectUser(userText.getText());
                if(user.getPassword().equals(passwordText.getText())){
                logIn.setVisible(false);
                
                initPlayer=1;
                
                logIn.remove(logInPanelButton);
                logIn.remove(userAlert);
                logIn.remove(passwordAlert);
                logIn.remove(verifyPasswordAlert);
                logIn.remove(verifyPasswordLabel);
                logIn.remove(verifyPasswordText);
                logIn.remove(userLabel);
                logIn.remove(userText);
                logIn.remove(passwordLabel);
                logIn.remove(passwordText);
                
                    ImageIcon imageFlag;
                    imageFlag = new ImageIcon("images/menu/main/Pi/flags/"+user.getCountry()+".png");
                    flag=new JLabel(imageFlag);
                    flag.setSize(140, 140);
                    flag.setLocation(390,180);
                    
                    String INSTRUCTIONS=new String();
                    INSTRUCTIONS=
                            "<html><center>PERSONAL INFORMATION" +
                            "<br><br>user: "+user.getUsername()+
                            "<br>"+user.getFName() +
                            "<br>"+user.getLName()+"</center>";
                    
                    int widthPI=logIn.getWidth()/2, heightPI=logIn.getHeight()/2;
                    personalInformation=new JLabel(INSTRUCTIONS);
                    personalInformation.setFont(new Font("Jokerman", Font.PLAIN, 16));
                    personalInformation.setSize(widthPI, heightPI);
                    personalInformation.setLocation(logIn.getWidth()/2-personalInformation.getWidth()/2+15, 0);
                    personalInformation.setForeground(Color.WHITE);
                    
                    logIn.add(scoreboardButton);
                    logIn.add(logOutPanelButton);
                    logIn.add(personalInformation);
                    logIn.add(flag);
                    
                contentPane2.remove(newGameButton);
                contentPane2.remove(loginButton);
                contentPane2.remove(registrationButton);
                contentPane2.remove(chatboxButton);
                contentPane2.remove(quitButton);
                
                loginButton=createButton("logout", "Sprawdź swoje konto bądź się wyloguj");
                
                contentPane2.add(newGameButton);
                contentPane2.add(loginButton);
                contentPane2.add(registrationButton);
                contentPane2.add(chatboxButton);
                contentPane2.add(quitButton);
                
                    
                }
                else{
                    passwordAlert.setToolTipText("Password is wrong");
                    passwordAlert.setVisible(true);
                    
                    verifyPasswordAlert.setToolTipText("Verify password is wrong");
                    verifyPasswordAlert.setVisible(true);
                }
                mysql.closeConnection();
            }
        }.start();
            
            }
        }
        if(src==logOutPanelButton)
        {
            user=new User();
            logIn.setVisible(false);
            
                contentPane2.remove(newGameButton);
                contentPane2.remove(loginButton);
                contentPane2.remove(registrationButton);
                contentPane2.remove(chatboxButton);
                contentPane2.remove(quitButton);
                
                loginButton=createButton("login", "Zaloguj się");
                
                contentPane2.add(newGameButton);
                contentPane2.add(loginButton);
                contentPane2.add(registrationButton);
                contentPane2.add(chatboxButton);
                contentPane2.add(quitButton);
                
                logIn.remove(scoreboardButton);
                logIn.remove(logOutPanelButton);
                logIn.remove(personalInformation);
                logIn.remove(flag);
                
                
                logIn.add(logInPanelButton);
                logIn.add(userAlert);
                logIn.add(passwordAlert);
                logIn.add(verifyPasswordAlert);
                logIn.add(verifyPasswordLabel);
                logIn.add(verifyPasswordText);
                logIn.add(userLabel);
                logIn.add(userText);
                logIn.add(passwordLabel);
                logIn.add(passwordText);
        }
        
        if(src==scoreboardButton)
        {
            scorePanel.setVisible(true);
        }
                    
        
        
    }
    
    protected void setAlertLocation(int which, JButton alert)
    {
        alert.setSize(20,20);
        alert.setLocation(logIn.getWidth()/2+widthTextField+10, first+which*30);
        
    }
    
    private void addField(JLabel jlabel, JTextField textField, int witch, String toolTipText)
       {
      
        Border border =
            BorderFactory.createLineBorder(Color.WHITE,1);
        
        jlabel.setSize(widthTextField, heightTextField);
        jlabel.setLocation(logIn.getWidth()/2-widthTextField, first+witch*30);
        jlabel.setFont(new Font("Jokerman", Font.PLAIN, 16));
        jlabel.setForeground(Color.WHITE);
        
        textField.setSize(widthTextField, heightTextField);
        textField.setLocation(logIn.getWidth()/2, first+witch*30);
        textField.setOpaque(false);
        textField.setBorder(border);
        textField.setToolTipText(toolTipText);
        textField.setFont(new Font("Jokerman", Font.PLAIN, 16));
        textField.addActionListener(this);
        textField.setForeground(Color.WHITE);
    }
    private int checkLoginAlert()
    {
        int alert=0;
        
        if(userText.getText().length()<=1)
        {
            userAlert.setToolTipText("Username is too short");
            userAlert.setVisible(true);
            alert++;
        }
        else
        {
            userAlert.setVisible(false);
        }
        
        if(passwordText.getText().length()<=7)
        {
            passwordAlert.setToolTipText("Password is too short");
            passwordAlert.setVisible(true);
            alert++;
        }
        else
        {
            passwordAlert.setVisible(false);
            if(!passwordText.getText().equals(verifyPasswordText.getText()))
            {
                passwordAlert.setToolTipText("Password and Verify Password isn't the same");
                passwordAlert.setVisible(true);
                alert++;
            
            }
            else
            {
                passwordAlert.setVisible(false);
            }
        }
        if(verifyPasswordText.getText().length()<=7)
        {
            verifyPasswordAlert.setToolTipText("Verify Password is too short");
            verifyPasswordAlert.setVisible(true);
            alert++;
        }
        else
        {
            verifyPasswordAlert.setVisible(false);
            if(!passwordText.getText().equals(verifyPasswordText.getText()))
            {
                verifyPasswordAlert.setToolTipText("Password and Verify Password isn't the same");
                verifyPasswordAlert.setVisible(true);
                alert++;
            
            }
            else
            {  
                verifyPasswordAlert.setVisible(false);
            }
        }
        
        return alert;
    }
    
    
    
    
}
