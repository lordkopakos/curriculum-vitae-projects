/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.Jattributes.text;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.JFormattedTextField;

public class InputComponent extends JFormattedTextField  {

        char tekstzklawiatury;
        String text;
        
        public InputComponent() {
            text = new String();
            setText();
            enableEvents(KeyEvent.KEY_EVENT_MASK |
                MouseEvent.MOUSE_EVENT_MASK |
                MouseEvent.MOUSE_MOTION_EVENT_MASK |
                MouseEvent.MOUSE_WHEEL_EVENT_MASK);
        }

        private void setText() {
            // Upewnij się, że nie wystąpi zakleszczenie.
            synchronized (getTreeLock()) {
                setText(text);
            }
            

        }

        @Override
        protected void processKeyEvent(KeyEvent e) {
           
            
            if (e.getID() == e.KEY_PRESSED && e.getKeyCode() != KeyEvent.VK_SHIFT && e.getKeyCode() != KeyEvent.VK_RIGHT&& e.getKeyCode() != KeyEvent.VK_ALT&& e.getKeyCode() != 17) {
                if(e.getKeyCode()==KeyEvent.VK_BACK_SPACE && text.length()>0)
                {
                    text=text.substring(0, text.length()-1);
                    setText();
                }
                
                else
                {
                
                tekstzklawiatury =e.getKeyChar();
                char x;
                x=tekstzklawiatury;
                text=text+x;
 
                if(e.getKeyCode()==KeyEvent.VK_BACK_SPACE)
                {
                    text="";
                }
                
                setText();
                }
                
            }
            
            e.consume();
        }

        private String toString(StringBuilder text) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public void fixDoubleText()
        {
            text="";
        }
        
    }