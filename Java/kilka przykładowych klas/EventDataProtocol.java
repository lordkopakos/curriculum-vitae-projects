/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.include.serverClient;

/**
 *
 * @author Dominik
 */
public class EventDataProtocol {
    
    private String username;
    private String playerType;
    private String playerX;
    private String playerY;
    private String score;
    private String state;
    private String map;
    private String direction;
    
    public String getUsername()
    {
        return this.username;
    }
    public String getPlayerType()
    {
        return this.playerType;
    }
    public String getPlayerX()
    {
        return this.playerX;
    }
    public String getPlayerY()
    {
        return this.playerY;
    }
    public String getScore()
    {
        return this.score;
    }
    public String getState()
    {
        return this.state;
    }
    public String getMap()
    {
        return this.map;
    }
    public String getDirection()
    {
        return this.direction;
    }
    
    public void setUsername(String username)
    {
        this.username=username;
    }
    public void setPlayerType(String playerType)
    {
        this.playerType=playerType;
    }
    public void setPlayerX(String playerX)
    {
        this.playerX=playerX;
    }
    public void setPlayerY(String playerY)
    {
        this.playerY=playerY;
    }
    public void setScore(String score)
    {
        this.score=score;
    }
    public void setState(String state)
    {
        this.state=state;
    }
    public void setMap(String map)
    {
        this.map=map;
    }
    public void setDirection(String direction)
    {
        this.direction=direction;
    }
    
    public EventDataProtocol(){}
    public EventDataProtocol(String username, String playerType, String playerX, String playerY,String score, String state, String map, String direction)
    {
        this.username=username;
        this.playerType=playerType;
        this.playerX=playerX;
        this.playerY=playerY;
        this.score=score;
        this.state=state;
        this.map=map;
        this.direction=direction;
    }
    
    public String getDateProtocol()
    {
        String returnData= getUsername()+":"+getPlayerType()+":"
                +getPlayerX()+":"+getPlayerY()+":"+getScore()+":"+getState()
                +":"+getMap()+":"+getDirection();
        return returnData;
        
    }
    
}
