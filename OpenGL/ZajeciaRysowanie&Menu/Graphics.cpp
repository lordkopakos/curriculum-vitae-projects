#include "Graphics.h"



void Graphics::createVboPosition()
{
	glGenBuffers(1, &position_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(position), position, GL_STATIC_DRAW);
}

void Graphics::createVboColours()
{
	glGenBuffers(1, &colour_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, colour_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colours), colours, GL_STATIC_DRAW);
}

void Graphics::createVao()
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, colour_vbo);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
}

void Graphics::setPosition()
{
	for (int i = 0; i < n * 3; i++)
	{
		cout << "Podaj punkt(x,y,z)" << endl;
		cin >> position[i];
		system("cls");
	}
}

void Graphics::setColours()
{
	for (int i = 0; i < n * 3; i++)
	{
		cout << "Podaj kolor(r,g,b)" << endl;
		cin >> colours[i];
		system("cls");
	}
}

Graphics::Graphics(int n)
{
	vao = 0;
	colour_vbo = 0;
	position_vbo = 0;
	this->n = n;
	position = new GLfloat[n*3];
	colours = new GLfloat[n*3];
}


Graphics::~Graphics()
{
}
