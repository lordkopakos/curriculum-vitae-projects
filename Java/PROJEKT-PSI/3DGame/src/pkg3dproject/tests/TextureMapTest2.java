/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3dproject.tests;

import java.awt.*;
import java.awt.image.*;

import pkg3dproject.math3D.*;
import pkg3dproject.graphics3D.*;
import pkg3dproject.graphics3D.texture.*;
import pkg3dproject.core.GameCore3D;
import pkg3dproject.math3D.SolidPolygon3D;

public class TextureMapTest2 extends GameCore3D {

    public static void main(String[] args) {
        new TextureMapTest2().run();
    }

    public void init() {
        init(HIG_RES_MODES);
    }

    // utwórz dom (wypukłą bryłę)
    public void createPolygons() {

        // utwórz tekstury Texture
        Texture wall = loadTexture("images/wall1.png");
        Texture roof = loadTexture("images/roof1.png");

        TexturedPolygon3D poly;

        SolidPolygon3D spoly;
        
        // ściany
        poly = new TexturedPolygon3D(
                new Vector3D(-200, 250, -1000),
                new Vector3D(-200, 0, -1000),
                new Vector3D(200, 0, -1000),
                new Vector3D(200, 250, -1000));
        setTexture(poly, wall);
        polygons.add(poly);

        poly = new TexturedPolygon3D(
                new Vector3D(200, 250, -1400),
                new Vector3D(200, 0, -1400),
                new Vector3D(-200, 0, -1400),
                new Vector3D(-200, 250, -1400));
        setTexture(poly, wall);
        polygons.add(poly);

        poly = new TexturedPolygon3D(
                new Vector3D(-200, 250, -1400),
                new Vector3D(-200, 0, -1400),
                new Vector3D(-200, 0, -1000),
                new Vector3D(-200, 250, -1000));
        setTexture(poly, wall);
        polygons.add(poly);

        poly = new TexturedPolygon3D(
                new Vector3D(200, 250, -1000),
                new Vector3D(200, 0, -1000),
                new Vector3D(200, 0, -1400),
                new Vector3D(200, 250, -1400));
        setTexture(poly, wall);
        polygons.add(poly);

        // dach
        poly = new TexturedPolygon3D(
                new Vector3D(-200, 250, -1000),
                new Vector3D(200, 250, -1000),
                new Vector3D(75, 400, -1200),
                new Vector3D(-75, 400, -1200));
        setTexture(poly, roof);
        polygons.add(poly);

        poly = new TexturedPolygon3D(
                new Vector3D(-200, 250, -1400),
                new Vector3D(-200, 250, -1000),
                new Vector3D(-75, 400, -1200));
        setTexture(poly, roof);
        polygons.add(poly);

        poly = new TexturedPolygon3D(
                new Vector3D(200, 250, -1400),
                new Vector3D(-200, 250, -1400),
                new Vector3D(-75, 400, -1200),
                new Vector3D(75, 400, -1200));
        setTexture(poly, roof);
        polygons.add(poly);

        poly = new TexturedPolygon3D(
                new Vector3D(200, 250, -1000),
                new Vector3D(200, 250, -1400),
                new Vector3D(75, 400, -1200));
        setTexture(poly, roof);
        polygons.add(poly);
        
        // drzwi i okna
        spoly = new SolidPolygon3D(
                new Vector3D(0, 0, -1000),
                new Vector3D(75, 0, -1000),
                new Vector3D(75, 125, -1000),
                new Vector3D(0, 125, -1000));
        spoly.setColor(Color.DARK_GRAY);
        polygons.add(spoly);
        spoly = new SolidPolygon3D(
                new Vector3D(-150, 150, -1000),
                new Vector3D(-100, 150, -1000),
                new Vector3D(-100, 200, -1000),
                new Vector3D(-150, 200, -1000));
        spoly.setColor(Color.ORANGE);
        polygons.add(spoly);
    }

    public void setTexture(TexturedPolygon3D poly,
            Texture texture) {
        Vector3D origin = poly.getVertex(0);

        Vector3D dv = new Vector3D(poly.getVertex(1));
        dv.subtract(origin);

        Vector3D du = new Vector3D();
        du.setToCrossProduct(poly.getNormal(), dv);

        Rectangle3D textureBounds = new Rectangle3D(origin, du, dv,
                texture.getWidth(), texture.getHeight());

        poly.setTexture(texture, textureBounds);
    }

    public Texture loadTexture(String imageName) {
        return Texture.createTexture(imageName);
    }

    public void createPolygonRenderer() {
        viewWindow = new ViewWindow(0, 0,
                screen.getWidth(), screen.getHeight(),
                (float) Math.toRadians(75));

        Transform3D camera = new Transform3D(0, 100, 0);
        polygonRenderer = new FastTexturedPolygonRenderer(
                camera, viewWindow);
    }
    

}
