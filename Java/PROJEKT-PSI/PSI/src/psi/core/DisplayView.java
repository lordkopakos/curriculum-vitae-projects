/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.core;

import psi.graphics.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import psi.core.ProgramCore;
import psi.layer.Layer;
import psi.network.Network;

/**
 *
 * @author PC
 */
public class DisplayView extends ProgramCore {
   
    public static void main(String[] args) {
        new DisplayView().menurun();
    }
    
    
    private Image background;
    private Sprite menuLogo;
    
    
    
    int screenWidth;
    int screenHeight;

    
    
    @Override
    public void menuinit()
    {
        super.menuinit();
        
        screenWidth=screen.getWidth();
        screenHeight=screen.getHeight();
        background=loadImage("menuBackground1920.jpg");
        loadLogoSprite();
        
    }
    @Override
     public void menurun()
    {
        super.menurun();
    }
    
    public void setMenuLogo(Sprite menuLogo)
    {
        this.menuLogo=menuLogo;
    }
    
    /*
        Ustawienie tła do narysowania.
    */

    /*
        Rysowanie obiektu TileMap.
    */
    @Override
    public void draw(Graphics2D g)
    {
        if (background == null ||
            screenHeight > background.getHeight(null))
        {
            g.setColor(Color.black);
            g.fillRect(0, 0, screenWidth, screenHeight);
        }
        g.setColor(Color.black);
         g.fillRect(0, 0, screenWidth, screenHeight);
        g.drawImage(background, 0, 0, null);
        g.drawImage(menuLogo.getImage(), screenWidth/2-menuLogo.getWidth()/2, screenHeight/2-menuLogo.getHeight()/2, null);
    
    }
    /*
        Pobranie rysunków z katalogu images/.
    */
    @Override
    public Image loadImage(String name) {
        String filename = "images/" + name;
        return new ImageIcon(filename).getImage();
    }
    
    public void loadLogoSprite()
    {
        ArrayList<Image> images =new ArrayList<>(24);
        for(int i=10; i<34; i++){
            String number=Integer.toString(i);
            Image image= loadImage("backgroundNeural/backgroundNeural_0"+number+".png");
            images.add(image);
        }
        Animation logoAnimation = new Animation();
        logoAnimation=createLogo(images);
        
        menuLogo=new Sprite(logoAnimation);
        
        
    }
    private Animation createLogo(ArrayList<Image> img1)
    {
        Animation anim = new Animation();
        img1.stream().forEach((img11) -> {
            anim.addFrame(img11, 100);
        });
        for(int i=(img1.size()-1); i>=0; i--){
            anim.addFrame(img1.get(i), 100);
        }
        return anim;
    }
 
    @Override
    public void update(long elapsedTime) {
        menuLogo.update(elapsedTime);
    }
}
