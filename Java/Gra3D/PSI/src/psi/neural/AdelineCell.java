package psi.neural;

import static java.lang.Math.pow;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import psi.network.VectorX;

public class AdelineCell extends NeuralCell implements ActivationFunctions{

    private double beta;

    private double errorSignal;

    private double learningCoefficient;
    
    private double weightFrom, 
                    weightTo;

    
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    public enum Function {
        BIPOLAR_SIGMOID_FUNCTION,
        UNIPOLAR_SIGMOID_FUNCTION,
        BIPOLAR_THRESHOLD_FUNCTION,
        UNIPOLAR_THRESHOLD_FUNCTION
    }

    Function state;

    public AdelineCell() {
        this(Function.BIPOLAR_THRESHOLD_FUNCTION, 1.0, 0.1);
    }

    /*
		 * Kontruktor klasy.
     */
    public AdelineCell(Function state, double beta, double learningCoefficient) {
        super();
        this.beta = beta;
        this.state = state;
        this.learningCoefficient = learningCoefficient;
    }

    /*
		 * Metoda do przesłonięcia, aby uzyskać unikalne wyjście obliczeń. Domyślnie ma charakter liniowy.
		 * @param membranePotential
		 * @return	Dane po obróbce finalnych kalkulacji.
     */
    @Override
    public double finalizeData(double membranePotential) {
        if (state == Function.BIPOLAR_SIGMOID_FUNCTION) {
            return bipolarSigmoidFunction(membranePotential);
        } else if (state == Function.UNIPOLAR_SIGMOID_FUNCTION) {
            return unipolarSigmoidFunction(membranePotential);
        } else if (state == Function.BIPOLAR_THRESHOLD_FUNCTION) {
            return bipolarThresholdFunction(membranePotential);
        } else if (state == Function.UNIPOLAR_THRESHOLD_FUNCTION) {
            return unipolarThresholdFunction(membranePotential);
        } else {
            System.out.println("Podana funkcja nie jest obsługiwana przez program, "
                    + "użyję bipolarnej funkcji progowej");
            return bipolarSigmoidFunction(membranePotential);
        }
    }

    //Funkcje aktywacji
    @Override
    public double bipolarSigmoidFunction(double membranePotential) {
        return (1 - (1 + pow(Math.E, (-beta * membranePotential)))
                / (1 + pow(Math.E, (-beta * membranePotential))));
    }

    @Override
    public double unipolarSigmoidFunction(double membranePotential) {
        return (1
                / (1 + pow(Math.E, (-beta * membranePotential))));
    }

    @Override
    public double bipolarThresholdFunction(double membranePotential) {
        if (membranePotential > 0) {
            return 1;
        } else if (membranePotential < 0) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public double unipolarThresholdFunction(double membranePotential) {
        if (membranePotential > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public void learnNeuralCell(double y, VectorX vectorX) {
        double error = vectorX.getZ() - y;
        this.errorSignal = error;
        for (int i = 0; i < vectorX.getLength(); i++) {
            this.setInputWeight(i, this.getInputWeight(i) + this.getInputData(i) * learningCoefficient * error);
        }
    }

    public void setRandomWeight(double from, double to) {
        setBorderRandomWeight(from, to);
        double difference = Math.abs(this.weightFrom) + Math.abs(this.weightTo);
        for (int i = 0; i < Synapses.size(); i++) {
            Random random = new Random();
            double weight = random.nextDouble() * difference + this.weightFrom;
            setInputWeight(i, weight);
        }
    }
    
    public void setBorderRandomWeight(double from, double to){
        this.weightFrom=from;
        this.weightTo=to;
    }

    //Funkcje do pobierania i ustawiania stanu
    public Function getState() {
        return state;
    }

    public void setState(Function state) {
        this.state = state;
    }

    //Funkcje do pobierania i ustawiania błędu sygnału
    public double getErrorSignal() {
        return errorSignal;
    }

    public void setErrorSignal(double errorSignal) {
        this.errorSignal = errorSignal;
    }

    //Funkcje do pobierania i ustawiania bety
    public double getBeta() {
        return beta;
    }

    public void setBeta(double beta) {
        this.beta = beta;
    }

    public double getLearningCoefficient() {
        return this.learningCoefficient;
    }

    public void setLearningCoefficient(double learningCoefficient) {
        this.learningCoefficient = learningCoefficient;
    }
    

    public void showWeight() {
        for (int i = 0; i < Synapses.size(); i++) {
            System.out.println("Waga " + i + ": " + getInputWeight(i));
        }
    }

}
