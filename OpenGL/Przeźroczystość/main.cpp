#include<GL/glew.h>
#include<GLFW/glfw3.h>

#include<glm.hpp>
#include<gtc/matrix_transform.hpp>
#include<gtc\type_ptr.hpp>

#include "materials.h"
#include<iostream>
#include "glut.h"
#include<fstream>
#include<string>
using namespace std;
enum
{
	// obszar renderingu
	FULL_WINDOW, // aspekt obrazu - ca�e okno
	ASPECT_1_1, // aspekt obrazu 1:1
	EXIT // wyj�cie
};
struct wezel
{
	double x, y, z, id, srednica;
};
wezel * tablica;
// aspekt obrazu

int aspect = FULL_WINDOW;

// rozmiary bry�y obcinania

const GLdouble leftx = -2.0;
const GLdouble rightx = 2.0;
const GLdouble bottomx = -2.0;
const GLdouble topx = 2.0;
const GLdouble nearx = 3.0;
const GLdouble farx = 12.0;

// k�ty obrotu obiekt�w sceny

GLfloat rotatex = 0.0;
GLfloat rotatey = 0.0;

// wska�nik naci�ni�cia lewego przycisku myszki

int button_state = GLUT_UP;

// po�o�enie kursora myszki

int button_x, button_y;

// identyfikatory list wy�wietlania

GLint GOLD_SPHERE;
GLint RUBY_SPHERE;

// funkcja generuj�ca scen� 3D

void Display()
{
	// kolor t�a - zawarto�� bufora koloru
	glClearColor(1.0, 1.0, 1.0, 1.0);

	// czyszczenie bufora koloru i bufora g��boko�ci
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// wyb�r macierzy modelowania
	glMatrixMode(GL_MODELVIEW);

	// macierz modelowania = macierz jednostkowa
	glLoadIdentity();

	// w��czenie testu bufora g��boko�ci
	glEnable(GL_DEPTH_TEST);

	// w��czenie o�wietlenia
	glEnable(GL_LIGHTING);

	// w��czenie �wiat�a GL_LIGHT0 z parametrami domy�lnymi
	glEnable(GL_LIGHT0);

	// o�wietlenie tylko przednich stron wielok�t�w
	glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);

	// przesuni�cie uk�adu wsp�rz�dnych obiektu do �rodka bry�y odcinania
	glTranslatef(0, 0, -(nearx + farx) / 2);

	// obroty obiekt�w sceny
	glRotatef(rotatex, 1.0, 0.0, 0.0);
	glRotatef(rotatey, 0.0, 1.0, 0.0);

	

	for (int i = 0; i < 27; i++)
	{
		

		glPushMatrix();

		glTranslatef(tablica[i].x, tablica[i].y, tablica[i].z);

		glCallList(GOLD_SPHERE);

		glPopMatrix();
	}





	// o�wietlenie przednich i tylnych stron wielok�t�w
	glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	// w��czenie mieszania kolor�w
	glEnable(GL_BLEND);

	// wsp�czynniki mieszania kolor�w
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// wy��czenie zapisu wsp�rz�dnych z do bufora g��boko�ci
	glDepthMask(GL_FALSE);

	// od�o�enie macierzy modelowania na stos
	glPushMatrix();

	// czwarta kula - rubinowa
	glCallList(RUBY_SPHERE);

	// zdj�cie macierzy modelowania ze stosu
	glPopMatrix();

	// w��czenie zapisu wsp�rz�dnych z do bufora g��boko�ci
	glDepthMask(GL_TRUE);

	// skierowanie polece� do wykonania
	glFlush();

	// zamiana bufor�w koloru
	glutSwapBuffers();
}

// zmiana wielko�ci okna

void Reshape(int width, int height)
{
	// obszar renderingu - ca�e okno
	glViewport(0, 0, width, height);

	// wyb�r macierzy rzutowania
	glMatrixMode(GL_PROJECTION);

	// macierz rzutowania = macierz jednostkowa
	glLoadIdentity();

	// parametry bry�y obcinania
	if (aspect == ASPECT_1_1)
	{
		// wysoko�� okna wi�ksza od wysoko�ci okna
		if (width < height && width > 0)
			glFrustum(leftx, rightx, bottomx * height / width, topx * height / width, nearx, farx);
		else

			// szeroko�� okna wi�ksza lub r�wna wysoko�ci okna
			if (width >= height && height > 0)
				glFrustum(leftx * width / height, rightx * width / height, bottomx, topx, nearx, farx);

	}
	else
		glFrustum(leftx, rightx, bottomx, topx, nearx, farx);

	// generowanie sceny 3D
	Display();
}

// obs�uga przycisk�w myszki

void MouseButton(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		// zapami�tanie stanu lewego przycisku myszki
		button_state = state;

		// zapami�tanie po�o�enia kursora myszki
		if (state == GLUT_DOWN)
		{
			button_x = x;
			button_y = y;
		}
	}
}

// obs�uga ruchu kursora myszki

void MouseMotion(int x, int y)
{
	if (button_state == GLUT_DOWN)
	{
		rotatey += 30 * (rightx - leftx) / glutGet(GLUT_WINDOW_WIDTH) *(x - button_x);
		button_x = x;
		rotatex -= 30 * (topx - bottomx) / glutGet(GLUT_WINDOW_HEIGHT) *(button_y - y);
		button_y = y;
		glutPostRedisplay();
	}
}

// obs�uga menu podr�cznego

void Menu(int value)
{
	switch (value)
	{
		// obszar renderingu - ca�e okno
	case FULL_WINDOW:
		aspect = FULL_WINDOW;
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
		break;

		// obszar renderingu - aspekt 1:1
	case ASPECT_1_1:
		aspect = ASPECT_1_1;
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
		break;

		// wyj�cie
	case EXIT:
		exit(0);
	}
}

// utworzenie list wy�wietlania
const GLfloat*a;
const GLfloat*b;
const GLfloat*c;
GLfloat d;
void GenerateDisplayLists()
{
	// generowanie identyfikatora pierwszej listy wy�wietlania
	GOLD_SPHERE = glGenLists(1);

	// pierwsza lista wy�wietlania - z�ota kula
	glNewList(GOLD_SPHERE, GL_COMPILE);

	// ustawnienie materia�u - obiekt nieprzezroczysty
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, a);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, b);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, c);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, d);

	// narysowanie nieprzezroczystej kuli
	glutSolidSphere(0.8, 20, 20);

	// koniec pierwszej listy wy�wietlania
	glEndList();

	
}

int main(int argc, char * argv[])
{
	
	
	fstream plik;
	plik.open("dane_box.txt", ios::in);
	if (!plik.good())
	{
		cout << "Nie da sie otworzyc pliku!" << endl;
		system("pause");
		return 0;
	}
	int ileJestDanych = 0;
	string pom = "";
	while (!plik.eof())
	{
		getline(plik, pom);
		if (pom != "")
		{
			ileJestDanych++;
		}
	}
	plik.close();



	tablica = new wezel[ileJestDanych + 1];

	plik.open("dane_box.txt", ios::in);
	if (!plik.good())
	{
		cout << "Nie da sie otworzyc pliku!" << endl;
		system("pause");
		return 0;
	}
	int numer = 0;
	while (!plik.eof())
	{
		string linijka;
		getline(plik, linijka);
		string tab[5] = { "","","","","" };
		for (int i = 0, p = 0; i < linijka.size(); i++)
		{
			if (linijka[i] == ',')
			{
				p++;
			}
			else
			{
				tab[p] += linijka[i];
			}
		}

		if (numer < ileJestDanych)
		{
			cout << numer << endl;
			tablica[numer].id = stod(tab[0]);
			tablica[numer].x = stod(tab[1]);
			tablica[numer].y = stod(tab[2]);
			tablica[numer].z = stod(tab[3]);
			tablica[numer].srednica = stod(tab[4]);
			cout << tablica[numer].id << tablica[numer].x << tablica[numer].y << tablica[numer].z << tablica[numer].srednica<<endl;
		}
		numer++;
	}

	plik.close();

	int option;

	cout << "Co chcesz narysowac?" << endl;
	cout << "NR.1 Przezroczysty rubinowy kwadrat z kul" << endl;
	cout << "NR.2 Nieprzezroczysty chromowany kwadrat z kul" << endl;
	
	cin >> option;


	if (option == 1)
	{
		a = RubyAmbient;
		b = RubyDiffuse;
		c = RubySpecular;
		d = RubyShininess;
	}
	else
	{
		a = ChromeAmbient;
		b = ChromeDiffuse;
		c = ChromeSpecular;
		d = ChromeShininess;
	}


	// inicjalizacja biblioteki GLUT
	glutInit(&argc, argv);

	// inicjalizacja bufora ramki
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

	// rozmiary g��wnego okna programu
	glutInitWindowSize(500, 500);

	// utworzenie g��wnego okna programu
#ifdef WIN32

	glutCreateWindow("Przezroczysto��");
#else

	glutCreateWindow("Przezroczystosc");
#endif

	// do��czenie funkcji generuj�cej scen� 3D
	glutDisplayFunc(Display);

	// do��czenie funkcji wywo�ywanej przy zmianie rozmiaru okna
	glutReshapeFunc(Reshape);

	// obs�uga przycisk�w myszki
	glutMouseFunc(MouseButton);

	// obs�uga ruchu kursora myszki
	glutMotionFunc(MouseMotion);

	// utworzenie menu podr�cznego
	glutCreateMenu(Menu);

	// utworzenie podmenu - Aspekt obrazu
	int MenuAspect = glutCreateMenu(Menu);
#ifdef WIN32

	glutAddMenuEntry("Aspekt obrazu - ca�e okno", FULL_WINDOW);
#else

	glutAddMenuEntry("Aspekt obrazu - cale okno", FULL_WINDOW);
#endif

	glutAddMenuEntry("Aspekt obrazu 1:1", ASPECT_1_1);

	// menu g��wne
	glutCreateMenu(Menu);
	glutAddSubMenu("Aspekt obrazu", MenuAspect);
#ifdef WIN32

	glutAddMenuEntry("Wyj�cie", EXIT);
#else

	glutAddMenuEntry("Wyjscie", EXIT);
#endif

	// okre�lenie przycisku myszki obs�uguj�cej menu podr�czne
	glutAttachMenu(GLUT_RIGHT_BUTTON);

	// utworzenie list wy�wietlania
	GenerateDisplayLists();

	// wprowadzenie programu do obs�ugi p�tli komunikat�w
	glutMainLoop();
	return 0;
}