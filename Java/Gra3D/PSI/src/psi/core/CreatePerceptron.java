package psi.core;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Transparency;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicComboBoxUI;
import psi.Jattributes.ImagePanel;
import psi.Jattributes.Input;
import psi.Jattributes.text.InputComponent;
import psi.network.VectorX;
import psi.neural.AdelineCell;

/**
 *
 * @author lordkopakos
 */
public class CreatePerceptron extends SelectNetwork {

    public static void main(String[] args) {
        new CreatePerceptron().menurun();
    }

    private ImagePanel plPerceptron;
    private JButton btnClose;
    private JLabel lbTitlePerceptron;
    private ImageIcon imgTitle;

    //Options
    private ImagePanel plOptions;
    private int first;
    private JComboBox cbTrainingSet;
    private JComboBox cbValidSet;
    private JComboBox cbTestSet;
    private JButton btnLearn;
    private JButton btnValid;
    private JButton btnTest;

    //Construction of Neural
    private ImagePanel plNeuralConstruction;
    private JButton btnNeuralCell;
    private Input xAndWeight;
    private int elementsXAndWeight;
    private int sizeXAndWeight;
    private JLabel lbY;
    private JLabel lbZ;
    private InputComponent txtY;
    private InputComponent txtZ;
    

    //Neural functions panel.
    private ImagePanel plFunctions;
    private JButton btnMSE;
    private JButton btnMAPE;

    //Neural parametrs
    private ImagePanel plParametrs;
    private JButton btnCloseParametrs;
    private JButton btnSetParametrs;
    private ImageIcon imgBias;
    private ImageIcon imgBeta;
    private JLabel lbBias;
    private JLabel lbBeta;
    private InputComponent txtBias;
    private InputComponent txtBeta;
    private InputComponent txtRandomWeightFrom;
    private InputComponent txtRandomWeightTo;
    private int widthLabel, widthText, height;

    //Activation Function
    private JButton btnBipolarSigmoidal;
    private JButton btnUnipolarSigmoidal;
    private JButton btnBipolarThreshold;
    private JButton btnUnipolarThreshold;

    //Perceptron learning
    private AdelineCell perceptron;
    private boolean perceptronVectorInit;

    //Training and Valid set
    private ArrayList<VectorX> vectors;
    private String filePath;
    private String fileName;

    //Graph
    private JButton btnCloseGraphMSE;
    private JButton btnCloseGraphMAPE;
    private ImagePanel plGraphMSE;
    private ImagePanel plGraphMAPE;
    private static XYGraph graphMSE;
    private static XYGraph graphMAPE;
    private ArrayList<Integer> errorMAPE;
    private ArrayList<Integer> errorMSE;
    
    //messages
    private JButton btnCloseFinishedLearning;
    private ImagePanel plFinishedLearning;
    private JButton btnCloseFinishedValid;
    private ImagePanel plFinishedValid;
    private JLabel lbGoodAnswers;

    @Override
    public void menurun() {
        super.menurun();
    }

    @Override
    public void menuinit() {
        super.menuinit();

        ///////////////////       PERCEPTRON          //////////////////////////
        ////////////////////////////////////////////////////////////////////////
        perceptron = new AdelineCell();
        perceptronVectorInit=false;
        perceptron.setBias(0);
        perceptron.setBeta(0);
        errorMAPE=new ArrayList<>(1000);
        errorMSE=new ArrayList<>(1000);

        ///////////////////           GUI           ////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        first = 180;

        Border border
                = BorderFactory.createLineBorder(new Color((float) 0.9, (float) 0.4, (float) 0.02), 5);

        //Panele używane w wizualizacji perceptronu
        plPerceptron = new ImagePanel("images/attributes/backgroundNetwork.jpg");
        setPanel(plPerceptron, border, false);

        plOptions = new ImagePanel("images/attributes/optionsPanel.png");
        setPanel(plOptions, border, true);

        plNeuralConstruction = new ImagePanel("images/attributes/neuralPanel.png");
        setPanel(plNeuralConstruction, border, true);

        plFunctions = new ImagePanel("images/attributes/functionsPanel.png");
        setPanel(plFunctions, border, true);

        plParametrs = new ImagePanel("images/attributes/parametrsPanel.png");
        setPanel(plParametrs, border, false);
        
        plFinishedLearning = new ImagePanel("images/attributes/goodlearningPanel.png");
        setPanel(plFinishedLearning, border, false);
        
        plFinishedValid = new ImagePanel("images/attributes/validPanel.png");
        setPanel(plFinishedValid, border, false);
        
        plFinishedValid = new ImagePanel("images/attributes/validPanel.png");
        setPanel(plFinishedValid, border, false);
        
        plGraphMSE = new ImagePanel("images/attributes/msePanel.png");
        setPanel(plGraphMSE, border, false);
        
        plGraphMAPE = new ImagePanel("images/attributes/mapePanel.png");
        setPanel(plGraphMAPE, border, false);

        //Tytuł panelu
        imgTitle = new ImageIcon("images/menu/main/titlePerceptron.png");
        lbTitlePerceptron = new JLabel(imgTitle);
        lbTitlePerceptron.setSize(900, 150);
        lbTitlePerceptron.setLocation(plOptions.getWidth() + 100, plPerceptron.getHeight() / 2 - plOptions.getHeight() / 2);
        
        lbGoodAnswers = new JLabel("www");
        lbGoodAnswers.setSize(140, 100);
        lbGoodAnswers.setLocation(plFinishedValid.getWidth()-lbGoodAnswers.getWidth()-10, plFinishedValid.getHeight()-lbGoodAnswers.getHeight()-5);
        lbGoodAnswers.setBackground(new Color((float)0.5, (float)0.8, (float)0.1));
        lbGoodAnswers.setForeground(new Color((float)0.5, (float)0.8, (float)0.1));
        lbGoodAnswers.setFont(new Font("Gill Sans Ultra Bold", Font.PLAIN, 40));
        
        //Obrazki w zmianie parametrów BIAS
        imgBias = new ImageIcon("images/menu/main/parametrs/bias.png");
        lbBias= new JLabel(imgBias);
        txtBias= new InputComponent();
        widthLabel=30; widthText=30; height=30;
        txtBias.setText("0");
        setFieldWithLabel(lbBias, txtBias, Color.yellow, border, "Bias", plParametrs.getWidth()/3-(widthLabel+widthText)/2, 120, widthLabel, widthText, height, true);

        //Obrazki w zmianie parametrów BETA
        imgBeta = new ImageIcon("images/menu/main/parametrs/beta.png");
        lbBeta= new JLabel(imgBeta);
        txtBeta= new InputComponent();
        widthLabel=30; widthText=30; height=30;
        setFieldWithLabel(lbBeta, txtBeta, Color.yellow, border, "Beta", 2*plParametrs.getWidth()/3-(widthLabel+widthText)/2, 120, widthLabel, widthText, height, true);
        txtBeta.setText("0");
        
        //Random Weight
        txtRandomWeightFrom=new InputComponent();
        setField(txtRandomWeightFrom, Color.yellow, border, "Beginning of the interval", plParametrs.getWidth()/4-(widthText*3)/2, 250, widthText*3, height, true);
        
        txtRandomWeightTo=new InputComponent();
        setField(txtRandomWeightTo, Color.yellow, border, "Ending of the interval", 3*plParametrs.getWidth()/4-(widthText*3)/2, 250, widthText*3, height, true);
        
        //Wartości oczekiwane i wyjściowe
        widthLabel=30; widthText=50; height=50;
        lbY=new JLabel("Y:");
        txtY=new InputComponent();
        setFieldWithLabel(lbY, txtY, Color.yellow, border, "Y:", plNeuralConstruction.getWidth()-(widthLabel+widthText)-20, plNeuralConstruction.getHeight()/2-height*2, widthLabel, widthText, height, false);
        
        lbZ=new JLabel("Z:");
        txtZ=new InputComponent();
        setFieldWithLabel(lbZ, txtZ, Color.yellow, border, "Z:", plNeuralConstruction.getWidth()-(widthLabel+widthText)-20, plNeuralConstruction.getHeight()/2+height*2, widthLabel, widthText, height, false);
        
        //Rysunek Neuronu
        btnNeuralCell = createButton("/neuralCell/neural", "Select a neural parametrs");
        btnNeuralCell.setSize(600, 463);
        btnNeuralCell.setLocation(plNeuralConstruction.getWidth() / 2 - btnNeuralCell.getWidth() / 2, plNeuralConstruction.getHeight() / 2 - btnNeuralCell.getHeight() / 2);

        //Tworzenie przycisków
        btnClose = createButton("close", "Zamknij");
        btnClose.setSize(27, 24);
        btnClose.setLocation(plPerceptron.getWidth() - btnClose.getWidth() - 10, 10);
        
        btnCloseGraphMAPE = createButton("close", "Zamknij");
        btnCloseGraphMAPE.setSize(27, 24);
        btnCloseGraphMAPE.setLocation(plGraphMAPE.getWidth() - btnCloseGraphMAPE.getWidth() - 10, 10);
        
        btnCloseGraphMSE = createButton("close", "Zamknij");
        btnCloseGraphMSE.setSize(27, 24);
        btnCloseGraphMSE.setLocation(plGraphMSE.getWidth() - btnCloseGraphMSE.getWidth() - 10, 10);

        btnCloseParametrs = createButton("close", "Zamknij");
        btnCloseParametrs.setSize(27, 24);
        btnCloseParametrs.setLocation(plParametrs.getWidth() - btnCloseParametrs.getWidth() - 10, 10);
        
        btnCloseFinishedLearning = createButton("close", "Zamknij");
        btnCloseFinishedLearning.setSize(27, 24);
        btnCloseFinishedLearning.setLocation(plFinishedLearning.getWidth() - btnCloseFinishedLearning.getWidth() - 10, 10);
        
        btnCloseFinishedValid = createButton("close", "Zamknij");
        btnCloseFinishedValid.setSize(27, 24);
        btnCloseFinishedValid.setLocation(plFinishedValid.getWidth() - btnCloseFinishedValid.getWidth() - 10, 10);
        
        btnSetParametrs = createButton("parametrs/set", "Zamknij");
        btnSetParametrs.setSize(121, 50);
        btnSetParametrs.setLocation(plParametrs.getWidth()/2 - btnSetParametrs.getWidth()/2, plParametrs.getHeight()-btnSetParametrs.getHeight()-20);

        btnLearn = createButton("learn", "Zamknij");
        btnLearn.setSize(188, 70);
        btnLearn.setLocation(plOptions.getWidth() / 2 - btnLearn.getWidth() / 2, 600);
        btnLearn.setVisible(false);

        btnValid = createButton("valid", "Zamknij");
        btnValid.setSize(182, 70);
        btnValid.setLocation(plOptions.getWidth() / 2 - btnValid.getWidth() / 2, 600);
        btnValid.setVisible(false);

        btnTest = createButton("test", "Zamknij");
        btnTest.setSize(170, 70);
        btnTest.setLocation(plOptions.getWidth() / 2 - btnValid.getWidth() / 2, 600);
        btnTest.setVisible(false);
        
        btnMSE = createButton("functions/mse", "Zamknij");
        btnMSE.setSize(121, 50);
        btnMSE.setLocation(plFunctions.getWidth() / 2 - btnMSE.getWidth() / 2, 120);
        
        btnMAPE = createButton("functions/mape", "Zamknij");
        btnMAPE.setSize(121, 50);
        btnMAPE.setLocation(plFunctions.getWidth() / 2 - btnMAPE.getWidth() / 2, 180);

        //Funkcje Aktywacji
        btnBipolarSigmoidal = createButton("/activFunction/bipolarSigmoidal", "Select an activation function");
        btnBipolarSigmoidal.setSize(274, 243);
        btnBipolarSigmoidal.setLocation(plOptions.getWidth() + plNeuralConstruction.getWidth() + 150, plPerceptron.getHeight() / 2 - plOptions.getHeight() / 2);

        btnUnipolarSigmoidal = createButton("/activFunction/unipolarSigmoidal", "Select an activation function");
        btnUnipolarSigmoidal.setSize(274, 243);
        btnUnipolarSigmoidal.setLocation(plOptions.getWidth() + plNeuralConstruction.getWidth() + 150, plPerceptron.getHeight() / 2 - plOptions.getHeight() / 2);
        btnUnipolarSigmoidal.setVisible(false);

        btnBipolarThreshold = createButton("/activFunction/bipolarThreshold", "Select an activation function");
        btnBipolarThreshold.setSize(274, 243);
        btnBipolarThreshold.setLocation(plOptions.getWidth() + plNeuralConstruction.getWidth() + 150, plPerceptron.getHeight() / 2 - plOptions.getHeight() / 2);
        btnBipolarThreshold.setVisible(false);

        btnUnipolarThreshold = createButton("/activFunction/unipolarThreshold", "Select an activation function");
        btnUnipolarThreshold.setSize(274, 243);
        btnUnipolarThreshold.setLocation(plOptions.getWidth() + plNeuralConstruction.getWidth() + 150, plPerceptron.getHeight() / 2 - plOptions.getHeight() / 2);
        btnUnipolarThreshold.setVisible(false);

        elementsXAndWeight = 13;
        sizeXAndWeight = 50;
        xAndWeight = new Input(elementsXAndWeight);
        for (int i = 0; i < elementsXAndWeight; i++) {
            xAndWeight.addVector(i, sizeXAndWeight, sizeXAndWeight, 20, sizeXAndWeight * i);
        }

        //Wybór zbiorów
        cbTrainingSet = new JComboBox();
        comboSet(0, 200, 50, border, "Select TrainingSet", cbTrainingSet, plOptions);
        cbTrainingSet.addActionListener((ActionEvent e) -> {
            JComboBox combo = (JComboBox) e.getSource();
            System.out.println(combo.getSelectedItem());
            fileName = combo.getSelectedItem().toString();
            filePath = "learn/trainingSet/";

            new Thread() {
                @Override
                public void run() {
                    
                    try {
                        loadTrainingSet(fileName);
                        
                        perceptronVectorInit=true;
                        System.out.println("Ile Inputów dodaję:"+vectors.get(0).getLength()+"\n");
                        perceptron.addInput(vectors.get(0).getLength());
                        
                        drawInput();
                        
                        showZAndY();
                        
                        cleanWeight();

                    } catch (IOException ex) {
                        Logger.getLogger(CreatePerceptron.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }.start();

            btnLearn.setVisible(true);
            btnValid.setVisible(false);
            btnTest.setVisible(false);
        });

        cbValidSet = new JComboBox();
        comboSet(1, 200, 50, border, "Select ValidSet", cbValidSet, plOptions);
        cbValidSet.addActionListener((ActionEvent e) -> {
            JComboBox combo = (JComboBox) e.getSource();
            System.out.println(combo.getSelectedItem());
            fileName = combo.getSelectedItem().toString();
            filePath = "learn/validSet/";

            new Thread() {
                @Override
                public void run() {

                    try {
                        loadTrainingSet(fileName);

                        perceptronVectorInit=true; 
                        
                        drawInput();
                        
                        showZAndY();

                    } catch (IOException ex) {
                        Logger.getLogger(CreatePerceptron.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }.start();

            btnLearn.setVisible(false);
            btnValid.setVisible(true);
            btnTest.setVisible(false);
        });

        cbTestSet = new JComboBox();
        comboSet(2, 200, 50, border, "Select TestSet", cbTestSet, plOptions);
        cbTestSet.addActionListener((ActionEvent e) -> {
            JComboBox combo = (JComboBox) e.getSource();
            System.out.println(combo.getSelectedItem());
            fileName = combo.getSelectedItem().toString();

            new Thread() {
                @Override
                public void run() {

                    try {
                        loadTrainingSet(fileName);
                        
                        perceptronVectorInit=true; 

                        drawInput();
                        
                        showZAndY();

                    } catch (IOException ex) {
                        Logger.getLogger(CreatePerceptron.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }.start();

            filePath = "learn/testSet/";
            btnLearn.setVisible(false);
            btnValid.setVisible(false);
            btnTest.setVisible(true);
        });

        //GRAPH
        
        graphMAPE = new XYGraph();
		graphMAPE.setBorder(new LineBorder(new Color(0, 0, 0)));
		graphMAPE.setBackground(Color.WHITE);
		graphMAPE.setBounds(10, 11, 360, 360);
                
        graphMAPE.setLocation(plGraphMAPE.getWidth()/2-graphMAPE.getWidth()/2, 40);
        
        graphMSE = new XYGraph();
		graphMSE.setBorder(new LineBorder(new Color(0, 0, 0)));
		graphMSE.setBackground(Color.WHITE);
		graphMSE.setBounds(10, 11, 360, 360);
                
        graphMSE.setLocation(plGraphMSE.getWidth()/2-graphMSE.getWidth()/2, 40);
         
        //Ustawienie lokalizacji paneli
        plPerceptron.setLocation((screen.getWidth() - plPerceptron.getWidth()) / 2,
                (screen.getHeight() - plPerceptron.getHeight()) / 2 + 50);

        plOptions.setLocation(50,
                plPerceptron.getHeight() / 2 - plOptions.getHeight() / 2);

        plNeuralConstruction.setLocation(plOptions.getWidth() + 100, plPerceptron.getHeight() / 2 - plOptions.getHeight() / 2 + lbTitlePerceptron.getHeight() + 50);

        plFunctions.setLocation(plOptions.getWidth() + plNeuralConstruction.getWidth() + 150,
                plPerceptron.getHeight() / 2 - plOptions.getHeight() / 2 + lbTitlePerceptron.getHeight() + 150);

        plParametrs.setLocation(plNeuralConstruction.getWidth() - plParametrs.getWidth() - 10,
                plNeuralConstruction.getHeight() / 2 - plParametrs.getHeight() / 2);
        
        plFinishedLearning.setLocation(plPerceptron.getWidth()/2-plFinishedLearning.getWidth()/2, plPerceptron.getHeight()/2-plFinishedLearning.getHeight()/2);
        
        plFinishedValid.setLocation(plPerceptron.getWidth()/2-plFinishedLearning.getWidth()/2, plPerceptron.getHeight()/2-plFinishedLearning.getHeight()/2);
        
        plGraphMSE.setLocation(plPerceptron.getWidth()/2-plGraphMSE.getWidth()/2, plPerceptron.getHeight()/2-plGraphMSE.getHeight()/2);
        
        plGraphMAPE.setLocation(plPerceptron.getWidth()/2-plGraphMAPE.getWidth()/2, plPerceptron.getHeight()/2-plGraphMAPE.getHeight()/2);

        plGraphMAPE.add(btnCloseGraphMAPE);
        plGraphMAPE.add(graphMAPE);
        
        plGraphMSE.add(btnCloseGraphMSE);
        plGraphMSE.add(graphMSE);
        
        plFunctions.add(btnMAPE);
        plFunctions.add(btnMSE);
        
        plFinishedLearning.add(btnCloseFinishedLearning);
        
        plFinishedValid.add(btnCloseFinishedValid);
        plFinishedValid.add(lbGoodAnswers);
        
        //Dodanie do paneli obiektów
        plOptions.add(cbTrainingSet);
        plOptions.add(cbValidSet);
        plOptions.add(cbTestSet);
        plOptions.add(btnLearn);
        plOptions.add(btnValid);
        plOptions.add(btnTest);

        plParametrs.add(btnCloseParametrs);
        plParametrs.add(lbBias);
        plParametrs.add(txtBias);
        plParametrs.add(lbBeta);
        plParametrs.add(txtBeta);
        plParametrs.add(btnSetParametrs);
        plParametrs.add(txtRandomWeightFrom);
        plParametrs.add(txtRandomWeightTo);

        plNeuralConstruction.add(plParametrs);
        //wizualizacja wejść i wag do perceptronu
        xAndWeight.lbXs.stream().forEach((label) -> {
            plNeuralConstruction.add(label);
        });
        xAndWeight.txtXs.stream().forEach((txt) -> {
            plNeuralConstruction.add(txt);
        });
        xAndWeight.lbArrows.stream().forEach((label) -> {
            plNeuralConstruction.add(label);
        });
        xAndWeight.lbWeights.stream().forEach((label) -> {
            plNeuralConstruction.add(label);
        });
        xAndWeight.txtWeights.stream().forEach((txt) -> {
            plNeuralConstruction.add(txt);
        });
        plNeuralConstruction.add(lbY);
        plNeuralConstruction.add(lbZ);
        plNeuralConstruction.add(txtY);
        plNeuralConstruction.add(txtZ);
        plNeuralConstruction.add(btnNeuralCell);
        

        plPerceptron.add(btnClose);
        plPerceptron.add(plGraphMSE);
        plPerceptron.add(plGraphMAPE);
        plPerceptron.add(plFinishedValid);
        plPerceptron.add(plFinishedLearning);
        plPerceptron.add(plOptions);
        plPerceptron.add(plNeuralConstruction);
        plPerceptron.add(plFunctions);
        plPerceptron.add(lbTitlePerceptron);
        //plPerceptron.add(graphx);

        plPerceptron.add(btnBipolarSigmoidal);
        plPerceptron.add(btnUnipolarSigmoidal);
        plPerceptron.add(btnBipolarThreshold);
        plPerceptron.add(btnUnipolarThreshold);

        screen.getFullScreenWindow().getLayeredPane().add(plPerceptron,
                JLayeredPane.MODAL_LAYER);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        Object src = e.getSource();
        if (src == btnPerceptron) {
            selectNetwork.setVisible(false);
            plPerceptron.setVisible(true);
        }
        if (src == btnClose) {
            plPerceptron.setVisible(false);
        }
        
        if(src==btnCloseFinishedLearning){
            plFinishedLearning.setVisible(false);
        }
        
        if(src==btnCloseFinishedValid){
            plFinishedValid.setVisible(false);
        }

        if (src == btnBipolarSigmoidal) {
            btnBipolarSigmoidal.setVisible(false);
            btnUnipolarSigmoidal.setVisible(true);
            btnBipolarThreshold.setVisible(false);
            btnUnipolarThreshold.setVisible(false);

            lbBeta.setVisible(true);
            txtBeta.setVisible(true);
            
            lbBias.setLocation(plParametrs.getWidth()/3-(widthLabel+widthText)/2, 120);
            txtBias.setLocation(plParametrs.getWidth()/3-(widthLabel+widthText)/2+widthLabel, 120);
            
            perceptron.setState(AdelineCell.Function.UNIPOLAR_SIGMOID_FUNCTION);

        }
        if (src == btnUnipolarSigmoidal) {
            btnBipolarSigmoidal.setVisible(false);
            btnUnipolarSigmoidal.setVisible(false);
            btnBipolarThreshold.setVisible(true);
            btnUnipolarThreshold.setVisible(false);
            
            lbBeta.setVisible(false);
            txtBeta.setVisible(false);
            
            lbBias.setLocation(plParametrs.getWidth()/2-(widthLabel+widthText)/2, 120);
            txtBias.setLocation(plParametrs.getWidth()/2-(widthLabel+widthText)/2+widthLabel, 120);
            
            perceptron.setState(AdelineCell.Function.BIPOLAR_THRESHOLD_FUNCTION);
        }
        if (src == btnBipolarThreshold) {
            btnBipolarSigmoidal.setVisible(false);
            btnUnipolarSigmoidal.setVisible(false);
            btnBipolarThreshold.setVisible(false);
            btnUnipolarThreshold.setVisible(true);
            
            lbBeta.setVisible(false);
            txtBeta.setVisible(false);
            
            lbBias.setLocation(plParametrs.getWidth()/2-(widthLabel+widthText)/2, 120);
            txtBias.setLocation(plParametrs.getWidth()/2-(widthLabel+widthText)/2+widthLabel, 120);

            perceptron.setState(AdelineCell.Function.UNIPOLAR_THRESHOLD_FUNCTION);
        }
        if (src == btnUnipolarThreshold) {
            btnBipolarSigmoidal.setVisible(true);
            btnUnipolarSigmoidal.setVisible(false);
            btnBipolarThreshold.setVisible(false);
            btnUnipolarThreshold.setVisible(false);

            lbBeta.setVisible(true);
            txtBeta.setVisible(true);
            
            lbBias.setLocation(plParametrs.getWidth()/3-(widthLabel+widthText)/2, 120);
            txtBias.setLocation(plParametrs.getWidth()/3-(widthLabel+widthText)/2+widthLabel, 120);
            
            perceptron.setState(AdelineCell.Function.BIPOLAR_SIGMOID_FUNCTION);
        }

        if (src == btnNeuralCell) {
            plParametrs.setVisible(true);
        }
        if (src == btnCloseParametrs) {
            plParametrs.setVisible(false);
        }
        
        if(src==btnSetParametrs){
            if(txtBias.getText().equals("")){
                perceptron.setBias(0);
            }
            else{
                perceptron.setBias(Double.valueOf(txtBias.getText()));
            }
            if(txtBeta.getText().equals("")){
                perceptron.setBeta(0);
            }
            else{
                perceptron.setBeta(Double.valueOf(txtBeta.getText()));
            }
            
            setAndDrawWeight(true);
            
            plParametrs.setVisible(false);
        }
        
        if(src==btnMSE){
            plGraphMSE.setVisible(true);
        }
        
        if(src==btnMAPE){
            plGraphMAPE.setVisible(true);
        }
        if(src==btnCloseGraphMSE){
            plGraphMSE.setVisible(false);
        }
        
        if(src==btnCloseGraphMAPE){
            plGraphMAPE.setVisible(false);
        }

        if (src == btnLearn) {
            runSet(true);
            plFinishedLearning.setVisible(true);
            int pointX=0;
                for(Integer pointY :errorMSE){
                    //System.out.println("Kolejny punkt do narysowania: "+pointY);
                    graphMSE.drawPoint(pointX*5, pointY, Color.blue);
                    pointX++;
                }
                pointX=0;
                for(Integer pointY :errorMAPE){
                    System.out.println("Kolejny punkt do narysowania: "+pointY);
                    graphMAPE.drawPoint(pointX*5, pointY, Color.blue);
                    pointX++;
                }
        }

        if (src == btnValid) {
            runSet(false);
            plFinishedValid.setVisible(true);
            
        }

        /*
        if (src == activationFunction) {
            System.out.println(state);
            if (state < 3) {
                state++;
            } else {
                state=0;
            }
            switch (state) {
                case 0:
                    changeButtonImage("/activFunction/bipolarSigmoidal", "Select an activation function", activationFunction);
                    break;
                case 1:
                    changeButtonImage("/activFunction/unipolarSigmoidal", "Select an activation function", activationFunction);
                    break;
                case 2:
                    changeButtonImage("/activFunction/bipolarThreshold", "Select an activation function", activationFunction);
                    break;
                case 3:
                    changeButtonImage("/activFunction/unipolarThreshold", "Select an activation function", activationFunction);
                    break;
                default:
                    System.out.println("Nieobsługiwana funkcja");
            }
        }
         */
    }
    
    private void setAndDrawWeight(boolean randomWeight){
        Border border;
        
        String txtWeightFrom=txtRandomWeightFrom.getText();
        String txtWeightTo=txtRandomWeightTo.getText();
        
        double weightFrom=Double.valueOf(txtWeightFrom);
        double weightTo=Double.valueOf(txtWeightTo);
        
        if(weightTo<weightFrom){
            double buffer=weightFrom;
            weightFrom=weightTo;
            weightTo=buffer;
            txtRandomWeightFrom.setText(Double.toString(weightFrom));
            txtRandomWeightTo.setText(Double.toString(weightTo));
        }
        
        double difference=Math.abs(weightFrom)+Math.abs(weightTo);
        Random random=new Random();
        
        for(int i=0; i<vectors.get(0).getLength(); i++){
            double weight;
            if(randomWeight){
                weight=random.nextDouble()*difference+weightFrom;
                System.out.println("Wylosowane wagi:"+weight+"\n");
                //Ustawienie wagi w perceptronie
                perceptron.setInputWeight(i, weight);
            }
            else{
                weight=perceptron.getInputWeight(i);
            }
            
            
            
            if(weight>0){
                xAndWeight.txtWeights.get(i).setBackground(Color.GREEN);
                xAndWeight.txtWeights.get(i).setForeground(Color.GREEN);
                border = BorderFactory.createLineBorder(Color.GREEN, 3);
                xAndWeight.txtWeights.get(i).setBorder(border);
                String txtWeight=Double.toString(weight);
                if(txtWeight.length()>4){
                    xAndWeight.txtWeights.get(i).setText(Double.toString(weight).substring(0, 4));
                }
                else{
                    xAndWeight.txtWeights.get(i).setText(Double.toString(weight));
                }
                xAndWeight.lbWeights.get(i).setBackground(Color.GREEN);
                xAndWeight.lbWeights.get(i).setForeground(Color.GREEN);
            }
            else{
                xAndWeight.txtWeights.get(i).setBackground(Color.RED);
                xAndWeight.txtWeights.get(i).setForeground(Color.RED);
                border = BorderFactory.createLineBorder(Color.RED, 3);
                xAndWeight.txtWeights.get(i).setBorder(border);
                String txtWeight=Double.toString(weight);
                if(txtWeight.length()>4){
                    xAndWeight.txtWeights.get(i).setText(Double.toString(weight).substring(0, 4));
                }
                else{
                    xAndWeight.txtWeights.get(i).setText(Double.toString(weight));
                }
                xAndWeight.lbWeights.get(i).setBackground(Color.RED);
                xAndWeight.lbWeights.get(i).setForeground(Color.RED);
            }
        }
        
        
    }
    private void runSet(boolean learning){
        int numberOfVector=0;
            int drawCoordinates=0;
            double y=0;
            int goodAnswers=0;
            for(VectorX vectorX : vectors){
                drawCoordinates=0;
                //Ładowanie datyX do perceptronu 
                for(InputComponent txt :xAndWeight.txtXs){
                    if(drawCoordinates<vectors.get(numberOfVector).getLength()){
                    perceptron.setInputData(drawCoordinates, vectors.get(numberOfVector).getCoordinates(drawCoordinates));
                    //txt.setText(Double.toString(vectors.get(numberOfVector).getCoordinates(drawCoordinates)));
                    drawCoordinates++;
                    }
                }
                
                
                y=perceptron.getOutput();
                System.out.println("TAKI ZE MNIE MADRY PERCEPTRON: "+y );
                //Zmieniamy wagi, uczymy perceptron
                if(learning){
                    perceptron.learnNeuralCell(y, vectorX);
                    System.out.println("Co doaję do MAPE: "+100*perceptron.getErrorSignal()/(y+0.00001)+"\n");
                    errorMAPE.add((int)(100*perceptron.getErrorSignal()/y));
                    errorMSE.add((int)(perceptron.getErrorSignal()*perceptron.getErrorSignal()));
                    //graphMAPE.drawPoint(numberOfVector, (int)(perceptron.getErrorSignal()/y), Color.blue);
                }
                else{
                    if(y==vectorX.getZ()){
                        goodAnswers++;
                    }
                }
                numberOfVector++;
                
            }
            
            if(!learning){
                System.out.println("Ile dobrych odpowiedzi"+goodAnswers+"\n");
                lbGoodAnswers.setText(Double.toString(100*goodAnswers/numberOfVector).substring(0, 2)+"%");
            }
            
            //RYSOWANIE
            drawCoordinates=0;
            numberOfVector-=1;
            for(InputComponent txt :xAndWeight.txtXs){
                if(drawCoordinates<vectors.get(numberOfVector).getLength()){
                    txt.setText(Double.toString(vectors.get(numberOfVector).getCoordinates(drawCoordinates)));
                    drawCoordinates++;
                }
            }
            
            txtY.setText(Double.toString(y));
            txtZ.setText(Double.toString(vectors.get(numberOfVector).getZ()));
            
            //rysuje graficznie Wagi
            setAndDrawWeight(false);
    }
    
    private void cleanWeight(){
        Border border = BorderFactory.createLineBorder(Color.GREEN, 3);
        for(int i=0; i<vectors.get(0).getLength(); i++){
                xAndWeight.txtWeights.get(i).setBackground(Color.GREEN);
                xAndWeight.txtWeights.get(i).setForeground(Color.GREEN);
                border = BorderFactory.createLineBorder(Color.GREEN, 3);
                xAndWeight.txtWeights.get(i).setBorder(border);
                xAndWeight.txtWeights.get(i).setText("");
                
                xAndWeight.lbWeights.get(i).setBackground(Color.GREEN);
                xAndWeight.lbWeights.get(i).setForeground(Color.GREEN);
        }    
    }

    private void changeButtonImage(String name, String toolTip, JButton button) {

        // Ładowanie rysunku:
        String imagePath = "images/menu/main/" + name + ".png";
        ImageIcon iconRollover = new ImageIcon(imagePath);

        int w = iconRollover.getIconWidth();
        int h = iconRollover.getIconHeight();

        // Pobranie kursora dla tego przycisku:
        Cursor cursor
                = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

        // Tworzenie domyślnego, przeświecającego przycisku:
        Image image = screen.createCompatibleImage(w, h,
                Transparency.TRANSLUCENT);
        Graphics2D g = (Graphics2D) image.getGraphics();
        Composite alpha = AlphaComposite.getInstance(
                AlphaComposite.SRC_OVER, .5f);
        g.setComposite(alpha);
        g.drawImage(iconRollover.getImage(), 0, 0, null);
        g.dispose();
        ImageIcon iconDefault = new ImageIcon(image);

        // Tworzenie przycisku przyciśniętego:
        image = screen.createCompatibleImage(w, h,
                Transparency.TRANSLUCENT);
        g = (Graphics2D) image.getGraphics();
        g.drawImage(iconRollover.getImage(), 2, 2, null);
        g.dispose();
        ImageIcon iconPressed = new ImageIcon(image);

        // Tworzenie przycisku:
        button.addActionListener(this);
        button.setIgnoreRepaint(true);
        button.setFocusable(false);
        button.setToolTipText(toolTip);
        button.setBorder(null);
        button.setContentAreaFilled(false);
        button.setCursor(cursor);
        button.setIcon(iconDefault);
        button.setRolloverIcon(iconRollover);
        button.setPressedIcon(iconPressed);
    }


    private void comboSet(int witch, int widthTextField, int heightTextField, Border border, String toolTipText, JComboBox combo, ImagePanel panel) {
        combo.setSize(widthTextField, heightTextField);
        combo.setLocation(panel.getWidth() / 2 - widthTextField / 2, first + witch * 140);
        combo.setBorder(border);
        combo.setToolTipText(toolTipText);
        combo.setFont(new Font("Jokerman", Font.PLAIN, 16));
        switch (witch) {

            case 0:
                Path dir = Paths.get("learn/trainingSet");
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "*.ts")) {
                    for (Path file : stream) {
                        combo.addItem(file.getFileName());
                    }
                } catch (IOException | DirectoryIteratorException x) {
                    // IOException can never be thrown by the iteration.
                    // In this snippet, it can only be thrown by newDirectoryStream.
                    System.err.println(x);
                }
                break;
            case 1:
                Path dir2 = Paths.get("learn/validSet");
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir2, "*.vs")) {
                    for (Path file : stream) {
                        combo.addItem(file.getFileName());
                    }
                } catch (IOException | DirectoryIteratorException x) {
                    // IOException can never be thrown by the iteration.
                    // In this snippet, it can only be thrown by newDirectoryStream.
                    System.err.println(x);
                }
                break;
            case 2:
                Path dir3 = Paths.get("learn/testSet");
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir3, "*.test")) {
                    for (Path file : stream) {
                        combo.addItem(file.getFileName());
                    }
                } catch (IOException | DirectoryIteratorException x) {
                    // IOException can never be thrown by the iteration.
                    // In this snippet, it can only be thrown by newDirectoryStream.
                    System.err.println(x);
                }
                break;
        }
        combo.setOpaque(false);
        combo.addActionListener(this);
        //comboBox.setBackground(new Color(0,0,0,0));
        ((JTextField) combo.getEditor().getEditorComponent()).setOpaque(false);
        combo.setUI(new BasicComboBoxUI() {
            public void paintCurrentValueBackground(Graphics g, Rectangle bounds, boolean hasFocus) {
            }
        });
    }

    private void loadTrainingSet(String fileName) throws IOException {

        vectors = new ArrayList<>();

        //lines = new ArrayList();
        int height = 0;

        // odczytanie wszystkich wierszy z pliku do listy
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath + fileName));
        while (true) {
            String line = reader.readLine();
            // koniec wierszy do odczytywania
            if (line == null) {
                reader.close();
                break;
            }

            // dodawanie wszystkich wierszy poza komentarzami
            if (!line.startsWith("#")) {
                Pattern comma = Pattern.compile(",");
                String[] xn = comma.split(line);
                VectorX v = new VectorX(xn.length - 1);
                for (int i = 0; i < (xn.length - 1); i++) {
                    double value = Double.valueOf(xn[i]);
                    v.setCoordinates(i, value);
                }
                double value = Double.valueOf(xn[xn.length - 1]);
                v.setZ(value);
                boolean add = vectors.add(v);
                
            }
        }
    }

    //Rysuje tyle pól x ile wynosi długość vectora
    private void drawX(ImagePanel imgPanel) {
        int vectorLength = vectors.get(0).getLength();
        int start = imgPanel.getHeight() / (vectorLength + 1);
        int count = 1;
        for (JLabel label : xAndWeight.lbXs) {
            if (vectorLength > 0) {
                label.setForeground(new Color(0, (float) 0.5, (float) 1.0));
                label.setLocation(20, start * count - sizeXAndWeight / 2);
                label.setVisible(true);
                count++;
                vectorLength--;
            } else {
                label.setVisible(false);
                label.setForeground(Color.BLACK);
            }
        }
        vectorLength = vectors.get(0).getLength();
        count = 1;
        for (InputComponent text : xAndWeight.txtXs) {
            if (vectorLength > 0) {
                text.setForeground(Color.CYAN);
                Border border
                        = BorderFactory.createLineBorder(new Color(0, (float) 0.5, (float) 1.0), 3);
                text.setBorder(border);
                text.setLocation(20 + sizeXAndWeight, start * count - sizeXAndWeight / 2);
                text.setVisible(true);
                text.setText(Double.toString(vectors.get(0).getCoordinates(count-1)));
                
                //ustawienie wejscia perceptronu
                perceptron.setInputData(count-1, vectors.get(0).getCoordinates(count-1));
                
                count++;
                vectorLength--;
            } else {
                text.setVisible(false);
                text.setForeground(Color.BLACK);
                Border border
                        = BorderFactory.createLineBorder(Color.BLACK, 3);
                text.setBorder(border);
            }
        }
    }
    
    //Rysuje tyle pól STRZAŁEK ile wynosi długość vectora
    private void drawArrows(ImagePanel imgPanel) {
        int vectorLength = vectors.get(0).getLength();
        int start = imgPanel.getHeight() / (vectorLength + 1);
        int count = 1;
        for (JLabel arrow : xAndWeight.lbArrows) {
            if (vectorLength > 0) {
                arrow.setLocation(20 + 2 * sizeXAndWeight, start * count - sizeXAndWeight / 2 + ((sizeXAndWeight - arrow.getHeight()) / 2));
                arrow.setVisible(true);
                count++;
                vectorLength--;
            } else {
                arrow.setVisible(false);
            }
        }
    }
    
    //Rysuje tyle pól WAG ile wynosi długość vectora
    private void drawWeight(ImagePanel imgPanel){
        int vectorLength = vectors.get(0).getLength();
        int start = imgPanel.getHeight() / (vectorLength + 1);
        int count = 1;
        for (JLabel weight : xAndWeight.lbWeights) {
            if (vectorLength > 0) {
                weight.setForeground(Color.GREEN);
                weight.setLocation(20 + 2 * sizeXAndWeight + xAndWeight.getArrowWidth(), start * count - sizeXAndWeight / 2);
                weight.setVisible(true);
                count++;
                vectorLength--;
            } else {
                weight.setVisible(false);
                weight.setForeground(Color.BLACK);
            }
        }
        vectorLength = vectors.get(0).getLength();
        count = 1;
        for (InputComponent text : xAndWeight.txtWeights) {
            if (vectorLength > 0) {
                text.setForeground(Color.GREEN);
                Border border
                        = BorderFactory.createLineBorder(Color.GREEN, 3);
                text.setBorder(border);
                text.setLocation(20 + 3 * sizeXAndWeight + xAndWeight.getArrowWidth(), start * count - sizeXAndWeight / 2);
                text.setVisible(true);
                count++;
                vectorLength--;
            } else {
                text.setVisible(false);
                text.setForeground(Color.BLACK);
                Border border
                        = BorderFactory.createLineBorder(Color.BLACK, 3);
                text.setBorder(border);
            }
        }
    }

    private void drawInput() {
        //Rysowanie Xów
        drawX(plNeuralConstruction);
        //Rysowanie strzałek
        drawArrows(plNeuralConstruction);
        //Rysowanie Wag
        drawWeight(plNeuralConstruction);
    }

    class XYGraph extends JPanel {

        /**
         * Variables which contains info about currently added point.
         */
        Color clr;
        ArrayList<Integer> x_var;
        ArrayList<Integer> y_var;

        public XYGraph(){
            x_var=new ArrayList<>(100);
            y_var=new ArrayList<>(100);
        }
        /**
         * Override to write custom point.
         */
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.setColor(clr);
            int i=0;
            for (VectorX v:vectors) {
                g.fillRect(x_var.get(i)-2, y_var.get(i)-2, 4, 4);
                i++;
            }
            paintCross(g);
        }

        /**
         * Yet another smart function to draw a cross on the component.
         *
         * @param g
         */
        private void paintCross(Graphics g) {
            g.setColor(Color.BLACK);
            g.fillRect(180, 0, 1, 360);
            g.drawLine(176, 8, 180, 0);
            g.drawLine(184, 8, 180, 0);

            g.fillRect(0, 180, 360, 1);
            g.drawLine(352, 176, 360, 180);
            g.drawLine(352, 184, 360, 180);
            g.drawString("0", 183, 193);
        }

        /**
         * Writes a point at selected coordinates with selected colour.
         *
         * @param x coordinate X
         * @param y coordinate Y
         * @param clr selected colour
         */
        public void drawPoint(int x, int y, Color clr) {

            x += 180;
            y += 180;
            y = 360 - y;
            x_var.add(x);
            y_var.add(y);
            this.clr = clr;
            paintImmediately(x, y, 4, 4);
            
        }
    }

    private void setPanel(ImagePanel ip, Border b, boolean visible) {
        ip.setBorder(b);
        ip.setVisible(visible);
    }
    
    private void setFieldWithLabel(JLabel label, InputComponent text, Color color, Border border, String toolTipText,
            int locationX, int locationY, int widthLabel, int widthText,int height, boolean visible){
        label.setSize(widthLabel, height);
        label.setLocation(locationX, locationY);
        label.setVisible(visible);
        label.setBackground(color);
        label.setForeground(color);
        label.setFont(new Font("Gill Sans Ultra Bold", Font.PLAIN, 15));
        
        setField(text, color, border, toolTipText, locationX+widthLabel, locationY, widthText, height, visible);
    }
    private void setField(InputComponent text, Color color, Border border, String toolTipText,
            int locationX, int locationY, int widthText,int height, boolean visible){
        text.setSize(widthText, height);
        text.setLocation(locationX, locationY);
        text.setOpaque(false);
        text.setBorder(border);
        text.setBackground(color);
        text.setForeground(color);
        text.setVisible(visible);
        text.setToolTipText(toolTipText);
        text.setFont(new Font("Gill Sans Ultra Bold", Font.PLAIN, 15));
    }
    private void showZAndY(){
        lbY.setVisible(true);
        lbZ.setVisible(true);
        txtY.setVisible(true);
        
        txtZ.setText(Double.toString(vectors.get(0).getZ()));
        txtZ.setVisible(true);
    }
}
