/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convert;

/**
 *Klasa reprezentująca meta dane.
 * @see ConvertFile
 * @author Dominik Kopaczka
 * @version 1.0
 * @since   2016-12-29
 */
public class DocumentMetaData {
    private String title;
    private String author;
    private String subject;
    private String keywords;
    private String creator;

    /**
     *Konstruktor domyślny klasy DocumentMetaData uzypełniający metadane dokumentu wartościami domyślnymi.
     */
    public DocumentMetaData() {
        this.title = "Agenda";
        this.author = "Author";
        this.subject = "SE-software engineering";
        this.keywords = ".pdf";
        this.creator = "My program using iText";
    }

    /**
     *Konstruktor klasy DocumentMetaData uzypełniający metadane dokumentu wartościami podanymi jako parametry.
     * @param title tytuł tworzonego dokumentu.
     * @param author autor tworzonego dokumentu.
     * @param subject temat tworzonego dokumentu.
     * @param keywords rozszerzenie tworzonego dokumentu.
     * @param creator opis tworzonego dokumentu.
     */
    public DocumentMetaData(String title, String author, String subject, String keywords, String creator) {
        this.title = title;
        this.author = author;
        this.subject = subject;
        this.keywords = keywords;
        this.creator = creator;
    }

    /**
     *Metoda ustawia imię autora
     * @param author Imię autora.
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     *Metoda ustawia opis tworzonego dokumentu.
     * @param creator Opis tworzonego dokumentu.
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     *Metoda ustawia rozszerzenie tworzonego dokumentu.
     * @param keywords Rozszerzenie tworzonego dokumentu.
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    /**
     *Metoda ustawia temat tworzonego dokumentu.
     * @param subject Temat tworzonego dokumentu.
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     *Metoda ustawia tytuł tworzonego dokumentu.
     * @param title Tytuł tworzonego dokumentu.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *Metoda pobiera imię autora
     * @return Imię autora dokumentu.
     */
    public String getAuthor() {
        return author;
    }

    /**
     *Metoda pobiera opis tworzonego dokumentu.
     * @return Opis tworzonego dokumentu.
     */
    public String getCreator() {
        return creator;
    }

    /**
     *Metoda pobiera rozszerzenie tworzonego dokumentu.
     * @return Rozszerzenie tworzonego dokumentu.
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     *Metoda pobiera temat tworzonego dokumentu.
     * @return Temat tworzonego dokumentu.
     */
    public String getSubject() {
        return subject;
    }

    /**
     *Metoda pobiera tytuł tworzonego dokumentu.
     * @return Tytuł tworzonego dokumentu.
     */
    public String getTitle() {
        return title;
    }
}