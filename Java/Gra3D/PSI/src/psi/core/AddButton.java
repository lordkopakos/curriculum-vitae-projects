/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.core;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import psi.graphics.NullRepaintManager;

/**
 *
 * @author lordkopakos
 */
public class AddButton extends DisplayView implements ActionListener{
  protected JButton newNetworkButton;
    protected JButton quitButton;
    
    private JFrame frame2;
    protected Container contentPane2;
    
    int dif;

    
    public static void main(String[] args) {
        new AddButton().menurun();
    }
    
    /**
     *
     */
    @Override
    public void menurun() {
        super.menurun();
    }
    

    @Override
    public void menuinit(){
        super.menuinit();
        NullRepaintManager.install();
        
        newNetworkButton= createButton("newNetwork", "Zagraj po stronie dobra lub zła");

        quitButton=createButton("quit", "Do zobaczenia wkrótce");
       
        

        frame2 = super.screen.getFullScreenWindow();
	contentPane2 = frame2.getContentPane();
			
        
        // Upewnij się, że panel jest przezroczysty:
        if (contentPane2 instanceof JComponent) {
            ((JComponent)contentPane2).setOpaque(false);
        }

        // Dodaj komponenty do panelu:
        contentPane2.setLayout(new FlowLayout(FlowLayout.CENTER));
        
        contentPane2.add(newNetworkButton);
        contentPane2.add(quitButton);

        // Jawne ułożenie komponentów (wymagane w niektórych systemach).
        frame2.validate();
        
        
    }
    @Override
    public void draw(Graphics2D g) {
        super.draw(g);
         
        frame2 = super.screen.getFullScreenWindow();
        // Panel warstwowy zawiera takie elementy, jak "wyskakujące"
        // okna podpowiedzi, menu oraz panel zawartości.
         frame2.getLayeredPane().paintComponents(g);
        
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
         if (src == quitButton) {
            lazilyExit();
        }
    }
    
    protected JButton createButton(String name, String toolTip) {

        // Ładowanie rysunku:
        String imagePath = "images/menu/main/" + name + ".png";
        ImageIcon iconRollover = new ImageIcon(imagePath);
        
        int w = iconRollover.getIconWidth();
        int h = iconRollover.getIconHeight();

        // Pobranie kursora dla tego przycisku:
        Cursor cursor =
            Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

        // Tworzenie domyślnego, przeświecającego przycisku:
        Image image = screen.createCompatibleImage(w, h,
            Transparency.TRANSLUCENT);
        Graphics2D g = (Graphics2D)image.getGraphics();
        Composite alpha = AlphaComposite.getInstance(
            AlphaComposite.SRC_OVER, .5f);
        g.setComposite(alpha);
        g.drawImage(iconRollover.getImage(), 0, 0, null);
        g.dispose();
        ImageIcon iconDefault = new ImageIcon(image);

        // Tworzenie przycisku przyciśniętego:
        image = screen.createCompatibleImage(w, h,
            Transparency.TRANSLUCENT);
        g = (Graphics2D)image.getGraphics();
        g.drawImage(iconRollover.getImage(), 2, 2, null);
        g.dispose();
        ImageIcon iconPressed = new ImageIcon(image);

        // Tworzenie przycisku:
        JButton button = new JButton();
        button.addActionListener(this);
        button.setIgnoreRepaint(true);
        button.setFocusable(false);
        button.setToolTipText(toolTip);
        button.setBorder(null);
        button.setContentAreaFilled(false);
        button.setCursor(cursor);
        button.setIcon(iconDefault);
        button.setRolloverIcon(iconRollover);
        button.setPressedIcon(iconPressed);
        
        return button;
    }

      
}
