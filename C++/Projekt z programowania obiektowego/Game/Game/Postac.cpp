#include "Postac.h"


void Mag::statystyki()
{
	cout << nazwa << endl;
	cout << "HP: ";
	cout<<hp;
	cout << endl;
	cout << "MANA: " << mana << endl;
	cout << "DAMAGE: " << damage << endl;
}


bool Mag::walka(Enemy enemy)
{
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hOut, FOREGROUND_BLUE);
	cout << "WALKA" << endl;
	bool victory=1;
	while ((hp > 0) && (enemy.hp > 0))
	{
		
		enemy.hp = enemy.hp - damage;
		hp = hp - enemy.damage;
		SetConsoleTextAttribute(hOut, FOREGROUND_GREEN);
		cout << "HP Postac: " << hp << "\tEnemy: "<<enemy.hp<<endl;
		SetConsoleTextAttribute(hOut, BACKGROUND_RED);
		cout.width(20);
		cout << "\t\t\t\tDAMAGE Postac: " << damage << "\tEnemy: " << enemy.damage << endl;
		if (hp <= 0) victory = 0;

	}
	SetConsoleTextAttribute(hOut, FOREGROUND_INTENSITY);
	return victory;
}



Mag::Mag(string n,int h, int m, int d):hp(h), mana(m), nazwa(n), damage(d)
{
	state = mag; 
}

Mag Mag::operator+(Mag m)
{
	Mag nowy;
	nowy.hp = hp + m.hp;
	nowy.damage = damage + m.damage;
	nowy.mana = mana + m.mana;
	nowy.nazwa = "Nowy Mag";
	return nowy;
}

istream& operator>>(istream & in, Mag & M)
{
	in >> M.mana;
	return in;
}

ostream& operator<<(ostream & out, Mag & M)
{
	out <<"MANA: "<< M.mana<<endl;
	return out;
}
Rycerz::Rycerz(string n,int h, int s, int d) :nazwa(n),hp(h), stamina(s), damage(d)
{
	state = rycerz;
}
void Rycerz::wyswietl()
{
	cout << "STAMINA: " << stamina << endl;
}
void Rycerz::statystyki()
{
	cout << nazwa << endl;
	cout << "HP: ";
	cout << hp;
	cout << endl;
	cout << "STAMINA: " << stamina << endl;
}
bool Rycerz::walka(Enemy enemy)
{
	
		hOut = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hOut, FOREGROUND_BLUE);
		cout << "WALKA" << endl;
		bool victory = 1;
		while ((hp > 0) && (enemy.hp > 0))
		{

			enemy.hp = enemy.hp - damage;
			hp = hp - enemy.damage;
			SetConsoleTextAttribute(hOut, FOREGROUND_GREEN);
			cout << "HP Postac: " << hp << "\tEnemy: " << enemy.hp << endl;
			SetConsoleTextAttribute(hOut, BACKGROUND_RED);
			cout.width(20);
			cout << "\t\t\t\tDAMAGE Postac: " << damage << "\tEnemy: " << enemy.damage << endl;
			if (hp <= 0) victory = 0;

		}
		SetConsoleTextAttribute(hOut, FOREGROUND_INTENSITY);
		return victory;
	
}
//Zapis i wczytanie z pliku
