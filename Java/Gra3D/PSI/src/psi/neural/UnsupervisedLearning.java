/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.neural;

import java.util.ArrayList;
import psi.network.VectorX;

/**
 *
 * @author lordkopakos
 */
public interface UnsupervisedLearning {
    public double HebbRule(VectorX v);
    public double expandedHebbRule(VectorX v);
    public double OjiRule(VectorX v);
    public double instarRuleGrossberg(VectorX v);
    public double outstarRuleGrossberg(VectorX v);
    public double antiHebbRule(VectorX v);
    
    public double getNewValueWithUseThreshold(double inValue);
}
