//Server2 jest minimalnym serverem "echo" i serverem zliczającym
package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"
)

var mu sync.Mutex
var count int

func main() {
	http.HandleFunc("/", handler)
	http.HandleFunc("/count", counter)
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

//handler zwraca komponent Path żądanego adresu URL
func handler(w http.ResponseWriter, r *http.Request) {
	mu.Lock()
	count++
	mu.Unlock()
	fmt.Fprint(w, "URL.Path=", r.URL.Path)
}

//counter zwracal liczbę dotychczas wykonanych wywołań
func counter(w http.ResponseWriter, r *http.Request) {
	mu.Lock()
	fmt.Fprintf(w, "Liczba wywołań %d\n", count)
	mu.Unlock()
}
