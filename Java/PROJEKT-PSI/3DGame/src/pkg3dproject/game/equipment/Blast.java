package pkg3dproject.game.equipment;

import pkg3dproject.math3D.*;
import pkg3dproject.game.GameObject;

/*
    Obiekt w grze (GameObject) Blast reprezentuje pocisk.
    Został zaprojektowany w taki sposób, by poruszał się
    w linii prostej przez pięć sekund i ulegał zniszczeniu.
    Obiekty Blast niszczą jednym trafieniem roboty (obiekty 
    klasy Bot).
*/
public class Blast extends GameObject {

    private static final long DIE_TIME = 5000;
    private static final float SPEED = 1.5f;
    private static final float ROT_SPEED = .008f;

    private long aliveTime;

    /*
        Tworzy nowy obiekt Blast z wykorzystaniem wskazanej grupy 
        PolygonGroup i znormalizowanego wektora kierunku.
    */
    public Blast(PolygonGroup polygonGroup, Vector3D direction) {
        super(polygonGroup);
        MovingTransform3D transform = polygonGroup.getTransform();
        Vector3D velocity = transform.getVelocity();
        velocity.setTo(direction);
        velocity.multiply(SPEED);
        transform.setVelocity(velocity);
        //transform.setAngleVelocityX(ROT_SPEED);
        transform.setAngleVelocityY(ROT_SPEED);
        transform.setAngleVelocityZ(ROT_SPEED);
        setState(STATE_ACTIVE);
    }


    @Override
    public void update(GameObject player, long elapsedTime) {
        aliveTime+=elapsedTime;
        if (aliveTime >= DIE_TIME) {
            setState(STATE_DESTROYED);
        }
        else {
            super.update(player, elapsedTime);
        }
    }

}
