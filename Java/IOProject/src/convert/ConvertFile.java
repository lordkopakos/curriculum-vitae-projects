/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convert;

import convert.description.A_DescriptionConvertFile;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import conteners.Lecture;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import read.ReadFile;

/**
 * (Wzorzec projektowy Singleton) Klasa reprezentująca konwertowany plik.
 * Zawiera metody umożliwiające wygenerowanie planu zajęć w formacie pdf i jest
 * łatwo rozszerzalna do konwersji do innych typów. Pozwala generować dwie
 * wersję planu zajęć horyzontalną i wertykalną używając dwóch zupełnie innych
 * algorytmów konwersji. Używa rekurencyjnego algorytmu zmiany zapisywanego
 * pliku, który zabezpiecza przed nadpisaniem się pliku. Algorytm działa tak jak
 * algorytm zapisu kopii plików w systemie Windows(Agenda.pdf, Agenda
 * -Copy.pdf,Agenda -Copy (2).pdf, Agenda -Copy (3).pdf).
 *
 * @see convert.description.A_DescriptionConvertFile
 * @see
 * <a href="http://developers.itextpdf.com/reference/com.itextpdf.text.Document">com.itextpdf.*</a>
 * @see
 * <a href="https://docs.oracle.com/javase/7/docs/api/java/io/package-summary.html">java.io.*</a>
 * @see
 * <a href="https://docs.oracle.com/javase/7/docs/api/java/util/logging/package-summary.html">java.util.logging.*</a>
 * @see conteners.Lecture
 * @see read.ReadFile
 * @author Dominik Kopaczka
 * @version 2.0
 * @since 2016-12-29
 */
@A_DescriptionConvertFile(classDescription = "Klasa reprezentująca konwertowany plik",
        author = "Dominik Kopaczka",
        classVersion = 2)
public final class ConvertFile implements Runnable {

    private static ConvertFile instance = null;

    /**
     * Typy możliwej konwersji planu zajęć, dzięki czemu kod jest łatwo
     * rozszerzalny.
     */
    public enum ConvertType {

        /**
         * Konwersja do PDF(Typ dostępny).
         */
        PDF,
        /**
         * Konwersja do RTF(Typ niedostępny).
         */
        RTF,
        /**
         * Konwersja do HTML(Typ niedostępny).
         */
        HTML,
        /**
         * Konwersja do DOC(Typ niedostępny).
         */
        DOC
    }

    /**
     * Typy możliwego układu stron. Dostępne są dwa typy: horyzontalny i
     * wertykalny. Każdy z nich stosuje inny algorytm wytwarzania planu.
     */
    public enum ArrangementType {

        /**
         * Układ horyzontalny(poziomy).
         */
        HORIZONTAL,
        /**
         * Układ wertykalny(pionowy).
         */
        VERTICAL
    }

    private ConvertType convertState;
    private ArrangementType arrangementState;

    private String filePath;
    private String fileName;
    private String fileEnlargement;

    private int copyCounter;
    private String numberOfCopy;

    private int hour;
    private int minutes;

    private ConvertFile() {
        numberOfCopy = "";
        copyCounter = 0;

        setFileLocation("agenda/", "Agenda", ".pdf");

        setConvertState(ConvertType.PDF);
        setArrangementState(ArrangementType.VERTICAL);

        hour = 7;
        minutes = 45;
    }

    /**
     * Metoda synchronicznie tworzy nową instancje klasy ConvertFile, jeśli
     * takowa nie istnieje lub zwraca istniejącą instancję.(Wzorzec projektowy
     * Singleton)
     *
     * @return istniejącą instancję klasy ConvertFile
     */
    public static synchronized ConvertFile getInstance() {
        if (instance == null) {
            instance = new ConvertFile();
        }
        return instance;
    }

    /**
     * Metoda konwertująca wyczytany plik do formatu pdf
     *
     * @param readFile Obiekt klasy readFile z określoną na sztywno lokalizacją
     * pliku txt z planem zajęć
     * @return prawdę jeśli operacja konwersji się powiodła i fałsz jeśli
     * zakończyła się wystąpieniem wyjątku
     * @exception IOException generuje błąd wyjścia.
     * @exception DocumentException generuje błąd pracy na obiekcie klasy
     * Document.
     */
    private synchronized boolean convertToPDF(ReadFile readFile) {
        changeNameIfFileExist();

        Document document = setPageArrangement();

        try {
            PdfWriter.getInstance(document,
                    new FileOutputStream(filePath + fileName + fileEnlargement));

            document.open();
            setDocumentMetaData(document);
            runSelectedMethodOfConvert(document, readFile);

        } catch (DocumentException | IOException de) {
            System.err.println(de.getMessage());
            return false;
        }
        document.close();
        copyCounter = 0;
        return true;
    }

    private Document setPageArrangement() {
        Document document = null;
        switch (arrangementState) {
            case VERTICAL:
                document = new Document(PageSize.A4);
                return document;
            case HORIZONTAL:
                document = new Document(PageSize.A4.rotate());
                return document;
            default:
                return document;
        }
    }

    private void runSelectedMethodOfConvert(Document document, ReadFile readFile) {
        switch (arrangementState) {
            case VERTICAL:
                createVerticalLecturesAgenda(document, readFile);
                break;
            case HORIZONTAL:
                createHorizontalLecturesAgenda(document, readFile);
                break;
        }
    }

    /**
     * Metoda konwertująca wyczytany plik txt do formatu pdf w wersji
     * wertykalnej
     *
     * @param readFile Obiekt klasy readFile z określoną na sztywno lokalizacją
     * pliku txt z planem zajęć
     * @param document Obiekt klasy Document reprezentujący zapisywany plik pdf.
     * @exception DocumentException generuje błąd pracy na obiekcie klasy
     * Document.
     */
    private void createVerticalLecturesAgenda(Document document, ReadFile readFile) {
        addHeadingImage(document);
        addLecturesTableToDocument(document, readFile);
        addUnderLineImage(document);
        addLogoImage(document);
    }

    private void addLecturesTableToDocument(Document document, ReadFile readFile) {
        try {
            document.add(new Paragraph(""));
            addTitleImage(document);
            addLineImage(document);
            document.add(Chunk.NEWLINE);
            document.add(Chunk.NEWLINE);
            document.add(createLecturesTable(readFile));
            document.add(Chunk.NEWLINE);
            document.add(Chunk.NEWLINE);
        } catch (DocumentException ex) {
            Logger.getLogger(ConvertFile.class.getName()).log(Level.SEVERE, null, ex);
            saveErrorRaport(ex.getMessage());
        }
    }

    private PdfPTable createLecturesTable(ReadFile readFile) {
        PdfPTable tableOfLectures = new PdfPTable(8);
        tableOfLectures.setWidthPercentage(110);
        addHeadingTable(tableOfLectures);
        addAllDaysOfLectures(tableOfLectures, readFile);
        return tableOfLectures;
    }

    private void addHeadingTable(PdfPTable table) {
        Font font = setBoldFont(FontFactory.TIMES_ROMAN, BaseColor.WHITE);
        setHeadingCell(table, font);
    }

    private Font setBoldFont(String fontFactory, BaseColor fontColor) {
        Font font = FontFactory.getFont(fontFactory, 12, Font.BOLD);
        font.setColor(fontColor);
        return font;
    }

    private void setHeadingCell(PdfPTable table, Font font) {
        PdfPCell cell = new PdfPCell();
        BaseColor headingColor = WebColors.getRGBColor("#2A43A8");
        cell.setBackgroundColor(headingColor);

        cell.setPhrase(new Phrase("DAY", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("FROM", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("TO", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("LECTURE", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("LECTURER", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("BUILDING", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("ROOM", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("FORM OF LECTURES", font));
        table.addCell(cell);
    }

    private void addAllDaysOfLectures(PdfPTable tableOfLectures, ReadFile readFile) {
        for (int dayIndex = 0; dayIndex < readFile.getNumberOfDays(); dayIndex++) {
            addDayOfLectures(tableOfLectures, readFile, dayIndex);
        }
    }

    private void addDayOfLectures(PdfPTable table, ReadFile readFile, int dayIndex) {
        if (readFile.getWeek().getDay(dayIndex).lessons_size() != 0) {
            String dayName = readFile.getWeek().getDay(dayIndex).getName();
            int numberOfLectures = readFile.getWeek().getDay(dayIndex).lessons_size();
            addDayCell(numberOfLectures, dayName, table);
            addLecturesRowsInOneDay(numberOfLectures, dayIndex, readFile, table);
        }
    }

    private void addDayCell(int numberOfLectures, String dayName, PdfPTable table) {
        PdfPCell cell = new PdfPCell(new Phrase(dayName));
        BaseColor cellColor = WebColors.getRGBColor("#FFD700");
        cell.setRowspan(numberOfLectures);
        cell.setBackgroundColor(cellColor);
        table.addCell(cell);
    }

    private void addLecturesRowsInOneDay(int numberOfLectures, int dayIndex, ReadFile readFile, PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        BaseColor cellColor = WebColors.getRGBColor("#05cacc");
        cell.setBackgroundColor(cellColor);
        for (int lectureIndex = 0; lectureIndex < numberOfLectures; lectureIndex++) {

            Lecture lecture = readFile.getWeek().getDay(dayIndex).getLessons(lectureIndex);

            cell.setPhrase(new Phrase(lecture.getHourFrom()));
            table.addCell(cell);
            cell.setPhrase(new Phrase(lecture.getHourTo()));
            table.addCell(cell);
            cell.setPhrase(new Phrase(lecture.getLectureName()));
            table.addCell(cell);
            cell.setPhrase(new Phrase(lecture.getLecturer()));
            table.addCell(cell);
            cell.setPhrase(new Phrase(lecture.getBuilding()));
            table.addCell(cell);
            cell.setPhrase(new Phrase(lecture.getRoom()));
            table.addCell(cell);
            cell.setPhrase(new Phrase(lecture.getFormOfLectures()));
            table.addCell(cell);

        }
    }

    private void addHeadingImage(Document document) {
        addImageOnCentre("images/heading.png", document);
    }

    private void addTitleImage(Document document) {
        addImageOnCentre("images/agenda.png", document);
    }

    private void addLineImage(Document document) {
        addImageOnCentre("images/line.png", document);
    }

    private void addUnderLineImage(Document document) {
        addImageOnCentre("images/underlineVertical.png", document);
    }

    private void addLogoImage(Document document) {
        addImageOnCentre("images/logo.png", document);
    }

    private void addImageOnCentre(String fileLocation, Document document) {
        Image headingImage = initHeadingImagine(fileLocation);
        addHeadingImageToDocument(headingImage, document);
    }

    private Image initHeadingImagine(String fileLocation) {
        Image headingImage = null;
        try {
            headingImage = Image.getInstance(fileLocation);
        } catch (BadElementException | IOException ex) {
            Logger.getLogger(ConvertFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        headingImage.setBorder(Image.NO_BORDER);
        headingImage.setAlignment(Element.ALIGN_CENTER);
        return headingImage;
    }

    private void addHeadingImageToDocument(Image headingImage, Document document) {
        try {
            document.add(headingImage);
        } catch (DocumentException ex) {
            Logger.getLogger(ConvertFile.class.getName()).log(Level.SEVERE, null, ex);
            saveErrorRaport(ex.getMessage());
        }
    }

    private void changeNameIfFileExist() {
        if (existFile(filePath, fileName + numberOfCopy, fileEnlargement)) {
            numberOfCopy = "";
            copyCounter++;
            if (copyCounter == 1) {
                fileName += " -Copy";
            } else if (copyCounter > 1) {
                numberOfCopy = " (" + copyCounter + ")";
            }
            changeNameIfFileExist();
        } else {
            fileName += numberOfCopy;
        }
    }

    private boolean existFile(String filePath, String fileName, String fileEnlargement) {
        File checkFile = new File(filePath + fileName + fileEnlargement);
        return checkFile.isFile();
    }

    /**
     * Metoda konwertująca wyczytany plik txt do formatu pdf w wersji
     * horyzontalnej
     *
     * @param readFile Obiekt klasy readFile z określoną na sztywno lokalizacją
     * pliku txt z planem zajęć
     * @param document Obiekt klasy Document reprezentujący zapisywany plik pdf.
     * @exception DocumentException generuje błąd pracy na obiekcie klasy
     * Document.
     */
    private void createHorizontalLecturesAgenda(Document document, ReadFile readFile) {
        addHorizontalHeadingImage(document);
        addHorizontalLecturesTableToDocument(document, readFile);
        addHorizontalUnderLine(document);
    }

    private void addHorizontalHeadingImage(Document document) {
        addImageOnCentre("images/headingHorizontal.png", document);
    }

    private void addHorizontalUnderLine(Document document) {
        addImageOnCentre("images/underline.png", document);
    }

    private void addHorizontalLecturesTableToDocument(Document document, ReadFile readFile) {
        try {
            document.add(createHorizontalLecturesTable(readFile));
        } catch (DocumentException ex) {
            Logger.getLogger(ConvertFile.class.getName()).log(Level.SEVERE, null, ex);
            saveErrorRaport(ex.getMessage());
        }
    }

    private PdfPTable createHorizontalLecturesTable(ReadFile readFile) {
        PdfPTable tableOfLectures = new PdfPTable(7);
        tableOfLectures.setWidthPercentage(110);
        addHorizontalHeadingTable(tableOfLectures);
        drawHorizontalContentTableOfLectures(tableOfLectures, readFile);
        return tableOfLectures;
    }

    private void addHorizontalHeadingTable(PdfPTable tableOfLectures) {
        Font font = setBoldFont(FontFactory.TIMES_ROMAN, BaseColor.WHITE);
        setHorizontalHeadingCell(tableOfLectures, font);
    }

    private void setHorizontalHeadingCell(PdfPTable tableOfLectures, Font font) {
        setHorizontalHeadingCellTitle(tableOfLectures, font);
        setHorizontalHeadingCellCategories(tableOfLectures, font);
    }

    private void setHorizontalHeadingCellTitle(PdfPTable tableOfLectures, Font font) {
        PdfPCell cell = new PdfPCell(new Phrase("AGENDA", font));
        setCellBackgroundColor(cell, "#2A43A8");
        cell.setColspan(7);
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderWidth(3);
        tableOfLectures.addCell(cell);
    }

    private void setHorizontalHeadingCellCategories(PdfPTable tableOfLectures, Font font) {
        PdfPCell cell = new PdfPCell();
        setCellBackgroundColor(cell, "#2A43A8");
        cell.setColspan(1);
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderWidth(3);

        cell.setPhrase(new Phrase("FROM-TO", font));
        cell.setVerticalAlignment(Element.ALIGN_RIGHT);
        tableOfLectures.addCell(cell);
        cell.setPhrase(new Phrase("MONDAY", font));
        cell.setVerticalAlignment(Element.ALIGN_RIGHT);
        tableOfLectures.addCell(cell);
        cell.setPhrase(new Phrase("TUESDAY", font));
        cell.setVerticalAlignment(Element.ALIGN_RIGHT);
        tableOfLectures.addCell(cell);
        cell.setPhrase(new Phrase("WEDNESDAY", font));
        cell.setVerticalAlignment(Element.ALIGN_RIGHT);
        tableOfLectures.addCell(cell);
        cell.setPhrase(new Phrase("THURSDAY", font));
        cell.setVerticalAlignment(Element.ALIGN_RIGHT);
        tableOfLectures.addCell(cell);
        cell.setPhrase(new Phrase("FRIDAY", font));
        cell.setVerticalAlignment(Element.ALIGN_RIGHT);
        tableOfLectures.addCell(cell);
        cell.setPhrase(new Phrase("COMMENTS", font));
        cell.setVerticalAlignment(Element.ALIGN_RIGHT);
        tableOfLectures.addCell(cell);
    }

    private void setCellBackgroundColor(PdfPCell cell, String color) {
        BaseColor headingColor = WebColors.getRGBColor(color);
        cell.setBackgroundColor(headingColor);
    }

    private void drawHorizontalContentTableOfLectures(PdfPTable tableOfLectures, ReadFile readFile) {

        PdfPCell cell = new PdfPCell();
        String[] formOfLectures = new String[]{"CL", "W", "CP", "S", "L", "WF"};
        GraphicLectureBlock lectureBlock = new GraphicLectureBlock("", "");
        setCellBackgroundColor(cell, "#2A43A8");
        setCellParametrs(cell);
        for (int hourIndex = 0; hourIndex < 8; hourIndex++) {
            Font font = setBoldFont(FontFactory.TIMES_ROMAN, BaseColor.BLACK);
            setDate(cell, font);
            setCellBackgroundColor(cell, "#FFD700");
            tableOfLectures.addCell(cell);

            setCellBackgroundColor(cell, "#FFFFFF");
            cell.setPhrase(new Phrase());
            setLectureInDay(0, cell, readFile);
            tableOfLectures.addCell(cell);
            setCellBackgroundColor(cell, "#FFFFFF");
            cell.setPhrase(new Phrase());
            setLectureInDay(1, cell, readFile);
            tableOfLectures.addCell(cell);
            setCellBackgroundColor(cell, "#FFFFFF");
            cell.setPhrase(new Phrase());
            setLectureInDay(2, cell, readFile);
            tableOfLectures.addCell(cell);
            setCellBackgroundColor(cell, "#FFFFFF");
            cell.setPhrase(new Phrase());
            setLectureInDay(3, cell, readFile);
            tableOfLectures.addCell(cell);
            setCellBackgroundColor(cell, "#FFFFFF");
            cell.setPhrase(new Phrase());
            setLectureInDay(4, cell, readFile);
            tableOfLectures.addCell(cell);

            if (hourIndex < 6) {
                lectureBlock.setColor(formOfLectures[hourIndex]);
                setCellBackgroundColor(cell, lectureBlock.getColor());
                cell.setBorderWidth(2);
                cell.setPhrase(new Phrase(formOfLectures[hourIndex] + "-" + lectureBlock.getComments(formOfLectures[hourIndex])));
                tableOfLectures.addCell(cell);
            } else {
                setCellBackgroundColor(cell, "#FFFFFF");
                cell.setBorderWidth(0);
                cell.setPhrase(new Phrase());
                tableOfLectures.addCell(cell);
            }

        }
        hour = 7;
        minutes = 45;
    }

    private void setCellParametrs(PdfPCell cell) {
        cell.setColspan(1);
        cell.setMinimumHeight(40);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderWidth(2);
    }

    private void setDate(PdfPCell cell, Font font) {
        minutes = minutes + 15;
        Phrase phrase = new Phrase("", font);

        checkFullMinutes();
        String currentHour = setCurrentHour();
        phrase.add(currentHour + "-");

        hour++;
        minutes = minutes + 30;
        checkFullMinutes();

        currentHour = setCurrentHour();

        phrase.add(currentHour);
        cell.setBorderWidth(2);
        cell.setPhrase(phrase);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

    }

    private void checkFullMinutes() {
        if (minutes / 60 == 1) {
            hour++;
            minutes = minutes % 60;
        }
    }

    private String changeMinutesIntToString() {
        if (minutes == 0) {
            return "00";
        } else {
            return Integer.toString(minutes);
        }
    }

    private void setLectureInDay(int dayIndex, PdfPCell cell, ReadFile readFile) {
        String currentHour = setCurrentHour();
        cell.setBorderWidth(0);
        int numberOfLectures = readFile.getWeek().getDay(dayIndex).lessons_size();
        for (int lectureIndex = 0; lectureIndex < numberOfLectures; lectureIndex++) {
            String hourToFile = readFile.getWeek().getDay(dayIndex).getLessons(lectureIndex).getHourTo();

            if (currentHour.equals(hourToFile)) {
                cell.setBorderWidth(2);
                Lecture lecture = readFile.getWeek().getDay(dayIndex).getLessons(lectureIndex);
                GraphicLectureBlock lectureBlock = new GraphicLectureBlock(lecture);
                setCellBackgroundColor(cell, lectureBlock.getColor());
                cell.setPhrase(new Phrase(lectureBlock.getBlockText()));
                break;
            }
        }
    }

    private String setCurrentHour() {
        String currentHour = Integer.toString(hour);
        currentHour = currentHour + ":" + changeMinutesIntToString();
        return currentHour;
    }

    /**
     * Metoda konwertująca wyczytany plik do formatu doc(funkcja niedostępna)
     * umożliwia łatwe rozszerzenie programu o nowy sposób konwersji.
     *
     * @param readFile Obiekt klasy readFile z określoną na sztywno lokalizacją
     * pliku txt z planem zajęć.
     * @return prawdę jeśli operacja konwersji się powiodła i fałsz jeśli
     * zakończyła się wystąpieniem wyjątku.
     */
    private boolean convertToDOC(ReadFile readFile) {
        return false;
    }

    /**
     * Wykonuje operacje konwersji równolegle wybraną metodą i do wybranego
     * formatu.
     */
    @Override
    public void run() {
        ReadFile readFile = new ReadFile("agenda");
        convertChosenMethod(readFile);
    }

    private void convertChosenMethod(ReadFile readFile) {
        switch (convertState) {
            case DOC:
                this.convertToDOC(readFile);
                break;
            case PDF:
                this.convertToPDF(readFile);
                break;
            case RTF:
                break;
            case HTML:
                break;

        }
    }

    /**
     * Metoda ustawiająca metadane tworzonego dokumentu
     *
     * @param document Dokument na rzecz którego zostaną ustawione metadane
     */
    public void setDocumentMetaData(Document document) {
        DocumentMetaData documentMetaData = new DocumentMetaData();
        document.addTitle(documentMetaData.getTitle());
        document.addAuthor(documentMetaData.getAuthor());
        document.addSubject(documentMetaData.getSubject());
        document.addKeywords(documentMetaData.getKeywords());
        document.addCreator(documentMetaData.getCreator());
    }

    private void setFileLocation(String filePath, String fileName, String fileEnlargement) {
        setFilePath(filePath);
        setFileName(fileName);
        setFileEnlargement(fileEnlargement);
    }

    /**
     * Metoda ustawiająca ścieżkę zapisu generowanego pliku.
     *
     * @param filePath Ścieżkę zapisu generowanego pliku bez konieczności
     * podawania rozszerzenia.
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Metoda ustawiająca nazwę generowanego pliku.
     *
     * @param fileName Nazwa generowanego pliku bez konieczności podawania
     * rozszerzenia.
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private void setFileEnlargement(String fileEnlargement) {
        this.fileEnlargement = fileEnlargement;
    }

    /**
     * Metoda ustawiająca wszystkie możliwe stany klasy ConvertFile.
     *
     * @param convertState Wybrany typ konwersji.
     * @param arrangementState Wybrany układ strony.
     */
    public void setAllState(ConvertType convertState, ArrangementType arrangementState) {
        setConvertState(convertState);
        setArrangementState(arrangementState);
    }

    /**
     * Metoda ustawiająca typ konwersji.
     *
     * @param convertState Wybrany typ konwersji.
     */
    public void setConvertState(ConvertType convertState) {
        this.convertState = convertState;
    }

    /**
     * Metoda ustawiająca układ stron.
     *
     * @param arrangementState Wybrany układ strony.
     */
    public void setArrangementState(ArrangementType arrangementState) {
        this.arrangementState = arrangementState;
    }

    private void saveErrorRaport(String errorMassage) {
        File errorFile = new File("/errorRaport/errorRaport.txt");
        if (errorFile.exists()) {
            saveErrorMassageToOutputFile(errorFile, errorMassage);
        } 
        else {
            createNewErrorRaportFile(errorFile);
            saveErrorMassageToOutputFile(errorFile, errorMassage);
        }
    }

    private void createNewErrorRaportFile(File errorRaportFile) {
        try {
            errorRaportFile.createNewFile();
        } 
        catch (IOException ex) {
            Logger.getLogger(ConvertFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void saveErrorMassageToOutputFile(File errorRaportFile, String errorMassage){
        try {
                PrintWriter errorRaportOutput = new PrintWriter(errorRaportFile);
                errorRaportOutput.println(errorMassage);
            } 
        catch (FileNotFoundException ex) {
                Logger.getLogger(ConvertFile.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
}
