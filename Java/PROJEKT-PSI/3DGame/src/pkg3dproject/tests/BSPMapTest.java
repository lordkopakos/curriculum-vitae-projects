package pkg3dproject.tests;

import java.awt.Graphics2D;
import java.io.IOException;
import java.util.*;

import pkg3dproject.math3D.*;
import pkg3dproject.bsp2D.*;
import pkg3dproject.shooter3D.*;
import pkg3dproject.game.*;

public class BSPMapTest extends ShooterCore {

    public static void main(String[] args) {
        new BSPMapTest(args).run();
    }

    protected BSPTree bspTree;
    protected String mapFile;

    public BSPMapTest(String[] args) {
        super(args);
        for (int i=0; mapFile == null && i<args.length; i++) {
            if (mapFile == null && !args[i].startsWith("-")) {
                mapFile = args[i];
            }
        }
        if (mapFile == null) {
            mapFile = "../images/sample.map";
        }
    }

    public void createPolygons() {
        float ambientLightIntensity = .2f;
        List lights = new LinkedList();
        lights.add(new PointLight3D(-100,100,100, .3f, -1));
        lights.add(new PointLight3D(100,100,0, .3f, -1));

        MapLoader loader = new MapLoader();
        loader.setObjectLights(lights, ambientLightIntensity);

        try {
            bspTree = loader.loadMap(mapFile);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }

        gameObjectManager = new SimpleGameObjectManager();
        gameObjectManager.addPlayer(new GameObject(
            new PolygonGroup("Player")));
        ((BSPRenderer)polygonRenderer).
            setGameObjectManager(gameObjectManager);

        createGameObjects(loader.getObjectsInMap());
        Transform3D start = loader.getPlayerStartLocation();
        gameObjectManager.getPlayer().getTransform().setTo(start);
    }

    private void createGameObjects(List mapObjects) {
        Iterator i= mapObjects.iterator();
        while (i.hasNext()) {
            PolygonGroup group = (PolygonGroup)i.next();
            String filename = group.getFilename();
            if ("robot.obj".equals(filename)) {
                gameObjectManager.add(new Bot(group));
            }
            else {
                // obiekt statyczny
                gameObjectManager.add(new GameObject(group));
            }
        }
    }


    public void draw(Graphics2D g) {

        polygonRenderer.startFrame(g);

        // rysuje wielok�t przechowywany w drzewie BSP (ustawia z-bufor)
        ((BSPRenderer)polygonRenderer).draw(g, bspTree);

        // rysuje wielok�ty obiektu w grze (sprawdza i ustawia z-bufor)
        gameObjectManager.draw(g,
            (GameObjectRenderer)polygonRenderer);

        polygonRenderer.endFrame(g);

        super.drawText(g);
    }
}
