/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioproject;

/**
 *(Wzorzec projektowy Command) Interfejs zawierający wszystkie metody abstrakcyjne potrzebne do 
 * inicjalizacji widoku okna programu. 
 * @see ApplicationView
 * @author Dominik Kopaczka
 * @version 1.0
 * @since   2016-01-02
 */
public interface I_View {

    /**
     *Główna metoda uruchamiająca wszystkie inne metodyinicjalizujące.
     */
    void init();

    /**
     *Metoda inicjalizująca okno programu.
     */
    void initFrame();

    /**
     *Metoda inicjalizująca główny panel programu.
     */
    void initMainPanel();

    /**
     *Metoda inicjalizująca wszystkie komponenty swing.
     */
    void initAllComponent();

    /**
     *Metoda inicjalizująca pole wyboru.
     */
    void initComboBoxes();

    /**
     *Metoda inicjalizująca przyciski.
     */
    void initButtons();

    /**
     *Metoda inicjalizująca pola tekstowe wraz z etykietami.
     */
    void initFields();
}
