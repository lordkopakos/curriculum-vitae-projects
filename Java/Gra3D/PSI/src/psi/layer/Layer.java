package psi.layer;


import java.util.LinkedList;
import java.util.Random;
import psi.network.VectorX;
import psi.neural.AdelineCell;
import psi.neural.LearningCell;

//Klasa Layer reprezentuje pojedyńczą warstwe sieci neutronowych
public class Layer {

    //Zmienne definiujące warstwe sieci neuronowej
    private int numberOfNeurals;
    public LinkedList<LearningCell> neurals;
    public VectorX finalizeData;
    public boolean  initRandomWeight;


    //Warstwa sieci neuronowej o podanej liczbie neutronów i każdy
    //z nich ma określoną liczbę wejść
    public Layer(int numberNeural){
        neurals = new LinkedList<>();
        this.numberOfNeurals = numberNeural;
        this.initRandomWeight=false;
        addNeural(numberNeural);
    }
    private void addNeural(int numberNeural){
        for (int i=0; i<numberNeural;i++){
            neurals.add(new LearningCell());
        }
    }

    public void addInputWithRandomWeight(VectorX v) {
        finalizeData=new VectorX(numberOfNeurals);
        neurals.stream().map((neural) -> {
            neural.addInput(v.getLength());
            return neural;
        }).forEach((neural) -> {
            for(int i=0; i<v.getLength();i++){
               neural.setInputData(i, v.getCoordinates(i));
               Random rand = new Random();
               double rx = rand.nextDouble();
               neural.setInputWeight(i, rx);
            }
        });
    }
    public void setInput(VectorX v) {
        finalizeData=new VectorX(numberOfNeurals);
        neurals.stream().forEach((neural) -> {
            for(int i=0; i<v.getLength();i++){
                neural.setInputData(i, v.getCoordinates(i));
                Random rand = new Random();
                double rx = rand.nextDouble();
                neural.setInputWeight(i, rx);
            }
        });
    }
    
    public void setFinalizeData(){
        int count=0;
        for(AdelineCell neural : neurals){
            finalizeData.setCoordinates(count, neural.getOutput());
            count++;
        }
    }
    
    public VectorX getFinalizeData(){
        return finalizeData;
    }
    
    public void setRandomWeight(double from, double to){
        for(int i=0; i<neurals.size(); i++){
            neurals.get(i).setRandomWeight(from, to);
        }
    }
    
    public int getNumberOfNeurals(){
        return numberOfNeurals;
    }
    
    public void showWeight(){
        for(int i=0; i<neurals.size();i++){
            System.out.println("Neuron "+i+": ");
            neurals.get(i).showWeight();
        }
    }
    
    /////////////////////////Unsupervised Learning/////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    
    public void HebbLearning(VectorX v){
        for(LearningCell lc: neurals){
            lc.HebbRule(v);
        }
    }
    public void expandedHebbLearning(VectorX v){
        for(LearningCell lc: neurals){
            lc.expandedHebbRule(v);
        }
        
    }
    public void OjiLearning(VectorX v){
        for(LearningCell lc: neurals){
            lc.OjiRule(v);
        }
        
    }
    public void instarGrossbergLearning(VectorX v){
        for(LearningCell lc: neurals){
            lc.instarRuleGrossberg(v);
        }
        
    }
    public void outstarGrossbergLearning(VectorX v){
        for(LearningCell lc: neurals){
            lc.outstarRuleGrossberg(v);
        }
    }
    public void antiHebbLearning(VectorX v){
        for(LearningCell lc: neurals){
            lc.antiHebbRule(v);
        }
    }
    
    //////////////////////////////////////////////////////////////////////////
}
