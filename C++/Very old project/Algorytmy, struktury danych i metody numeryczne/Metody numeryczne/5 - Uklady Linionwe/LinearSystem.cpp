#include "LinearSystem.h"

void LinearSystem::loadGaussMatrix()
{
	fstream file;
	file.open("gauss.txt", ios::in);
	if (file.good()) {
		unsigned int N;
		double a;
		file >> N;
		
		if (M.size() > 0)	M.clear();
		if (b.size() > 0)	b.clear();
		M.reserve(N);
		for (unsigned int i = 0; i < N; ++i)
			M.push_back(vector<double>(N + 1));	//N+1 za wczasu (i tak rozszerzamy macierz o wektor X[])

		//Wype�nianie wektora
		b.reserve(N);
		for (unsigned int i = 0; i < N; ++i) {
			file >> a;
			b.push_back(a);
		}

		//Wype�nianie macierzy
		for (unsigned int i = 0; i < N; ++i) {
			for (unsigned int j = 0; j < N; ++j) {
				file >> a;
				M[i][j] = a;
			}
		}

		//Rozszerzanie macierzy o wektor X
		for (unsigned int i = 0; i < N; ++i)
			M[i][N] = b[i];
	}
	else throw 1;
}

void LinearSystem::loadJacobiMatrix()
{
	fstream file;
	file.open("jacobi.txt", ios::in);
	if (file.good()) {
		unsigned int N;
		double a;
		file >> N;

		if (M.size() > 0)	M.clear();
		if (b.size() > 0)	b.clear();
		if (Xn.size() > 0)	Xn.clear();
		M.reserve(N);
		for (unsigned int i = 0; i < N; ++i)
			M.push_back(vector<double>(N));	

		//Wypelnianie wektora b
		b.reserve(N);
		for (unsigned int i = 0; i < N; ++i) {
			file >> a;
			b.push_back(a);
		}

		//Wype�nianie wekt. warunku poczatkowego
		Xn.reserve(N);
		for (unsigned int i = 0; i < N; ++i) {
			file >> a;
			Xn.push_back(a);
		}

		//Wype�nianie macierzy
		for (unsigned int i = 0; i < N; ++i) {
			for (unsigned int j = 0; j < N; ++j) {
				file >> a;
				M[i][j] = -a;
			}
		}
	}
	else throw 1;
}

void LinearSystem::solveGauss()
{
	/* Zerowanie macierzy */
	for (unsigned int k = 0; k < M.size() + 1; k++) {
		for (unsigned int i = 1 + k; i < M.size(); i++)
		{
			//if (fabs(M[k][k] < eps)) throw 1.0;
			double coefficient = M[i][k] / M[k][k];
			for (unsigned int j = k; j < M[i].size(); j++)
				M[i][j] -= coefficient * M[k][j];
		}
	}

	/* Redukowanie macierzy o wektor X */
	int last = b.size();
	for (unsigned int i = 0; i < b.size(); ++i) {
		b[i] = M[i][last];
		M[i].pop_back();
	}

	//Wyliczanie xn-1....x0
	vector<double> X(b.size());
	int n = b.size() - 1;
	//if (n == b.size() - 1)
	X[n] = b[n] / M[n][n];
	if (b.size() > 1) {
		int k = 2;
		for (int i = b.size() - 2; i >= 0; i--, k++) {

			double eq = b[i];
			for (int j = b.size() - 1; j > b.size() - k; j--) {
				eq -= X[j] * M[i][j];
			}
			//if (fabs(M[i][i] < eps)) throw 1.0;
			X[i] = eq / M[i][i];
		}
	}

	cout << "Metoda eliminacji Gaussa:\n";
	for (unsigned int i = 0; i < X.size(); ++i)
		cout << "\nx" << i << "= " << X[i];
	cout << endl;

}

void LinearSystem::solveJacobi(int steps)
{
	checkDiagonalDominance();
	/* invD to odwrotno�� macierzy D */
	vector<vector<double>> invD(M.size(), vector<double>(M.size()));
	for (unsigned int i = 0; i < M.size(); ++i) {
		invD[i][i] = -1.0 / M[i][i];
		M[i][i] = 0.0;
	}

	//  M_0 = (U+L) x invD - oszcz�dzamy sobie mno�enia na pizdeczke za ka�dym razem 
	//	V_0 = invD x b jak wy�ej 
	vector<vector<double>> M_0(M.size(), vector<double>(M.size()));
	vector<double> V_0(M.size());
	unsigned int N = M.size(); 

	/* Korzystam z faktu ze macierz diagonalna ma wartosci tylko na przekatnej - oszczednosc mnozenia/dodawania = profit :^) */
	for (unsigned int y = 0; y < N; ++y) 
		for (unsigned int x = 0; x < N; ++x) 
			M_0[y][x] = invD[y][y] * M[y][x];

	//Macierz M ju� nie jest potrzebna
	M.clear();

	/* Znowu korzystamy z faktu macierzy diagonalnej - znowu profit :^) */
	for (unsigned int y = 0; y < N; ++y)
		V_0[y] = b[y] * invD[y][y];
	b.clear();

	/* Iteracyjne generowanie rozwiazan - im wiecej iteracji tym bardziej zblizony wynik (patrz wynik z Gaussa) */
	cout << "\nMetoda Jacobiego ("<<steps<<" krokow):\n\nx0" << " = [";
	for (unsigned int x = 0; x < N; ++x) {
		cout << Xn[x];
		if (x != N - 1)cout << ", ";
	}
	cout << "]"; 

	vector<double> temp(N);
	for (int i = 1; i < steps + 1; ++i) {
		for (unsigned int y = 0; y < N; ++y) {
			double sum = 0.0;
			for (unsigned int j = 0; j < N; ++j)
				sum += Xn[j] * M_0[y][j];
			sum += V_0[y];
			temp[y] = sum;
		}
		cout << "\nx" << i << " = [";
		for (unsigned int x = 0; x < N; ++x) {
			cout << temp[x];
			if (x != N - 1)cout << ", ";
		}
		cout << "]";
		Xn = temp;
	}
	cout << endl;
}

void LinearSystem::checkDiagonalDominance()
{
	bool strictInequalityConditon = false;
	for (unsigned int i = 0; i < M.size(); ++i) {
		double rowSum = 0.0;
		for (unsigned int j = 0; j < M[i].size(); ++j) {
			if (i != j)	rowSum += fabs(M[i][j]);
		}
		if (rowSum > fabs(M[i][i]))	throw false;
		if (fabs(M[i][i]) > rowSum && !strictInequalityConditon)	strictInequalityConditon = true;
	}
	if (!strictInequalityConditon)	throw false;
	cout << "\nMacierz M jest diagonalnie dominujaca.\n";
}

LinearSystem::LinearSystem() : eps(1e-12)
{
}

/* kody wyklete w zabe zaklete

for (unsigned int y = 0; y < N; ++y) {
	for (unsigned int x = 0; x < N; ++x) {
		double sum = 0.0;
		for (unsigned int i = 0; i < N; ++i)
			sum += invD[y][i] * M[i][x];
		M_0[y][x] = sum;
	}
}

for (unsigned int y = 0; y < N; ++y) {
	double sum = 0.0;
	for (unsigned int j = 0; j < N; ++j) {
		sum += b[j] * invD[y][j];
	}
	V_0[y] = sum;
}

for (unsigned int i = 1; i < N; ++i) {
	for (unsigned int y = 0; y < N; ++y) {
		double sum = 0.0;
		for (unsigned int j = 0; j < N; ++j)
			sum += X[i - 1][j] * M_0[y][j];
		sum += V_0[y];
		X[i][y] = sum;
	}
}

for (unsigned int y = 0; y < X.size(); ++y) {
	cout << "\nx" << y << " = [";
	for (unsigned int x = 0; x < X[y].size(); ++x) {
		cout << X[y][x];
		if (x != X.size() - 1)cout << ", ";
	}
	cout << "]";
}
*/