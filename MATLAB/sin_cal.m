function varargout = sin_cal(varargin)
% SIN_CAL M-file for sin_cal.fig
%      SIN_CAL, by itself, creates a new SIN_CAL or raises the existing
%      singleton*.
%
%      H = SIN_CAL returns the handle to a new SIN_CAL or the handle to
%      the existing singleton*.
%
%      SIN_CAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SIN_CAL.M with the given input arguments.
%
%      SIN_CAL('Property','Value',...) creates a new SIN_CAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sin_cal_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sin_cal_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sin_cal

% Last Modified by GUIDE v2.5 24-Apr-2017 10:16:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @sin_cal_OpeningFcn, ...
                   'gui_OutputFcn',  @sin_cal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before sin_cal is made visible.
function sin_cal_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sin_cal (see VARARGIN)

% Choose default command line output for sin_cal
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes sin_cal wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = sin_cal_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu.
function popupmenu_Callback(hObject, ~, handles)
% hObject    handle to popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global A 
global B
global which_axes
% Hints: contents = get(hObject,'String') returns popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu
contents = get(hObject, 'String');
popupmenuValue = contents{get(hObject, 'Value')};

switch popupmenuValue
    case 'Wczytaj parabole'
        hold off;
        
        [A, B] = textread('parabola.txt', '%f %f');
        
        plot(A, B);
        hold on;
        
        %Visible
        
        set(handles.uipanel2, 'Visible','on');
        set(handles.axes, 'Visible','on');
        
        which_axes=1;
        
    case 'Wczytaj sinus'
        hold off;
        
        set(handles.uipanel2, 'Visible','on');
        set(handles.axes, 'Visible','on');
        x= 0:0.01:3;
        y=sin(x);
        plot(x,y);
        hold on;
        which_axes=2;
end

% --- Executes during object creation, after setting all properties.
function popupmenu_CreateFcn(hObject, ~, ~)
% hObject    handle to popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Xw_Callback(hObject, eventdata, handles)
% hObject    handle to Xw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Xw as text
%        str2double(get(hObject,'String')) returns contents of Xw as a double


% --- Executes during object creation, after setting all properties.
function Xw_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Xw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Yw_Callback(hObject, eventdata, handles)
% hObject    handle to Yw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Yw as text
%        str2double(get(hObject,'String')) returns contents of Yw as a double


% --- Executes during object creation, after setting all properties.
function Yw_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Yw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkboxWSP_W.
function checkboxWSP_W_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxWSP_W (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global which_axes
% Hint: get(hObject,'Value') returns toggle state of checkboxWSP_W
switch which_axes
    case 1
    
if get(hObject, 'Value')==1
          set(handles.uipanel3, 'Visible','on');
        
        
else
    set(handles.uipanel3, 'Visible','off');
        
end



A = handles.A;
B = handles.B;

p1 = polyfit( A, B, 2);
delta = p1(2)^2 - 4 * p1(1) * p1(3);
xw = -p1(2) / (2 * p1(1));
yw = -delta / (4 * p1(1));

axes(handles.axes);
plot(xw, yw, 'g*');
hold on;

set( handles.Xw, 'string', num2str(xw) );
set( handles.Yw, 'string', num2str(yw) );
case 2
    x=0:0.01:3;
    y=sin(x);
    p1=polyfit(x, y, 2);
    delta = p1(2)^2 - 4 * p1(1) * p1(3);
xw = -p1(2) / (2 * p1(1));
yw = -delta / (4 * p1(1));
axes(handles.axes);
plot(xw, yw, 'g*');
xw
yw
hold on;
end
% --- Executes on button press in checkboxC.
function checkboxC_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxC

if get(hObject, 'Value')==1
           set(handles.uipanel4, 'Visible','on');
        set(handles.sliderIntegral, 'Visible','on');
        set(handles.text7, 'Visible','on');
        set(handles.text9, 'Visible','on');
        set(handles.text8, 'Visible','on');
        
else
    set(handles.uipanel4, 'Visible','off');
        set(handles.sliderIntegral, 'Visible','off');
        set(handles.text7, 'Visible','off');
        set(handles.text9, 'Visible','off');
        set(handles.text8, 'Visible','off');
end


function limA_Callback(hObject, eventdata, handles)
% hObject    handle to limA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of limA as text
%        str2double(get(hObject,'String')) returns contents of limA as a double


% --- Executes during object creation, after setting all properties.
function limA_CreateFcn(hObject, eventdata, handles)
% hObject    handle to limA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function limB_Callback(hObject, eventdata, handles)
% hObject    handle to limB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of limB as text
%        str2double(get(hObject,'String')) returns contents of limB as a double


% --- Executes during object creation, after setting all properties.
function limB_CreateFcn(hObject, eventdata, handles)
% hObject    handle to limB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderIntegral_Callback(hObject, eventdata, handles)
% hObject    handle to sliderIntegral (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global A 
global B
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
lim = get( hObject, 'Value');

set( handles.limA, 'String', num2str(-lim) );
set( handles.limB, 'String', num2str(lim) );

x = linspace(-lim, lim, 100);

pp=polyfit(A,B,2);

fx=pp(1)*x.^2+pp(2)*x+pp(3);
x1=[1 2 3 4]
x2=[1; 2; 3; 4]
x1*x2
x1.*x1
I = trapz(x, fx);

set( handles.integralValue, 'String', num2str(I) );

hold off

logicx=(A>-lim & A<lim);
%xx= find(logicx==1);
wekX=[-lim;A(logicx);lim];
wekY=[0;B(logicx);0];
axes(handles.axes);
plot(A, B, 'b-');
hold on;
%area(A(xx), B(xx),'r')
fill(wekX, wekY, 'm');


% --- Executes during object creation, after setting all properties.
function sliderIntegral_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderIntegral (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
set(hObject, 'SliderStep', [1/18 , 1/18 ]);
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function integralValue_Callback(hObject, eventdata, handles)
% hObject    handle to integralValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of integralValue as text
%        str2double(get(hObject,'String')) returns contents of integralValue as a double


% --- Executes during object creation, after setting all properties.
function integralValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to integralValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
