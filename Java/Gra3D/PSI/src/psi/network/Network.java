
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.network;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Pattern;
import psi.layer.Layer;
import psi.neural.AdelineCell;
import psi.neural.LearningCell;

/**
 *
 * @author lordkopakos
 */
public class Network implements NetworkLerning {

    //Zmienne potrzebne do załadowanie pliku trainingSet.txt
    private String filepath;
    private ArrayList lines;
    private ArrayList<Double> MAPE;
    private ArrayList<Double> MSE;
    private int eras;
    private boolean initNetwork = false;

    //Wartosci na wejsciu
    private ArrayList<VectorX> m_VectorX;
    private ArrayList<VectorX> validVectorX;

    //Zmienne definiujące warstwy
    private ArrayList<Layer> layers;
    private ArrayList<Integer> numberNeuralInLayers;

    private void addNewLayers(int numberOfNeuralInLayers) {
        layers.add(new Layer(numberOfNeuralInLayers));
        numberNeuralInLayers.add(numberOfNeuralInLayers);
    }

    public Network() throws IOException {
        this(1);
    }

    public Network(int eras) throws IOException {
        numberNeuralInLayers = new ArrayList<>(5);
        layers = new ArrayList<>(5);
        MAPE=new ArrayList<>(100);
        MSE=new ArrayList<>(100);
        this.eras=eras;
        filepath = "learn/trainingSet/";
        initSetTraining("trainingSet.txt");
        initSetValid("trainingSet.txt");
        //initSetTraining("trainingSet.txt",validVectorX);
        addNewLayers(2);
        addNewLayers(1);
        
        startAllLearningMethod();
        
    }
    
    public void  startAllLearningMethod(){
        System.out.println("//////////////////////////////////////////////////");
        System.out.println("////////////////Back Propagation//////////////////");
        System.out.println("//////////////////////////////////////////////////");
        long startTime = System.nanoTime();
        backPropagation(0, 1);
        long endTime=System.nanoTime();
        System.out.println("Czas uczenia sieci metodą wstecznej propagacji wynosi: "+(endTime-startTime));
        for(VectorX v:validVectorX){
            checkValid(v);
        }
        System.out.println("//////////////////////////////////////////////////");
        System.out.println("/////////////////Hebb Learning////////////////////");
        System.out.println("//////////////////////////////////////////////////");
        startTime = System.nanoTime();
        HebbLearning();
        endTime=System.nanoTime();
        System.out.println("Czas uczenia sieci metodą Hebba: "+(endTime-startTime));
        for(VectorX v:validVectorX){
            checkValid(v);
        }
        System.out.println("//////////////////////////////////////////////////");
        System.out.println("/////////////Expend Hebb Learning/////////////////");
        System.out.println("//////////////////////////////////////////////////");
        startTime = System.nanoTime();
        expandedHebbLearning();
        endTime=System.nanoTime();
        System.out.println("Czas uczenia sieci rozszerzoną metodą Hebba: "+(endTime-startTime));
        for(VectorX v:validVectorX){
            checkValid(v);
        }
        System.out.println("//////////////////////////////////////////////////");
        System.out.println("/////////////////Oji Learning/////////////////////");
        System.out.println("//////////////////////////////////////////////////");
        startTime = System.nanoTime();
        OjiLearning();
        endTime=System.nanoTime();
        System.out.println("Czas uczenia sieci metodą Oji: "+(endTime-startTime));
        for(VectorX v:validVectorX){
            checkValid(v);
        }
        System.out.println("//////////////////////////////////////////////////");
        System.out.println("///////////Instar Grossberg Learning//////////////");
        System.out.println("//////////////////////////////////////////////////");
        startTime = System.nanoTime();
        instarGrossbergLearning();
        endTime=System.nanoTime();
        System.out.println("Czas uczenia sieci metodą gwiazd wejścia Grossberge: "+(endTime-startTime));
        for(VectorX v:validVectorX){
            checkValid(v);
        }
        System.out.println("//////////////////////////////////////////////////");
        System.out.println("///////////Outstar Grossberg Learning/////////////");
        System.out.println("//////////////////////////////////////////////////");
        startTime = System.nanoTime();
        outstarGrossbergLearning();
        endTime=System.nanoTime();
        System.out.println("Czas uczenia sieci metodą gwiazd wyjścia Grossberge: "+(endTime-startTime));
        for(VectorX v:validVectorX){
            checkValid(v);
        }
        System.out.println("//////////////////////////////////////////////////");
        System.out.println("///////////////Anti-Hebb Learning/////////////////");
        System.out.println("//////////////////////////////////////////////////");
        startTime = System.nanoTime();
        antiHebbLearning();
        endTime=System.nanoTime();
        System.out.println("Czas uczenia sieci metodą Anty-Hebba: "+(endTime-startTime));
        for(VectorX v:validVectorX){
            checkValid(v);
        }
    }

    private void addLayers() {
        for (Integer number : numberNeuralInLayers) {
            Layer layer = new Layer(number);
            layers.add(layer);
        }
    }

    private double setNetworkConstruct(VectorX v) {
        System.out.println(v);
        layers.get(0).addInputWithRandomWeight(v);
        layers.get(0).setFinalizeData();
        for (int i = 1; i < layers.size(); i++) {
            VectorX newV = layers.get(i - 1).finalizeData;
            System.out.println(newV);
            layers.get(i).addInputWithRandomWeight(newV);
            layers.get(i).setFinalizeData();
            System.out.println(layers.get(i).finalizeData);
        }
        double outputY = layers.get(layers.size() - 1).getFinalizeData().getCoordinates(0);
        layers.get(layers.size() - 1).neurals.get(0).setErrorSignal(v.getZ() - outputY);
        return outputY;
    }

    private double countOutputYForOneVector(VectorX v) {
        System.out.println(v);
        layers.get(0).setInput(v);
        layers.get(0).setFinalizeData();
        for (int i = 1; i < layers.size(); i++) {
            VectorX newV = layers.get(i - 1).finalizeData;
            System.out.println(newV);
            layers.get(i).setInput(newV);
            layers.get(i).setFinalizeData();
            System.out.println(layers.get(i).finalizeData);
        }
        double outputY = layers.get(layers.size() - 1).getFinalizeData().getCoordinates(0);
        //Błąd MSE i MAPE dla jednej iteracji
        MAPE.add((v.getZ() - outputY)/outputY);
        MSE.add((v.getZ() - outputY)*(v.getZ() - outputY));
        layers.get(layers.size() - 1).neurals.get(0).setErrorSignal(v.getZ() - outputY);
        
        return outputY;
    }
    
    private double checkValid(VectorX v) {
        System.out.println(v);
        layers.get(0).setInput(v);
        layers.get(0).setFinalizeData();
        for (int i = 1; i < layers.size(); i++) {
            VectorX newV = layers.get(i - 1).finalizeData;
            System.out.println(newV);
            layers.get(i).setInput(newV);
            layers.get(i).setFinalizeData();
            System.out.println(layers.get(i).finalizeData);
        }
        double outputY = layers.get(layers.size() - 1).getFinalizeData().getCoordinates(0);
        if(outputY==v.getZ()){
            System.out.println("///////////////////////////////////Poprawna odpowiedz");
        }
        else{
            System.out.println("///////////////////////////////////Zła odopwiedź");
        }
        
        return outputY;
    }

    private void countAllErrorSignalForOneVector(double y) {
        for (int i = layers.size() - 2; i >= 0; i--) {
            for (int j = 0; j < layers.get(i).getNumberOfNeurals(); j++) {
                double backError = 0;
                for (int k = 0; k < layers.get(i + 1).getNumberOfNeurals(); k++) {
                    backError += layers.get(i + 1).neurals.get(k).getErrorSignal() * layers.get(i + 1).neurals.get(k).getInputWeight(j);
                }
                layers.get(i).neurals.get(j).setErrorSignal(backError);
            }
        }
    }

    private void changeAllWeightForOneVector() {
        for (int i = 0; i < layers.size(); i++) {
            for (int j = 0; j < layers.get(i).neurals.size(); j++) {
                for (int k = 0; k < layers.get(i).neurals.get(j).Synapses.size(); k++) {
                    layers.get(i).neurals.get(j).setInputWeight(k,
                            layers.get(i).neurals.get(j).getInputWeight(k)
                            + layers.get(i).neurals.get(j).getErrorSignal() * layers.get(i).neurals.get(j).getLearningCoefficient() * layers.get(i).neurals.get(j).getInputData(k));
                }
            }
        }
    }

    public void initSetTraining(String fileName) throws IOException {

        lines = new ArrayList();
        int height = 0;

        // odczytanie wszystkich wierszy z pliku do listy
        BufferedReader reader = new BufferedReader(
                new FileReader(filepath + fileName));
        while (true) {
            String line = reader.readLine();
            // koniec wierszy do odczytywania
            if (line == null) {
                reader.close();
                break;
            }

            // dodawanie wszystkich wierszy poza komentarzami
            if (!line.startsWith("#")) {
                lines.add(line);
            }
        }

        // analizowanie wiersza
        int numberInputs;
        int numberVectors;

        height = lines.size();

        String settings = (String) lines.get(0);
        Pattern pattern = Pattern.compile(",");
        String[] infoTable = pattern.split(settings);
        numberInputs = Integer.valueOf(infoTable[0]);
        numberVectors = Integer.valueOf(infoTable[1]);

        if (numberVectors != (height - 1)) {
            System.out.println("Podana liczba wektorow w pliku "
                    + "jest inna niż faktyczna liczba wektorow");

        }
        m_VectorX = new ArrayList<>(numberVectors);
        

        for (int y = 1; y < height; y++) {

            String line = (String) lines.get(y);

            Pattern comma = Pattern.compile(",");
            String[] xn = comma.split(line);

            VectorX v = new VectorX(numberInputs);
            for (int i = 0; i < numberInputs; i++) {
                double value = Double.valueOf(xn[i]);
                v.setCoordinates(i, value);
            }
            double value = Double.valueOf(xn[numberInputs]);
            v.setZ(value);
            boolean add = m_VectorX.add(v);
            if (add) {
                System.out.println("Nowy VectorX został dodany");
            }
        }
    }
    public void initSetValid(String fileName) throws IOException {

        lines = new ArrayList();
        int height = 0;

        // odczytanie wszystkich wierszy z pliku do listy
        BufferedReader reader = new BufferedReader(
                new FileReader(filepath + fileName));
        while (true) {
            String line = reader.readLine();
            // koniec wierszy do odczytywania
            if (line == null) {
                reader.close();
                break;
            }

            // dodawanie wszystkich wierszy poza komentarzami
            if (!line.startsWith("#")) {
                lines.add(line);
            }
        }

        // analizowanie wiersza
        int numberInputs;
        int numberVectors;

        height = lines.size();

        String settings = (String) lines.get(0);
        Pattern pattern = Pattern.compile(",");
        String[] infoTable = pattern.split(settings);
        numberInputs = Integer.valueOf(infoTable[0]);
        numberVectors = Integer.valueOf(infoTable[1]);

        if (numberVectors != (height - 1)) {
            System.out.println("Podana liczba wektorow w pliku "
                    + "jest inna niż faktyczna liczba wektorow");

        }
        validVectorX = new ArrayList<>(numberVectors);
        

        for (int y = 1; y < height; y++) {

            String line = (String) lines.get(y);

            Pattern comma = Pattern.compile(",");
            String[] xn = comma.split(line);

            VectorX v = new VectorX(numberInputs);
            for (int i = 0; i < numberInputs; i++) {
                double value = Double.valueOf(xn[i]);
                v.setCoordinates(i, value);
            }
            double value = Double.valueOf(xn[numberInputs]);
            v.setZ(value);
            boolean add = validVectorX.add(v);
            if (add) {
                System.out.println("Nowy VectorX został dodany");
            }
        }
    }

    public void setRandomWeight(double from, double to) {
        for (int i = 0; i < layers.size(); i++) {
            layers.get(i).setRandomWeight(from, to);
        }
    }

    @Override
    public void backPropagation(double weightFrom, double weightTo) {
        //setRandomWeight(weightFrom, weightTo);
        double y;
        for (int i = 0; i < eras; i++) {
            for (VectorX v : m_VectorX) {
                if (!initNetwork) {
                    y = setNetworkConstruct(v);
                    initNetwork = true;
                } else {
                    y = countOutputYForOneVector(v);
                }
                showWeight();
                countAllErrorSignalForOneVector(y);
                changeAllWeightForOneVector();
            }
            showWeight();
        }
    }

    public void showWeight() {
        for (int i = 0; i < layers.size(); i++) {
            System.out.println("Warstwa " + i + ": ");
            layers.get(i).showWeight();
        }
    }
    
    /////////////////////////Unsupervised Learning/////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    public void HebbLearning(){
        double y;
        for (int i = 0; i < eras; i++) {
            System.out.println("Epoka uczenia "+i+1+":");
            for (VectorX v : m_VectorX) {
                if (!initNetwork) {
                    y = setNetworkConstruct(v);
                    initNetwork = true;
                } 
                showWeight();
                for (Layer l:layers){
                    l.HebbLearning(v);
                }
            }
            showWeight();
        }
    }
    public void expandedHebbLearning(){
        double y;
        for (int i = 0; i < eras; i++) {
            System.out.println("Epoka uczenia "+i+1+":");
            for (VectorX v : m_VectorX) {
                if (!initNetwork) {
                    y = setNetworkConstruct(v);
                    initNetwork = true;
                } 
                showWeight();
                for (Layer l:layers){
                    l.expandedHebbLearning(v);
                }
            }
            showWeight();
        }
    }
    public void OjiLearning(){
        double y;
        for (int i = 0; i < eras; i++) {
            System.out.println("Epoka uczenia "+i+1+":");
            for (VectorX v : m_VectorX) {
                if (!initNetwork) {
                    y = setNetworkConstruct(v);
                    initNetwork = true;
                } 
                showWeight();
                for (Layer l:layers){
                    l.OjiLearning(v);
                }
            }
            showWeight();
        }
    }
    public void instarGrossbergLearning(){
        double y;
        for (int i = 0; i < eras; i++) {
            System.out.println("Epoka uczenia "+i+1+":");
            for (VectorX v : m_VectorX) {
                if (!initNetwork) {
                    y = setNetworkConstruct(v);
                    initNetwork = true;
                } 
                showWeight();
                for (Layer l:layers){
                    l.instarGrossbergLearning(v);
                }
            }
            showWeight();
        }
    }
    public void outstarGrossbergLearning(){
        double y;
        for (int i = 0; i < eras; i++) {
            System.out.println("Epoka uczenia "+i+1+":");
            for (VectorX v : m_VectorX) {
                if (!initNetwork) {
                    y = setNetworkConstruct(v);
                    initNetwork = true;
                } 
                showWeight();
                for (Layer l:layers){
                    l.outstarGrossbergLearning(v);
                }
            }
            showWeight();
        }
    }
    public void antiHebbLearning(){
        double y;
        for (int i = 0; i < eras; i++) {
            System.out.println("Epoka uczenia "+i+1+":");
            for (VectorX v : m_VectorX) {
                if (!initNetwork) {
                    y = setNetworkConstruct(v);
                    initNetwork = true;
                } 
                showWeight();
                for (Layer l:layers){
                    l.antiHebbLearning(v);
                }
            }
            showWeight();
        }
    } 
    
    
    ///////////////////////////////////////////////////////////////////////////
    
}
