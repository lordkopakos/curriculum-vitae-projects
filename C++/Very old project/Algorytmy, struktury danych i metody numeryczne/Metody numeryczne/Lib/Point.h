﻿#pragma once
#include <ostream>

class Point
{
public:
	double x, y;

	Point(double x, double y)
		: x(x),
		  y(y)
	{
	}


	friend std::ostream& operator<<(std::ostream& os, const Point& obj)
	{
		return os
			<< "x: " << obj.x
			<< " y: " << obj.y;
	}
};
