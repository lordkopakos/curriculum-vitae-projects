#pragma once
#include<iostream>
#include<string>
#include<fstream>
#include<iomanip>
#include"Enemy.h"
#include<stdio.h>
#include<Windows.h>


using namespace std;
class Mag;
class Enemy;
class Game;
class Postac
{
	
public:
	enum Postate{rycerz, mag};
	virtual void statystyki() = 0;
	virtual bool walka(Enemy) = 0;
	friend class Game;

	//WPROWADZANIE PARAMETROW
	/////////////////////////////////////////////
	virtual void setNAME(string n) = 0;
	virtual void setHP(int h) = 0;
	virtual void setPOWER(int p) = 0;
	virtual void setDAMAGE(int d) = 0;
	/////////////////////////////////////////////////

	//ZWRACANIE PARAMATROW (przez return)
	////////////////////////////////////////////////
	virtual string getNAME() = 0;
	virtual int getHP() = 0;
	virtual int getPOWER() = 0;
	virtual int getDAMAGE() = 0;
	////////////////////////////////////////////////
};

class Mag :public Postac
{
	string nazwa;
	Postate state;
	int hp;
	int mana;
	int damage;
	HANDLE hOut;
	

public:
	//KLASY ZAPRZYJAZNIONE
	/////////////////////////////////////////
	friend class Game;
	friend class File;
	friend class Display;
	/////////////////////////////////////////

	//FUNKCJE ZAPRZYJAZNIONE
	/////////////////////////////////////////
	friend double live(Mag &M);
	/////////////////////////////////////////
	
	//FUNKCJE WIRTUALNE KLASY ABSTRAKCYJNEJ
	////////////////////////////////////////
	virtual void statystyki();
	virtual bool walka(Enemy);

	virtual void setNAME(string n) { nazwa = n; }
	virtual void setHP(int h) { hp=h; }
	virtual void setPOWER(int p) { mana=p; }
	virtual void setDAMAGE(int d) { damage=d; }
	virtual string getNAME() { return nazwa; }
	virtual int getHP() { return hp; }
	virtual int getPOWER() { return mana; }
	virtual int getDAMAGE() { return damage; }
	////////////////////////////////////////

	//KONSTRUKTORY
	//////////////////////////////////////////
	Mag(string= "MAG",int = 1, int = 1, int=1);
	//////////////////////////////////////////

	//PRZECIAZENIA OPERATOROW
	//////////////////////////////////////////
	Mag operator+(Mag m);
	friend istream& operator>>(istream & in, Mag & M);
	friend ostream& operator<<(ostream & out, Mag & M);
	///////////////////////////////////////////
};

class Rycerz :public Postac
{
	Postate state;
	string nazwa;
	int hp;
	int stamina;
	int damage;
	int tablicaStatystyk[10];
	HANDLE hOut;
public:
	friend class Game;
	//FUNKCJE WIRTUALNE KLASY ABSTRAKCYJNEJ
	////////////////////////////////////////
	virtual void statystyki();
	virtual bool walka(Enemy);

	virtual void setNAME(string n) { nazwa = n; }
	virtual void setHP(int h) { hp = h; }
	virtual void setPOWER(int p) { stamina=p; }
	virtual void setDAMAGE(int d) { damage=d; }
	virtual string getNAME() { return nazwa; }
	virtual int getHP() { return hp; }
	virtual int getPOWER() { return stamina; }
	virtual int getDAMAGE() { return damage; }
	/////////////////////////////////////////////

	//KONSTRUKTORY
	/////////////////////////////////////////////
	Rycerz(string= "Rycerz",int = 1000, int = 1000, int=100);
	/////////////////////////////////////////////

	 void wyswietl();
};


template<int ilosc, class typ>
class tabliceStatystyk
{
	typ tablica[ilosc];
public:
	tabliceStatystyk(int=1);
	void wyswietlTablice();
};

template<int ilosc, class typ>
inline tabliceStatystyk<ilosc, typ>::tabliceStatystyk(int j)
{
	for (int i = 0; i < ilosc; i++)
	{
		tablica[i] = j;
	}
}

template<int ilosc, class typ>
inline void tabliceStatystyk<ilosc, typ>::wyswietlTablice()
{
	for (int i = 0; i < ilosc; i++)
	{
		cout<<tablica[i]<<"\t";
	}
}



