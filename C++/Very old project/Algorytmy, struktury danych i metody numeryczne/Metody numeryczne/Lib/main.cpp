#include "Interpolation.h"
#include "Point.h"
#include <iostream>
#include <vector>

using namespace std;

int main(int argc, char* argv[])
{
	cout << "\n\n\n" << "LAGRANGE INTERPOLATION" << "\n";

	vector<Point> i_points = { {1,1}, {2,3}, {4,4},{5,2},{6,3} };
	vector<bool> i_flags = {true, true, true,true, true };

	auto interpolation = Interpolation(i_points, i_flags);

	interpolation.print_b_values();
	interpolation.print_x(3.5);
	interpolation.print_stat();

	system("pause");
	return 0;
}
