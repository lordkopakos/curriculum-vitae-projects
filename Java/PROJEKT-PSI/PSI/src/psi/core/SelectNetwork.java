/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.core;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.event.ActionEvent;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLayeredPane;
import javax.swing.border.Border;
import psi.Jattributes.ImagePanel;

/**
 *
 * @author lordkopakos
 */
public class SelectNetwork extends AddButton{
    
    public static void main(String[] args) {
        new SelectNetwork().menurun();
    }
    
    protected ImagePanel selectNetwork;
    protected JButton btnPerceptron;
    protected JButton btnMultiLayerNetwork;
    private JButton closeButton;
    
    @Override
    public void menurun(){
        super.menurun();
    }
    
    @Override
    public void menuinit(){
        super.menuinit();
        
        Border scoreBorder =
            BorderFactory.createLineBorder(new Color((float) 0.8, (float) 0.4, 0),5);
        
        selectNetwork= new ImagePanel("images/attributes/selectNetwork.png");
        selectNetwork.setBorder(scoreBorder);
        selectNetwork.setVisible(false);
        
        //Tworzenie przycisków
        closeButton= createButton("close", "Zamknij");
        closeButton.setSize(27, 24);
        closeButton.setLocation(selectNetwork.getWidth()-closeButton.getWidth()-10, 10);
        
        btnPerceptron= createButton("perceptron", "Create a simple Perceptron Network");
        btnPerceptron.setSize(460, 184);
        btnPerceptron.setLocation(selectNetwork.getWidth()/2-btnPerceptron.getWidth()/2, 
                selectNetwork.getHeight()/2-btnPerceptron.getHeight()-10);
        
        btnMultiLayerNetwork= createButton("multiLayer", "Create a new Multi-Layer Network");
        btnMultiLayerNetwork.setSize(460, 187);
        btnMultiLayerNetwork.setLocation(selectNetwork.getWidth()/2-btnMultiLayerNetwork.getWidth()/2, selectNetwork.getHeight()/2+10);
        
        selectNetwork.setLocation(
            (screen.getWidth() - selectNetwork.getWidth()) / 2,
            (screen.getHeight() - selectNetwork.getHeight()) / 2);
        
        selectNetwork.add(closeButton);
        selectNetwork.add(btnPerceptron);
        selectNetwork.add(btnMultiLayerNetwork);
        
        screen.getFullScreenWindow().getLayeredPane().add(selectNetwork,
            JLayeredPane.MODAL_LAYER);
    }
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        super.actionPerformed(e);
        Object src = e.getSource();
        if (src == closeButton) {
            selectNetwork.setVisible(false);
        }
        if(src==newNetworkButton){
            selectNetwork.setVisible(true);
        }
    }
    
    @Override
    public void draw(Graphics2D g) 
    {
        super.draw(g);
        
    }
    
}
