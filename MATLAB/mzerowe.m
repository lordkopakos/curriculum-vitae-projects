function zera = mzerowe(t, y)

dane = xlsread('oscillations');
t = dane(:, 1);
y = dane(:, 2);

a=480;
b=530;

f=1;
fs=1/(t(2) - t(1));

NT=floor(fs/f); %liczba punkt�w w jednym okresie

ax=480:NT/2:length(t)
bx=530:NT/2:length(t)

plot(t, y, 'ro')

hold on

p= []
for i=1:1:length(ax)-1
  plot(t(ax(i):bx(i)), y(ax(i):bx(i)), 'g*')
  p=polyfit(t(ax(i):bx(i)), y(ax(i):bx(i)), 1)
  hold on;
  r=roots(p);
  plot(r,0,'b*')
  
end
size(p)
xz=-b/a
xz=p(:,2)./p(:,1)
%hold on
%plot(t(ax:bx), y(ax:bx), 'g*')