#version 330
layout(location = 0) in vec3 vp;

uniform float trans;

void main()
{
   gl_Position = vec4(vp.x +trans, vp.y+0.25+trans, vp.z, 1.0);
}
