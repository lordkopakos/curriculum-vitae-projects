function varargout = guiProject(varargin)
% GUIPROJECT M-file for guiProject.fig
%      GUIPROJECT, by itself, creates A0 new GUIPROJECT or raises the existing
%      singleton*.
%
%      H = GUIPROJECT returns the handle to A0 new GUIPROJECT or the handle to
%      the existing singleton*.
%
%      GUIPROJECT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUIPROJECT.M with the given input arguments.
%
%      GUIPROJECT('Property','Value',...) creates A0 new GUIPROJECT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before guiProject_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to guiProject_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help guiProject

% Last Modified by GUIDE v2.5 08-May-2017 00:45:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @guiProject_OpeningFcn, ...
                   'gui_OutputFcn',  @guiProject_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before guiProject is made visible.
function guiProject_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to guiProject (see VARARGIN)

% Choose default command line output for guiProject
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes guiProject wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = guiProject_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in btnStart.
function btnStart_Callback(hObject, eventdata, handles)
% hObject    handle to btnStart (see GCBO)
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
A0=str2num(get(handles.A0, 'string'))
nT=str2num(get(handles.nT, 'string'))
f=str2num(get(handles.f, 'string'))
fs=str2num(get(handles.fs, 'string'))
d=str2num(get(handles.d, 'string'))
t=0:1/fs:nT/f
sinus=get(handles.sinus, 'Value')
if sinus==1
    y=sin(t)   
    axes(handles.axes2);
    plot(t,y)
else
    y = A0*sin(2*pi*f*t).*exp(-d*f*t)
    axes(handles.wykres);
    plot(t,y)
end

function A0_Callback(hObject, eventdata, handles)
% hObject    handle to A0 (see GCBO)
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of A0 as text
%        str2double(get(hObject,'String')) returns contents of A0 as A0 double

% --- Executes during object creation, after setting all properties.
function A0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A0 (see GCBO)
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have A0 white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nT_Callback(hObject, eventdata, handles)
% hObject    handle to nT (see GCBO)
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nT as text
%        str2double(get(hObject,'String')) returns contents of nT as A0 double


% --- Executes during object creation, after setting all properties.
function nT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nT (see GCBO)
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have A0 white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function f_Callback(hObject, eventdata, handles)
% hObject    handle to f (see GCBO)
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of f as text
%        str2double(get(hObject,'String')) returns contents of f as A0 double


% --- Executes during object creation, after setting all properties.
function f_CreateFcn(hObject, eventdata, handles)
% hObject    handle to f (see GCBO)
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have A0 white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function fs_Callback(hObject, eventdata, handles)
% hObject    handle to fs (see GCBO)
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fs as text
%        str2double(get(hObject,'String')) returns contents of fs as A0 double


% --- Executes during object creation, after setting all properties.
function fs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fs (see GCBO)
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have A0 white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function d_Callback(hObject, eventdata, handles)
% hObject    handle to d (see GCBO)
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of d as text
%        str2double(get(hObject,'String')) returns contents of d as A0 double


% --- Executes during object creation, after setting all properties.
function d_CreateFcn(hObject, eventdata, handles)
% hObject    handle to d (see GCBO)
% eventdata  reserved - to be defined in A0 future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have A0 white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_Callback(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
A0=get(hObject,'Value');
set(handles.A0, 'string', num2str(A0));
btnStart_Callback(hObject, eventdata, handles)



% --- Executes during object creation, after setting all properties.
function slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in sinus.
function sinus_Callback(hObject, eventdata, handles)
% hObject    handle to sinus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of sinus
axes(handles.wykres);
hold off;
plot(0,0,'g*');
btnStart_Callback(handles.btnStart, eventdata, handles);


% --- Executes on button press in sinusT.
function sinusT_Callback(hObject, eventdata, handles)
% hObject    handle to sinusT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of sinusT
axes(handles.axes2);
hold off;
plot(0,0,'g*');
btnStart_Callback(handles.btnStart, eventdata, handles);
