#include <iostream>
#include <math.h>

using namespace std;


double kwadraty(double(*fn)(double), double a, double b) {
	return fn(a) * (b - a);
}

double trapezy(double(*fn)(double), double a, double b) {
	return (fn(a) + fn(b)) * (b - a) / 2;
}

double simpson(double(*fn)(double), double a, double b) {
	double ret = (b - a) / 6;
	ret *= fn(a) + 4 * fn((a + b) / 2) + fn(b);
	return ret;
}

double integrate(double(*fn)(double), double a, double b, double(*method)(double(*)(double), double, double), int parts) {
	double area = 0;

	double step = (b - a) / parts;
	double xi = a;

	for (int i = 0; i < parts - 1; ++i) {
		area += method(fn, xi, xi + step);
		xi += step;
	}

	area += method(fn, xi, b);

	return area;
}

double gauss2(double(*fn)(double), double a, double b) {
	const int n = 2;
	double x[n] = { 0.577350, -0.577350 };
	double A[n] = { 1, 1 };
	double t[n];
	double f[n];

	for (int i = 0; i < n; i++) {
		t[i] = (a + b) / 2;
		t[i] += (b - a) / 2 * x[i];
	}

	for (int i = 0; i < n; i++)
		f[i] = fn(t[i]);

	double ret = 0;

	for (int i = 0; i < n; i++)
		ret += f[i] * A[i];
	ret *= (b - a) / 2;
	return ret;
}

double gauss4(double(*fn)(double), double a, double b) {
	const int n = 4;
	double x[n] = { -0.861136,-0.339981, 0.339981, 0.861136 };
	double A[n] = { 0.347855, 0.652145, 0.652145, 0.347855 };
	double t[n];
	double f[n];

	for (int i = 0; i < n; i++) {
		t[i] = (a + b) / 2;
		t[i] += (b - a) / 2 * x[i];
	}

	for (int i = 0; i < n; i++)
		f[i] = fn(t[i]);

	double ret = 0;

	for (int i = 0; i < n; i++)
		ret += f[i] * A[i];
	ret *= (b - a) / 2;
	return ret;
}

///// FUNKCJE PODCALKOWE
double fun(double x) {
	return 3 * x * x;
}

double x2(double x) {
	return x*x;
}

double inverse(double x) {
	return 1 / x;
}


int main() {
	int parts; 
	
	parts = 100;
	cout << integrate(fun, 3, 10, kwadraty, parts) << endl;
	cout << integrate(fun, 3, 10, trapezy, parts) << endl;
	cout << integrate(fun, 3, 10, simpson, parts) << endl << endl;

	parts = 1;
	cout << integrate(x2, 1, 2, kwadraty, parts) << endl;
	cout << integrate(x2, 1, 2, trapezy, parts) << endl;
	cout << integrate(x2, 1, 2, simpson, parts) << endl << endl;

	cout << "Kwadratura Gaussa" << "\n";
	cout << gauss2(inverse, 1, 2) << endl;
	cout << gauss4(inverse, 1, 2) << endl << endl;

	system("pause");
	return 0;
}

