package pkg3dproject.util;
/*
    Klasa MoreMath dostarcza funkcji, których nie ma 
    w klasach java.lang.Math oraz java.lang.StrictMath.
*/
public class MoreMath {

    /*
        Zwraca znak liczby. -1 dla liczb ujemnych,
        1 dla dodatnich, a 0 w pozostałych przypadkach.
    */
    public static int sign(short v) {
        return (v>0)?1:(v<0)?-1:0;
    }


    /*
        Zwraca znak liczby. -1 dla liczb ujemnych,
        1 dla dodatnich, a 0 w pozostałych przypadkach.
    */
    public static int sign(int v) {
        return (v>0)?1:(v<0)?-1:0;
    }


    /*
        Zwraca znak liczby. -1 dla liczb ujemnych,
        1 dla dodatnich, a 0 w pozostałych przypadkach.
    */
    public static int sign(long v) {
        return (v>0)?1:(v<0)?-1:0;
    }


    /*
        Zwraca znak liczby. -1 dla liczb ujemnych,
        1 dla dodatnich, a 0 w pozostałych przypadkach.
    */
    public static int sign(float v) {
        return (v>0)?1:(v<0)?-1:0;
    }


    /*
        Zwraca znak liczby. -1 dla liczb ujemnych,
        1 dla dodatnich, a 0 w pozostałych przypadkach.
    */
    public static int sign(double v) {
        return (v>0)?1:(v<0)?-1:0;
    }


    /*
        Szybsza funkcja ceil konwertująca wartości float na int.
        W odróżnieniu od funkcji ceil z klasy java.lang.Math, ta
        wersja pobiera argument typu float, a zwraca wartość int
        zamiast wartości double i nie rozważa przypadków specjalnych.
    */
    public static int ceil(float f) {
        if (f > 0) {
            return (int)f + 1;
        }
        else {
           return (int)f;
        }
    }


    /*
        Szybsza funkcja floor konwertująca wartości float na int.
        W odróżnieniu od funkcji floor z klasy java.lang.Math, ta
        wersja pobiera argument typu float, a zwraca wartość int
        zamiast wartości double i nie rozważa przypadków specjalnych.
    */
    public static int floor(float f) {
        if (f >= 0) {
            return (int)f;
        }
        else {
           return (int)f - 1;
        }
    }


    /*
        Zwraca true, jeśli podana liczba jest potęgą liczby 2.
    */
    public static boolean isPowerOfTwo(int n) {
        return ((n & (n-1)) == 0);
    }


    /*
        Pobiera liczbę "włączonych" bitów w liczbie całkowitej.
    */
    public static int getBitCount(int n) {
        int count = 0;
        while (n > 0) {
            count+=(n & 1);
            n>>=1;
        }
        return count;
    }
}
