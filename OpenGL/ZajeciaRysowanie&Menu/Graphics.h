#pragma once
#include<GL/glew.h>
#include<GLFW/glfw3.h>

#include<glm.hpp>
#include<gtc/matrix_transform.hpp>
#include<gtc\type_ptr.hpp>

#include"FreeImage.h"

#include<cmath>
#include<iostream>
#include<fstream>
#include<string>
#include<iostream>
using namespace std;
class Graphics
{
public:
	int n;
	GLfloat * position;
	GLfloat * colours;
	GLuint position_vbo;
	GLuint colour_vbo;
	GLuint vao;

	void createVboPosition();
	void createVboColours();
	void createVao();
	void setPosition();
	void setColours();

	Graphics(int= 1);
	~Graphics();
};

