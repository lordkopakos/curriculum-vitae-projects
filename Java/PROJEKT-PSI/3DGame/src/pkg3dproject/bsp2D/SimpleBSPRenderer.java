package pkg3dproject.bsp2D;

import java.awt.*;
import java.awt.image.*;
import java.util.HashMap;
import pkg3dproject.math3D.*;
import pkg3dproject.bsp2D.*;
import pkg3dproject.graphics3D.*;
import pkg3dproject.graphics3D.texture.*;


/**
    Klasa SimpleBSPRenderer odpowiada za renderowanie widocznych
    wielok�t�w przechowywanych w drzewie BSP i wszystkich wielok�t�w
    tworz�cych obiekty na scenie. Klasa nie wykorzystuje z-bufora.
*/
public class SimpleBSPRenderer extends ShadedSurfacePolygonRenderer
    implements BSPTreeTraverseListener
{
    protected Graphics2D currentGraphics2D;
    protected BSPTreeTraverser traverser;
    protected boolean viewNotFilledFirstTime;


    /**
        Tworzy nowy obiekt BSPRenderer dla przekazanego obiektu kamery
        i okna widoku.
    */
    public SimpleBSPRenderer(Transform3D camera,
        ViewWindow viewWindow)
    {
        super(camera, viewWindow, false);
        viewNotFilledFirstTime = true;
    }


    protected void init() {
        traverser = new BSPTreeTraverser(this);
        destPolygon = new TexturedPolygon3D();
        scanConverter = new SortedScanConverter(viewWindow);
        ((SortedScanConverter)scanConverter).setSortedMode(true);

        // tworzy obiekty renderuj�ce dla wszystkich tekstur (optymalizacja HotSpot)
        scanRenderers = new HashMap();
        scanRenderers.put(PowerOf2Texture.class,
            new PowerOf2TextureRenderer());
        scanRenderers.put(ShadedTexture.class,
            new ShadedTextureRenderer());
        scanRenderers.put(ShadedSurface.class,
            new ShadedSurfaceRenderer());
    }


    public void startFrame(Graphics2D g) {
        super.startFrame(g);
        ((SortedScanConverter)scanConverter).clear();
    }


    public void endFrame(Graphics2D g) {
        super.endFrame(g);
        if (!((SortedScanConverter)scanConverter).isFilled()) {
            g.drawString("Ekran nie jest wype�niony", 5,
                viewWindow.getTopOffset() +
                viewWindow.getHeight() - 5);
            if (viewNotFilledFirstTime) {
                viewNotFilledFirstTime = false;
                // wy�wietla na konsoli odpowiedni komunikat
                System.out.println("Ekran nie jest w ca�o�ci wype�niony.");
            }
            // czy�ci t�o
            clearViewEveryFrame = true;
        }
        else {
            clearViewEveryFrame = false;
        }
    }

    /**
        Rysuje widoczne wielok�ty z drzewa BSP w oparciu o po�o�enie
        kamery. Wielok�ty s� rysowane od pierwszego do ostatniego planu.
    */
    public void draw(Graphics2D g, BSPTree tree) {
        currentGraphics2D = g;
        traverser.traverse(tree, camera.getLocation());
    }


    // z interfejsu BSPTreeTraverseListener
    public boolean visitPolygon(BSPPolygon poly, boolean isBack) {
        draw(currentGraphics2D, poly);
        return !((SortedScanConverter)scanConverter).isFilled();
    }


    protected void drawCurrentPolygon(Graphics2D g) {
        if (!(sourcePolygon instanceof TexturedPolygon3D)) {
            // wielok�t bez tekstury - opu�� metod�
            return;
        }
        buildSurface();
        SortedScanConverter scanConverter =
            (SortedScanConverter)this.scanConverter;
        TexturedPolygon3D poly = (TexturedPolygon3D)destPolygon;
        Texture texture = poly.getTexture();
        ScanRenderer scanRenderer =
            (ScanRenderer)scanRenderers.get(texture.getClass());
        scanRenderer.setTexture(texture);
        Rectangle3D textureBounds = poly.getTextureBounds();

        a.setToCrossProduct(textureBounds.getDirectionV(),
            textureBounds.getOrigin());
        b.setToCrossProduct(textureBounds.getOrigin(),
            textureBounds.getDirectionU());
        c.setToCrossProduct(textureBounds.getDirectionU(),
            textureBounds.getDirectionV());

        int y = scanConverter.getTopBoundary();
        viewPos.y = viewWindow.convertFromScreenYToViewY(y);
        viewPos.z = -viewWindow.getDistance();

        while (y<=scanConverter.getBottomBoundary()) {
            for (int i=0; i<scanConverter.getNumScans(y); i++) {
                ScanConverter.Scan scan =
                    scanConverter.getScan(y, i);

                if (scan.isValid()) {
                    viewPos.x = viewWindow.
                        convertFromScreenXToViewX(scan.left);
                    int offset = (y - viewWindow.getTopOffset()) *
                        viewWindow.getWidth() +
                        (scan.left - viewWindow.getLeftOffset());

                    scanRenderer.render(offset, scan.left,
                        scan.right);
                }
            }
            y++;
            viewPos.y--;
        }
    }
}
