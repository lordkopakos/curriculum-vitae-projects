﻿#pragma once
#include <vector>
#include "Point.h"
#include <iostream>
#include <cassert>

using namespace std;

class Interpolation
{
public:
	Interpolation(vector<Point>& points, vector<bool> flags);

	double apply(double x);

	void print_x_prompted();
	void print_x(double x);

	void print_b_values() const;
	void print_stat();


private:
	vector<Point> all_points_;
	vector<Point> selected_points_;

	int degree;
	double *b;
	double *p;

};
