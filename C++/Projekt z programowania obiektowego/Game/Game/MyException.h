#pragma once
#include<iostream>
#include<string>
using namespace std;
class MyException :public exception
{
public:
	MyException(string err)
	{
		Error = err;
	}
	virtual ~MyException() throw() {}
	virtual const char* what() const throw()
	{
		return Error.c_str();
	}
private:
	string Error;
};
