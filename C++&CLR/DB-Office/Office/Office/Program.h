#pragma once
namespace Office {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace MySql::Data::MySqlClient;

	/// <summary>
	/// Podsumowanie informacji o Program
	/// </summary>
	public ref class Program : public System::Windows::Forms::Form
	{
		
	public:
		int userId;

	protected:
		/// <summary>
		/// Wyczy�� wszystkie u�ywane zasoby.
		/// </summary>
		~Program()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::Button^  btnDelete;
	public:
	private: System::Windows::Forms::Button^  btnUpdate;
	private: System::Windows::Forms::Button^  btnAdd;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::CheckBox^  chkEmployee;

	private: System::Windows::Forms::TextBox^  txtUsername;

	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::TextBox^  txtLastName;

	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::TextBox^  txtFirstName;

	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Button^  btnSearch;
	private: System::Windows::Forms::TextBox^  txtSearch;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::DataGridView^  dgvUser;
	private: System::Windows::Forms::TabControl^  tabControl1;
	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::TabPage^  tabPage2;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::TextBox^  txtNewPassword;
	private: System::Windows::Forms::Button^  btnChange;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  txtRepeatPassword;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  txtOldPassword;


	private:
		/// <summary>
		/// Wymagana zmienna projektanta.
		/// </summary>
		System::ComponentModel::Container ^components;
	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::GroupBox^  groupBox4;
	private: System::Windows::Forms::TextBox^  txtE6e;

	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::TextBox^  txtE6b;

	private: System::Windows::Forms::Label^  label19;
	private: System::Windows::Forms::TextBox^  txtE5e;

	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::TextBox^  txtE5b;

	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::TextBox^  txtE4e;

	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::TextBox^  txtE4b;

	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::TextBox^  txtE3e;

	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::TextBox^  txtE3b;

	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::TextBox^  txtE2e;

	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::TextBox^  txtE2b;

	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::TextBox^  txtE1e;

	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::TextBox^  txtE1b;

	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Button^  btn10;
	private: System::Windows::Forms::Button^  btn9;
	private: System::Windows::Forms::Button^  btn8;
	private: System::Windows::Forms::Button^  btn7;
	private: System::Windows::Forms::TabPage^  tabPage3;
	private: System::Windows::Forms::DataGridView^  dgIServices;

	private: System::Windows::Forms::Button^  btnISearch;
	private: System::Windows::Forms::TextBox^  txtISearch;
	private: System::Windows::Forms::Label^  label20;
private: System::Windows::Forms::Button^  btnIDelete;
private: System::Windows::Forms::Button^  btnIUpdate;
private: System::Windows::Forms::Button^  btnIAdd;
private: System::Windows::Forms::GroupBox^  groupBox5;
private: System::Windows::Forms::TextBox^  txtIDescription;
private: System::Windows::Forms::Label^  label24;
private: System::Windows::Forms::TextBox^  txtITime;
private: System::Windows::Forms::Label^  label23;
private: System::Windows::Forms::TextBox^  txtIPrice;
private: System::Windows::Forms::Label^  label22;
private: System::Windows::Forms::TextBox^  txtIName;
private: System::Windows::Forms::Label^  lbl;
private: System::Windows::Forms::TabPage^  tabPage4;
private: System::Windows::Forms::Label^  label27;
private: System::Windows::Forms::DataGridView^  dtvESAddService;
private: System::Windows::Forms::Label^  label26;
private: System::Windows::Forms::DataGridView^  dgvESServices;
private: System::Windows::Forms::Label^  label25;
private: System::Windows::Forms::DataGridView^  dgvESEmployees;
private: System::Windows::Forms::Button^  btnESSearch;
private: System::Windows::Forms::TextBox^  txtESSearch;
private: System::Windows::Forms::Label^  label21;
private: System::Windows::Forms::TextBox^  txtESLastName;
private: System::Windows::Forms::Label^  label29;
private: System::Windows::Forms::TextBox^  txtESFirstName;
private: System::Windows::Forms::Label^  label28;
private: System::Windows::Forms::TabPage^  tabPage5;
private: System::Windows::Forms::GroupBox^  gbClientsData;
private: System::Windows::Forms::TextBox^  txtCPhone;



private: System::Windows::Forms::Label^  label34;
private: System::Windows::Forms::TextBox^  txtCEmail;

private: System::Windows::Forms::Label^  label33;
private: System::Windows::Forms::TextBox^  txtCLastName;

private: System::Windows::Forms::Label^  label32;
private: System::Windows::Forms::TextBox^  txtCFirstName;

private: System::Windows::Forms::Label^  label31;
private: System::Windows::Forms::DataGridView^  dgvClients;
private: System::Windows::Forms::Button^  btnCSearch;
private: System::Windows::Forms::TextBox^  txtCSearch;
private: System::Windows::Forms::Label^  label30;
private: System::Windows::Forms::Button^  btnCDelete;

private: System::Windows::Forms::Button^  btnCModify;

private: System::Windows::Forms::Button^  btnCAdd;




private: System::Windows::Forms::Label^  label36;
private: System::Windows::Forms::TextBox^  txtCCity;

private: System::Windows::Forms::Label^  label37;
private: System::Windows::Forms::TextBox^  txtCStreet;

private: System::Windows::Forms::Label^  label38;
private: System::Windows::Forms::MaskedTextBox^  mtxtCPCode;
private: System::Windows::Forms::TextBox^  txtCNumber;
private: System::Windows::Forms::Label^  label35;
private: System::Windows::Forms::TabPage^  tabPage6;
private: System::Windows::Forms::Button^  btnRAdd;
private: System::Windows::Forms::Button^  btnRModify;
private: System::Windows::Forms::Button^  btnRDelete;
private: System::Windows::Forms::TextBox^  txtRShowTerm;
private: System::Windows::Forms::Label^  label44;
private: System::Windows::Forms::TextBox^  txtRShowService;
private: System::Windows::Forms::Label^  label43;
private: System::Windows::Forms::TextBox^  txtRShowClient;
private: System::Windows::Forms::Label^  label42;
private: System::Windows::Forms::MonthCalendar^  monthCalendar1;
private: System::Windows::Forms::GroupBox^  gbRHours;

private: System::Windows::Forms::DataGridView^  dgvREmployee;
private: System::Windows::Forms::Button^  btnRSearchEmployee;
private: System::Windows::Forms::TextBox^  txtRSearchEmployee;
private: System::Windows::Forms::Label^  label41;
private: System::Windows::Forms::DataGridView^  dgvRClient;
private: System::Windows::Forms::Button^  btnRSearchClient;
private: System::Windows::Forms::TextBox^  txtRSearchClient;
private: System::Windows::Forms::Label^  label40;
private: System::Windows::Forms::DataGridView^  dgvRService;
private: System::Windows::Forms::Button^  btnRSearchService;
private: System::Windows::Forms::TextBox^  txtRSearchService;
private: System::Windows::Forms::Label^  label39;
private: System::Windows::Forms::ToolStrip^  toolStrip1;
private: System::Windows::Forms::ToolStripButton^  toolStripButton4;
private: System::Windows::Forms::ToolStripButton^  toolStripButton2;
private: System::Windows::Forms::ToolStripButton^  toolStripButton3;
private: System::Windows::Forms::ToolStripButton^  toolStripButton5;
private: System::Windows::Forms::ToolStripButton^  toolStripButton6;
private: System::Windows::Forms::ToolStripButton^  toolStripButton1;


	public:
		String^config = L"datasource=localhost;port=3306;username=root;password=lord130695;database=office";
		Program(int userId)
		{
			InitializeComponent();
			//
			//TODO: W tym miejscu dodaj kod konstruktora
			//
			this->userId = userId;

		}

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Wymagana metoda obs�ugi projektanta � nie nale�y modyfikowa� 
		/// zawarto�� tej metody z edytorem kodu.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Program::typeid));
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage6 = (gcnew System::Windows::Forms::TabPage());
			this->btnRAdd = (gcnew System::Windows::Forms::Button());
			this->btnRModify = (gcnew System::Windows::Forms::Button());
			this->btnRDelete = (gcnew System::Windows::Forms::Button());
			this->txtRShowTerm = (gcnew System::Windows::Forms::TextBox());
			this->label44 = (gcnew System::Windows::Forms::Label());
			this->txtRShowService = (gcnew System::Windows::Forms::TextBox());
			this->label43 = (gcnew System::Windows::Forms::Label());
			this->txtRShowClient = (gcnew System::Windows::Forms::TextBox());
			this->label42 = (gcnew System::Windows::Forms::Label());
			this->monthCalendar1 = (gcnew System::Windows::Forms::MonthCalendar());
			this->gbRHours = (gcnew System::Windows::Forms::GroupBox());
			this->dgvREmployee = (gcnew System::Windows::Forms::DataGridView());
			this->btnRSearchEmployee = (gcnew System::Windows::Forms::Button());
			this->txtRSearchEmployee = (gcnew System::Windows::Forms::TextBox());
			this->label41 = (gcnew System::Windows::Forms::Label());
			this->dgvRClient = (gcnew System::Windows::Forms::DataGridView());
			this->btnRSearchClient = (gcnew System::Windows::Forms::Button());
			this->txtRSearchClient = (gcnew System::Windows::Forms::TextBox());
			this->label40 = (gcnew System::Windows::Forms::Label());
			this->dgvRService = (gcnew System::Windows::Forms::DataGridView());
			this->btnRSearchService = (gcnew System::Windows::Forms::Button());
			this->txtRSearchService = (gcnew System::Windows::Forms::TextBox());
			this->label39 = (gcnew System::Windows::Forms::Label());
			this->tabPage5 = (gcnew System::Windows::Forms::TabPage());
			this->btnCDelete = (gcnew System::Windows::Forms::Button());
			this->btnCModify = (gcnew System::Windows::Forms::Button());
			this->btnCAdd = (gcnew System::Windows::Forms::Button());
			this->gbClientsData = (gcnew System::Windows::Forms::GroupBox());
			this->txtCNumber = (gcnew System::Windows::Forms::TextBox());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->mtxtCPCode = (gcnew System::Windows::Forms::MaskedTextBox());
			this->label36 = (gcnew System::Windows::Forms::Label());
			this->txtCCity = (gcnew System::Windows::Forms::TextBox());
			this->label37 = (gcnew System::Windows::Forms::Label());
			this->txtCStreet = (gcnew System::Windows::Forms::TextBox());
			this->label38 = (gcnew System::Windows::Forms::Label());
			this->txtCPhone = (gcnew System::Windows::Forms::TextBox());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->txtCEmail = (gcnew System::Windows::Forms::TextBox());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->txtCLastName = (gcnew System::Windows::Forms::TextBox());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->txtCFirstName = (gcnew System::Windows::Forms::TextBox());
			this->label31 = (gcnew System::Windows::Forms::Label());
			this->dgvClients = (gcnew System::Windows::Forms::DataGridView());
			this->btnCSearch = (gcnew System::Windows::Forms::Button());
			this->txtCSearch = (gcnew System::Windows::Forms::TextBox());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->tabPage4 = (gcnew System::Windows::Forms::TabPage());
			this->txtESLastName = (gcnew System::Windows::Forms::TextBox());
			this->label29 = (gcnew System::Windows::Forms::Label());
			this->txtESFirstName = (gcnew System::Windows::Forms::TextBox());
			this->label28 = (gcnew System::Windows::Forms::Label());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->dtvESAddService = (gcnew System::Windows::Forms::DataGridView());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->dgvESServices = (gcnew System::Windows::Forms::DataGridView());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->dgvESEmployees = (gcnew System::Windows::Forms::DataGridView());
			this->btnESSearch = (gcnew System::Windows::Forms::Button());
			this->txtESSearch = (gcnew System::Windows::Forms::TextBox());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
			this->btnIDelete = (gcnew System::Windows::Forms::Button());
			this->btnIUpdate = (gcnew System::Windows::Forms::Button());
			this->btnIAdd = (gcnew System::Windows::Forms::Button());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->txtIDescription = (gcnew System::Windows::Forms::TextBox());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->txtITime = (gcnew System::Windows::Forms::TextBox());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->txtIPrice = (gcnew System::Windows::Forms::TextBox());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->txtIName = (gcnew System::Windows::Forms::TextBox());
			this->lbl = (gcnew System::Windows::Forms::Label());
			this->dgIServices = (gcnew System::Windows::Forms::DataGridView());
			this->btnISearch = (gcnew System::Windows::Forms::Button());
			this->txtISearch = (gcnew System::Windows::Forms::TextBox());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->btn10 = (gcnew System::Windows::Forms::Button());
			this->btn9 = (gcnew System::Windows::Forms::Button());
			this->btn8 = (gcnew System::Windows::Forms::Button());
			this->btn7 = (gcnew System::Windows::Forms::Button());
			this->txtE6e = (gcnew System::Windows::Forms::TextBox());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->txtE6b = (gcnew System::Windows::Forms::TextBox());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->txtE5e = (gcnew System::Windows::Forms::TextBox());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->txtE5b = (gcnew System::Windows::Forms::TextBox());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->txtE4e = (gcnew System::Windows::Forms::TextBox());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->txtE4b = (gcnew System::Windows::Forms::TextBox());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->txtE3e = (gcnew System::Windows::Forms::TextBox());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->txtE3b = (gcnew System::Windows::Forms::TextBox());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->txtE2e = (gcnew System::Windows::Forms::TextBox());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->txtE2b = (gcnew System::Windows::Forms::TextBox());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->txtE1e = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->txtE1b = (gcnew System::Windows::Forms::TextBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->btnDelete = (gcnew System::Windows::Forms::Button());
			this->btnUpdate = (gcnew System::Windows::Forms::Button());
			this->btnAdd = (gcnew System::Windows::Forms::Button());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->chkEmployee = (gcnew System::Windows::Forms::CheckBox());
			this->txtUsername = (gcnew System::Windows::Forms::TextBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->txtLastName = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->txtFirstName = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->btnSearch = (gcnew System::Windows::Forms::Button());
			this->txtSearch = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->dgvUser = (gcnew System::Windows::Forms::DataGridView());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->txtNewPassword = (gcnew System::Windows::Forms::TextBox());
			this->btnChange = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->txtRepeatPassword = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->txtOldPassword = (gcnew System::Windows::Forms::TextBox());
			this->toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
			this->toolStripButton4 = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton2 = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton3 = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton5 = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton6 = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton1 = (gcnew System::Windows::Forms::ToolStripButton());
			this->tabControl1->SuspendLayout();
			this->tabPage6->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvREmployee))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvRClient))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvRService))->BeginInit();
			this->tabPage5->SuspendLayout();
			this->gbClientsData->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvClients))->BeginInit();
			this->tabPage4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dtvESAddService))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvESServices))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvESEmployees))->BeginInit();
			this->tabPage3->SuspendLayout();
			this->groupBox5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgIServices))->BeginInit();
			this->tabPage1->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvUser))->BeginInit();
			this->tabPage2->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->toolStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// tabControl1
			// 
			this->tabControl1->Alignment = System::Windows::Forms::TabAlignment::Bottom;
			this->tabControl1->Controls->Add(this->tabPage6);
			this->tabControl1->Controls->Add(this->tabPage5);
			this->tabControl1->Controls->Add(this->tabPage4);
			this->tabControl1->Controls->Add(this->tabPage3);
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->tabControl1->Location = System::Drawing::Point(12, 93);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(1112, 715);
			this->tabControl1->TabIndex = 0;
			// 
			// tabPage6
			// 
			this->tabPage6->Controls->Add(this->btnRAdd);
			this->tabPage6->Controls->Add(this->btnRModify);
			this->tabPage6->Controls->Add(this->btnRDelete);
			this->tabPage6->Controls->Add(this->txtRShowTerm);
			this->tabPage6->Controls->Add(this->label44);
			this->tabPage6->Controls->Add(this->txtRShowService);
			this->tabPage6->Controls->Add(this->label43);
			this->tabPage6->Controls->Add(this->txtRShowClient);
			this->tabPage6->Controls->Add(this->label42);
			this->tabPage6->Controls->Add(this->monthCalendar1);
			this->tabPage6->Controls->Add(this->gbRHours);
			this->tabPage6->Controls->Add(this->dgvREmployee);
			this->tabPage6->Controls->Add(this->btnRSearchEmployee);
			this->tabPage6->Controls->Add(this->txtRSearchEmployee);
			this->tabPage6->Controls->Add(this->label41);
			this->tabPage6->Controls->Add(this->dgvRClient);
			this->tabPage6->Controls->Add(this->btnRSearchClient);
			this->tabPage6->Controls->Add(this->txtRSearchClient);
			this->tabPage6->Controls->Add(this->label40);
			this->tabPage6->Controls->Add(this->dgvRService);
			this->tabPage6->Controls->Add(this->btnRSearchService);
			this->tabPage6->Controls->Add(this->txtRSearchService);
			this->tabPage6->Controls->Add(this->label39);
			this->tabPage6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->tabPage6->Location = System::Drawing::Point(4, 4);
			this->tabPage6->Name = L"tabPage6";
			this->tabPage6->Padding = System::Windows::Forms::Padding(3);
			this->tabPage6->Size = System::Drawing::Size(1104, 682);
			this->tabPage6->TabIndex = 5;
			this->tabPage6->Text = L"Visits";
			this->tabPage6->UseVisualStyleBackColor = true;
			// 
			// btnRAdd
			// 
			this->btnRAdd->BackColor = System::Drawing::Color::Green;
			this->btnRAdd->FlatAppearance->BorderSize = 0;
			this->btnRAdd->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnRAdd->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnRAdd->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnRAdd->Location = System::Drawing::Point(37, 587);
			this->btnRAdd->Name = L"btnRAdd";
			this->btnRAdd->Size = System::Drawing::Size(260, 28);
			this->btnRAdd->TabIndex = 22;
			this->btnRAdd->Text = L"Add";
			this->btnRAdd->UseVisualStyleBackColor = false;
			this->btnRAdd->Click += gcnew System::EventHandler(this, &Program::btnRAdd_Click);
			// 
			// btnRModify
			// 
			this->btnRModify->BackColor = System::Drawing::Color::DeepSkyBlue;
			this->btnRModify->Enabled = false;
			this->btnRModify->FlatAppearance->BorderSize = 0;
			this->btnRModify->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnRModify->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnRModify->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnRModify->Location = System::Drawing::Point(171, 538);
			this->btnRModify->Name = L"btnRModify";
			this->btnRModify->Size = System::Drawing::Size(126, 34);
			this->btnRModify->TabIndex = 21;
			this->btnRModify->Text = L"Modify";
			this->btnRModify->UseVisualStyleBackColor = false;
			this->btnRModify->Click += gcnew System::EventHandler(this, &Program::btnRModify_Click);
			// 
			// btnRDelete
			// 
			this->btnRDelete->BackColor = System::Drawing::Color::Red;
			this->btnRDelete->Enabled = false;
			this->btnRDelete->FlatAppearance->BorderSize = 0;
			this->btnRDelete->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnRDelete->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnRDelete->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnRDelete->Location = System::Drawing::Point(37, 538);
			this->btnRDelete->Name = L"btnRDelete";
			this->btnRDelete->Size = System::Drawing::Size(119, 34);
			this->btnRDelete->TabIndex = 20;
			this->btnRDelete->Text = L"Delete";
			this->btnRDelete->UseVisualStyleBackColor = false;
			this->btnRDelete->Click += gcnew System::EventHandler(this, &Program::btnRDelete_Click);
			// 
			// txtRShowTerm
			// 
			this->txtRShowTerm->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtRShowTerm->Location = System::Drawing::Point(104, 398);
			this->txtRShowTerm->Name = L"txtRShowTerm";
			this->txtRShowTerm->Size = System::Drawing::Size(193, 26);
			this->txtRShowTerm->TabIndex = 19;
			// 
			// label44
			// 
			this->label44->AutoSize = true;
			this->label44->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label44->Location = System::Drawing::Point(33, 401);
			this->label44->Name = L"label44";
			this->label44->Size = System::Drawing::Size(49, 20);
			this->label44->TabIndex = 18;
			this->label44->Text = L"Term:";
			// 
			// txtRShowService
			// 
			this->txtRShowService->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtRShowService->Location = System::Drawing::Point(104, 339);
			this->txtRShowService->Name = L"txtRShowService";
			this->txtRShowService->Size = System::Drawing::Size(193, 26);
			this->txtRShowService->TabIndex = 17;
			// 
			// label43
			// 
			this->label43->AutoSize = true;
			this->label43->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label43->Location = System::Drawing::Point(33, 342);
			this->label43->Name = L"label43";
			this->label43->Size = System::Drawing::Size(65, 20);
			this->label43->TabIndex = 16;
			this->label43->Text = L"Service:";
			// 
			// txtRShowClient
			// 
			this->txtRShowClient->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtRShowClient->Location = System::Drawing::Point(104, 273);
			this->txtRShowClient->Name = L"txtRShowClient";
			this->txtRShowClient->Size = System::Drawing::Size(193, 26);
			this->txtRShowClient->TabIndex = 15;
			// 
			// label42
			// 
			this->label42->AutoSize = true;
			this->label42->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label42->Location = System::Drawing::Point(33, 276);
			this->label42->Name = L"label42";
			this->label42->Size = System::Drawing::Size(53, 20);
			this->label42->TabIndex = 14;
			this->label42->Text = L"Client:";
			// 
			// monthCalendar1
			// 
			this->monthCalendar1->Enabled = false;
			this->monthCalendar1->Location = System::Drawing::Point(28, 38);
			this->monthCalendar1->Name = L"monthCalendar1";
			this->monthCalendar1->TabIndex = 13;
			this->monthCalendar1->DateSelected += gcnew System::Windows::Forms::DateRangeEventHandler(this, &Program::monthCalendar1_DateSelected);
			// 
			// gbRHours
			// 
			this->gbRHours->Location = System::Drawing::Point(309, 28);
			this->gbRHours->Name = L"gbRHours";
			this->gbRHours->Size = System::Drawing::Size(317, 597);
			this->gbRHours->TabIndex = 12;
			this->gbRHours->TabStop = false;
			this->gbRHours->Text = L"Hours:";
			// 
			// dgvREmployee
			// 
			this->dgvREmployee->AllowUserToAddRows = false;
			this->dgvREmployee->AllowUserToOrderColumns = true;
			this->dgvREmployee->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dgvREmployee->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvREmployee->Location = System::Drawing::Point(635, 481);
			this->dgvREmployee->Name = L"dgvREmployee";
			this->dgvREmployee->Size = System::Drawing::Size(435, 145);
			this->dgvREmployee->TabIndex = 11;
			this->dgvREmployee->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Program::dgvREmployee_CellClick);
			// 
			// btnRSearchEmployee
			// 
			this->btnRSearchEmployee->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->btnRSearchEmployee->FlatAppearance->BorderSize = 0;
			this->btnRSearchEmployee->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnRSearchEmployee->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnRSearchEmployee->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnRSearchEmployee->Location = System::Drawing::Point(995, 446);
			this->btnRSearchEmployee->Name = L"btnRSearchEmployee";
			this->btnRSearchEmployee->Size = System::Drawing::Size(75, 26);
			this->btnRSearchEmployee->TabIndex = 10;
			this->btnRSearchEmployee->Text = L"Search";
			this->btnRSearchEmployee->UseVisualStyleBackColor = false;
			this->btnRSearchEmployee->Click += gcnew System::EventHandler(this, &Program::btnRSearchEmployee_Click);
			// 
			// txtRSearchEmployee
			// 
			this->txtRSearchEmployee->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtRSearchEmployee->Location = System::Drawing::Point(721, 446);
			this->txtRSearchEmployee->Name = L"txtRSearchEmployee";
			this->txtRSearchEmployee->Size = System::Drawing::Size(263, 26);
			this->txtRSearchEmployee->TabIndex = 9;
			// 
			// label41
			// 
			this->label41->AutoSize = true;
			this->label41->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label41->Location = System::Drawing::Point(632, 449);
			this->label41->Name = L"label41";
			this->label41->Size = System::Drawing::Size(83, 20);
			this->label41->TabIndex = 8;
			this->label41->Text = L"Employee:";
			// 
			// dgvRClient
			// 
			this->dgvRClient->AllowUserToAddRows = false;
			this->dgvRClient->AllowUserToOrderColumns = true;
			this->dgvRClient->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dgvRClient->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvRClient->Location = System::Drawing::Point(635, 273);
			this->dgvRClient->Name = L"dgvRClient";
			this->dgvRClient->Size = System::Drawing::Size(435, 145);
			this->dgvRClient->TabIndex = 7;
			this->dgvRClient->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Program::dgvRClient_CellClick);
			// 
			// btnRSearchClient
			// 
			this->btnRSearchClient->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->btnRSearchClient->FlatAppearance->BorderSize = 0;
			this->btnRSearchClient->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnRSearchClient->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnRSearchClient->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnRSearchClient->Location = System::Drawing::Point(995, 233);
			this->btnRSearchClient->Name = L"btnRSearchClient";
			this->btnRSearchClient->Size = System::Drawing::Size(75, 30);
			this->btnRSearchClient->TabIndex = 6;
			this->btnRSearchClient->Text = L"Search";
			this->btnRSearchClient->UseVisualStyleBackColor = false;
			this->btnRSearchClient->Click += gcnew System::EventHandler(this, &Program::btnRSearchClient_Click);
			// 
			// txtRSearchClient
			// 
			this->txtRSearchClient->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtRSearchClient->Location = System::Drawing::Point(721, 235);
			this->txtRSearchClient->Name = L"txtRSearchClient";
			this->txtRSearchClient->Size = System::Drawing::Size(263, 26);
			this->txtRSearchClient->TabIndex = 5;
			// 
			// label40
			// 
			this->label40->AutoSize = true;
			this->label40->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label40->Location = System::Drawing::Point(632, 238);
			this->label40->Name = L"label40";
			this->label40->Size = System::Drawing::Size(53, 20);
			this->label40->TabIndex = 4;
			this->label40->Text = L"Client:";
			// 
			// dgvRService
			// 
			this->dgvRService->AllowUserToAddRows = false;
			this->dgvRService->AllowUserToOrderColumns = true;
			this->dgvRService->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dgvRService->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvRService->Location = System::Drawing::Point(635, 55);
			this->dgvRService->Name = L"dgvRService";
			this->dgvRService->Size = System::Drawing::Size(435, 145);
			this->dgvRService->TabIndex = 3;
			this->dgvRService->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Program::dgvRService_CellClick);
			// 
			// btnRSearchService
			// 
			this->btnRSearchService->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->btnRSearchService->FlatAppearance->BorderSize = 0;
			this->btnRSearchService->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnRSearchService->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnRSearchService->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnRSearchService->Location = System::Drawing::Point(995, 14);
			this->btnRSearchService->Name = L"btnRSearchService";
			this->btnRSearchService->Size = System::Drawing::Size(75, 32);
			this->btnRSearchService->TabIndex = 2;
			this->btnRSearchService->Text = L"Search";
			this->btnRSearchService->UseVisualStyleBackColor = false;
			this->btnRSearchService->Click += gcnew System::EventHandler(this, &Program::btnRSearchService_Click);
			// 
			// txtRSearchService
			// 
			this->txtRSearchService->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtRSearchService->Location = System::Drawing::Point(721, 17);
			this->txtRSearchService->Name = L"txtRSearchService";
			this->txtRSearchService->Size = System::Drawing::Size(263, 26);
			this->txtRSearchService->TabIndex = 1;
			// 
			// label39
			// 
			this->label39->AutoSize = true;
			this->label39->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label39->Location = System::Drawing::Point(632, 20);
			this->label39->Name = L"label39";
			this->label39->Size = System::Drawing::Size(65, 20);
			this->label39->TabIndex = 0;
			this->label39->Text = L"Service:";
			// 
			// tabPage5
			// 
			this->tabPage5->Controls->Add(this->btnCDelete);
			this->tabPage5->Controls->Add(this->btnCModify);
			this->tabPage5->Controls->Add(this->btnCAdd);
			this->tabPage5->Controls->Add(this->gbClientsData);
			this->tabPage5->Controls->Add(this->dgvClients);
			this->tabPage5->Controls->Add(this->btnCSearch);
			this->tabPage5->Controls->Add(this->txtCSearch);
			this->tabPage5->Controls->Add(this->label30);
			this->tabPage5->Location = System::Drawing::Point(4, 4);
			this->tabPage5->Name = L"tabPage5";
			this->tabPage5->Padding = System::Windows::Forms::Padding(3);
			this->tabPage5->Size = System::Drawing::Size(1104, 682);
			this->tabPage5->TabIndex = 4;
			this->tabPage5->Text = L"Clients";
			this->tabPage5->UseVisualStyleBackColor = true;
			// 
			// btnCDelete
			// 
			this->btnCDelete->BackColor = System::Drawing::Color::Red;
			this->btnCDelete->FlatAppearance->BorderSize = 0;
			this->btnCDelete->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnCDelete->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
			this->btnCDelete->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnCDelete->Location = System::Drawing::Point(812, 526);
			this->btnCDelete->Name = L"btnCDelete";
			this->btnCDelete->Size = System::Drawing::Size(152, 31);
			this->btnCDelete->TabIndex = 7;
			this->btnCDelete->Text = L"Delete";
			this->btnCDelete->UseVisualStyleBackColor = false;
			this->btnCDelete->Visible = false;
			this->btnCDelete->Click += gcnew System::EventHandler(this, &Program::btnCDelete_Click);
			// 
			// btnCModify
			// 
			this->btnCModify->BackColor = System::Drawing::Color::DeepSkyBlue;
			this->btnCModify->FlatAppearance->BorderSize = 0;
			this->btnCModify->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnCModify->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
			this->btnCModify->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnCModify->Location = System::Drawing::Point(812, 481);
			this->btnCModify->Name = L"btnCModify";
			this->btnCModify->Size = System::Drawing::Size(152, 30);
			this->btnCModify->TabIndex = 6;
			this->btnCModify->Text = L"Modify";
			this->btnCModify->UseVisualStyleBackColor = false;
			this->btnCModify->Visible = false;
			this->btnCModify->Click += gcnew System::EventHandler(this, &Program::btnCModify_Click);
			// 
			// btnCAdd
			// 
			this->btnCAdd->BackColor = System::Drawing::Color::Green;
			this->btnCAdd->FlatAppearance->BorderSize = 0;
			this->btnCAdd->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnCAdd->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
			this->btnCAdd->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnCAdd->Location = System::Drawing::Point(812, 438);
			this->btnCAdd->Name = L"btnCAdd";
			this->btnCAdd->Size = System::Drawing::Size(152, 30);
			this->btnCAdd->TabIndex = 5;
			this->btnCAdd->Text = L"Add";
			this->btnCAdd->UseVisualStyleBackColor = false;
			this->btnCAdd->Click += gcnew System::EventHandler(this, &Program::btnCAdd_Click);
			// 
			// gbClientsData
			// 
			this->gbClientsData->Controls->Add(this->txtCNumber);
			this->gbClientsData->Controls->Add(this->label35);
			this->gbClientsData->Controls->Add(this->mtxtCPCode);
			this->gbClientsData->Controls->Add(this->label36);
			this->gbClientsData->Controls->Add(this->txtCCity);
			this->gbClientsData->Controls->Add(this->label37);
			this->gbClientsData->Controls->Add(this->txtCStreet);
			this->gbClientsData->Controls->Add(this->label38);
			this->gbClientsData->Controls->Add(this->txtCPhone);
			this->gbClientsData->Controls->Add(this->label34);
			this->gbClientsData->Controls->Add(this->txtCEmail);
			this->gbClientsData->Controls->Add(this->label33);
			this->gbClientsData->Controls->Add(this->txtCLastName);
			this->gbClientsData->Controls->Add(this->label32);
			this->gbClientsData->Controls->Add(this->txtCFirstName);
			this->gbClientsData->Controls->Add(this->label31);
			this->gbClientsData->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->gbClientsData->Location = System::Drawing::Point(9, 359);
			this->gbClientsData->Name = L"gbClientsData";
			this->gbClientsData->Size = System::Drawing::Size(634, 277);
			this->gbClientsData->TabIndex = 4;
			this->gbClientsData->TabStop = false;
			this->gbClientsData->Text = L"Edit Clients Data:";
			// 
			// txtCNumber
			// 
			this->txtCNumber->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtCNumber->Location = System::Drawing::Point(529, 86);
			this->txtCNumber->Name = L"txtCNumber";
			this->txtCNumber->Size = System::Drawing::Size(58, 26);
			this->txtCNumber->TabIndex = 16;
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label35->Location = System::Drawing::Point(321, 89);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(69, 20);
			this->label35->TabIndex = 15;
			this->label35->Text = L"Number:";
			// 
			// mtxtCPCode
			// 
			this->mtxtCPCode->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->mtxtCPCode->Location = System::Drawing::Point(529, 172);
			this->mtxtCPCode->Mask = L"00-999";
			this->mtxtCPCode->Name = L"mtxtCPCode";
			this->mtxtCPCode->Size = System::Drawing::Size(58, 26);
			this->mtxtCPCode->TabIndex = 14;
			// 
			// label36
			// 
			this->label36->AutoSize = true;
			this->label36->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label36->Location = System::Drawing::Point(321, 175);
			this->label36->Name = L"label36";
			this->label36->Size = System::Drawing::Size(99, 20);
			this->label36->TabIndex = 12;
			this->label36->Text = L"Postal Code:";
			// 
			// txtCCity
			// 
			this->txtCCity->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtCCity->Location = System::Drawing::Point(407, 129);
			this->txtCCity->Name = L"txtCCity";
			this->txtCCity->Size = System::Drawing::Size(180, 26);
			this->txtCCity->TabIndex = 11;
			// 
			// label37
			// 
			this->label37->AutoSize = true;
			this->label37->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label37->Location = System::Drawing::Point(321, 132);
			this->label37->Name = L"label37";
			this->label37->Size = System::Drawing::Size(39, 20);
			this->label37->TabIndex = 10;
			this->label37->Text = L"City:";
			// 
			// txtCStreet
			// 
			this->txtCStreet->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtCStreet->Location = System::Drawing::Point(407, 45);
			this->txtCStreet->Name = L"txtCStreet";
			this->txtCStreet->Size = System::Drawing::Size(180, 26);
			this->txtCStreet->TabIndex = 9;
			// 
			// label38
			// 
			this->label38->AutoSize = true;
			this->label38->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label38->Location = System::Drawing::Point(321, 48);
			this->label38->Name = L"label38";
			this->label38->Size = System::Drawing::Size(57, 20);
			this->label38->TabIndex = 8;
			this->label38->Text = L"Street:";
			// 
			// txtCPhone
			// 
			this->txtCPhone->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtCPhone->Location = System::Drawing::Point(112, 167);
			this->txtCPhone->Name = L"txtCPhone";
			this->txtCPhone->Size = System::Drawing::Size(180, 26);
			this->txtCPhone->TabIndex = 7;
			// 
			// label34
			// 
			this->label34->AutoSize = true;
			this->label34->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label34->Location = System::Drawing::Point(16, 173);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(59, 20);
			this->label34->TabIndex = 6;
			this->label34->Text = L"Phone:";
			// 
			// txtCEmail
			// 
			this->txtCEmail->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtCEmail->Location = System::Drawing::Point(112, 122);
			this->txtCEmail->Name = L"txtCEmail";
			this->txtCEmail->Size = System::Drawing::Size(180, 26);
			this->txtCEmail->TabIndex = 5;
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label33->Location = System::Drawing::Point(16, 132);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(52, 20);
			this->label33->TabIndex = 4;
			this->label33->Text = L"Email:";
			// 
			// txtCLastName
			// 
			this->txtCLastName->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtCLastName->Location = System::Drawing::Point(112, 83);
			this->txtCLastName->Name = L"txtCLastName";
			this->txtCLastName->Size = System::Drawing::Size(180, 26);
			this->txtCLastName->TabIndex = 3;
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label32->Location = System::Drawing::Point(16, 89);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(90, 20);
			this->label32->TabIndex = 2;
			this->label32->Text = L"Last Name:";
			// 
			// txtCFirstName
			// 
			this->txtCFirstName->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtCFirstName->Location = System::Drawing::Point(112, 45);
			this->txtCFirstName->Name = L"txtCFirstName";
			this->txtCFirstName->Size = System::Drawing::Size(180, 26);
			this->txtCFirstName->TabIndex = 1;
			// 
			// label31
			// 
			this->label31->AutoSize = true;
			this->label31->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label31->Location = System::Drawing::Point(16, 48);
			this->label31->Name = L"label31";
			this->label31->Size = System::Drawing::Size(90, 20);
			this->label31->TabIndex = 0;
			this->label31->Text = L"First Name:";
			// 
			// dgvClients
			// 
			this->dgvClients->AllowUserToAddRows = false;
			this->dgvClients->AllowUserToOrderColumns = true;
			this->dgvClients->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dgvClients->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvClients->Location = System::Drawing::Point(8, 76);
			this->dgvClients->Name = L"dgvClients";
			this->dgvClients->Size = System::Drawing::Size(1090, 272);
			this->dgvClients->TabIndex = 3;
			this->dgvClients->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Program::dgvClients_CellClick);
			// 
			// btnCSearch
			// 
			this->btnCSearch->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->btnCSearch->FlatAppearance->BorderSize = 0;
			this->btnCSearch->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnCSearch->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
			this->btnCSearch->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnCSearch->Location = System::Drawing::Point(505, 30);
			this->btnCSearch->Name = L"btnCSearch";
			this->btnCSearch->Size = System::Drawing::Size(75, 28);
			this->btnCSearch->TabIndex = 2;
			this->btnCSearch->Text = L"Search";
			this->btnCSearch->UseVisualStyleBackColor = false;
			this->btnCSearch->Click += gcnew System::EventHandler(this, &Program::btnCSearch_Click);
			// 
			// txtCSearch
			// 
			this->txtCSearch->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->txtCSearch->Location = System::Drawing::Point(121, 32);
			this->txtCSearch->Name = L"txtCSearch";
			this->txtCSearch->Size = System::Drawing::Size(353, 26);
			this->txtCSearch->TabIndex = 1;
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label30->Location = System::Drawing::Point(49, 35);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(53, 20);
			this->label30->TabIndex = 0;
			this->label30->Text = L"Client:";
			// 
			// tabPage4
			// 
			this->tabPage4->Controls->Add(this->txtESLastName);
			this->tabPage4->Controls->Add(this->label29);
			this->tabPage4->Controls->Add(this->txtESFirstName);
			this->tabPage4->Controls->Add(this->label28);
			this->tabPage4->Controls->Add(this->label27);
			this->tabPage4->Controls->Add(this->dtvESAddService);
			this->tabPage4->Controls->Add(this->label26);
			this->tabPage4->Controls->Add(this->dgvESServices);
			this->tabPage4->Controls->Add(this->label25);
			this->tabPage4->Controls->Add(this->dgvESEmployees);
			this->tabPage4->Controls->Add(this->btnESSearch);
			this->tabPage4->Controls->Add(this->txtESSearch);
			this->tabPage4->Controls->Add(this->label21);
			this->tabPage4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->tabPage4->Location = System::Drawing::Point(4, 4);
			this->tabPage4->Name = L"tabPage4";
			this->tabPage4->Padding = System::Windows::Forms::Padding(3);
			this->tabPage4->Size = System::Drawing::Size(1104, 682);
			this->tabPage4->TabIndex = 3;
			this->tabPage4->Text = L"Employee-Service";
			this->tabPage4->UseVisualStyleBackColor = true;
			// 
			// txtESLastName
			// 
			this->txtESLastName->Location = System::Drawing::Point(115, 151);
			this->txtESLastName->Name = L"txtESLastName";
			this->txtESLastName->Size = System::Drawing::Size(250, 26);
			this->txtESLastName->TabIndex = 12;
			// 
			// label29
			// 
			this->label29->AutoSize = true;
			this->label29->Location = System::Drawing::Point(17, 154);
			this->label29->Name = L"label29";
			this->label29->Size = System::Drawing::Size(90, 20);
			this->label29->TabIndex = 11;
			this->label29->Text = L"Last Name:";
			// 
			// txtESFirstName
			// 
			this->txtESFirstName->Location = System::Drawing::Point(115, 96);
			this->txtESFirstName->Name = L"txtESFirstName";
			this->txtESFirstName->Size = System::Drawing::Size(250, 26);
			this->txtESFirstName->TabIndex = 10;
			// 
			// label28
			// 
			this->label28->AutoSize = true;
			this->label28->Location = System::Drawing::Point(17, 99);
			this->label28->Name = L"label28";
			this->label28->Size = System::Drawing::Size(90, 20);
			this->label28->TabIndex = 9;
			this->label28->Text = L"First Name:";
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(40, 316);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(98, 20);
			this->label27->TabIndex = 8;
			this->label27->Text = L"Add Service:";
			// 
			// dtvESAddService
			// 
			this->dtvESAddService->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dtvESAddService->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dtvESAddService->Location = System::Drawing::Point(44, 348);
			this->dtvESAddService->Name = L"dtvESAddService";
			this->dtvESAddService->Size = System::Drawing::Size(476, 261);
			this->dtvESAddService->TabIndex = 7;
			this->dtvESAddService->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Program::dtvESAddService_CellClick);
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Location = System::Drawing::Point(543, 316);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(151, 20);
			this->label26->TabIndex = 6;
			this->label26->Text = L"Performed Services:";
			// 
			// dgvESServices
			// 
			this->dgvESServices->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dgvESServices->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvESServices->Location = System::Drawing::Point(547, 348);
			this->dgvESServices->Name = L"dgvESServices";
			this->dgvESServices->Size = System::Drawing::Size(476, 261);
			this->dgvESServices->TabIndex = 5;
			this->dgvESServices->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Program::dgvESServices_CellClick);
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(543, 15);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(91, 20);
			this->label25->TabIndex = 4;
			this->label25->Text = L"Employees:";
			// 
			// dgvESEmployees
			// 
			this->dgvESEmployees->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dgvESEmployees->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvESEmployees->Location = System::Drawing::Point(547, 48);
			this->dgvESEmployees->Name = L"dgvESEmployees";
			this->dgvESEmployees->Size = System::Drawing::Size(476, 261);
			this->dgvESEmployees->TabIndex = 3;
			this->dgvESEmployees->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Program::dgvESEmployees_CellClick);
			// 
			// btnESSearch
			// 
			this->btnESSearch->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->btnESSearch->FlatAppearance->BorderSize = 0;
			this->btnESSearch->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnESSearch->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnESSearch->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnESSearch->Location = System::Drawing::Point(405, 44);
			this->btnESSearch->Name = L"btnESSearch";
			this->btnESSearch->Size = System::Drawing::Size(75, 27);
			this->btnESSearch->TabIndex = 2;
			this->btnESSearch->Text = L"Search";
			this->btnESSearch->UseVisualStyleBackColor = false;
			this->btnESSearch->Click += gcnew System::EventHandler(this, &Program::btnESSearch_Click);
			// 
			// txtESSearch
			// 
			this->txtESSearch->Location = System::Drawing::Point(115, 45);
			this->txtESSearch->Name = L"txtESSearch";
			this->txtESSearch->Size = System::Drawing::Size(250, 26);
			this->txtESSearch->TabIndex = 1;
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(17, 48);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(79, 20);
			this->label21->TabIndex = 0;
			this->label21->Text = L"Employee";
			// 
			// tabPage3
			// 
			this->tabPage3->Controls->Add(this->btnIDelete);
			this->tabPage3->Controls->Add(this->btnIUpdate);
			this->tabPage3->Controls->Add(this->btnIAdd);
			this->tabPage3->Controls->Add(this->groupBox5);
			this->tabPage3->Controls->Add(this->dgIServices);
			this->tabPage3->Controls->Add(this->btnISearch);
			this->tabPage3->Controls->Add(this->txtISearch);
			this->tabPage3->Controls->Add(this->label20);
			this->tabPage3->Location = System::Drawing::Point(4, 4);
			this->tabPage3->Name = L"tabPage3";
			this->tabPage3->Padding = System::Windows::Forms::Padding(3);
			this->tabPage3->Size = System::Drawing::Size(1104, 682);
			this->tabPage3->TabIndex = 2;
			this->tabPage3->Text = L"Services";
			this->tabPage3->UseVisualStyleBackColor = true;
			// 
			// btnIDelete
			// 
			this->btnIDelete->BackColor = System::Drawing::Color::Red;
			this->btnIDelete->Enabled = false;
			this->btnIDelete->FlatAppearance->BorderSize = 0;
			this->btnIDelete->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnIDelete->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnIDelete->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnIDelete->Location = System::Drawing::Point(327, 602);
			this->btnIDelete->Name = L"btnIDelete";
			this->btnIDelete->Size = System::Drawing::Size(148, 33);
			this->btnIDelete->TabIndex = 10;
			this->btnIDelete->Text = L"Delete";
			this->btnIDelete->UseVisualStyleBackColor = false;
			this->btnIDelete->Click += gcnew System::EventHandler(this, &Program::btnIDelete_Click);
			// 
			// btnIUpdate
			// 
			this->btnIUpdate->BackColor = System::Drawing::Color::DeepSkyBlue;
			this->btnIUpdate->Enabled = false;
			this->btnIUpdate->FlatAppearance->BorderSize = 0;
			this->btnIUpdate->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnIUpdate->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnIUpdate->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnIUpdate->Location = System::Drawing::Point(171, 602);
			this->btnIUpdate->Name = L"btnIUpdate";
			this->btnIUpdate->Size = System::Drawing::Size(150, 33);
			this->btnIUpdate->TabIndex = 9;
			this->btnIUpdate->Text = L"Update";
			this->btnIUpdate->UseVisualStyleBackColor = false;
			this->btnIUpdate->Click += gcnew System::EventHandler(this, &Program::btnIUpdate_Click);
			// 
			// btnIAdd
			// 
			this->btnIAdd->BackColor = System::Drawing::Color::Green;
			this->btnIAdd->FlatAppearance->BorderSize = 0;
			this->btnIAdd->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnIAdd->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnIAdd->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnIAdd->Location = System::Drawing::Point(10, 602);
			this->btnIAdd->Name = L"btnIAdd";
			this->btnIAdd->Size = System::Drawing::Size(155, 33);
			this->btnIAdd->TabIndex = 8;
			this->btnIAdd->Text = L"Add";
			this->btnIAdd->UseVisualStyleBackColor = false;
			this->btnIAdd->Click += gcnew System::EventHandler(this, &Program::btnIAdd_Click);
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->txtIDescription);
			this->groupBox5->Controls->Add(this->label24);
			this->groupBox5->Controls->Add(this->txtITime);
			this->groupBox5->Controls->Add(this->label23);
			this->groupBox5->Controls->Add(this->txtIPrice);
			this->groupBox5->Controls->Add(this->label22);
			this->groupBox5->Controls->Add(this->txtIName);
			this->groupBox5->Controls->Add(this->lbl);
			this->groupBox5->Location = System::Drawing::Point(6, 150);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(469, 373);
			this->groupBox5->TabIndex = 4;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"Intervention:";
			// 
			// txtIDescription
			// 
			this->txtIDescription->Location = System::Drawing::Point(23, 226);
			this->txtIDescription->Multiline = true;
			this->txtIDescription->Name = L"txtIDescription";
			this->txtIDescription->Size = System::Drawing::Size(426, 115);
			this->txtIDescription->TabIndex = 7;
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Location = System::Drawing::Point(19, 188);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(93, 20);
			this->label24->TabIndex = 6;
			this->label24->Text = L"Description:";
			// 
			// txtITime
			// 
			this->txtITime->Location = System::Drawing::Point(368, 129);
			this->txtITime->Name = L"txtITime";
			this->txtITime->Size = System::Drawing::Size(81, 26);
			this->txtITime->TabIndex = 5;
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(19, 132);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(47, 20);
			this->label23->TabIndex = 4;
			this->label23->Text = L"Time:";
			// 
			// txtIPrice
			// 
			this->txtIPrice->Location = System::Drawing::Point(368, 82);
			this->txtIPrice->Name = L"txtIPrice";
			this->txtIPrice->Size = System::Drawing::Size(81, 26);
			this->txtIPrice->TabIndex = 3;
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(18, 85);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(48, 20);
			this->label22->TabIndex = 2;
			this->label22->Text = L"Price:";
			// 
			// txtIName
			// 
			this->txtIName->Location = System::Drawing::Point(80, 40);
			this->txtIName->Name = L"txtIName";
			this->txtIName->Size = System::Drawing::Size(369, 26);
			this->txtIName->TabIndex = 1;
			// 
			// lbl
			// 
			this->lbl->AutoSize = true;
			this->lbl->Location = System::Drawing::Point(19, 43);
			this->lbl->Name = L"lbl";
			this->lbl->Size = System::Drawing::Size(55, 20);
			this->lbl->TabIndex = 0;
			this->lbl->Text = L"Name:";
			// 
			// dgIServices
			// 
			this->dgIServices->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dgIServices->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgIServices->Location = System::Drawing::Point(481, 26);
			this->dgIServices->Name = L"dgIServices";
			this->dgIServices->Size = System::Drawing::Size(598, 609);
			this->dgIServices->TabIndex = 3;
			this->dgIServices->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Program::dgIServices_CellClick);
			// 
			// btnISearch
			// 
			this->btnISearch->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->btnISearch->FlatAppearance->BorderSize = 0;
			this->btnISearch->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnISearch->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnISearch->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnISearch->Location = System::Drawing::Point(380, 29);
			this->btnISearch->Name = L"btnISearch";
			this->btnISearch->Size = System::Drawing::Size(75, 26);
			this->btnISearch->TabIndex = 2;
			this->btnISearch->Text = L"Search";
			this->btnISearch->UseVisualStyleBackColor = false;
			this->btnISearch->Click += gcnew System::EventHandler(this, &Program::btnISearch_Click);
			// 
			// txtISearch
			// 
			this->txtISearch->Location = System::Drawing::Point(105, 29);
			this->txtISearch->Name = L"txtISearch";
			this->txtISearch->Size = System::Drawing::Size(249, 26);
			this->txtISearch->TabIndex = 1;
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(6, 32);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(97, 20);
			this->label20->TabIndex = 0;
			this->label20->Text = L"Intervention:";
			// 
			// tabPage1
			// 
			this->tabPage1->Controls->Add(this->groupBox3);
			this->tabPage1->Controls->Add(this->btnDelete);
			this->tabPage1->Controls->Add(this->btnUpdate);
			this->tabPage1->Controls->Add(this->btnAdd);
			this->tabPage1->Controls->Add(this->groupBox2);
			this->tabPage1->Controls->Add(this->btnSearch);
			this->tabPage1->Controls->Add(this->txtSearch);
			this->tabPage1->Controls->Add(this->label4);
			this->tabPage1->Controls->Add(this->dgvUser);
			this->tabPage1->Location = System::Drawing::Point(4, 4);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(1104, 682);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"Employees";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->groupBox4);
			this->groupBox3->Controls->Add(this->txtE6e);
			this->groupBox3->Controls->Add(this->label18);
			this->groupBox3->Controls->Add(this->txtE6b);
			this->groupBox3->Controls->Add(this->label19);
			this->groupBox3->Controls->Add(this->txtE5e);
			this->groupBox3->Controls->Add(this->label16);
			this->groupBox3->Controls->Add(this->txtE5b);
			this->groupBox3->Controls->Add(this->label17);
			this->groupBox3->Controls->Add(this->txtE4e);
			this->groupBox3->Controls->Add(this->label14);
			this->groupBox3->Controls->Add(this->txtE4b);
			this->groupBox3->Controls->Add(this->label15);
			this->groupBox3->Controls->Add(this->txtE3e);
			this->groupBox3->Controls->Add(this->label12);
			this->groupBox3->Controls->Add(this->txtE3b);
			this->groupBox3->Controls->Add(this->label13);
			this->groupBox3->Controls->Add(this->txtE2e);
			this->groupBox3->Controls->Add(this->label10);
			this->groupBox3->Controls->Add(this->txtE2b);
			this->groupBox3->Controls->Add(this->label11);
			this->groupBox3->Controls->Add(this->txtE1e);
			this->groupBox3->Controls->Add(this->label9);
			this->groupBox3->Controls->Add(this->txtE1b);
			this->groupBox3->Controls->Add(this->label8);
			this->groupBox3->Location = System::Drawing::Point(6, 327);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(459, 288);
			this->groupBox3->TabIndex = 8;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Work Hours";
			this->groupBox3->Visible = false;
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->btn10);
			this->groupBox4->Controls->Add(this->btn9);
			this->groupBox4->Controls->Add(this->btn8);
			this->groupBox4->Controls->Add(this->btn7);
			this->groupBox4->Location = System::Drawing::Point(293, 44);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(142, 211);
			this->groupBox4->TabIndex = 24;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"Templates";
			// 
			// btn10
			// 
			this->btn10->BackColor = System::Drawing::Color::DeepSkyBlue;
			this->btn10->FlatAppearance->BorderSize = 0;
			this->btn10->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btn10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btn10->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btn10->Location = System::Drawing::Point(27, 152);
			this->btn10->Name = L"btn10";
			this->btn10->Size = System::Drawing::Size(75, 36);
			this->btn10->TabIndex = 3;
			this->btn10->Text = L"10-18";
			this->btn10->UseVisualStyleBackColor = false;
			this->btn10->Click += gcnew System::EventHandler(this, &Program::btn10_Click);
			// 
			// btn9
			// 
			this->btn9->BackColor = System::Drawing::Color::DeepSkyBlue;
			this->btn9->FlatAppearance->BorderSize = 0;
			this->btn9->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btn9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btn9->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btn9->Location = System::Drawing::Point(27, 110);
			this->btn9->Name = L"btn9";
			this->btn9->Size = System::Drawing::Size(75, 36);
			this->btn9->TabIndex = 2;
			this->btn9->Text = L"9-17";
			this->btn9->UseVisualStyleBackColor = false;
			this->btn9->Click += gcnew System::EventHandler(this, &Program::btn9_Click);
			// 
			// btn8
			// 
			this->btn8->BackColor = System::Drawing::Color::DeepSkyBlue;
			this->btn8->FlatAppearance->BorderSize = 0;
			this->btn8->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btn8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btn8->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btn8->Location = System::Drawing::Point(27, 68);
			this->btn8->Name = L"btn8";
			this->btn8->Size = System::Drawing::Size(75, 36);
			this->btn8->TabIndex = 1;
			this->btn8->Text = L"8-16";
			this->btn8->UseVisualStyleBackColor = false;
			this->btn8->Click += gcnew System::EventHandler(this, &Program::btn8_Click);
			// 
			// btn7
			// 
			this->btn7->BackColor = System::Drawing::Color::DeepSkyBlue;
			this->btn7->FlatAppearance->BorderSize = 0;
			this->btn7->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btn7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btn7->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btn7->Location = System::Drawing::Point(27, 26);
			this->btn7->Name = L"btn7";
			this->btn7->Size = System::Drawing::Size(75, 36);
			this->btn7->TabIndex = 0;
			this->btn7->Text = L"7-15";
			this->btn7->UseVisualStyleBackColor = false;
			this->btn7->Click += gcnew System::EventHandler(this, &Program::btn7_Click);
			// 
			// txtE6e
			// 
			this->txtE6e->Location = System::Drawing::Point(197, 226);
			this->txtE6e->Name = L"txtE6e";
			this->txtE6e->Size = System::Drawing::Size(74, 26);
			this->txtE6e->TabIndex = 23;
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(181, 229);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(14, 20);
			this->label18->TabIndex = 22;
			this->label18->Text = L"-";
			// 
			// txtE6b
			// 
			this->txtE6b->Location = System::Drawing::Point(103, 226);
			this->txtE6b->Name = L"txtE6b";
			this->txtE6b->Size = System::Drawing::Size(72, 26);
			this->txtE6b->TabIndex = 21;
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(4, 226);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(73, 20);
			this->label19->TabIndex = 20;
			this->label19->Text = L"Saturday";
			// 
			// txtE5e
			// 
			this->txtE5e->Location = System::Drawing::Point(197, 186);
			this->txtE5e->Name = L"txtE5e";
			this->txtE5e->Size = System::Drawing::Size(74, 26);
			this->txtE5e->TabIndex = 19;
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(181, 189);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(14, 20);
			this->label16->TabIndex = 18;
			this->label16->Text = L"-";
			// 
			// txtE5b
			// 
			this->txtE5b->Location = System::Drawing::Point(103, 186);
			this->txtE5b->Name = L"txtE5b";
			this->txtE5b->Size = System::Drawing::Size(72, 26);
			this->txtE5b->TabIndex = 17;
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(4, 186);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(52, 20);
			this->label17->TabIndex = 16;
			this->label17->Text = L"Friday";
			// 
			// txtE4e
			// 
			this->txtE4e->Location = System::Drawing::Point(197, 147);
			this->txtE4e->Name = L"txtE4e";
			this->txtE4e->Size = System::Drawing::Size(74, 26);
			this->txtE4e->TabIndex = 15;
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(181, 150);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(14, 20);
			this->label14->TabIndex = 14;
			this->label14->Text = L"-";
			// 
			// txtE4b
			// 
			this->txtE4b->Location = System::Drawing::Point(103, 147);
			this->txtE4b->Name = L"txtE4b";
			this->txtE4b->Size = System::Drawing::Size(72, 26);
			this->txtE4b->TabIndex = 13;
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(3, 147);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(74, 20);
			this->label15->TabIndex = 12;
			this->label15->Text = L"Thursday";
			// 
			// txtE3e
			// 
			this->txtE3e->Location = System::Drawing::Point(197, 112);
			this->txtE3e->Name = L"txtE3e";
			this->txtE3e->Size = System::Drawing::Size(74, 26);
			this->txtE3e->TabIndex = 11;
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(181, 115);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(14, 20);
			this->label12->TabIndex = 10;
			this->label12->Text = L"-";
			// 
			// txtE3b
			// 
			this->txtE3b->Location = System::Drawing::Point(103, 112);
			this->txtE3b->Name = L"txtE3b";
			this->txtE3b->Size = System::Drawing::Size(72, 26);
			this->txtE3b->TabIndex = 9;
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(4, 115);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(93, 20);
			this->label13->TabIndex = 8;
			this->label13->Text = L"Wednesday";
			// 
			// txtE2e
			// 
			this->txtE2e->Location = System::Drawing::Point(197, 78);
			this->txtE2e->Name = L"txtE2e";
			this->txtE2e->Size = System::Drawing::Size(74, 26);
			this->txtE2e->TabIndex = 7;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(181, 81);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(14, 20);
			this->label10->TabIndex = 6;
			this->label10->Text = L"-";
			// 
			// txtE2b
			// 
			this->txtE2b->Location = System::Drawing::Point(103, 78);
			this->txtE2b->Name = L"txtE2b";
			this->txtE2b->Size = System::Drawing::Size(72, 26);
			this->txtE2b->TabIndex = 5;
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(3, 78);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(69, 20);
			this->label11->TabIndex = 4;
			this->label11->Text = L"Tuesday";
			// 
			// txtE1e
			// 
			this->txtE1e->Location = System::Drawing::Point(197, 44);
			this->txtE1e->Name = L"txtE1e";
			this->txtE1e->Size = System::Drawing::Size(74, 26);
			this->txtE1e->TabIndex = 3;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(181, 47);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(14, 20);
			this->label9->TabIndex = 2;
			this->label9->Text = L"-";
			// 
			// txtE1b
			// 
			this->txtE1b->Location = System::Drawing::Point(103, 44);
			this->txtE1b->Name = L"txtE1b";
			this->txtE1b->Size = System::Drawing::Size(72, 26);
			this->txtE1b->TabIndex = 1;
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(4, 44);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(65, 20);
			this->label8->TabIndex = 0;
			this->label8->Text = L"Monday";
			// 
			// btnDelete
			// 
			this->btnDelete->BackColor = System::Drawing::Color::Red;
			this->btnDelete->Enabled = false;
			this->btnDelete->FlatAppearance->BorderSize = 0;
			this->btnDelete->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnDelete->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnDelete->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnDelete->Location = System::Drawing::Point(311, 634);
			this->btnDelete->Name = L"btnDelete";
			this->btnDelete->Size = System::Drawing::Size(154, 30);
			this->btnDelete->TabIndex = 7;
			this->btnDelete->Text = L"Delete";
			this->btnDelete->UseVisualStyleBackColor = false;
			this->btnDelete->Click += gcnew System::EventHandler(this, &Program::btnDelete_Click);
			// 
			// btnUpdate
			// 
			this->btnUpdate->BackColor = System::Drawing::Color::DeepSkyBlue;
			this->btnUpdate->Enabled = false;
			this->btnUpdate->FlatAppearance->BorderSize = 0;
			this->btnUpdate->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnUpdate->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnUpdate->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnUpdate->Location = System::Drawing::Point(165, 634);
			this->btnUpdate->Name = L"btnUpdate";
			this->btnUpdate->Size = System::Drawing::Size(140, 30);
			this->btnUpdate->TabIndex = 6;
			this->btnUpdate->Text = L"Update";
			this->btnUpdate->UseVisualStyleBackColor = false;
			this->btnUpdate->Click += gcnew System::EventHandler(this, &Program::btnUpdate_Click);
			// 
			// btnAdd
			// 
			this->btnAdd->BackColor = System::Drawing::Color::Green;
			this->btnAdd->FlatAppearance->BorderSize = 0;
			this->btnAdd->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnAdd->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnAdd->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnAdd->Location = System::Drawing::Point(6, 634);
			this->btnAdd->Name = L"btnAdd";
			this->btnAdd->Size = System::Drawing::Size(153, 30);
			this->btnAdd->TabIndex = 5;
			this->btnAdd->Text = L"Add";
			this->btnAdd->UseVisualStyleBackColor = false;
			this->btnAdd->Click += gcnew System::EventHandler(this, &Program::btnAdd_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->chkEmployee);
			this->groupBox2->Controls->Add(this->txtUsername);
			this->groupBox2->Controls->Add(this->label7);
			this->groupBox2->Controls->Add(this->txtLastName);
			this->groupBox2->Controls->Add(this->label6);
			this->groupBox2->Controls->Add(this->txtFirstName);
			this->groupBox2->Controls->Add(this->label5);
			this->groupBox2->Location = System::Drawing::Point(6, 73);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(459, 237);
			this->groupBox2->TabIndex = 4;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Edit User Information";
			// 
			// chkEmployee
			// 
			this->chkEmployee->AutoSize = true;
			this->chkEmployee->Location = System::Drawing::Point(355, 113);
			this->chkEmployee->Name = L"chkEmployee";
			this->chkEmployee->Size = System::Drawing::Size(98, 24);
			this->chkEmployee->TabIndex = 6;
			this->chkEmployee->Text = L"Employee";
			this->chkEmployee->UseVisualStyleBackColor = true;
			this->chkEmployee->CheckedChanged += gcnew System::EventHandler(this, &Program::chkEmployee_CheckedChanged);
			// 
			// txtUsername
			// 
			this->txtUsername->Location = System::Drawing::Point(103, 166);
			this->txtUsername->Name = L"txtUsername";
			this->txtUsername->Size = System::Drawing::Size(237, 26);
			this->txtUsername->TabIndex = 5;
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(7, 169);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(87, 20);
			this->label7->TabIndex = 4;
			this->label7->Text = L"Username:";
			// 
			// txtLastName
			// 
			this->txtLastName->Location = System::Drawing::Point(103, 111);
			this->txtLastName->Name = L"txtLastName";
			this->txtLastName->Size = System::Drawing::Size(237, 26);
			this->txtLastName->TabIndex = 3;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(7, 114);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(90, 20);
			this->label6->TabIndex = 2;
			this->label6->Text = L"Last Name:";
			// 
			// txtFirstName
			// 
			this->txtFirstName->Location = System::Drawing::Point(103, 53);
			this->txtFirstName->Name = L"txtFirstName";
			this->txtFirstName->Size = System::Drawing::Size(237, 26);
			this->txtFirstName->TabIndex = 1;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(7, 56);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(90, 20);
			this->label5->TabIndex = 0;
			this->label5->Text = L"First Name:";
			// 
			// btnSearch
			// 
			this->btnSearch->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->btnSearch->FlatAppearance->BorderSize = 0;
			this->btnSearch->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnSearch->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnSearch->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnSearch->Location = System::Drawing::Point(390, 15);
			this->btnSearch->Name = L"btnSearch";
			this->btnSearch->Size = System::Drawing::Size(75, 36);
			this->btnSearch->TabIndex = 3;
			this->btnSearch->Text = L"Search";
			this->btnSearch->UseVisualStyleBackColor = false;
			this->btnSearch->Click += gcnew System::EventHandler(this, &Program::btnSearch_Click);
			// 
			// txtSearch
			// 
			this->txtSearch->Location = System::Drawing::Point(95, 20);
			this->txtSearch->Name = L"txtSearch";
			this->txtSearch->Size = System::Drawing::Size(279, 26);
			this->txtSearch->TabIndex = 2;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(6, 23);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(83, 20);
			this->label4->TabIndex = 1;
			this->label4->Text = L"Employee:";
			// 
			// dgvUser
			// 
			this->dgvUser->AllowUserToAddRows = false;
			this->dgvUser->AllowUserToOrderColumns = true;
			this->dgvUser->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dgvUser->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvUser->Location = System::Drawing::Point(471, 20);
			this->dgvUser->Name = L"dgvUser";
			this->dgvUser->Size = System::Drawing::Size(613, 644);
			this->dgvUser->TabIndex = 0;
			this->dgvUser->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Program::dgvUser_CellClick);
			// 
			// tabPage2
			// 
			this->tabPage2->Controls->Add(this->groupBox1);
			this->tabPage2->Location = System::Drawing::Point(4, 4);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(1104, 682);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"Password Change";
			this->tabPage2->UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->txtNewPassword);
			this->groupBox1->Controls->Add(this->btnChange);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->txtRepeatPassword);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->txtOldPassword);
			this->groupBox1->Location = System::Drawing::Point(347, 189);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(409, 270);
			this->groupBox1->TabIndex = 13;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Password Change";
			// 
			// txtNewPassword
			// 
			this->txtNewPassword->Location = System::Drawing::Point(164, 94);
			this->txtNewPassword->Name = L"txtNewPassword";
			this->txtNewPassword->Size = System::Drawing::Size(223, 26);
			this->txtNewPassword->TabIndex = 10;
			this->txtNewPassword->TextChanged += gcnew System::EventHandler(this, &Program::txtNewPassword_TextChanged);
			// 
			// btnChange
			// 
			this->btnChange->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->btnChange->Enabled = false;
			this->btnChange->FlatAppearance->BorderSize = 0;
			this->btnChange->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnChange->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->btnChange->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->btnChange->Location = System::Drawing::Point(129, 212);
			this->btnChange->Name = L"btnChange";
			this->btnChange->Size = System::Drawing::Size(155, 34);
			this->btnChange->TabIndex = 12;
			this->btnChange->Text = L"Change";
			this->btnChange->UseVisualStyleBackColor = false;
			this->btnChange->Click += gcnew System::EventHandler(this, &Program::btnChange_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 49);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(110, 20);
			this->label1->TabIndex = 6;
			this->label1->Text = L"Old Password:";
			// 
			// txtRepeatPassword
			// 
			this->txtRepeatPassword->Location = System::Drawing::Point(164, 145);
			this->txtRepeatPassword->Name = L"txtRepeatPassword";
			this->txtRepeatPassword->Size = System::Drawing::Size(223, 26);
			this->txtRepeatPassword->TabIndex = 11;
			this->txtRepeatPassword->TextChanged += gcnew System::EventHandler(this, &Program::txtRepeatPassword_TextChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(6, 97);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(117, 20);
			this->label2->TabIndex = 7;
			this->label2->Text = L"New Password:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(6, 151);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(139, 20);
			this->label3->TabIndex = 8;
			this->label3->Text = L"Repeat Password:";
			// 
			// txtOldPassword
			// 
			this->txtOldPassword->Location = System::Drawing::Point(164, 46);
			this->txtOldPassword->Name = L"txtOldPassword";
			this->txtOldPassword->Size = System::Drawing::Size(223, 26);
			this->txtOldPassword->TabIndex = 9;
			this->txtOldPassword->TextChanged += gcnew System::EventHandler(this, &Program::txtOldPassword_TextChanged);
			// 
			// toolStrip1
			// 
			this->toolStrip1->BackColor = System::Drawing::Color::DeepSkyBlue;
			this->toolStrip1->Font = (gcnew System::Drawing::Font(L"Impact", 14.25F, System::Drawing::FontStyle::Bold));
			this->toolStrip1->ImageScalingSize = System::Drawing::Size(60, 60);
			this->toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(6) {
				this->toolStripButton4,
					this->toolStripButton2, this->toolStripButton3, this->toolStripButton5, this->toolStripButton6, this->toolStripButton1
			});
			this->toolStrip1->Location = System::Drawing::Point(0, 0);
			this->toolStrip1->MinimumSize = System::Drawing::Size(0, 80);
			this->toolStrip1->Name = L"toolStrip1";
			this->toolStrip1->Size = System::Drawing::Size(1132, 80);
			this->toolStrip1->TabIndex = 1;
			this->toolStrip1->Text = L"toolStrip1";
			// 
			// toolStripButton4
			// 
			this->toolStripButton4->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->toolStripButton4->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButton4.Image")));
			this->toolStripButton4->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton4->Margin = System::Windows::Forms::Padding(30, 1, 0, 2);
			this->toolStripButton4->Name = L"toolStripButton4";
			this->toolStripButton4->Size = System::Drawing::Size(189, 77);
			this->toolStripButton4->Text = L"Reservations";
			this->toolStripButton4->Click += gcnew System::EventHandler(this, &Program::toolStripButton4_Click);
			// 
			// toolStripButton2
			// 
			this->toolStripButton2->Font = (gcnew System::Drawing::Font(L"Impact", 14.25F, System::Drawing::FontStyle::Bold));
			this->toolStripButton2->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->toolStripButton2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButton2.Image")));
			this->toolStripButton2->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton2->Margin = System::Windows::Forms::Padding(30, 1, 0, 2);
			this->toolStripButton2->Name = L"toolStripButton2";
			this->toolStripButton2->Size = System::Drawing::Size(137, 77);
			this->toolStripButton2->Text = L"Clients";
			this->toolStripButton2->Click += gcnew System::EventHandler(this, &Program::toolStripButton2_Click);
			// 
			// toolStripButton3
			// 
			this->toolStripButton3->Font = (gcnew System::Drawing::Font(L"Impact", 14.25F, System::Drawing::FontStyle::Bold));
			this->toolStripButton3->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->toolStripButton3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButton3.Image")));
			this->toolStripButton3->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton3->Margin = System::Windows::Forms::Padding(30, 1, 0, 2);
			this->toolStripButton3->Name = L"toolStripButton3";
			this->toolStripButton3->Size = System::Drawing::Size(169, 77);
			this->toolStripButton3->Text = L"Employees";
			this->toolStripButton3->Click += gcnew System::EventHandler(this, &Program::toolStripButton3_Click);
			// 
			// toolStripButton5
			// 
			this->toolStripButton5->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->toolStripButton5->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButton5.Image")));
			this->toolStripButton5->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton5->Margin = System::Windows::Forms::Padding(30, 1, 0, 2);
			this->toolStripButton5->Name = L"toolStripButton5";
			this->toolStripButton5->Size = System::Drawing::Size(149, 77);
			this->toolStripButton5->Text = L"Services";
			this->toolStripButton5->Click += gcnew System::EventHandler(this, &Program::toolStripButton5_Click);
			// 
			// toolStripButton6
			// 
			this->toolStripButton6->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->toolStripButton6->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButton6.Image")));
			this->toolStripButton6->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton6->Margin = System::Windows::Forms::Padding(30, 1, 0, 2);
			this->toolStripButton6->Name = L"toolStripButton6";
			this->toolStripButton6->Size = System::Drawing::Size(119, 77);
			this->toolStripButton6->Text = L"Work";
			this->toolStripButton6->Click += gcnew System::EventHandler(this, &Program::toolStripButton6_Click);
			// 
			// toolStripButton1
			// 
			this->toolStripButton1->Font = (gcnew System::Drawing::Font(L"Impact", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->toolStripButton1->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->toolStripButton1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButton1.Image")));
			this->toolStripButton1->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton1->Margin = System::Windows::Forms::Padding(30, 1, 0, 2);
			this->toolStripButton1->Name = L"toolStripButton1";
			this->toolStripButton1->Size = System::Drawing::Size(141, 77);
			this->toolStripButton1->Text = L"Options";
			this->toolStripButton1->Click += gcnew System::EventHandler(this, &Program::toolStripButton1_Click);
			// 
			// Program
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::DeepSkyBlue;
			this->ClientSize = System::Drawing::Size(1132, 819);
			this->Controls->Add(this->toolStrip1);
			this->Controls->Add(this->tabControl1);
			this->Name = L"Program";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Program";
			this->Load += gcnew System::EventHandler(this, &Program::Program_Load);
			this->tabControl1->ResumeLayout(false);
			this->tabPage6->ResumeLayout(false);
			this->tabPage6->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvREmployee))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvRClient))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvRService))->EndInit();
			this->tabPage5->ResumeLayout(false);
			this->tabPage5->PerformLayout();
			this->gbClientsData->ResumeLayout(false);
			this->gbClientsData->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvClients))->EndInit();
			this->tabPage4->ResumeLayout(false);
			this->tabPage4->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dtvESAddService))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvESServices))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvESEmployees))->EndInit();
			this->tabPage3->ResumeLayout(false);
			this->tabPage3->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgIServices))->EndInit();
			this->tabPage1->ResumeLayout(false);
			this->tabPage1->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dgvUser))->EndInit();
			this->tabPage2->ResumeLayout(false);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->toolStrip1->ResumeLayout(false);
			this->toolStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		//variable declaration
		int idRecord;
		int employee_type;
		int idService;
		int idClient;
		int idEmployee;
		int idVisit;
		String^ selectDate;
		String^ selectHour;

	private: System::Void Program_Load(System::Object^  sender, System::EventArgs^  e) {
	}
			 private: Void btnChangeShow()
			 {
				 if (txtOldPassword->Text != "" && txtNewPassword->Text != "" && txtRepeatPassword->Text ==txtNewPassword->Text)
				 {
					 btnChange->Enabled = true;
				 }
				 else
				 {
					 btnChange->Enabled = false;
				 }
			 }


	
private: System::Void txtOldPassword_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	btnChangeShow();
}
private: System::Void txtNewPassword_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	btnChangeShow();
}
private: System::Void txtRepeatPassword_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	btnChangeShow();
}
private: System::Void btnChange_Click(System::Object^  sender, System::EventArgs^  e) {
	
	MySqlConnection ^connectDB = gcnew MySqlConnection(config);
	MySqlCommand ^qwery = gcnew MySqlCommand("UPDATE user SET password=PASSWORD('"+txtNewPassword->Text+"') WHERE id_user="+userId+" AND password=PASSWORD('"+txtOldPassword->Text+"');", connectDB);

	try {
		connectDB->Open();
		if (qwery->ExecuteNonQuery())
		{
			MessageBox::Show("The password has been changed", "Information", MessageBoxButtons::OK, MessageBoxIcon::Asterisk);
		}
		else
		{
			MessageBox::Show("The password hasn't been changed", "Information", MessageBoxButtons::OK, MessageBoxIcon::Asterisk);
		}
		connectDB->Close();
	}
	catch (Exception ^e)
	{
		MessageBox::Show(e->Message, "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}

}

		 private: Void showGrid()
		 {
			 MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			 MySqlCommand ^qwery = gcnew MySqlCommand("SELECT id_user, username, first_name, last_name, employee FROM user ORDER BY last_name;", connectDB);

			 try {
				 connectDB->Open();

				 MySqlDataAdapter ^adapter = gcnew MySqlDataAdapter();
				 adapter->SelectCommand = qwery;
				 DataTable^ table = gcnew DataTable();
				 adapter->Fill(table);

				 BindingSource^ source = gcnew BindingSource();
				 source->DataSource = table;
				 dgvUser->DataSource = source;

				 connectDB->Close();
			 }
			 catch (Exception ^e)
			 {
				 MessageBox::Show(e->Message);
			 }
		 }

		private: Void clean(Control ^control)
		{
			for each(Control ^element in control->Controls)
			{
				if(element->GetType()==TextBox::typeid || element->GetType()==MaskedTextBox::typeid)
				element->Text = "";
			}
		}

		private: Void showEmployees(TextBox ^txt, DataGridView ^dgv)
		{
			MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			MySqlCommand ^qwery = gcnew MySqlCommand("SELECT id_user, username, first_name, last_name, employee FROM user WHERE CONCAT(username,' ', first_name,' ', last_name) LIKE '%" + txt->Text + "%' ORDER BY last_name;", connectDB);

			try {
				connectDB->Open();

				MySqlDataAdapter ^adapter = gcnew MySqlDataAdapter();
				adapter->SelectCommand = qwery;
				DataTable^ table = gcnew DataTable();
				adapter->Fill(table);

				BindingSource^ source = gcnew BindingSource();
				source->DataSource = table;
				dgv->DataSource = source;

				connectDB->Close();
			}
			catch (Exception ^e)
			{
				MessageBox::Show(e->Message);
			}
		}
private: System::Void btnSearch_Click(System::Object^  sender, System::EventArgs^  e) {
	showEmployees(txtSearch, dgvUser);
	dgvUser->Columns[0]->Visible = false;
}
private: System::Void dgvUser_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	if (e->RowIndex >= 0)
	{
		idRecord = Convert::ToInt32(dgvUser->Rows[e->RowIndex]->Cells[0]->Value);
		txtUsername->Text = dgvUser->Rows[e->RowIndex]->Cells[1]->Value->ToString();
		txtFirstName->Text = dgvUser->Rows[e->RowIndex]->Cells[2]->Value->ToString();
		txtLastName->Text = dgvUser->Rows[e->RowIndex]->Cells[3]->Value->ToString();
		chkEmployee->Checked = Convert::ToBoolean(dgvUser->Rows[e->RowIndex]->Cells["employee"]->Value);
		btnDelete->Enabled = true;
		btnUpdate->Enabled = true;
		if (chkEmployee->Checked)
		{
			MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			MySqlCommand ^qwery = gcnew MySqlCommand("SELECT * FROM hours WHERE user_id_user=" + idRecord + ";", connectDB);
			MySqlDataReader ^reader;
			try {
				connectDB->Open();

				reader = qwery->ExecuteReader();
				while (reader->Read())
				{
					txtE1b->Text = reader->GetString("monday_begin");
					txtE1e->Text = reader->GetString("monday_end");
					txtE2b->Text = reader->GetString("tuesday_begin");
					txtE2e->Text = reader->GetString("tuesday_end");
					txtE3b->Text = reader->GetString("wednesday_begin");
					txtE3e->Text = reader->GetString("wednesday_end");
					txtE4b->Text = reader->GetString("thursday_begin");
					txtE4e->Text = reader->GetString("thursday_end");
					txtE5b->Text = reader->GetString("friday_begin");
					txtE5e->Text = reader->GetString("friday_end");
					txtE6b->Text = reader->GetString("saturday_begin");
					txtE6e->Text = reader->GetString("saturday_end");
				}

				connectDB->Close();
			}
			catch (Exception ^e)
			{
				MessageBox::Show(e->Message);
			}
		}
	}
}
		 private: Void checkEmployee()
		 {
			 if (chkEmployee->Checked)
			 {
				 employee_type = 1;
			 }
			 else
			 {
				 employee_type = 0;
			 }
		 }
private: System::Void btnAdd_Click(System::Object^  sender, System::EventArgs^  e) {

	if (txtFirstName->Text->Length < 3 || txtLastName->Text->Length<3 || txtUsername->Text->Length<3)
	{
		MessageBox::Show("The first name/the last name/the username field is empty", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	else
	{
		if (MessageBox::Show("Do you want add a new user to emloyee table", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
		{
			checkEmployee();
			MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			MySqlCommand ^qwery = connectDB->CreateCommand();
			MySqlTransaction ^ transaction;
			connectDB->Open();

			transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);
			qwery->Connection = connectDB;
			qwery->Transaction = transaction;

			try {
				qwery->CommandText = "INSERT INTO user SET first_name='" + txtFirstName->Text + "', last_name='" + txtLastName->Text + "', username='" + txtUsername->Text + "', password=PASSWORD('" + txtUsername->Text + "'), employee=" + employee_type + ";";
				qwery->ExecuteNonQuery();

				if (chkEmployee->Checked)
				{
					qwery->CommandText = "INSERT INTO hours SET user_id_user=last_insert_id(), monday_begin='" + txtE1b->Text + "',monday_end='" + txtE1e->Text + "',tuesday_begin='" + txtE2b->Text + "', tuesday_end='" + txtE2e->Text + "',wednesday_begin='" + txtE3b->Text + "',wednesday_end='" + txtE3e->Text + "', thursday_begin='" + txtE4b->Text + "',thursday_end='" + txtE4e->Text + "',friday_begin='" + txtE5b->Text + "',friday_end='" + txtE5e->Text + "',saturday_begin='" + txtE6b->Text + "',saturday_end='" + txtE6e->Text + "';";
					qwery->ExecuteNonQuery();
				}

				transaction->Commit();
				MessageBox::Show("A new user has been added", "Add", MessageBoxButtons::OK, MessageBoxIcon::Information);
			}
			catch (Exception ^e)
			{
				MessageBox::Show(e->Message);
				transaction->Rollback();
			}
			connectDB->Close();
		}
	}
	showGrid();
}
private: System::Void btnUpdate_Click(System::Object^  sender, System::EventArgs^  e) {
	if (txtFirstName->Text->Length < 3 || txtLastName->Text->Length<3 || txtUsername->Text->Length<3)
	{
		MessageBox::Show("The first name/the last name/the username field is empty", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	else
	{
		checkEmployee();
		MySqlConnection ^connectDB = gcnew MySqlConnection(config);
		MySqlCommand ^qwery = connectDB->CreateCommand();
		MySqlTransaction ^ transaction;
		connectDB->Open();

		transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);
		qwery->Connection = connectDB;
		qwery->Transaction = transaction;

		try {
			if (MessageBox::Show("Do you want modify this user from emloyee table", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
			{
				qwery->CommandText = "SELECT * FROM hours WHERE user_id_user=" + idRecord + ";";
				MySqlDataReader ^reader = qwery->ExecuteReader();

				bool result = reader->HasRows;

				reader->Close();

				qwery->CommandText = "UPDATE user SET first_name='" + txtFirstName->Text + "', last_name='" + txtLastName->Text + "', username='"
					+ txtUsername->Text + "', employee=" + employee_type + " WHERE id_user=" + idRecord + ";";
				qwery->ExecuteNonQuery();

				if ((result == true) && (chkEmployee->Checked == true))
				{
					qwery->CommandText = "UPDATE hours SET monday_begin='" + txtE1b->Text + "',monday_end='" + txtE1e->Text + "', tuesday_begin='"
						+ txtE2b->Text + "', tuesday_end='" + txtE2e->Text + "', wednesday_begin='" + txtE3b->Text + "', wednesday_end='" + txtE3e->Text
						+ "', thursday_begin='" + txtE4b->Text + "',thursday_end='" + txtE4e->Text + "',friday_begin='" + txtE5b->Text + "',friday_end='"
						+ txtE5e->Text + "',saturday_begin='" + txtE6b->Text + "',saturday_end='" + txtE6e->Text + "' WHERE user_id_user=" + idRecord + ";";
					qwery->ExecuteNonQuery();
				}
				else if (result == false && chkEmployee->Checked)
				{
					qwery->CommandText = "INSERT INTO hours SET user_id_user=" + idRecord + ", monday_begin='" + txtE1b->Text + "',monday_end='" + txtE1e->Text
						+ "',tuesday_begin='" + txtE2b->Text + "', tuesday_end='" + txtE2e->Text + "',wednesday_begin='" + txtE3b->Text + "',wednesday_end='"
						+ txtE3e->Text + "', thursday_begin='" + txtE4b->Text + "',thursday_end='" + txtE4e->Text + "',friday_begin='" + txtE5b->Text
						+ "',friday_end='" + txtE5e->Text + "',saturday_begin='" + txtE6b->Text + "',saturday_end='" + txtE6e->Text + "';";
					qwery->ExecuteNonQuery();
				}

				transaction->Commit();
				MessageBox::Show("This user has been modified", "Modify", MessageBoxButtons::OK, MessageBoxIcon::Information);
			}
		}
		catch (Exception ^e)
		{
			MessageBox::Show(e->Message);
			transaction->Rollback();
		}
		connectDB->Close();
	}
	showGrid();
}
private: System::Void btnDelete_Click(System::Object^  sender, System::EventArgs^  e) {
	if (idRecord==1)
	{
		MessageBox::Show("You don't delete admin user", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	else
	{
		checkEmployee();
		MySqlConnection ^connectDB = gcnew MySqlConnection(config);
		MySqlCommand ^qwery = connectDB->CreateCommand();
		MySqlTransaction ^ transaction;
		connectDB->Open();

		transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);
		qwery->Connection = connectDB;
		qwery->Transaction = transaction;

		try {
			if (MessageBox::Show("Do you want delete this user from emloyee table", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
			{
				qwery->CommandText = "DELETE FROM hours WHERE user_id_user=" + idRecord + ";";
				qwery->ExecuteNonQuery();

				qwery->CommandText = "DELETE FROM user_services WHERE id_user=" + idRecord + ";";
				qwery->ExecuteNonQuery();

				qwery->CommandText = "DELETE FROM visits WHERE id_user=" + idRecord + ";";
				qwery->ExecuteNonQuery();

				qwery->CommandText = "DELETE FROM user WHERE id_user=" + idRecord + ";";
				qwery->ExecuteNonQuery();

				transaction->Commit();
				MessageBox::Show("The user has deleted");
			}
		}
		catch (Exception ^e)
		{
			MessageBox::Show(e->Message);
			transaction->Rollback();
		}
		connectDB->Close();
	}

	/*
	txtFirstName->Text = "";
	txtLastName->Text = "";
	txtUsername->Text = "";
	*/
	chkEmployee->Checked = false;
	clean(groupBox2);

	showGrid();
}
private: System::Void chkEmployee_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	if (chkEmployee->Checked)
	{
		groupBox3->Visible = true;
	}
	else
	{
		groupBox3->Visible = false;
	}
	clean(groupBox3);
}
		 private: Void templatesHours(String ^begin, String ^end)
		 {
			 array<TextBox^> ^timeBegin = { txtE1b,txtE2b ,txtE3b ,txtE4b ,txtE5b ,txtE6b };
			 array<TextBox^> ^timeEnd = { txtE1e,txtE2e ,txtE3e ,txtE4e ,txtE5e ,txtE6e };

			 for (int i = 0; i < 6; i++)
			 {
				 timeBegin[i]->Text = begin;
				 timeEnd[i]->Text = end;
			 }
		 }
private: System::Void btn7_Click(System::Object^  sender, System::EventArgs^  e) {
	templatesHours("7:00", "15:00");
}
private: System::Void btn8_Click(System::Object^  sender, System::EventArgs^  e) {
	templatesHours("8:00", "16:00");
}
private: System::Void btn9_Click(System::Object^  sender, System::EventArgs^  e) {
	templatesHours("9:00", "17:00");
}
private: System::Void btn10_Click(System::Object^  sender, System::EventArgs^  e) {
	templatesHours("10:00", "18:00");
}
		 private: Void showServices(System::Windows::Forms::TextBox ^txt, System::Windows::Forms::DataGridView ^ dgv)
		 {
			 MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			 MySqlCommand ^qwery = gcnew MySqlCommand("SELECT * FROM services WHERE name LIKE '%" + txt->Text + "%' ORDER BY name;", connectDB);

			 try {
				 connectDB->Open();

				 MySqlDataAdapter ^adapter = gcnew MySqlDataAdapter();
				 adapter->SelectCommand = qwery;
				 DataTable^ table = gcnew DataTable();
				 adapter->Fill(table);

				 BindingSource^ source = gcnew BindingSource();
				 source->DataSource = table;
				 dgv->DataSource = source;

				 connectDB->Close();
			 }
			 catch (Exception ^e)
			 {
				 MessageBox::Show(e->Message);
			 }
			 dgv->Columns[0]->Visible = false;
		 }
private: System::Void btnISearch_Click(System::Object^  sender, System::EventArgs^  e) {
	showServices(txtISearch, dgIServices);
}
private: System::Void btnIAdd_Click(System::Object^  sender, System::EventArgs^  e) {
	if (txtIName->Text->Length < 3 || txtIPrice->Text->Length<1 || txtITime->Text->Length<5)
	{
		MessageBox::Show("The name/The price/The time field is empty", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	else
	{
		if (MessageBox::Show("Do you want add a new service to services table?", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
		{
			checkEmployee();
			MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			MySqlCommand ^qwery = connectDB->CreateCommand();
			MySqlTransaction ^ transaction;
			connectDB->Open();

			transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);
			qwery->Connection = connectDB;
			qwery->Transaction = transaction;

			try {
				String ^price = txtIPrice->Text->Replace(",", ".");

				qwery->CommandText = "INSERT INTO services SET name='" + txtIName->Text + "', price='" + price + "', time='" + txtITime->Text + "', description='" + txtIDescription->Text + "';";
				qwery->ExecuteNonQuery();

				transaction->Commit();
				MessageBox::Show("A new service has been added", "Add", MessageBoxButtons::OK, MessageBoxIcon::Information);
			}
			catch (Exception ^e)
			{
				MessageBox::Show(e->Message);
				transaction->Rollback();
			}
			connectDB->Close();
		}
	}
	showServices(txtISearch, dgIServices);
}
private: System::Void dgIServices_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {

	if (e->RowIndex >= 0)
	{
		idRecord = Convert::ToInt32(dgIServices->Rows[e->RowIndex]->Cells[0]->Value);
		txtIName->Text = dgIServices->Rows[e->RowIndex]->Cells[1]->Value->ToString();
		txtIPrice->Text = dgIServices->Rows[e->RowIndex]->Cells[2]->Value->ToString();
		txtITime->Text = dgIServices->Rows[e->RowIndex]->Cells[3]->Value->ToString();
		txtIDescription->Text = dgIServices->Rows[e->RowIndex]->Cells[4]->Value->ToString();
		btnIUpdate->Enabled = true;
		btnIDelete->Enabled = true;
	}
}
private: System::Void btnIUpdate_Click(System::Object^  sender, System::EventArgs^  e) {
	if (txtIName->Text->Length < 3 || txtIPrice->Text->Length<1 || txtITime->Text->Length<5)
	{
		MessageBox::Show("The name/The price/The time field is empty", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	else
	{
		checkEmployee();
		MySqlConnection ^connectDB = gcnew MySqlConnection(config);
		MySqlCommand ^qwery = connectDB->CreateCommand();
		MySqlTransaction ^ transaction;
		connectDB->Open();

		transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);
		qwery->Connection = connectDB;
		qwery->Transaction = transaction;

		try {
			if (MessageBox::Show("Do you want modify this service from services table", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
			{
				String ^price = txtIPrice->Text->Replace(",", ".");

				qwery->CommandText = "UPDATE services SET name='" + txtIName->Text + "', price='" + price + "', time='"
					+ txtITime->Text + "', description='" + txtIDescription->Text + "' WHERE id_services=" + idRecord + ";";
				qwery->ExecuteNonQuery();

				transaction->Commit();
				MessageBox::Show("This service has been modified", "Modify", MessageBoxButtons::OK, MessageBoxIcon::Information);
			}
		}
		catch (Exception ^e)
		{
			MessageBox::Show(e->Message);
			transaction->Rollback();
		}
		connectDB->Close();
	}
	showServices(txtISearch, dgIServices);
}
private: System::Void btnIDelete_Click(System::Object^  sender, System::EventArgs^  e) {
	MySqlConnection ^connectDB = gcnew MySqlConnection(config);
	MySqlCommand ^qwery = connectDB->CreateCommand();
	MySqlTransaction ^ transaction;
	connectDB->Open();

	transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);
	qwery->Connection = connectDB;
	qwery->Transaction = transaction;

	try {
		if (MessageBox::Show("Do you want delete this service from services table", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
		{
			qwery->CommandText = "DELETE FROM user_services WHERE id_services=" + idRecord + ";";
			qwery->ExecuteNonQuery();

			qwery->CommandText = "DELETE FROM visits WHERE id_services=" + idRecord + ";";
			qwery->ExecuteNonQuery();

			qwery->CommandText = "DELETE FROM services WHERE id_services=" + idRecord + ";";
			qwery->ExecuteNonQuery();

			transaction->Commit();
			MessageBox::Show("The service has been deleted");
		}
	}
	catch (Exception ^e)
	{
		MessageBox::Show(e->Message);
		transaction->Rollback();
	}
	connectDB->Close();
	showServices(txtISearch, dgIServices);
	clean(groupBox5);
}
private: System::Void btnESSearch_Click(System::Object^  sender, System::EventArgs^  e) {
	showEmployees(txtESSearch, dgvESEmployees);
	dgvESEmployees->Columns[0]->Visible = false;
}
private: System::Void dgvESEmployees_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	if (e->RowIndex >= 0)
	{
		idRecord = Convert::ToInt32(dgvESEmployees->Rows[e->RowIndex]->Cells[0]->Value);
		txtESFirstName->Text = dgvESEmployees->Rows[e->RowIndex]->Cells[2]->Value->ToString();
		txtESLastName->Text = dgvESEmployees->Rows[e->RowIndex]->Cells[3]->Value->ToString();

		MySqlConnection ^connectDB = gcnew MySqlConnection(config);
		showEmployeeServices();
		MySqlCommand ^qwery = gcnew MySqlCommand("SELECT * FROM services ORDER BY name;", connectDB);

		try {
			connectDB->Open();

			MySqlDataAdapter ^adapter = gcnew MySqlDataAdapter();
			adapter->SelectCommand = qwery;
			DataTable^ table = gcnew DataTable();
			adapter->Fill(table);

			BindingSource^ source = gcnew BindingSource();
			source->DataSource = table;
			dtvESAddService->DataSource = source;

			connectDB->Close();
		}
		catch (Exception ^e)
		{
			MessageBox::Show(e->Message);
		}
	}
	dtvESAddService->Columns[0]->Visible = false;
}
		 private: Void showEmployeeServices()
		 {

			 MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			 MySqlCommand ^qwery = gcnew MySqlCommand("SELECT services.id_services, services.name, services.price, services.time FROM services, user_services WHERE services.id_services=user_services.id_services AND user_services.id_user=" + idRecord + " ORDER BY name;", connectDB);

			 try {
				 connectDB->Open();

				 MySqlDataAdapter ^adapter = gcnew MySqlDataAdapter();
				 adapter->SelectCommand = qwery;
				 DataTable^ table = gcnew DataTable();
				 adapter->Fill(table);

				 BindingSource^ source = gcnew BindingSource();
				 source->DataSource = table;
				 dgvESServices->DataSource = source;

				 connectDB->Close();
			 }
			 catch (Exception ^e)
			 {
				 MessageBox::Show(e->Message);
			 }
			 dgvESServices->Columns[0]->Visible = false;
		 }
private: System::Void dtvESAddService_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	if (e->RowIndex >= 0)
	{
		idService = Convert::ToInt32(dtvESAddService->Rows[e->RowIndex]->Cells[0]->Value);
		
		if (MessageBox::Show("Do you want add a new service in services table", "Add", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
		{
			MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			MySqlCommand ^qwery = connectDB->CreateCommand();
			MySqlTransaction ^ transaction;
			connectDB->Open();

			transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);
			qwery->Connection = connectDB;
			qwery->Transaction = transaction;

			try {
				qwery->CommandText = "INSERT INTO user_services SET id_user="+idRecord+", id_services="+idService+";";
				qwery->ExecuteNonQuery();

				transaction->Commit();
				MessageBox::Show("A new service has been added to the services performed by the employee", "Add", MessageBoxButtons::OK, MessageBoxIcon::Information);
			}
			catch (Exception ^e)
			{
				MessageBox::Show(e->Message);
				transaction->Rollback();
			}
			connectDB->Close();
		}
		showEmployeeServices();
	}
}
private: System::Void dgvESServices_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	if (e->RowIndex >= 0)
	{
		idService = Convert::ToInt32(dgvESServices->Rows[e->RowIndex]->Cells[0]->Value);

		if (MessageBox::Show("Do you want delete a service from services table", "Add", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
		{
			MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			MySqlCommand ^qwery = connectDB->CreateCommand();
			MySqlTransaction ^ transaction;
			connectDB->Open();

			transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);
			qwery->Connection = connectDB;
			qwery->Transaction = transaction;

			try {
				qwery->CommandText = "DELETE FROM user_services WHERE id_user=" + idRecord + " AND id_services=" + idService + ";";
				qwery->ExecuteNonQuery();

				transaction->Commit();
				MessageBox::Show("A new service has been added to the services performed by the employee", "Add", MessageBoxButtons::OK, MessageBoxIcon::Information);
			}
			catch (Exception ^e)
			{
				MessageBox::Show(e->Message);
				transaction->Rollback();
			}
			connectDB->Close();
		}
		showEmployeeServices();
	}
}
		 private: Void showClients(TextBox ^txt, DataGridView ^dgv)
		 {
			 MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			 MySqlCommand ^qwery = gcnew MySqlCommand("SELECT * FROM clients WHERE CONCAT(city,' ', first_name,' ', last_name) LIKE '%" + txt->Text + "%' ORDER BY last_name;", connectDB);

			 try {
				 connectDB->Open();

				 MySqlDataAdapter ^adapter = gcnew MySqlDataAdapter();
				 adapter->SelectCommand = qwery;
				 DataTable^ table = gcnew DataTable();
				 adapter->Fill(table);

				 BindingSource^ source = gcnew BindingSource();
				 source->DataSource = table;
				 dgv->DataSource = source;

				 connectDB->Close();
			 }
			 catch (Exception ^e)
			 {
				 MessageBox::Show(e->Message);
			 }
		 }
private: System::Void btnCSearch_Click(System::Object^  sender, System::EventArgs^  e) {
	showClients(txtCSearch, dgvClients);
	dgvClients->Columns[0]->Visible = false;
	dgvClients->Columns["modification"]->Visible = false;
}
private: System::Void dgvClients_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	if (e->RowIndex >= 0)
	{
		idClient = Convert::ToInt32(dgvClients->Rows[e->RowIndex]->Cells[0]->Value);
		txtCFirstName->Text = dgvClients->Rows[e->RowIndex]->Cells[1]->Value->ToString();
		txtCLastName->Text = dgvClients->Rows[e->RowIndex]->Cells[2]->Value->ToString();
		txtCEmail->Text = dgvClients->Rows[e->RowIndex]->Cells[3]->Value->ToString();
		txtCPhone->Text = dgvClients->Rows[e->RowIndex]->Cells[4]->Value->ToString();
		txtCStreet->Text = dgvClients->Rows[e->RowIndex]->Cells["street"]->Value->ToString();
		txtCNumber->Text = dgvClients->Rows[e->RowIndex]->Cells["number"]->Value->ToString();
		txtCCity->Text = dgvClients->Rows[e->RowIndex]->Cells["city"]->Value->ToString();
		mtxtCPCode->Text = dgvClients->Rows[e->RowIndex]->Cells["postal_code"]->Value->ToString();

		btnCDelete->Visible = true;
		btnCModify->Visible = true;
	}
}
private: System::Void btnCAdd_Click(System::Object^  sender, System::EventArgs^  e) {
	if (txtCFirstName->Text->Length < 3 || txtCLastName->Text->Length<3)
	{
		MessageBox::Show("The first name/The last name field is empty", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	else
	{
		if (MessageBox::Show("Do you want add a new client to clients table?", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
		{
			MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			MySqlCommand ^qwery = connectDB->CreateCommand();
			MySqlTransaction ^ transaction;
			connectDB->Open();

			transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);
			
			qwery->Connection = connectDB;
			qwery->Transaction = transaction;

			try {
				qwery->CommandText = "INSERT INTO clients SET first_name='" + txtCFirstName->Text + "', last_name='" + txtCLastName->Text + "', email='" + txtCEmail->Text + "', phone='" + txtCPhone->Text + "', street='"+txtCStreet->Text+"', number='"+txtCNumber->Text+"', city='"+txtCCity->Text+"', postal_code='"+mtxtCPCode->Text+"';";
				qwery->ExecuteNonQuery();

				transaction->Commit();
				MessageBox::Show("A new client: "+txtCFirstName->Text+" "+txtCLastName->Text+" has been added", "Add", MessageBoxButtons::OK, MessageBoxIcon::Information);
			}
			catch (Exception ^e)
			{
				MessageBox::Show(e->Message);
				transaction->Rollback();
			}
			connectDB->Close();
		}
	}
	showClients(txtCSearch, dgvClients);
}
private: System::Void btnCModify_Click(System::Object^  sender, System::EventArgs^  e) {
	if (txtCFirstName->Text->Length < 3 || txtCLastName->Text->Length<3)
	{
		MessageBox::Show("The first name/The last name field is empty", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	else
	{
		if (MessageBox::Show("Do you want modify the client in clients table?", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
		{
			MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			MySqlCommand ^qwery = connectDB->CreateCommand();
			MySqlTransaction ^ transaction;
			connectDB->Open();

			transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);

			qwery->Connection = connectDB;
			qwery->Transaction = transaction;

			try {
				qwery->CommandText = "UPDATE clients SET first_name='" + txtCFirstName->Text + "', last_name='" + txtCLastName->Text + "', email='" + txtCEmail->Text + "', phone='" + txtCPhone->Text + "', street='" + txtCStreet->Text + "', number='" + txtCNumber->Text + "', city='" + txtCCity->Text + "', postal_code='" + mtxtCPCode->Text + "' WHERE id_clients="+idClient+";";
				qwery->ExecuteNonQuery();

				transaction->Commit();
				MessageBox::Show("A client: " + txtCFirstName->Text + " " + txtCLastName->Text + " has been modified", "Add", MessageBoxButtons::OK, MessageBoxIcon::Information);
			}
			catch (Exception ^e)
			{
				MessageBox::Show(e->Message);
				transaction->Rollback();
			}
			connectDB->Close();
		}
	}
	showClients(txtCSearch, dgvClients);
}
private: System::Void btnCDelete_Click(System::Object^  sender, System::EventArgs^  e) {
	if (MessageBox::Show("Do you want delete the client from clients table?", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
	{
		MySqlConnection ^connectDB = gcnew MySqlConnection(config);
		MySqlCommand ^qwery = connectDB->CreateCommand();
		MySqlTransaction ^ transaction;
		connectDB->Open();

		transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);

		qwery->Connection = connectDB;
		qwery->Transaction = transaction;

		try {
			if (MessageBox::Show("You are sure that you want delete the client: "+txtCFirstName->Text+" "+txtCLastName->Text+" from clients table?", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
			{
				qwery->CommandText = "DELETE FROM visits WHERE id_clients=" + idClient + ";";
				qwery->ExecuteNonQuery();

				qwery->CommandText = "DELETE FROM clients WHERE id_clients=" + idClient + ";";
				qwery->ExecuteNonQuery();

				transaction->Commit();
				MessageBox::Show("A client: " + txtCFirstName->Text + " " + txtCLastName->Text + " has been deleted", "Add", MessageBoxButtons::OK, MessageBoxIcon::Information);

			}
		}
		catch (Exception ^e)
		{
			MessageBox::Show(e->Message);
			transaction->Rollback();
		}
		connectDB->Close();
	}

		 showClients(txtCSearch, dgvClients);
		 clean(gbClientsData);
}
private: System::Void btnRSearchService_Click(System::Object^  sender, System::EventArgs^  e) {
	showServices(txtRSearchService, dgvRService);
}
private: System::Void btnRSearchClient_Click(System::Object^  sender, System::EventArgs^  e) {
	showClients(txtRSearchClient, dgvRClient);
	dgvRClient->Columns[0]->Visible = false;
	dgvRClient->Columns["modification"]->Visible = false;
}
private: System::Void btnRSearchEmployee_Click(System::Object^  sender, System::EventArgs^  e) {
	showEmployees(txtRSearchEmployee, dgvREmployee);
	dgvREmployee->Columns[0]->Visible = false;
}
private: System::Void dgvRService_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	if (e->RowIndex >= 0)
	{
		idService = Convert::ToInt32(dgvRService->Rows[e->RowIndex]->Cells[0]->Value);
		txtRShowService->Text = dgvRService->Rows[e->RowIndex]->Cells["name"]->Value->ToString();

		//Show Employees, who are doing selected service
		MySqlConnection ^connectDB = gcnew MySqlConnection(config);
		MySqlCommand ^qwery = gcnew MySqlCommand("SELECT user.id_user, user.username, user.first_name, user.last_name, user.employee FROM user, user_services WHERE user.id_user=user_services.id_user AND user_services.id_services="+idService+" AND user.employee=1 ORDER BY last_name;", connectDB);

		try {
			connectDB->Open();

			MySqlDataAdapter ^adapter = gcnew MySqlDataAdapter();
			adapter->SelectCommand = qwery;
			DataTable^ table = gcnew DataTable();
			adapter->Fill(table);

			BindingSource^ source = gcnew BindingSource();
			source->DataSource = table;
			dgvREmployee->DataSource = source;

			connectDB->Close();
		}
		catch (Exception ^e)
		{
			MessageBox::Show(e->Message);
		}
		dgvREmployee->Columns[0]->Visible = false;
	}
}
private: System::Void dgvRClient_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	if (e->RowIndex >= 0)
	{
		idClient = Convert::ToInt32(dgvRClient->Rows[e->RowIndex]->Cells[0]->Value);
		txtRShowClient->Text = dgvRClient->Rows[e->RowIndex]->Cells[1]->Value->ToString() + " " +  dgvRClient->Rows[e->RowIndex]->Cells[2]->Value->ToString();
	}
}

		 private: void fieldClick(System::Object^  sender, System::EventArgs^  e)
		 {
			 TextBox^ field = safe_cast<TextBox^>(sender);
			 selectHour = field->Text;
			 txtRShowTerm->Text = selectDate + " " + selectHour;
			 idVisit = Convert::ToInt16(field->Tag);

			 MySqlConnection^ connectDB = gcnew MySqlConnection(config);
			 connectDB->Open();

			 MySqlCommand^ qwery = gcnew MySqlCommand("SELECT CONCAT(clients.first_name, ' ', clients.last_name) AS client, visits.id_clients , visits.id_user, visits.id_services, services.name FROM visits, services, clients WHERE visits.id_visits=" + idVisit + " AND clients.id_clients=visits.id_clients AND services.id_services=visits.id_services;", connectDB);
			 MySqlDataReader^ reader;

			 reader = qwery->ExecuteReader();
			 reader->Read();
			 if (reader->HasRows)
			 {
				 btnRDelete->Enabled = true;
				 btnRModify->Enabled = true;

				 txtRShowClient->Text = reader->GetString("client");
				 txtRShowService->Text = reader->GetString("name");

				 idService = reader->GetInt32("id_services");
				 idClient = reader->GetInt32("id_clients");
				 idEmployee = reader->GetInt32("id_user");
			 }
			 else
			 {
				 btnRDelete->Enabled = false;
				 btnRModify->Enabled = false;
			 }
			
		 }

				  private: void showTerm()
				  {
					  String^ time;
					  int count = 0;

					  String^ work_begin;
					  String^ work_end;
					  int h_start;
					  int h_stop;

					  String^ client = "";
					  String^ service = "";

					  DateTime^ dayOfWeek = Convert::ToDateTime(selectDate);
					  int day = Convert::ToInt32(dayOfWeek->DayOfWeek);

					  gbRHours->Controls->Clear();

					  if (idRecord >= 0)
					  {
						  idEmployee = Convert::ToInt32(dgvREmployee->Rows[idRecord]->Cells["id_user"]->Value);
					  }

					  switch (day)
					  {
					  case 1:
						  work_begin = "monday_begin";
						  work_end = "monday_end";
						  break;
					  case 2:
						  work_begin = "tuesday_begin";
						  work_end = "tuesday_end";
						  break;
					  case 3:
						  work_begin = "wednesday_begin";
						  work_end = "wednesday_end";
						  break;
					  case 4:
						  work_begin = "thursday_begin";
						  work_end = "thursday_end";
						  break;
					  case 5:
						  work_begin = "friday_begin";
						  work_end = "friday_end";
						  break;
					  case 6:
						  work_begin = "saturday_begin";
						  work_end = "saturday_end";
						  break;
					  case 0:
						  MessageBox::Show("A selected employee has free day!\nWe come back to monday");
						  work_begin = "monday_begin";
						  work_end = "monday_end";
						  break;
					  }
					  MySqlConnection^ connectDB = gcnew MySqlConnection(config);
					  connectDB->Open();

					  MySqlCommand^ qwery = gcnew MySqlCommand("SELECT date_format(" + work_begin + ", '%H') AS h_start, date_format(" + work_end + ", '%H') AS h_stop FROM hours WHERE user_id_user=" + idEmployee + ";", connectDB);
					  MySqlDataReader^ reader;

					  reader = qwery->ExecuteReader();
					  reader->Read();
					  if (reader->HasRows)
					  {
						  h_start = Convert::ToInt32(reader->GetInt32("h_start"));
						  h_stop = Convert::ToInt32(reader->GetInt32("h_stop"));
					  }

					  connectDB->Close();

					  for (int h = h_start; h < h_stop; h++)
					  {
						  for (int m = 0; m < 60; m += 30)
						  {
							  time = selectDate + " " + h + ":" + m + ":00";
							  DateTime date = Convert::ToDateTime(time);

							  connectDB->Open();

							  MySqlCommand^ qwery = gcnew MySqlCommand("SELECT date_format(visits.reservation_begin, '%H:%i') AS rB, date_format(visits.reservation_end, '%H:%i') AS rE, services.name, clients.last_name, visits.id_visits FROM visits, services, clients WHERE visits.reservation_begin='" + date + "' AND visits.id_user=" + idEmployee + " AND services.id_services=visits.id_services AND clients.id_clients=visits.id_clients;", connectDB);
							  MySqlDataReader^ reader;

							  reader = qwery->ExecuteReader();
							  reader->Read();

							  System::Windows::Forms::TextBox^ txtHours = gcnew System::Windows::Forms::TextBox();
							  gbRHours->Controls->Add(txtHours);
							  txtHours->Width = 300;

							  txtHours->BackColor = System::Drawing::Color::Green;
							  txtHours->BorderStyle = System::Windows::Forms::BorderStyle::None;
							  txtHours->ForeColor = System::Drawing::Color::White;
							  if (reader->HasRows) {
								  txtHours->BackColor = System::Drawing::Color::Red;
								  service = reader->GetString("name");
								  client = reader->GetString("last_name");
								  txtHours->Tag =reader->GetString("id_visits");
							  }
							  else {
								  service = "";
								  client = "";
							  }

							  txtHours->Text = String::Format(date.ToShortTimeString()) + " " + service + "" + client;
							  txtHours->Click += gcnew System::EventHandler(this, &Program::fieldClick);
							  txtHours->Location = System::Drawing::Point(10, 50 + 32 * count);
							  count++;

							  connectDB->Close();
						  }
					  }
				  }
private: System::Void dgvREmployee_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	monthCalendar1->Enabled = true;
	idRecord = e->RowIndex;
	idEmployee = idRecord;
	showTerm();
}
private: System::Void monthCalendar1_DateSelected(System::Object^  sender, System::Windows::Forms::DateRangeEventArgs^  e) {
	selectDate = String::Format(e->Start.ToShortDateString());
	txtRShowTerm->Text = selectDate;
	showTerm();
}
private: System::Void btnRAdd_Click(System::Object^  sender, System::EventArgs^  e) {
	if (idClient <= 0 || idService <= 0 || idEmployee <= 0 || txtRShowTerm->Text->Length<14)
	{
		MessageBox::Show("The first name/The last name field is empty", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	else
	{
		if (MessageBox::Show("Do you want add a new reservation to reservations table?", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
		{
			MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			MySqlCommand ^qwery = connectDB->CreateCommand();
			MySqlTransaction ^ transaction;
			connectDB->Open();

			transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);

			qwery->Connection = connectDB;
			qwery->Transaction = transaction;

			try {
				DateTime^ reservation_begin = Convert::ToDateTime(selectDate + " " + selectHour);

				qwery->CommandText = "INSERT INTO visits SET id_clients=" + idClient + ", id_services=" + idService + ", id_user=" + idEmployee + ", reservation_begin='" + reservation_begin + "',reservation_end='" + reservation_begin + "', status='waiting';";
				qwery->ExecuteNonQuery();

				transaction->Commit();
				MessageBox::Show("A new reservation for: " + txtRShowClient->Text + " has been added", "Add", MessageBoxButtons::OK, MessageBoxIcon::Information);
			}
			catch (Exception ^e)
			{
				MessageBox::Show(e->Message);
				transaction->Rollback();
			}
			connectDB->Close();
			showTerm();
		}
	}



}
private: System::Void btnRModify_Click(System::Object^  sender, System::EventArgs^  e) {
	
	if (idClient <= 0 || idService <= 0 || idEmployee <= 0 || txtRShowTerm->Text->Length<14)
	{
		MessageBox::Show("The first name/The last name field is empty", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	else
	{
		if (MessageBox::Show("Do you want modify the reservation from reservations table?", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
		{
			MySqlConnection ^connectDB = gcnew MySqlConnection(config);
			MySqlCommand ^qwery = connectDB->CreateCommand();
			MySqlTransaction ^ transaction;
			connectDB->Open();

			transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);

			qwery->Connection = connectDB;
			qwery->Transaction = transaction;

			try {
				qwery->CommandText = "UPDATE visits SET id_clients=" + idClient + ", id_services=" + idService + ", id_user=" + idEmployee + " WHERE id_visits=" + idVisit + ";";
				qwery->ExecuteNonQuery();

				transaction->Commit();
				MessageBox::Show("A new reservation for: " + txtRShowClient->Text + " has been modify", "Add", MessageBoxButtons::OK, MessageBoxIcon::Information);
			}
			catch (Exception ^e)
			{
				MessageBox::Show(e->Message);
				transaction->Rollback();
			}
			connectDB->Close();
			showTerm();
		}
	}
}
private: System::Void btnRDelete_Click(System::Object^  sender, System::EventArgs^  e) {
	if (MessageBox::Show("Do you want delete the term from reservation table?", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
	{
		MySqlConnection ^connectDB = gcnew MySqlConnection(config);
		MySqlCommand ^qwery = connectDB->CreateCommand();
		MySqlTransaction ^ transaction;
		connectDB->Open();

		transaction = connectDB->BeginTransaction(IsolationLevel::ReadCommitted);

		qwery->Connection = connectDB;
		qwery->Transaction = transaction;

		try {
			if (MessageBox::Show("You are sure that you want delete the term: " + txtCFirstName->Text + " " + txtCLastName->Text + " from reservation table?", "Attention", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
			{
				qwery->CommandText = "DELETE FROM visits WHERE id_visits=" + idVisit + ";";
				qwery->ExecuteNonQuery();

				transaction->Commit();
				MessageBox::Show("The reservation term has been deleted", "Add", MessageBoxButtons::OK, MessageBoxIcon::Information);

			}
		}
		catch (Exception ^e)
		{
			MessageBox::Show(e->Message);
			transaction->Rollback();
		}
		connectDB->Close();
		showTerm();
	}
}
private: System::Void toolStripButton4_Click(System::Object^  sender, System::EventArgs^  e) {
	this->tabControl1->SelectedTab = tabPage6;
}
private: System::Void toolStripButton2_Click(System::Object^  sender, System::EventArgs^  e) {
	this->tabControl1->SelectedTab = tabPage5;
}
private: System::Void toolStripButton3_Click(System::Object^  sender, System::EventArgs^  e) {
	this->tabControl1->SelectedTab = tabPage1;
}
private: System::Void toolStripButton5_Click(System::Object^  sender, System::EventArgs^  e) {
	this->tabControl1->SelectedTab = tabPage3;
}
private: System::Void toolStripButton6_Click(System::Object^  sender, System::EventArgs^  e) {
	this->tabControl1->SelectedTab = tabPage4;
}
private: System::Void toolStripButton1_Click(System::Object^  sender, System::EventArgs^  e) {
	this->tabControl1->SelectedTab = tabPage2;
}
};
}
