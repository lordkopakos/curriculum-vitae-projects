#include <iostream>


using namespace std;

struct Node {
	int rank;
	double x;
	double *y;

	Node() {

	}

	Node(double x, double y) {
		throw "TODO";
	}

	Node(double x, double *y, int rank) {
		this->x = x;
		this->rank = rank;

		this->y = new double[rank];
		for (int i = 0; i < rank; ++i)
		{
			this->y[i] = y[i];
		}
	}

	void init(double x, double *y, int rank) {
		this->x = x;
		this->rank = rank;

		this->y = new double[rank];
		for (int i = 0; i < rank; ++i)
		{
			this->y[i] = y[i];
		}
	}
};


int main()
{
	cout << "INTERPOLACJA HERMITE'A\n";
	int nodes;
	/////
	nodes = 2;
	/////
	int *ranks = new int[nodes];
	/////
	ranks[0] = 3;
	ranks[1] = 2;
	/////
	Node *data = new Node[nodes];

	double y0[] = { 1, 2, 0 };
	data[0].init(2, y0, 3);
	double y1[] = { 1, 2 };
	data[1].init(3, y1, 2);

	//cout << data[1].y[2];

	int size = 0;
	for (int i = 0; i < nodes; i++)
		size += data[i].rank;

	double **arr = new double *[size];
	for (int i = 0; i < size; ++i)
		arr[i] = new double[size + 1];

	for (int n = 0, j = 0; n < nodes; ++n)
	{
		for (int i = 0; i < data[n].rank; ++i, ++j) {
			arr[j][0] = data[n].x;
			arr[j][1] = data[n].y[0];
		}
	}


	for (int i = 0, n = 0, k = 0; i < size; ++i, ++k) {
		if (k == data[n].rank) {
			n++;
			k = 0;
		}
		for (int j = 2; j < i + 2; ++j) {
			int span = j - 1;
			double denom = arr[i][0] - arr[i - span][0];
			if (denom == 0) {
				// ten sam wezel w mianowniku
				arr[i][j] = data[n].y[j - 1];

				double factorial = 1;
				for (int f = 1; f < j; ++f) factorial *= f;

				arr[i][j] /= factorial;
			}
			else {
				// rozne wezly w mianowniku
				arr[i][j] = arr[i][j - 1] - arr[i - 1][j - 1];
				arr[i][j] /= denom;
			}
		}
	}


	// drukowanie
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < i + 2; ++j)
		{
			cout << arr[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;



	cout << "W(x) = " << arr[0][1];

	for (int i = 1; i < size; ++i) {
		cout << " " << showpos << arr[i][i + 1];
		for (int j = 0; j < i; ++j)
		{
			cout << noshowpos << "(x - " << arr[j][0] << ")";
		}
	}
	cout << endl;


	double x;
	cout << "Podaj x: ";
	cin >> x;

	double ret = 0;
	for (int i = 0; i < size; ++i)
	{
		double part = arr[i][i + 1];
		for (int j = 0; j < i; ++j)
		{
			part *= x - arr[j][0];
		}
		ret += part;
	}
	cout << "Wynik: " << ret << endl;


	system("pause");
	return 0;
}
