#include "GAME.h"


void GAME::choosehero()
{

	Mag M;
	P = &M;
	cout << "WYBOR BOHATERA" << endl << endl;
	cout << "1.RYCERZ" << endl;
	cout << "2.MAG" << endl;

	string opcja;
	cin >> opcja;

	if (opcja == "1")
	{
		Rycerz R;
		string imie;
		cout << "Podaj swoje imie RYCERZU" << endl;
		P = &R;
		cin >> imie;
		P->setNAME(imie);

	}
	else if (opcja == "2")
	{

		string imie;
		cout << "Podaj swoje imie CZARODZIEJU" << endl;
		P = &M;
		cin >> imie;
		P->setNAME(imie);
	}
	else throw(MyException("Zla komenda"));
	cout << &P << endl;

}

void GAME::newgame()
{
	Mag M("", 500, 24, 23);
	P = &M;
	cout << "WYBOR BOHATERA" << endl << endl;
	cout << "1.RYCERZ" << endl;
	cout << "2.MAG" << endl;

	string opcja;
	cin >> opcja;

	if (opcja == "1")
	{
		Rycerz R;
		string imie;
		cout << "Podaj swoje imie RYCERZU" << endl;
		P = &R;
		cin >> imie;
		P->setNAME(imie);

	}
	else if (opcja == "2")
	{

		string imie;
		cout << "Podaj swoje imie CZARODZIEJU" << endl;
		P = &M;
		cin >> imie;
		P->setNAME(imie);
	}
	else throw(MyException("Zla komenda"));

	cout << "WITAJ " << P->getNAME() << " Twoja podroz wlasnie sie rozpoczala" << endl;
	P->statystyki();
	cout << "Czy chcesz stoczyc walke z przeciwnikiem?" << endl;

	cout << "1.TAK" << endl;
	cout << "2.NIE" << endl;


	cin >> opcja;

	if (opcja == "1")
	{

		Enemy E(200, 20);
		P->statystyki();
		if (P->walka(E)) cout << "Zwyciezyles" << endl;
		else cout << "Przegrales" << endl;
		P->statystyki();
	}
	else if (opcja == "2")
	{
		Enemy E1(125, 51);
		Enemy E2(125, 51);
		cout << "ZOSTALES ZAATAKOWANY PRZEZ 2 PRZECIWNIKOW" << endl;
		if (P->walka(E1)) cout << "Zwyciezyles" << endl;
		else cout << "Przegrales" << endl;
		if (P->walka(E2)) cout << "Zwyciezyles" << endl;
		else cout << "Przegrales" << endl;

	}



	state = Menu;
}

void GAME::menu()
{
	cout << "MENU" << endl;
	cout << "1.NOWA GRA" << endl;
	cout << "2.ZAPISZ GRE" << endl;
	cout << "3.WCZYTAJ GRE" << endl;
	cout << "4.WYJDZ Z GRY" << endl;
	int opcja;
	cin >> opcja;

	switch (opcja)
	{
	case 1:
		state = Game;
		break;
	case 2:
		state = SaveGame;
		break;
	case 3:
		state = LoadGame;
		break;
	case 4:
		state = GameEnd;
		break;
	}
}

Postac* GAME::load()
{
	Mag M;
	P = &M;
	File F;
	bool Err_Flag = false;
	bool entry = 0;

	try {
		F.OdczytMag(P);
	}
	catch (MyException &ME)
	{
		cout << ME.what() << endl;
		Err_Flag = true;
	}
	catch (int)
	{
		cout << "Za d�uga nazwa pliku" << endl;
	}
	catch (string nazwap)
	{
		cout << "Niedozwolony znak" << endl;
		Err_Flag = true;
	}
	catch (bad_alloc)
	{
		cout << "Przepelnienie pamieci" << endl;
		Err_Flag = true;
	}
	P->statystyki();
	state = Menu;


	return P;
}

void GAME::save()
{

	File F;
	Mag M;
	P = &M;
	P->statystyki();
	bool Err_Flag = false;
	bool entry = 0;
	try {
		F.ZapisMag(P);
	}
	catch (MyException &ME)
	{
		cout << ME.what() << endl;
		Err_Flag = true;
	}
	catch (int)
	{
		cout << "Za d�uga nazwa pliku" << endl;
	}
	catch (string nazwap)
	{
		cout << "Niedozwolony znak" << endl;
		Err_Flag = true;
	}
	catch (bad_alloc)
	{
		cout << "Przepelnienie pamieci" << endl;
		Err_Flag = true;
	}
	state = Menu;
}

void GAME::runGame()
{

	while (state != GameEnd)
	{
		switch (state)
		{
		case StatGame::Menu:
			menu();
			break;
		case StatGame::Game:
			newgame();
			break;
		case StatGame::SaveGame:
			save();
			break;
		case StatGame::LoadGame:
			load();
			newgame();
			break;
		}
	}
}

GAME::GAME()
{
	state = Menu;

}


GAME::~GAME()
{
}
