package pkg3dproject.bsp2D;

import java.awt.geom.Point2D;
import java.awt.Rectangle;
import java.util.*;
import pkg3dproject.math3D.*;

/**
    Klasa BSPTreeBuilder buduje drzewo BSP na podstawie listy wielok�t�w.
    Wielok�ty musz� by� reprezentowane przez obiekty klasy BSPPolygon.

    Klasa nie pr�buje na razie optymalizowa� porz�dek podzia��w;
    mo�na wprowadzi� tak� optymalizacj� polegaj�c� na wybieraniu
    podzia��w w kolejno�ci umo�liwiaj�cej z minimalizowanie liczby
    dzielonych wielok�t�w i tworzenie bardziej zr�wnowa�onego,
    kompletnego drzewa.
*/
public class BSPTreeBuilder {

    /**
        Aktualnie budowane drzewo BSP.
    */
    protected BSPTree currentTree;

    /**
        Buduje drzewo BSP.
    */
    public BSPTree build(List polygons) {
        currentTree = new BSPTree(createNewNode(polygons));
        buildNode(currentTree.getRoot());
        return currentTree;
    }


    /**
        Buduje w�ze� w drzewie BSP.
    */
    protected void buildNode(BSPTree.Node node) {

        // je�li w�ze� jest li�ciem, ko�cz� jego budow�
        if (node instanceof BSPTree.Leaf) {
            return;
        }

        // klasyfikuje wszystkie wielok�ty zgodnie z podzia�em
        // (prz�d, ty� lub na linii podzia�u)
        ArrayList collinearList = new ArrayList();
        ArrayList frontList = new ArrayList();
        ArrayList backList = new ArrayList();
        List allPolygons = node.polygons;
        node.polygons = null;
        for (int i=0; i<allPolygons.size(); i++) {
            BSPPolygon poly = (BSPPolygon)allPolygons.get(i);
            int side = node.partition.getSide(poly);
            if (side == BSPLine.COLLINEAR) {
                collinearList.add(poly);
            }
            else if (side == BSPLine.FRONT) {
                frontList.add(poly);
            }
            else if (side == BSPLine.BACK) {
                backList.add(poly);
            }
            else if (side == BSPLine.SPANNING) {
                BSPPolygon front = clipBack(poly, node.partition);
                BSPPolygon back = clipFront(poly, node.partition);
                if (front != null) {
                    frontList.add(front);
                }
                if (back != null) {
                    backList.add(back);
                }

            }
        }

        // czy�ci i dostosowuje d�ugo�ci list
        collinearList.trimToSize();
        frontList.trimToSize();
        backList.trimToSize();
        node.polygons = collinearList;
        node.front = createNewNode(frontList);
        node.back = createNewNode(backList);

        // buduje przednie i tylne w�z�y
        buildNode(node.front);
        buildNode(node.back);
        if (node.back instanceof BSPTree.Leaf) {
            ((BSPTree.Leaf)node.back).isBack = true;
        }
    }



    /**
        Tworzy nowy w�ze� na podstawie listy wielok�t�w. Je�li �aden z
        wielok�t�w nie pe�ni roli �ciany, tworzony jest li��.
    */
    protected BSPTree.Node createNewNode(List polygons) {

        BSPLine partition = choosePartition(polygons);

        // brak dost�pnych podzia��w � w�ze� jest li�ciem
        if (partition == null) {
            BSPTree.Leaf leaf = new BSPTree.Leaf();
            leaf.polygons = polygons;
            buildLeaf(leaf);
            return leaf;
        }
        else {
            BSPTree.Node node = new BSPTree.Node();
            node.polygons = polygons;
            node.partition = partition;
            return node;
        }
    }


    /**
        Buduje li�� drzewa, oblicza kilka dodatkowych warto�ci, jak
        ograniczenia li�cia, wysoko�� pod�ogi i wysoko�� sufitu.
    */
    protected void buildLeaf(BSPTree.Leaf leaf) {

        if (leaf.polygons.size() == 0) {
            // li�� reprezentuje pust� przestrze�
            leaf.ceilHeight = Float.MAX_VALUE;
            leaf.floorHeight = Float.MIN_VALUE;
            leaf.bounds = null;
            return;
        }

        float minX = Float.MAX_VALUE;
        float maxX = Float.MIN_VALUE;
        float minY = Float.MAX_VALUE;
        float maxY = Float.MIN_VALUE;
        float minZ = Float.MAX_VALUE;
        float maxZ = Float.MIN_VALUE;

        // znajduje min y, maks y i ograniczenia
        Iterator i = leaf.polygons.iterator();
        while (i.hasNext()) {
            BSPPolygon poly = (BSPPolygon)i.next();
            for (int j=0; j<poly.getNumVertices(); j++) {
                Vector3D v = poly.getVertex(j);
                minX = Math.min(minX, v.x);
                maxX = Math.max(maxX, v.x);
                minY = Math.min(minY, v.y);
                maxY = Math.max(maxY, v.y);
                minZ = Math.min(minZ, v.z);
                maxZ = Math.max(maxZ, v.z);
            }
        }

        // znajduje dowoln� poziom� powierzchni�
        i = leaf.polygons.iterator();
        while (i.hasNext()) {
            BSPPolygon poly = (BSPPolygon)i.next();
            // je�li pod�oga
            if (poly.getNormal().y == 1) {
                float y = poly.getVertex(0).y;
                if (y > minY && y < maxY) {
                    minY = y;
                }
            }
        }

        // ustawia warto�ci reprezentowane w li�ciu
        leaf.ceilHeight = maxY;
        leaf.floorHeight = minY;
        leaf.bounds = new Rectangle(
            (int)Math.floor(minX), (int)Math.floor(minZ),
            (int)Math.ceil(maxX-minX+1),
            (int)Math.ceil(maxZ-minZ+1));
    }



    /**
        Z listy wielok�t�w wybiera lini�, kt�ra b�dzie pe�ni� rol� linii
        podzia�u. Metoda po prostu zwraca lini� stworzon� na podstawie
        pierwszego pionowego wielok�ty (�ciany) lub warto�� null, je�li
        na li�cie nie ma takich wielok�t�w. Mo�na w tym miejscu zastosowa�
        bardziej inteligentn� metod� wybieraj�c� lini� podzia�u w taki
        spos�b, by zminimalizowa� liczb� dzielonych wielok�t�w i
        doprowadzi� do stworzenia bardziej wywa�onego drzewa BSP.
    */
    protected BSPLine choosePartition(List polygons) {
        for (int i=0; i<polygons.size(); i++) {
            BSPPolygon poly = (BSPPolygon)polygons.get(i);
            if (poly.isWall()) {
                return new BSPLine(poly);
            }
        }
        return null;
    }


    /**
        Odcina cz�� wielok�ta znajduj�c� si� z przodu linii przekazanej
        w formie argumentu. Zwracany wielok�t jest jego cz�ci�
        znajduj�c� si� z ty�u linii podzia�u. Zwraca warto�� null, je�li
        dana linia nie przecina wielok�ta. Oryginalny wielok�t pozostaje
        niezmieniony.
    */
    protected BSPPolygon clipFront(BSPPolygon poly, BSPLine line) {
        return clip(poly, line, BSPLine.FRONT);
    }


    /**
        Odcina cz�� wielok�ta znajduj�c� si� z ty�u linii przekazanej
        w formie argumentu. Zwracany wielok�t jest jego cz�ci�
        znajduj�c� si� z przodu linii podzia�u. Zwraca warto�� null,
        je�li dana linia nie przecina wielok�ta. Oryginalny wielok�t
        pozostaje niezmieniony.
    */
    protected BSPPolygon clipBack(BSPPolygon poly, BSPLine line) {
        return clip(poly, line, BSPLine.BACK);
    }


    /**
        Przycina wielok�t BSPPolygon w taki spos�b, by jego cz��
        znajduj�ca si� z okre�lonej strony linii podzia�u BSPLine.FRONT
        lub BSPLine.BACK) zosta�a usuni�ta; zwraca przyci�ty wielok�t.
        Zwraca warto�� null, je�li dana linia nie przecina wielok�ta.
        Oryginalny wielok�t pozostaje niezmieniony.
    */
    protected BSPPolygon clip(BSPPolygon poly, BSPLine line, int clipSide)
    {
        ArrayList vertices = new ArrayList();
        BSPLine polyEdge = new BSPLine();

        // dodaje wierzcho�ki, kt�re nie znajduj� si� po tej stronie linii
        Point2D.Float intersection = new Point2D.Float();
        for (int i=0; i<poly.getNumVertices(); i++) {
            int next = (i+1) % poly.getNumVertices();
            Vector3D v1 = poly.getVertex(i);
            Vector3D v2 = poly.getVertex(next);
            int side1 = line.getSideThin(v1.x, v1.z);
            int side2 = line.getSideThin(v2.x, v2.z);
            if (side1 != clipSide) {
                vertices.add(v1);
            }

            if ((side1 == BSPLine.FRONT && side2 == BSPLine.BACK) ||
                (side2 == BSPLine.FRONT && side1 == BSPLine.BACK))
            {
                // upewnia si�, �e v1.z < v2.z
                if (v1.z > v2.z) {
                    Vector3D temp = v1;
                    v1 = v2;
                    v2 = temp;
                }
                polyEdge.setLine(v1.x, v1.z, v2.x, v2.z);
                float f = polyEdge.getIntersection(line);
                Vector3D tPoint = new Vector3D(
                    v1.x + f * (v2.x - v1.x),
                    v1.y + f * (v2.y - v1.y),
                    v1.z + f * (v2.z - v1.z));
                vertices.add(tPoint);
                // usuwa wszystkie stworzone T-z��cza
                removeTJunctions(v1, v2, tPoint);
            }

        }

        // Usuwa wszystkie pokrywaj�ce si� wierzcho�ki: (A->A) jest zamieniane na (A)
        for (int i=0; i<vertices.size(); i++) {
            Vector3D v = (Vector3D)vertices.get(i);
            Vector3D next = (Vector3D)vertices.get(
                (i+1) % vertices.size());
            if (v.equals(next)) {
                vertices.remove(i);
                i--;
            }
        }

        if (vertices.size() < 3) {
            return null;
        }

        // Tworzy wielok�t
        Vector3D[] array = new Vector3D[vertices.size()];
        vertices.toArray(array);
        return poly.clone(array);
    }



    /**
        Usu� z bie��cego drzewa wszystkie T-z��cza wzd�u� linii definiowanej
        za pomoc� pary (v1, v2). Znajd� wszystkie wielok�ty przylegaj�ce
        kraw�dziami do tej linii i wstaw pomi�dzy nimi punkty T-przeci�cia.
    */
    protected void removeTJunctions(final Vector3D v1, final Vector3D v2, final Vector3D tPoint)
    {
        BSPTreeTraverser traverser = new BSPTreeTraverser(
            new BSPTreeTraverseListener() {
                public boolean visitPolygon(BSPPolygon poly,
                    boolean isBackLeaf)
                {
                    removeTJunctions(poly, v1, v2, tPoint);
                    return true;
                }
            }
        );
        traverser.traverse(currentTree);
    }


    /**
        Usu� wszystkie T-z��cza z przekazanego wielok�ta. Punkt T-przeci�cia
        jest wstawiany pomi�dzy punktami v1 i v2, je�li pomi�dzy nimi nie
        istniej� �adne inne punkty.
    */
    protected void removeTJunctions(BSPPolygon poly, Vector3D v1, Vector3D v2, Vector3D tPoint)
    {
        for (int i=0; i<poly.getNumVertices(); i++) {
            int next = (i+1) % poly.getNumVertices();
            Vector3D p1 = poly.getVertex(i);
            Vector3D p2 = poly.getVertex(next);
            if ((p1.equals(v1) && p2.equals(v2)) ||
                (p1.equals(v2) && p2.equals(v1)))
            {
                poly.insertVertex(next, tPoint);
                return;
            }
        }
    }


}
