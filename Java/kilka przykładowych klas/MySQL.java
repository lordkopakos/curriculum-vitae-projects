
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.include.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
/**
 *
 * @author lordkopakos
 */
public class MySQL {
    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:user.db";
 
    private Connection conn;
    private Statement stat;
 
    public MySQL() {
        try {
            Class.forName(MySQL.DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
            e.printStackTrace();
        }
 
        try {
            conn = DriverManager.getConnection(DB_URL);
            stat = conn.createStatement();
        } catch (SQLException e) {
            System.err.println("Problem z otwarciem polaczenia");
            e.printStackTrace();
        }
 
        createTables();
    }
 
    public boolean createTables()  {
        
        String createUsers = "CREATE TABLE IF NOT EXISTS users (id_user INTEGER PRIMARY KEY AUTOINCREMENT, username varchar(255), password varchar(255), email varchar(255), fName varchar(255), lName varchar(255), country varchar(255), city varchar(255))";
        String createScores="CREATE TABLE IF NOT EXISTS scores (id_score INTEGER PRIMARY KEY AUTOINCREMENT, highScore int)";
        String createPlayerResults="CREATE TABLE IF NOT EXISTS playerResults (id_result INTEGER PRIMARY KEY AUTOINCREMENT, id_user int, id_score int)";
        
        String createChatRooms = "CREATE TABLE IF NOT EXISTS chatRooms (id_chatRoom INTEGER PRIMARY KEY AUTOINCREMENT, chatRoomName varchar(255), chatRoomPassword varchar(255),access varchar(255))";
        String createMessages="CREATE TABLE IF NOT EXISTS messages (id_message INTEGER PRIMARY KEY AUTOINCREMENT, massageContent varchar(255), id_user int)";
        String createConversations="CREATE TABLE IF NOT EXISTS conversations (id_conversation INTEGER PRIMARY KEY AUTOINCREMENT, id_chatRoom int, id_message int)";
        
        
        try {
           
            stat.execute(createUsers);
            stat.execute(createScores);
            stat.execute(createPlayerResults);
            
            stat.execute(createChatRooms);
            stat.execute(createMessages);
            stat.execute(createConversations);
            
            
        } catch (SQLException e) {
            System.err.println("Blad przy tworzeniu tabeli");
            e.printStackTrace();
            return false;
        }
        return true;
    }
 
    //Zarządzanie użytkownikami i ich punktami w grze
    /////////////////////////////////////////////////
    
    public boolean insertUser(String username,String password,String email, String fName, String lName,String country, String city) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into users values (NULL, ?, ?, ?, ?, ?, ?, ?);");
            prepStmt.setString(1, username);
            prepStmt.setString(2, password);
            prepStmt.setString(3, email);
            prepStmt.setString(4, fName);
            prepStmt.setString(5, lName);
            prepStmt.setString(6, country);
            prepStmt.setString(7, city);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("error insert user");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean insertScore(int highScore) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into scores values (NULL, ?);");
            prepStmt.setInt(1, highScore);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("error insert highScore");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean insertPlayerResult(int id_user, int id_score) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into playerResults values (NULL, ?, ?);");
            prepStmt.setInt(1, id_user);
            prepStmt.setInt(2, id_score);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("error insert playerResult");
            return false;
        }
        return true;
    }
    
 
    public List<User> selectUsers() {
        List<User> users = new LinkedList<>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM users");
            int id_user;
            String username, password;
            String email;
            String fName, lName, country, city;
            while(result.next()) {
                id_user = result.getInt("id_user");
                username = result.getString("username");
                password = result.getString("password");
                email = result.getString("email");
                fName = result.getString("fName");
                lName = result.getString("lName");
                country = result.getString("country");
                city = result.getString("city");
                users.add(new User(id_user, username, password, email, fName, lName, country, city));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return users;
    }
    public List<Score> selectScores() {
        List<Score> scores = new LinkedList<>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM scores");
            int id_score;
            int highScore;
            while(result.next()) {
                id_score = result.getInt("id_score");
                highScore = result.getInt("highScore");
                
                scores.add(new Score(id_score, highScore));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return scores;
    }
    
    public List<PlayerResult> selectPlayResults(){
        List<PlayerResult> playerResults=new LinkedList<>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM playerResults");
            int id_result;
            int id_user, id_score;
            while(result.next()) {
                id_result = result.getInt("id_result");
                id_user = result.getInt("id_user");
                id_score = result.getInt("id_score");
                
                playerResults.add(new PlayerResult(id_result, id_user, id_score));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return playerResults;
    }
    
    public User selectUser(String user) 
    {
        User returnUser= new User();
        try{
        ResultSet result = stat.executeQuery("SELECT * FROM users WHERE username LIKE" + "\"" + user + "\"");
        int id_user;
        String username, password;
        String email;
        String fName, lName, country, city;
        id_user = result.getInt("id_user");
        username = result.getString("username");
        password = result.getString("password");
        email=result.getString("email");
        fName=result.getString("fName");
        lName=result.getString("lName");
        country=result.getString("country");
        city=result.getString("city");
        returnUser.setId(id_user);
        returnUser.setUsername(username);
        returnUser.setPassword(password);
        returnUser.setEmail(email);
        returnUser.setFName(fName);
        returnUser.setLName(lName);
        returnUser.setCountry(country);
        returnUser.setCity(city);
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return returnUser; 
    }
    
    public String selectUsername(int id_user)
    {
        String username;
         try{
             if(id_user==-1)
             {
                 username="Player";
             }
             else{
        ResultSet result = stat.executeQuery("SELECT * FROM users WHERE id_user LIKE" + "\"" + id_user + "\"");
        
        username = result.getString("username");
             }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            return null;
        }
         return username;
    }
    public String selectCounntry(int id_user)
    {
        String country;
         try{
             if(id_user==-1)
             {
                 country="Unknown";
             }
             else{
        ResultSet result = stat.executeQuery("SELECT * FROM users WHERE id_user LIKE" + "\"" + id_user + "\"");
        
        country = result.getString("country");
             }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            return null;
        }
         return country;
    }
    public String selectScoreForScoreboard(int id_score)
    {
        
        int score;
        
         try{
        ResultSet result = stat.executeQuery("SELECT * FROM scores WHERE id_score LIKE" + "\"" + id_score + "\"");
        
        score = result.getInt("highScore");
        
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            return null;
        }
         String returnString=Integer.toString(score);
         return returnString;
    }
    
    public Score selectScore(int highScore)
    {
        Score returnScore=new Score();
        try{
        ResultSet result = stat.executeQuery("SELECT * FROM scores WHERE highScore LIKE" + "\"" + highScore + "\"");
            System.out.println(result.isAfterLast());
            
        if(!result.isAfterLast())
        {
        int id_score;
        int score;
        id_score = result.getInt("id_score");
        score = result.getInt("highScore");
 
        
        returnScore.setId(id_score);
        returnScore.setHighScore(score);
        }
        else
        {
            returnScore.setId(-1);
            returnScore.setHighScore(-1);
        }
        }
        catch(SQLException e)
        {
            
            e.printStackTrace();
            return null;
        }
        
        return returnScore;
        
    }
    public PlayerResult selectPlayerResult(int id_u) 
    {
        User returnUser= new User();
        PlayerResult returnPlayerResult = new PlayerResult();
        try{
        ResultSet result = stat.executeQuery("SELECT * FROM users WHERE username LIKE" + "\"" + id_u + "\"");
        
        int id_result;
        int id_user, id_score;
        
        id_result=result.getInt("id_result");
        id_score=result.getInt("id_score");
        id_user = result.getInt("id_user");
        
        returnPlayerResult.setId(id_result);
        returnPlayerResult.setIdScore(id_score);
        returnPlayerResult.setIdUser(id_user);
        
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return returnPlayerResult; 
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
    
    
    //Zarządzanie ChatRoomem Konwersacjami i bazą wysyłanych wiadomości
    public boolean insertChatRoom(String chatRoomName, String chatRoomPassword, String access) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into chatRooms values (NULL, ?, ?, ?);");
            prepStmt.setString(1, chatRoomName);
            prepStmt.setString(2, chatRoomPassword);
            prepStmt.setString(3, access);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("error insert chatRoom");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean insertMessage(String massageContent, int id_user) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into messages values (NULL, ?, ?);");
            prepStmt.setString(1, massageContent);
            prepStmt.setInt(2, id_user);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("error insert message");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean insertConversation(int id_chatRoom, int id_message) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into conversations values (NULL, ?, ?);");
            prepStmt.setInt(1, id_chatRoom);
            prepStmt.setInt(2, id_message);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("error insert conversation");
            return false;
        }
        return true;
    }
    
 
    public List<ChatRoom> selectChatRooms() {
        List<ChatRoom> chatRooms = new LinkedList<>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM chatRooms");
            int id_chatRoom;
            String chatRoomName, chatRoomPassword, access;
            while(result.next()) {
                id_chatRoom = result.getInt("id_chatRoom");
                chatRoomName = result.getString("chatRoomName");
                chatRoomPassword = result.getString("chatRoomPassword");
                access = result.getString("access");
                chatRooms.add(new ChatRoom(id_chatRoom, chatRoomName, chatRoomPassword,access));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return chatRooms;
    }
    public ChatRoom selectChatRoom(String Name) 
    {
        ChatRoom returnChatRoom= new ChatRoom();
        try{
        ResultSet result = stat.executeQuery("SELECT * FROM chatRooms WHERE chatRoomName LIKE" + "\"" + Name + "\"");
        int id_chatRoom;
            String chatRoomName, chatRoomPassword, access;
        id_chatRoom = result.getInt("id_chatRoom");
        chatRoomName = result.getString("chatRoomName");
        chatRoomPassword = result.getString("chatRoomPassword");
        access=result.getString("access");
        
        returnChatRoom.setId(id_chatRoom);
        returnChatRoom.setChatRoomName(chatRoomName);
        returnChatRoom.setChatRoomPassword(chatRoomPassword);
        returnChatRoom.setAccess(access);
        
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return returnChatRoom; 
    }
    
    public Message selectMessage(String Content) 
    {
        Message returnMessage= new Message();
        try{
        ResultSet result = stat.executeQuery("SELECT * FROM messages WHERE massageContent LIKE" + "\"" + Content + "\"");
        int id_message;
            String massageContent;
            int id_user;
                id_message = result.getInt("id_message");
                massageContent = result.getString("massageContent");
                id_user = result.getInt("id_user");
        
        returnMessage.setId(id_message);
        returnMessage.setMassageContent(massageContent);
        returnMessage.setIdUser(id_user);
        
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return returnMessage; 
    }
    public List<Message> selectMessages() {
        List<Message> messages = new LinkedList<>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM messages");
            int id_message;
            String massageContent;
            int id_user;
            while(result.next()) {
                id_message = result.getInt("id_message");
                massageContent = result.getString("massageContent");
                id_user = result.getInt("id_user");
                
                messages.add(new Message(id_message, massageContent, id_user));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return messages;
    }
    
    public List<Conversation> selectConversations(){
        List<Conversation> conversations=new LinkedList<>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM conversations");
            int id_conversation;
            int id_chatRoom, id_message;
            while(result.next()) {
                id_conversation = result.getInt("id_conversation");
                id_chatRoom = result.getInt("id_chatRoom");
                id_message = result.getInt("id_message");
                
                conversations.add(new Conversation(id_conversation, id_chatRoom, id_message));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return conversations;
    }
    
    
 
    public void closeConnection() {
        try {
            conn.close();
        } catch (SQLException e) {
            System.err.println("Problem z zamknieciem polaczenia");
            e.printStackTrace();
        }
    }
}

