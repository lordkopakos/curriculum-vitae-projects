/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.include.sql;



/**
 *
 * @author lordkopakos
 */
public class User {
    private int id;
    private String username;
    private String password;
    private String email;
    private String fName;
    private String lName;
    private String country;
    private String city;
    private String sname;
    
    public int getId() {
        return id;
    }
    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }
    public String getEmail() {
        return email;
    }
    public String getFName() {
        return fName;
    }
    public String getLName() {
        return lName;
    }
    public String getCountry() {
        return country;
    }
    public String getCity() {
        return city;
    }
    public String getsName() {
        return city;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setFName(String fName) {
        this.fName = fName;
    }
    
    public void setLName(String lName) {
        this.lName = lName;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    

    public User() {
        
        this.id = -1;
        this.username = "Player";
        this.password = "";
        this.email = "unknown";
        this.fName = "unknown";
        this.lName = "unknown";
        this.country = "unknown";
        this.city = "unknown";
        
    }
    public User(int id, String username, String password, String email, String fName, String lName, String country, String city) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.fName = fName;
        this.lName = lName;
        this.country = country;
        this.city = city;
        
        
    }
 
    @Override
    public String toString() {
        return "["+id+"] - "+username+" "+password+" "+email+" "+fName+" "+lName+" "+country+" "+city;
    }
}
