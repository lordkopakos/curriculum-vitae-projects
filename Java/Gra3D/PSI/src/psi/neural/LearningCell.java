/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.neural;

import java.util.ArrayList;
import psi.network.VectorX;

/**
 *
 * @author lordkopakos
 */
public class LearningCell extends AdelineCell implements UnsupervisedLearning{
    
    private double lambdaCoefficient;
    private double thresholdValue;
    private double oldY;
    
    public LearningCell() {
        this(0.001,2,2);
    }

    public LearningCell(double lambdaCoefficient, double thresholdValue,int eras) {
        super(Function.BIPOLAR_THRESHOLD_FUNCTION, 1.0, 0.1);
        this.lambdaCoefficient = lambdaCoefficient;
        this.thresholdValue = thresholdValue;
    }
    
    
    
    //UNSUPERVISED LEARNING////////////////////
    ///////////////////////////////////////////
    @Override
    public double HebbRule(VectorX v) {
            double y = getOutput();
            for (int i = 0; i < Synapses.size(); i++) {
                setInputWeight(i, getInputWeight(i) + getLearningCoefficient() * v.getCoordinates(i) * y);
            }
            return getOutput();
        
    }

    @Override
    public double expandedHebbRule(VectorX v) {
        double y=getOutput();
            for (int i = 0; i < v.getLength(); i++) {
                if(i==0){
                    setInputWeight(i, getInputWeight(i) + getLearningCoefficient() * v.getCoordinates(i) * y);
                    oldY=y;
                }
                else{
                    setInputWeight(i, getInputWeight(i) + getLearningCoefficient() * ((v.getCoordinates(i)-v.getCoordinates(i-1))*(v.getCoordinates(i)-oldY)));
                    oldY=y;
                }
            }
            oldY=y;
            return getOutput();
        
    }

    @Override
    public double OjiRule(VectorX v) {
            double y = getOutput();
            for (int i = 0; i < v.getLength(); i++) {
                setInputWeight(i, getInputWeight(i)+getLearningCoefficient()*y*(v.getCoordinates(i)-y*getInputWeight(i)));
            }
        return y;
    }

    @Override
    public double instarRuleGrossberg(VectorX v) {
        setLearningCoefficient(0.1);
        setLambdaCoefficient(0.0001);
            double y = getOutput();
            for (int i = 0; i < v.getLength(); i++) {
                setLambdaCoefficient(getLambdaCoefficient()*2);
                setLearningCoefficient(getLearningCoefficient()+getLambdaCoefficient());
                setInputWeight(i, getInputWeight(i)+getLearningCoefficient()*(v.getCoordinates(i)-getInputWeight(i)));
            }
            return y;
    }

    @Override
    public double outstarRuleGrossberg(VectorX v) {
        setLearningCoefficient(0.1);
        setLambdaCoefficient(0.0001);
            double y = getOutput();
            for (int i = 0; i < v.getLength(); i++) {
                setLambdaCoefficient(getLambdaCoefficient()*2);
                setLearningCoefficient(getLearningCoefficient()+getLambdaCoefficient());
                setInputWeight(i, getInputWeight(i)+getLearningCoefficient()*(y-getInputWeight(i)));
            }
        return getOutput();
    }

    @Override
    public double antiHebbRule(VectorX v) {
        double y= getOutput();
            for (int i = 0; i < v.getLength(); i++) {
                if(i==0){
                    setInputWeight(i, getInputWeight(i) + getLearningCoefficient() * getNewValueWithUseThreshold(v.getCoordinates(i)) * getNewValueWithUseThreshold(y));
                }
                else{
                    setInputWeight(i, getInputWeight(i) + getLearningCoefficient() * ((2*getNewValueWithUseThreshold(v.getCoordinates(i-1)))*(2*getNewValueWithUseThreshold(oldY))));
                }
            }
            oldY=y;
        return getOutput();
    }

    @Override
    public double getNewValueWithUseThreshold(double inValue) {
        if(inValue>thresholdValue){
            inValue=1;
            return  inValue;
        }
        else{
            inValue=0;
            return inValue;
        }
    }
    
    public double getLambdaCoefficient(){
        return this.lambdaCoefficient;
    }
    
    public void setLambdaCoefficient(double lambdaCoefficient){
        this.lambdaCoefficient=lambdaCoefficient;
    }

}
