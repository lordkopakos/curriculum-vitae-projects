package pkg3dproject.bsp2D;

import java.awt.Rectangle;
import java.awt.Point;
import java.util.List;
import pkg3dproject.math3D.*;
import pkg3dproject.graphics3D.texture.*;

/**
    Klasa BSPTree reprezentuje dwuwymiarowe drzewo dw�jkowego podzia�u
    wielok�t�w. Obiekt klasy BSPTree jest budowany z wykorzystaniem
    klasy BSPTreeBuilder i mo�e by� przegl�dany za pomoc� klasy
    BSPTreeTraverser.
*/
public class BSPTree {

    /**
        Klasa Node reprezentuje w�ze� drzewa. Wszystkie w�z�y potomne
        w�z�a znajduj� si� albo z przodu albo z ty�u podzia�u wprowadzanego
        przez w�ze�.
    */
    public static class Node {
        public Node front;
        public Node back;
        public BSPLine partition;
        public List polygons;
    }


    /**
        Klasa Leaf reprezentuje li�� drzewa. Li�� nie wprowadza podzia�u,
        nie zawiera wi�c w�z��w potomnych ani z przodu, ani z ty�u.
    */
    public static class Leaf extends Node {
        public float floorHeight;
        public float ceilHeight;
        public boolean isBack;
        public List portals;
        public Rectangle bounds;
    }

    private Node root;

    /**
        Tworzy nowy obiekt klasy BSPTree z okre�lonym korzeniem.
    */
    public BSPTree(Node root) {
       this.root = root;
    }


    /**
        Zwraca korze� drzewa.
    */
    public Node getRoot() {
        return root;
    }


    /**
        Oblicza dwuwymiarowy zakres dla wszystkich wielok�t�w w tym
        drzewie BSP. Zwraca prostok�t reprezentuj�cy ten zakres.
    */
    public Rectangle calcBounds() {

        final Point min =
            new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
        final Point max =
            new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);

        BSPTreeTraverser traverser = new BSPTreeTraverser();
        traverser.setListener(new BSPTreeTraverseListener() {

            public boolean visitPolygon(BSPPolygon poly,
                boolean isBack)
            {
                for (int i=0; i<poly.getNumVertices(); i++) {
                    Vector3D v = poly.getVertex(i);
                    int x = (int)Math.floor(v.x);
                    int y = (int)Math.floor(v.z);
                    min.x = Math.min(min.x, x);
                    max.x = Math.max(max.x, x);
                    min.y = Math.min(min.y, y);
                    max.y = Math.max(max.y, y);
                }

                return true;
            }
        });

        traverser.traverse(this);

        return new Rectangle(min.x, min.y,
            max.x - min.x, max.y - min.y);
    }


    /**
        Zwraca li��, do kt�rego nale�y punkt o wskazanych wsp�rz�dnych x, z.
    */
    public Leaf getLeaf(float x, float z) {
        return getLeaf(root, x, z);
    }


    protected Leaf getLeaf(Node node, float x, float z) {
        if (node == null || node instanceof Leaf) {
            return (Leaf)node;
        }
        int side = node.partition.getSideThin(x, z);
        if (side == BSPLine.BACK) {
            return getLeaf(node.back, x, z);
        }
        else {
            return getLeaf(node.front, x, z);
        }
    }


    /**
        Zwraca w�ze� pokrywaj�cy si� z przekazan� lini� podzia�u
        lub null, je�li taki w�ze� nie istnieje.
    */
    public Node getCollinearNode(BSPLine partition) {
        return getCollinearNode(root, partition);
    }


    protected Node getCollinearNode(Node node, BSPLine partition) {
        if (node == null || node instanceof Leaf) {
            return null;
        }
        int side = node.partition.getSide(partition);
        if (side == BSPLine.COLLINEAR) {
            return node;
        }
        if (side == BSPLine.FRONT) {
            return getCollinearNode(node.front, partition);
        }
        else if (side == BSPLine.BACK) {
            return getCollinearNode(node.back, partition);
        }
        else {
            // BSPLine.SPANNING: najpierw sprawd� z przodu, potem z ty�u
            Node front = getCollinearNode(node.front, partition);
            if (front != null) {
                return front;
            }
            else {
                return getCollinearNode(node.back, partition);
            }
        }
    }


    /**
        Zwraca li�� z przodu przekazanego podzia�u.
    */
    public Leaf getFrontLeaf(BSPLine partition) {
        return getLeaf(root, partition, BSPLine.FRONT);
    }


    /**
        Zwraca li�� z ty�u przekazanego podzia�u.
    */
    public Leaf getBackLeaf(BSPLine partition) {
        return getLeaf(root, partition, BSPLine.BACK);
    }


    protected Leaf getLeaf(Node node, BSPLine partition, int side)
    {
        if (node == null || node instanceof Leaf) {
            return (Leaf)node;
        }
        int segSide = node.partition.getSide(partition);
        if (segSide == BSPLine.COLLINEAR) {
            segSide = side;
        }
        if (segSide == BSPLine.FRONT) {
            return getLeaf(node.front, partition, side);
        }
        else if (segSide == BSPLine.BACK) {
            return getLeaf(node.back, partition, side);
        }
        else { // BSPLine.SPANNING
            // nie powinno si� zdarzy�
            return null;
        }
    }


    /**
        Tworzy tekstury na powierzni wszystkich wielok�t�w reprezentowanych w drzewie BSP.
    */
    public void createSurfaces(final List lights) {
        BSPTreeTraverser traverser = new BSPTreeTraverser();
        traverser.setListener(new BSPTreeTraverseListener() {

            public boolean visitPolygon(BSPPolygon poly,
                boolean isBack)
            {
                Texture texture = poly.getTexture();
                if (texture instanceof ShadedTexture) {
                    ShadedSurface.createShadedSurface(poly,
                        (ShadedTexture)texture,
                        poly.getTextureBounds(), lights,
                        poly.getAmbientLightIntensity());
                }
                return true;
            }
        });

        traverser.traverse(this);
    }

}
