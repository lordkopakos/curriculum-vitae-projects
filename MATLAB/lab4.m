dane = xlsread('oscillations');
t = dane(:, 1);
y = dane(:, 2);


%plot(t, y, 'ro')
%hold on

%plot(mz, wz, 'bs')
%xlabel('czas')
%ylabel('y')
%title('Wykres')
%legend('sinus','miejsca zerowe')

%figure
plot(t, y, 'ro')
hold on

[xw, yw]=wierzcholki(t,y)
plot(t, y, 'ro')
plot(xw, yw, 'bs')

f=1/(mz(3)-mz(1))
d=log(yw(1)/yw(2))

yt = 4.5*sin(2*pi*f*t).*exp(-d*f*t)
hold on
plot(t, yt, 'y-')

wiel=spline(xw,yw);
xx=linspace(xw(1), xw(end), length(t));
yy=ppval(wiel, xx);

hold on
plot(xx,yy,'b.')

yylog=log(yy);
p=polyfit(xx,yylog,1);
yprosta=p(1)*xx+p(2);
det=-p(1)/f

hold on
plot(xx,yylog,'g.')

hold on
plot(xx,yprosta,'k--')