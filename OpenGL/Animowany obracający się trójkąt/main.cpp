#include<GL/glew.h>
#include<GLFW/glfw3.h>

#include<glm.hpp>
#include<gtc/matrix_transform.hpp>
#include<gtc\type_ptr.hpp>

#include"FreeImage.h"

#include<cmath>
#include<iostream>
#include<fstream>
#include<string>

/////////////////////
int window_width;
int window_height;
GLFWwindow *window;
GLuint shaders1;
GLuint shaders2;
GLuint shaders3;
/////////////////////

GLint LoadShaders(std::string vertex_shader, std::string fragment_shader)
{
	GLuint vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);
	std::string vertex_shader_data;
	std::ifstream vertex_shader_file(vertex_shader.c_str(), std::ios::in);
	if (vertex_shader_file.is_open())
	{
		std::string line;
		while (std::getline(vertex_shader_file, line))
			vertex_shader_data += "\n" + line;
		
		vertex_shader_file.close();
	}
	//std::cout << vertex_shader_data;
	std::string fragment_shader_data;
	std::ifstream fragment_shader_file(fragment_shader.c_str(), std::ios::in);
	if (fragment_shader_file.is_open())
	{
		std::string line;
		while (std::getline(fragment_shader_file, line))
			fragment_shader_data += "\n" + line;

		fragment_shader_file.close();
	}
	//std::cout << fragment_shader_data;

	const char* vertex_ptr = vertex_shader_data.c_str();
	const char* fragment_ptr = fragment_shader_data.c_str();
	glShaderSource(vertex_shader_id, 1, &vertex_ptr, NULL);
	glShaderSource(fragment_shader_id, 1, &fragment_ptr, NULL);
	
	//Kompilacja shader�w
	glCompileShader(vertex_shader_id);
	glCompileShader(fragment_shader_id);

	//Sprawdzanie czy kompilacja zako�czy�a si� sukcesem
	std::cout << "VERTEX SHADER STATUS" ;
	int vertex_shader_status = -1;
	
	const int vertex_max_length = 2048;
	int vertex_length = 0;
	char vertex_log_text[vertex_max_length];
	glGetShaderInfoLog(vertex_shader_id, vertex_max_length, &vertex_length, vertex_log_text);
	std::cout << vertex_log_text << std::endl;
	
	glGetShaderiv(vertex_shader_id, GL_COMPILE_STATUS, &vertex_shader_status);
	if (vertex_shader_status != GL_TRUE)
	{
		std::cout << "Vertex shader \"" << vertex_shader << "\"compilate ERROR" << std::endl;
		return -1;
	}
	else
	{
		std::cout << "Vertex shader \"" << vertex_shader << "\"compilate SUCCESS" << std::endl;
	}

	std::cout << "FRAGMENT SHADER STATUS";
	int fragment_shader_status = -1;
	
	const int fragment_max_length = 2048;
	int fragment_length = 0;
	char fragment_log_text[fragment_max_length];
	glGetShaderInfoLog(fragment_shader_id, fragment_max_length, &fragment_length, fragment_log_text);
	std::cout << fragment_log_text << std::endl;

	glGetShaderiv(fragment_shader_id, GL_COMPILE_STATUS, &fragment_shader_status);
	if (fragment_shader_status != GL_TRUE)
	{
		std::cout << "Fragment shader \"" << fragment_shader << "\"compilate ERROR" << std::endl << std::endl;
		return -1;
	}
	else
		std::cout << "Fragment shader \"" << fragment_shader << "\"compilate SUCCESS" << std::endl << std::endl;

	GLuint shader_programme = glCreateProgram();
	glAttachShader(shader_programme, vertex_shader_id);
	glAttachShader(shader_programme, fragment_shader_id);
	
	glLinkProgram(shader_programme);

	//const int max_length = 2048;
	//int length = 0;
	//char log_text[max_length];
	//glGetProgramInfoLog(shader_programme, max_length, &length, log_text);
	//std::cout << log_text;

	std::cout << "LINK STATUS" << std::endl;
	int link_status = -1;
	glGetProgramiv(shader_programme, GL_LINK_STATUS, &link_status);
	if (link_status != GL_TRUE)
	{
		std::cout << "Shader programme link ERROR" << std::endl<< std::endl;
		return -1;
	}
	else
		std::cout << "Shader programme link SUCCESS" << std::endl<< std::endl;
	
	glDeleteShader(vertex_shader_id);
	glDeleteShader(fragment_shader_id);

	return shader_programme;

}


///////////////////////////////////////////////////////////////////////////////

static void error_callback(int error, const char* description)
{
	std::cerr << "Error: " << description;
}

///////////////////////////////////////////////////////////////////////////////

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

///////////////////////////////////////////////////////////////////////////////

static void WindowSizeCallback(GLFWwindow* /*window*/, int width, int height)
{
	window_width = width;
	window_height = height;
}

///////////////////////////////////////////////////////////////////////////////

int CreateWindow(int width, int height, std::string name, int simples, bool fullscreen)
{
	glfwSetErrorCallback(error_callback);
	
	if (!glfwInit())
		return -1;
	glfwWindowHint(GLFW_SAMPLES, 10);
	if (fullscreen)
	{
		const GLFWvidmode* video_mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		window_width = video_mode->width;
		window_height = video_mode->height;
		window = glfwCreateWindow(window_width, window_height, name.c_str(), glfwGetPrimaryMonitor(), NULL);
	}
	else
	{
		window_width = width;
		window_height = height;
		window = glfwCreateWindow(window_width, window_height, name.c_str(), NULL, NULL);
	}

	
	if (!window)
	{
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
		return -1;
	
	glfwSetKeyCallback(window, key_callback);
	glfwSetWindowSizeCallback(window, WindowSizeCallback);

	return 0;
}

///////////////////////////////////////////////////////////////////////////////

void EnableDepthTesting()
{
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
}

///////////////////////////////////////////////////////////////////////////////

void ProcessWindowEvents()
{
	glfwSwapBuffers(window);
	glfwPollEvents();
}

///////////////////////////////////////////////////////////////////////////////

void Terminate()
{
	glfwDestroyWindow(window);
	glfwTerminate();
}

///////////////////////////////////////////////////////////////////////////////

bool RenderingEnabled()
{
	if (glfwWindowShouldClose(window))
		return false;
	return true;
}

///////////////////////////////////////////////////////////////////////////////

void ClearColor(float r, float g, float b)
{
	glClearColor(r, g, b, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glViewport(0, 0, window_width, window_height);
}

///////////////////////////////////////////////////////////////////////////////

void FPSCounter(double &fps)
{
	static double prev_time = glfwGetTime();
	double actual_time = glfwGetTime();
	
	static int frames_counter = 0;

	double elapsed_time = actual_time - prev_time;
	if (elapsed_time >= 1.0)
	{
		prev_time = actual_time;
		fps = static_cast<double>(frames_counter) / elapsed_time;
		frames_counter = 0;
	}

	frames_counter++;
}
int main()
{
	
	

	int result = CreateWindow(640, 480, "GL WINDOW", 4, false);
	if (result)
		return -1;

	//const GLFWvidmode * video_mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	//GLFWwindow * window = glfwCreateWindow(video_mode->width, video_mode->height, "My GLF Window", glfwGetPrimaryMonitor(), NULL);
	

	GLfloat points[] = {
	   -0.5f, -0.5f, 0.0f,
		0.0f,  0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
	};

	GLfloat colours[] = {
		1.0f,0.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,0.0f,1.0f
	};
	
	GLuint position_vbo = 0;
	glGenBuffers(1, &position_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

	GLuint colour_vbo = 0;
	glGenBuffers(1, &colour_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, colour_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colours), colours, GL_STATIC_DRAW);

	GLuint vao1 = 0;
	glGenVertexArrays(1, &vao1);
	glBindVertexArray(vao1);
	glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, colour_vbo);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	GLuint vao2 = 0;
	glGenVertexArrays(1, &vao2);
	glBindVertexArray(vao2);
	glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, colour_vbo);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	GLuint vao3 = 0;
	glGenVertexArrays(1, &vao3);
	glBindVertexArray(vao3);
	glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, colour_vbo);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	EnableDepthTesting();


	glm::mat4 translate_matrix;
	glm::mat4 rotate_matrix;
	glm::mat4 scale_matrix;

	const float radius = 0.5;
	float x_trans = 0.0;
	float y_trans = 0.0;
	float angle = 0;
	float angle_rotation = 0;

	shaders1 = LoadShaders("vertex_shader1.glsl", "fragment_shader1.glsl");
	
	GLint trans2_uniform = glGetUniformLocation(shaders1, "trans_matrix");
	if (trans2_uniform == -1)
		std::cout << "Variable 'trans_matrix' not found" << std::endl;
	glUseProgram(shaders1);

	
	float a = 0.1;

	while (!glfwWindowShouldClose(window))
	{
	
	 
		static double fps = 0;
		FPSCounter(fps);
		std::string title = "Game @ FPS" + std::to_string(fps);
		glfwSetWindowTitle(window, title.c_str());

		ClearColor(0.5, 0.5, 0.5);

		static double prev_time = glfwGetTime();
		double actual_time = glfwGetTime();
		double elapsed_time = actual_time - prev_time;

		if (elapsed_time > 0.01)
		{
			prev_time = actual_time;
			angle += 1;
			angle_rotation += 0.1;
			x_trans = 0.5f + radius*cos(angle*3.14 / 180.0);
			y_trans = 0.5f + radius*sin(angle*3.14 / 180.0);
		}

		translate_matrix = glm::translate(glm::mat4(1.0), glm::vec3(x_trans, y_trans, 0.0));
		rotate_matrix = glm::rotate(translate_matrix, angle_rotation, glm::vec3(0, 0, 1));
		scale_matrix = glm::scale(rotate_matrix, glm::vec3(a, a, a));
		
		if(a<=1.0)
		a += 0.0001;

		glUniformMatrix4fv(trans2_uniform, 1, GL_FALSE, glm::value_ptr(scale_matrix));

		
		glBindVertexArray(vao1);
		glDrawArrays(GL_TRIANGLES, 0, 3);

		

		ProcessWindowEvents();
	}
	Terminate();
	
	return 0;
}


