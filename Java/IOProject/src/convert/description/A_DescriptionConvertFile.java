/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convert.description;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *Adnotacja zawierająca opis klasy ConvertFile w kodzie programu.
 * @author Dominik Kopaczka
 * @see convert.ConvertFile
 * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/annotation/package-summary.html">java.java.lang.annotation.*</a>
 * @version 1.0
 * @since   2016-12-29
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface A_DescriptionConvertFile {
    String classDescription() default "Brak opisu";
    
    String author() default "Dominik Kopaczka";
    
    double classVersion() default 1;
}
