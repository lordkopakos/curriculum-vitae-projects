/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.core;

import psi.graphics.ScreenManager;
import java.awt.Color;
import java.awt.DisplayMode;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Window;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

/**
 *
 * @author lordkopakos
 */
public abstract class ProgramCore {

    protected static final int FONT_SIZE = 24;

    private static final DisplayMode POSSIBLE_MODES[] = {
        new DisplayMode(1920, 1080, 32, 0),
        new DisplayMode(1920, 1080, 24, 0),
        new DisplayMode(1920, 1080, 16, 0),
        new DisplayMode(1368, 768, 32, 0),
        new DisplayMode(1368, 768, 24, 0),
        new DisplayMode(1368, 768, 16, 0)
    };

    public boolean menuIsRunning;
    public ScreenManager screen;

    public int heroState;
    public long elapsedTime;

    /**
     * Sygnalizuje pętli gry, że czas kończyć pracę.
     */
    public void menustop() {
        menuIsRunning = false;
    }

    /**
     * Wywołuje init() i gameLoop()
     */
    public void menurun() {
        try {

            menuinit();
            menuLoop();

        } finally {
            lazilyExit();
        }

    }

    /*
        Kończy działanie maszyny wirtualnej z wątku demona. Wątek-demon
        czeka 2 sekundy a następnie wywołuje System.exit(0). Ponieważ maszyna
        wirtualna powinna kończyc się gdy działają tylko wątki-demony,
        metoda zapewnia, że System.exit(0) jest wywoływane tylko
        w ostatecznosci. Jest to niezbędne gy działa Java Sound.
     */
    public void lazilyExit() {
        Thread thread = new Thread() {
            public void run() {
                // na początku czekamy na samodzielne zakończenie działania VM
                try {
                    Thread.sleep(200);
                } catch (InterruptedException ex) {
                }
                // system nadal działa, więc wymuszamy zakończenie
                System.exit(0);
            }
        };
        thread.setDaemon(true);
        thread.start();
    }


    /*
        Ustawia tryb pełnoekranowy i inicjuje obiekty.
     */
    public void menuinit() {

        screen = new ScreenManager();
        DisplayMode displayMode
                = screen.findFirstCompatibleMode(POSSIBLE_MODES);
        screen.setFullScreen(displayMode);

        Window window = screen.getFullScreenWindow();
        window.setFont(new Font("Dialog", Font.PLAIN, FONT_SIZE));
        window.setBackground(Color.blue);
        window.setForeground(Color.white);

        menuIsRunning = true;
    }

    public Image loadImage(String fileName) {
        return new ImageIcon(fileName).getImage();
    }


    /*
        Uruchamia pętlę gry działającą aż do wywołania metody stop().
     */
    public void menuLoop() {
        long startTime = System.currentTimeMillis();
        long currTime = startTime;

        while (menuIsRunning) {
            elapsedTime
                    = System.currentTimeMillis() - currTime;
            currTime += elapsedTime;

            // aktualizacja
            update(elapsedTime);

            // rysowanie
            Graphics2D g = screen.getGraphics();

            draw(g);
            g.dispose();

            screen.update();
        }
    }


    /*
        Aktualizuje stan gry/animacji w oparciu o okres czas jaki minął
        od uruchomienia programu.
     */
    public void update(long elapsedTime) {
        // nic nie rób
    }

    /*
        Rysowanie na ekranie. Klasy pochodne muszą dziedziczyć tą metodę.
     */
    public abstract void draw(Graphics2D g);
}
