#include "LinearSystem.h"
#include <string>
using namespace std;

int main() {
	LinearSystem l;
	try {
		l.loadGaussMatrix();
		l.solveGauss();
		l.loadJacobiMatrix();
		l.solveJacobi(15);
	}
	catch (int) {
		cerr << "Wczytywanie danych sie nie powiodlo\n";
	}
	catch (bool) {
		cerr << "Blad. Macierz nie jest diagonalnie dominujaca.\n";
	}
	catch (double) {
		cerr << "Blad. Dzielenie przez zero przy metodzie Gaussa.\n";
	}
	system("pause");
	return 0;
}