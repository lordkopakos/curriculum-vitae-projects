package pkg3dproject.graphics3D;

import pkg3dproject.math3D.*;

/**
    Klas� ScanConverter wykorzystywali�my do rysowania wielok�t�w
    posortowanych od przodu do ty�u (bez nak�adania si� pikseli). 
    Wielok�ty s� dodawane do listy i przycinane w spos�b umo�liwiaj�cy
    ich dopasowanie do wielok�t�w ju� wy�wietlonych. Przed narysowaniem
    ka�dej klatki nale�y wywo�a� metod� clear().
*/
public class SortedScanConverter extends ScanConverter {

    protected static final int DEFAULT_SCANLIST_CAPACITY = 8;

    private SortedScanList[] viewScans;
    private SortedScanList[] polygonScans;
    private boolean sortedMode;

    /**
        Tworzy nowy obiekt klasy SortedScanConverter dla danego okna
        (obiektu klasy ViewWindow). W�asno�ci obiektu ViewWindow mog�
        si� zmienia� pomi�dzy operacjami konwersji �cie�ek. Tryb
        sortowania jest domy�lnie wy��czony, mo�na go jednak w ka�dej
        chwili zmieni� wywo�uj�c metod� setSortedMode().
    */
    public SortedScanConverter(ViewWindow view) {
        super(view);
        sortedMode = false;
    }


    /**
        Czy�ci bie��c� �cie�k�. Wywo�uj t� metod� przed przyst�pieniem
        do rysowania ka�dej klatki.
    */
    public void clear() {
        if (viewScans != null) {
            for (int y=0; y<viewScans.length; y++) {
                viewScans[y].clear();
            }
        }
    }


    /**
        Ustawia tryb sortowanie, co oznacza, �e ten konwerter �cie�ek
        mo�e zak�ada�, �e wielok�ty s� rysowane od przodu do ty�u i
        powinny by� przycinane w taki spos�b, by pasowa�y do wielok�t�w
        wcze�niej przekonwertowanych na �cie�ki.
    */
    public void setSortedMode(boolean b) {
        sortedMode = b;
    }


    /**
        Zwraca n-t� �cie�k� danego rz�du.
    */
    public Scan getScan(int y, int index) {
        return polygonScans[y].getScan(index);
    }


    /**
        Zwraca liczb� �cie�ek w danym rz�dzie.
    */
    public int getNumScans(int y) {
        return polygonScans[y].getNumScans();
    }


    /**
        Sprawdza, czy ekran jest wype�niony.
    */
    public boolean isFilled() {
        if (viewScans == null) {
            return false;
        }

        int left = view.getLeftOffset();
        int right = left + view.getWidth() - 1;
        for (int y=view.getTopOffset(); y<viewScans.length; y++) {
            if (!viewScans[y].equals(left, right)) {
                return false;
            }
        }
        return true;
    }


    protected void ensureCapacity() {
        super.ensureCapacity();
        int height = view.getTopOffset() + view.getHeight();
        int oldHeight = (viewScans == null)?0:viewScans.length;
        if (height != oldHeight) {
            SortedScanList[] newViewScans =
                new SortedScanList[height];
            SortedScanList[] newPolygonScans =
                new SortedScanList[height];
            if (oldHeight != 0) {
                System.arraycopy(viewScans, 0, newViewScans, 0,
                    Math.min(height, oldHeight));
                System.arraycopy(polygonScans, 0, newPolygonScans,
                    0, Math.min(height, oldHeight));
            }
            viewScans = newViewScans;
            polygonScans = newPolygonScans;
            for (int i=oldHeight; i<height; i++) {
                viewScans[i] = new SortedScanList();
                polygonScans[i] = new SortedScanList();
            }
        }
    }


    /**
        Przekszta�ca wielok�t na �cie�ki i � je�li tryb sortedMode jest
        w��czony - dodaje �cie�ki do listy i odpowiednio je przycina, by
        piksele nowego wielok�ta nie przykrywa�y pikseli ju� istniej�cych.
    */
    public boolean convert(Polygon3D polygon) {
        boolean visible = super.convert(polygon);
        if (!sortedMode || !visible) {
            return visible;
        }

        // przytnij �cie�k� tak, by nie zas�ania�a wcze�niej dodanych punkt�w
        visible = false;
        for (int y=getTopBoundary(); y<=getBottomBoundary(); y++) {
            Scan scan = getScan(y);
            SortedScanList diff = polygonScans[y];
            diff.clear();
            if (scan.isValid()) {
                viewScans[y].add(scan.left, scan.right, diff);
                visible |= (polygonScans[y].getNumScans() > 0);
            }
        }

        return visible;

    }


    /**
        Klasa SortedScanList reprezentuje szereg �cie�ek dla poziomego
        rz�du pikseli. Nowe �cie�ki mog� by� dodawane i przycinane w taki
        spos�b, by nie zas�ania�y istniej�cych wcze�niej dodanych �cie�ek.
    */
    private static class SortedScanList {
    
        private int length;
        private Scan[] scans;
    
        /**
            Tworzy nowy obiekt klasy SortedScanList o domy�lnej pojemno�ci
            (liczbie �cie�ek na poziomy rz�d pikseli).
        */
        public SortedScanList() {
            this(DEFAULT_SCANLIST_CAPACITY);
        }
    
    
        /**
            Tworzy nowy obiekt klasy SortedScanList o podanej pojemno�ci
            (liczbie �cie�ek na poziomy rz�d pikseli).
        */
        public SortedScanList(int capacity) {
            scans = new Scan[capacity];
            for (int i=0; i<capacity; i++) {
                scans[i] = new Scan();
            }
            length = 0;
        }
    
    
        /**
            Czy�ci t� list� �cie�ek.
        */
        public void clear() {
            length = 0;
        }
    
    
        /**
            Zwraca liczb� �cie�ek na tej li�cie.
        */
        public int getNumScans() {
            return length;
        }
    
    
        /**
            Zwraca n-t� �cie�k� z tej listy.
        */
        public Scan getScan(int index) {
            return scans[index];
        }
    
    
        /**
            Sprawdza, czy ta lista �cie�ek zawiera tylko jedn� �cie�k� i czy ta
            �cie�ka jest r�wna przekazanym warto�ciom argument�w left i right.
        */
        public boolean equals(int left, int right) {
            return (length == 1 && scans[0].equals(left,right));
        }
    
    
        /**
            Dodaje i przycina now� �cie�k� w rz�dzie, umieszcza widoczny
            fragment (r�nic�) w okre�lonym obiekcie klasy SortedScanList.
        */
        public void add(int left, int right, SortedScanList diff) {
            for (int i=0; i<length && left <= right; i++) {
                Scan scan = scans[i];
                int maxRight = scan.left - 1;
                if (left <= maxRight) {
                    if (right < maxRight) {
                        diff.add(left, right);
                        insert(left, right, i);
                        return;
                    }
                    else {
                        diff.add(left, maxRight);
                        scan.left = left;
                        left = scan.right + 1;
                        if (merge(i)) {
                            i--;
                        }
                    }
                }
                else if (left <= scan.right) {
                    left = scan.right + 1;
                }
            }
            if (left <= right) {
                insert(left, right, length);
                diff.add(left, right);
            }
    
        }
    
        // metody pomocnicze wykorzystywane przez metod� add()
    
        private void growCapacity() {
            int capacity = scans.length;
            int newCapacity = capacity*2;
            Scan[] newScans = new Scan[newCapacity];
            System.arraycopy(scans, 0, newScans, 0, capacity);
            for (int i=length; i<newCapacity; i++) {
                newScans[i] = new Scan();
            }
            scans = newScans;
        }
    
        private void add(int left, int right) {
            if (length == scans.length) {
                growCapacity();
            }
            scans[length].setTo(left, right);
            length++;
        }
    
        private void insert(int left, int right, int index) {
            if (index > 0) {
                Scan prevScan = scans[index-1];
                if (prevScan.right == left - 1) {
                    prevScan.right = right;
                    return;
                }
            }
    
            if (length == scans.length) {
                growCapacity();
            }
            Scan last = scans[length];
            last.setTo(left, right);
            for (int i=length; i>index; i--) {
                scans[i] = scans[i-1];
            }
            scans[index] = last;
            length++;
        }
    
        private void remove(int index) {
            Scan removed = scans[index];
            for (int i=index; i<length-1; i++) {
                scans[i] = scans[i+1];
            }
            scans[length-1] = removed;
            length--;
        }
    
        private boolean merge(int index) {
            if (index > 0) {
                Scan prevScan = scans[index-1];
                Scan thisScan = scans[index];
                if (prevScan.right == thisScan.left-1) {
                    prevScan.right = thisScan.right;
                    remove(index);
                    return true;
                }
            }
            return false;
        }

    }

}
