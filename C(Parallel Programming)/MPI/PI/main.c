#include <stdlib.h>
#include<stdio.h>
#include<math.h>
//#include "mpi.h"
#include<mpi.h>
int main(int argc, char** argv) {

	int rank, ranksent, size, source, dest, tag, i;
	MPI_Status status;
	int len;
	char name[MPI_MAX_PROCESSOR_NAME];

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	printf("rank=%d, size=%d\n", rank, size);
	if (size>1) {

		if (rank != 0) {
			dest = 0; tag = 0;
			MPI_Get_processor_name(name, &len);
			//MPI_Send(&rank, 1, MPI_INT, dest, tag, MPI_COMM_WORLD);
			MPI_Send(name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, 1, MPI_COMM_WORLD);
		}
		else {

			for (i = 1; i<size; i++) {

				//MPI_Recv(&ranksent, 1, MPI_INT, MPI_ANY_SOURCE,
					//MPI_ANY_TAG, MPI_COMM_WORLD, &status);
				MPI_Recv(name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, i, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
				printf("Dane od procesu o randze (i=%d): %s (%d)\n",
					i, name, status.MPI_SOURCE);
			}

		}

	}
	else {
		printf("Pojedynczy proces o randze: %d (brak komunikatów)\n", rank);
	}


	MPI_Finalize();
	return(0);

}
